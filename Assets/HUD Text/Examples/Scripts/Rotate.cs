using UnityEngine;

public class Rotate : IgnoreTimeScale
{
	public Vector3 axis;
	public float rate;
	
	public bool Active = true;
	public bool IgnoreTimeScale = true;
	
	void Update () 
	{
		if (!Active)
			return;
		
		float delta = IgnoreTimeScale ?
			UpdateRealTimeDelta() : Time.deltaTime;
		
		transform.Rotate(axis * delta * rate);
	}
}