//----------------------------------------------
//            NGUI: Next-Gen UI kit
// Copyright � 2011-2012 Tasharen Entertainment
//----------------------------------------------

using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Turns the popup list it's attached to into a language selection list.
/// </summary>

[RequireComponent(typeof(UIPopupList))]
[AddComponentMenu("NGUI/Interaction/Language Selection")]
public class LanguageSelection : MonoBehaviour
{
	// ** begin block - added by Markus

	/// <summary>
	/// Receiver for event when language is changed. We need another one besides just this object,
	/// which captures the pop-up lists event.
	/// </summary>

	public GameObject m_receiver;
	
	/// <summary>
	/// Name for the function of language changed event.
	/// </summary>

	public string m_functionName;
	
	// ** end block - added by Markus

	
	UIPopupList mList;

	void Start ()
	{
		mList = GetComponent<UIPopupList>();
		UpdateList();
		mList.eventReceiver = gameObject;
		mList.functionName = "OnLanguageSelection";
	}

	void UpdateList ()
	{
		if (Localization.instance != null && Localization.instance.languages != null)
		{
			mList.items.Clear();

			for (int i = 0, imax = Localization.instance.languages.Length; i < imax; ++i)
			{
				TextAsset asset = Localization.instance.languages[i];
				if (asset != null) mList.items.Add(asset.name);
			}
			mList.selection = Localization.instance.currentLanguage;
		}
	}

	void OnLanguageSelection (string language)
	{
		if (Localization.instance != null)
		{
			Localization.instance.currentLanguage = language;
			
			// ** begin block - added by Markus
			if (m_receiver)
			{
				m_receiver.SendMessage(m_functionName, language, SendMessageOptions.DontRequireReceiver);
			}
			// ** end block - added by Markus
		}
	}
}