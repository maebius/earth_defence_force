//----------------------------------------------
//            NGUI: Next-Gen UI kit
// Copyright � 2011-2012 Tasharen Entertainment
//----------------------------------------------

using UnityEngine;

/// <summary>
/// This class makes it possible to activate a button by pressing a key (such as space bar for example).
/// </summary>

[AddComponentMenu("Game/UI/Button Key Binding")]
public class UIButtonKeyBinding : MonoBehaviour
{
	public KeyCode keyCode = KeyCode.None;

	void Update ()
	{
		// ** begin block - edited by Markus
		// NOTE! We don't want to key bindings to go through if we have disabled to collider ...
		Collider col = gameObject.GetComponent<Collider>();
		
		if (!UICamera.inputHasFocus && (col == null || col.enabled))
		{
		// ** end block - edited by Markus
			
			if (keyCode == KeyCode.None) return;
			
			if (OuyaInputWrapper.GetKeyDown(keyCode))
			{
				SendMessage("OnPress", true, SendMessageOptions.DontRequireReceiver);
			}

			if (OuyaInputWrapper.GetKeyUp(keyCode))
			{
				SendMessage("OnPress", false, SendMessageOptions.DontRequireReceiver);
				SendMessage("OnClick", SendMessageOptions.DontRequireReceiver);
			}
		}
	}
}