//#define DEBUG_TEXTS

#define USE_OUYA

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OuyaInputWrapper : MonoBehaviour
#if USE_OUYA
	, OuyaSDK.IPauseListener
	, OuyaSDK.IResumeListener
#endif
{
	private static OuyaInputWrapper sm_instance = null;
	[HideInInspector]
	public static OuyaInputWrapper Instance 
	{
		get { return sm_instance; }
	}
	
	public static bool OuyaEnabled
	{
		get 
		{
			return sm_instance != null;
		}
	}
	// NOTE! Use this instead of above OuyaEnabled if in any case you want gamepad functionality
	// and set the flag here accordingly
	public static bool UseGamepad
	{
		get
		{
			// TODO: check that this is in order!
			//return true;
			return OuyaEnabled;
		}
	}
	
	public static bool GetKeyDown(UnityEngine.KeyCode key)
	{
#if USE_OUYA
		return (sm_instance) ? sm_instance.GetOuyaKeyDown(key) : Input.GetKeyDown (key);
#else
		return Input.GetKeyDown (key);
#endif
	}
	
	public static bool GetKeyUp(UnityEngine.KeyCode key)
	{
#if USE_OUYA
		return (sm_instance) ? sm_instance.GetOuyaKeyUp(key) : Input.GetKeyUp (key);
#else
		return Input.GetKeyUp (key);
#endif
	}
	
	public static float GetAxis(string axis)
	{
#if USE_OUYA
		return (sm_instance) ? sm_instance.GetOuyaAxis(axis) : Input.GetAxis(axis);
#else
		return Input.GetAxis(axis);
#endif
	}
	
	public static float GetAxisRaw(string axis)
	{
#if USE_OUYA
		return (sm_instance) ? sm_instance.GetOuyaAxis(axis) : Input.GetAxisRaw(axis);
#else
		return Input.GetAxisRaw(axis);
#endif
	}
	
#if USE_OUYA
	
	public static void SetAxisMap(string unityAxis, string ouyaAxis, OuyaSDK.OuyaPlayer player)
	{
		sm_unityToOuyaAxisMap[unityAxis] = ouyaAxis;
		sm_axisToPlayerMap[unityAxis] = player;
	}
	// NOTE! These are "predefined" and can (must) be overridden by calling SetAxisMap from the client
	private static Dictionary<string, string> sm_unityToOuyaAxisMap = new Dictionary<string, string>()
	{
		{ "Joy1 - AxisX", "LX" } 
		, { "Joy1 - AxisY", "LY" } 
		, { "Joy1 - Axis4", "RX" } 
		, { "Joy1 - Axis5", "RY" } 
		, { "Horizontal", "LX" } 
		, { "Vertical", "LY" } 
		, { "Horizontal DPAD", "DPL" }
		, { "Vertical DPAD", "DPU" }
		
		, { "Joy2 - AxisX", "LX" } 
		, { "Joy2 - AxisY", "LY" } 
		, { "Joy2 - Axis4", "RX" } 

		, { "Joy3 - AxisX", "LX" } 
		, { "Joy3 - AxisY", "LY" } 
		, { "Joy3 - Axis4", "RX" } 

		, { "Joy4 - AxisX", "LX" } 
		, { "Joy4 - AxisY", "LY" } 
		, { "Joy4 - Axis4", "RX" } 
	};
	
	// NOTE! These are "predefined" and can (must) be overridden by calling SetAxisMap from the client
	private static Dictionary<string, OuyaSDK.OuyaPlayer> sm_axisToPlayerMap = 
		new Dictionary<string, OuyaSDK.OuyaPlayer>()
	{
		{ "Joy1 - AxisX", OuyaSDK.OuyaPlayer.player1 }
		, { "Joy1 - AxisY", OuyaSDK.OuyaPlayer.player1 }
		, { "Joy1 - Axis3", OuyaSDK.OuyaPlayer.player1 }
		, { "Joy1 - Axis4", OuyaSDK.OuyaPlayer.player1 }
		, { "Joy1 - Axis5", OuyaSDK.OuyaPlayer.player1 }
		, { "Joy1 - Axis6", OuyaSDK.OuyaPlayer.player1 }
		, { "Joy1 - Axis7", OuyaSDK.OuyaPlayer.player1 }
		, { "Vertical", OuyaSDK.OuyaPlayer.player1 }
		, { "Horizontal", OuyaSDK.OuyaPlayer.player1 }
		, { "Vertical DPAD", OuyaSDK.OuyaPlayer.player1 }
		, { "Horizontal DPAD", OuyaSDK.OuyaPlayer.player1 }
		, { "Fire", OuyaSDK.OuyaPlayer.player1 }
		
		, { "Joy2 - AxisX", OuyaSDK.OuyaPlayer.player2 }
		, { "Joy2 - AxisY", OuyaSDK.OuyaPlayer.player2 }
		, { "Joy2 - Axis3", OuyaSDK.OuyaPlayer.player2 }
		, { "Joy2 - Axis4", OuyaSDK.OuyaPlayer.player2 }
		, { "Joy2 - Axis5", OuyaSDK.OuyaPlayer.player2 }
		, { "Joy2 - Axis6", OuyaSDK.OuyaPlayer.player2 }
		, { "Joy2 - Axis7", OuyaSDK.OuyaPlayer.player2 }

		, { "Joy3 - AxisX", OuyaSDK.OuyaPlayer.player3 }
		, { "Joy3 - AxisY", OuyaSDK.OuyaPlayer.player3 }
		, { "Joy3 - Axis3", OuyaSDK.OuyaPlayer.player3 }
		, { "Joy3 - Axis4", OuyaSDK.OuyaPlayer.player3 }
		, { "Joy3 - Axis5", OuyaSDK.OuyaPlayer.player3 }
		, { "Joy3 - Axis6", OuyaSDK.OuyaPlayer.player3 }
		, { "Joy3 - Axis7", OuyaSDK.OuyaPlayer.player3 }
		
		, { "Joy4 - AxisX", OuyaSDK.OuyaPlayer.player4 }
		, { "Joy4 - AxisY", OuyaSDK.OuyaPlayer.player4 }
		, { "Joy4 - Axis3", OuyaSDK.OuyaPlayer.player4 }
		, { "Joy4 - Axis4", OuyaSDK.OuyaPlayer.player4 }
		, { "Joy4 - Axis5", OuyaSDK.OuyaPlayer.player4 }
		, { "Joy4 - Axis6", OuyaSDK.OuyaPlayer.player4 }
		, { "Joy4 - Axis7", OuyaSDK.OuyaPlayer.player4 }
	};
	private static List<string> sm_specialMappings = new List<string>()
	{
		"LT", "RT", "DPL", "DPR", "DPU", "DPD"
	};
	
	private float GetOuyaAxis(string axis)
	{
		if (sm_unityToOuyaAxisMap.ContainsKey(axis))
		{
			OuyaSDK.OuyaPlayer player = sm_axisToPlayerMap[axis];
			string ouyaAxis = sm_unityToOuyaAxisMap[axis];
			
			if (sm_specialMappings.Contains(ouyaAxis))
			{
				switch (ouyaAxis)
				{
				case "LT":
				case "RT":
				{
					float left = OuyaInputManager.GetAxis("LT", player);
					float right = OuyaInputManager.GetAxis("RT", player);
					return left + right;
				}
				case "DPL":
				case "DPR":
				{
					bool left = OuyaInputManager.GetButton("DPL", player);
					bool right = OuyaInputManager.GetButton("DPR", player);
					return ((left) ? -1f : 0f) + ((right) ? 1f : 0f);
				}	
				case "DPU":
				case "DPD":
				{
					bool up = OuyaInputManager.GetButton("DPU", player);
					bool down = OuyaInputManager.GetButton("DPD", player);
					return ((down) ? -1f : 0f) + ((up) ? 1f : 0f);
				}
				default:
					break;
				}
			}
			else
			{
				return OuyaInputManager.GetAxis(ouyaAxis, player);
			}
		}
#if DEBUG_TEXTS
		//Debug.Log(string.Format("OuyaInputWrapper.GetOuyaAxis () no mapping for axis: {0}, returning 0f!", axis));
#endif
		return 0f;
	}
	
	private OuyaSDK.OuyaPlayer GetPlayer(UnityEngine.KeyCode keyCode)
	{
		if (keyCode >= UnityEngine.KeyCode.Joystick1Button0 && 
			keyCode <= UnityEngine.KeyCode.Joystick1Button19)
		{
			return OuyaSDK.OuyaPlayer.player1;
		}
		if (keyCode >= UnityEngine.KeyCode.Joystick2Button0 && 
			keyCode <= UnityEngine.KeyCode.Joystick2Button19)
		{
			return OuyaSDK.OuyaPlayer.player2;
		}
		if (keyCode >= UnityEngine.KeyCode.Joystick3Button0 && 
			keyCode <= UnityEngine.KeyCode.Joystick3Button19)
		{
			return OuyaSDK.OuyaPlayer.player3;
		}
		if (keyCode >= UnityEngine.KeyCode.Joystick4Button0 && 
			keyCode <= UnityEngine.KeyCode.Joystick4Button19)
		{
			return OuyaSDK.OuyaPlayer.player4;
		}
		return OuyaSDK.OuyaPlayer.none;
	}
	
	private static bool IsAll(UnityEngine.KeyCode keyCode)
	{
		return keyCode >= UnityEngine.KeyCode.JoystickButton0 && 
			keyCode <= UnityEngine.KeyCode.JoystickButton19;
	}
	
	private bool GetOuyaKeyDown(UnityEngine.KeyCode keyCode)
	{
		OuyaSDK.OuyaPlayer player = GetPlayer(keyCode);
#if DEBUG_TEXTS
		//Debug.Log(string.Format("OuyaInputWrapper.GetOuyaKeyDown () got player: {0} for key: {1}", player, keyCode));
#endif
		if (player == OuyaSDK.OuyaPlayer.none && IsAll (keyCode))
		{
			return m_allKeyCodes.GetState(keyCode, ButtonState.Down);
		}
		
		if (!m_keyCodes.ContainsKey(player))
		{
#if DEBUG_TEXTS
			//Debug.Log(string.Format("OuyaInputWrapper.GetKeyDown () we have not mapped player: {0} for key: {1}! Returning false.", player, keyCode));
#endif
			return false;
		}
#if DEBUG_TEXTS
		//Debug.Log(string.Format("OuyaInputWrapper.GetOuyaKeyDown () state for key [{0}] = {1}.", keyCode, m_keyCodes[player].GetState(keyCode, ButtonState.Down)));
#endif		
		
		return m_keyCodes[player].GetState(keyCode, ButtonState.Down);
	}
	
	private bool GetOuyaKeyUp(UnityEngine.KeyCode keyCode)
	{
		OuyaSDK.OuyaPlayer player = GetPlayer(keyCode);

#if DEBUG_TEXTS
		//Debug.Log(string.Format("OuyaInputWrapper.GetOuyaKeyUp () got player: {0} for key: {1}", player, keyCode));
#endif		
		if (player == OuyaSDK.OuyaPlayer.none && IsAll (keyCode))
		{
			return m_allKeyCodes.GetState(keyCode, ButtonState.Up);
		}
		if (!m_keyCodes.ContainsKey(player))
		{
#if DEBUG_TEXTS
			//Debug.Log(string.Format("OuyaInputWrapper.GetKeyUp () we have not mapped player: {0} for key: {1}! Returning false.", player, keyCode));
#endif
			return false;
		}
#if DEBUG_TEXTS
		//Debug.Log(string.Format("OuyaInputWrapper.GetOuyaKeyUp () state for key [{0}] = {1}.", keyCode, m_keyCodes[player].GetState(keyCode, ButtonState.Up)));
#endif
		return m_keyCodes[player].GetState(keyCode, ButtonState.Up);
	}
	
	private enum ButtonState
	{
		Up
		, Down
	}
	
	private class KeyCodes
	{
		private class KeyState
		{
			UnityEngine.KeyCode keyCode;
			public int upCounter;
			public int downCounter;
			
			OuyaSDK.KeyEnum button;
			
			
			public KeyState(UnityEngine.KeyCode key, OuyaSDK.KeyEnum b)
			{
				keyCode = key;
				button = b;
				upCounter = 0;
				downCounter = 0;
			}
			
			public override string ToString ()
			{
				return string.Format ("key: {0}, button: {1}, up/down: {2}/{3}", keyCode, button, upCounter, downCounter);
			}
		}	
		private Dictionary<UnityEngine.KeyCode, int> m_unityKey = new Dictionary<UnityEngine.KeyCode, int>();
		private Dictionary<OuyaSDK.KeyEnum, int> m_ouyaKey = new Dictionary<OuyaSDK.KeyEnum, int>();
		
		private List<KeyState> m_mappings = new List<KeyState>();
		
		public KeyCodes(UnityEngine.KeyCode baseNumber)
		{
			// Follows XBOX 360 ButtonMap ( only has 13 buttons )
	
			
			// NOTE! These need to match to scriptPalyerInput.cs::Awake defined mappings (m_gamepadButtonMap)
			
			AddMapping(baseNumber+0, OuyaSDK.KeyEnum.BUTTON_O);
			AddMapping(baseNumber+1, OuyaSDK.KeyEnum.BUTTON_A);
			AddMapping(baseNumber+2, OuyaSDK.KeyEnum.BUTTON_U);
			AddMapping(baseNumber+3, OuyaSDK.KeyEnum.BUTTON_Y);
			
			AddMapping(baseNumber+4, OuyaSDK.KeyEnum.BUTTON_LB);
			AddMapping(baseNumber+5, OuyaSDK.KeyEnum.BUTTON_RB);

			AddMapping(baseNumber+7, OuyaSDK.KeyEnum.BUTTON_SYSTEM);
			
		}
		
		public void Reset()
		{
			foreach (KeyState state in m_mappings)
			{
				state.upCounter = 0;
				state.downCounter = 0;
			}
		}
		
		private void AddMapping(UnityEngine.KeyCode keyCode, OuyaSDK.KeyEnum button)
		{
			KeyState state = new KeyState(keyCode, button);
			m_mappings.Add(state);
			m_unityKey[keyCode] = m_mappings.Count-1;
			m_ouyaKey[button] = m_mappings.Count-1;
		}

		
		public void HandleEvent(OuyaSDK.KeyEnum key, OuyaSDK.InputAction action)
		{
			if (!m_ouyaKey.ContainsKey(key))
			{
#if DEBUG_TEXTS
				Debug.Log(string.Format ("KeyCodes.SetState() WARNING! No mapping for : {0} / {1} defined!", key, action));
#endif
				return;
			}
			
			switch (action)
			{
			case OuyaSDK.InputAction.KeyDown:
				m_mappings[m_ouyaKey[key]].downCounter++;
				break;
			case OuyaSDK.InputAction.KeyUp:
				m_mappings[m_ouyaKey[key]].upCounter++;
				break;
			case OuyaSDK.InputAction.GenericMotionEvent:
			case OuyaSDK.InputAction.TouchEvent:
			case OuyaSDK.InputAction.TrackballEvent:
			case OuyaSDK.InputAction.None:
				break;
			}
		}
		public bool GetState(UnityEngine.KeyCode key, ButtonState state)
		{
			if (!m_unityKey.ContainsKey(key))
			{
				//Debug.Log(string.Format ("KeyCodes.GetState() WARNING! No mapping for : {0} defined!", key));
				return false;
			}
			KeyState keyState = m_mappings[m_unityKey[key]];
#if DEBUG_TEXTS
			//Debug.Log(string.Format ("KeyCodes.GetState() keyState: {0}", keyState.ToString ()));
#endif
			return (state == ButtonState.Down) ? keyState.downCounter > 0 : keyState.upCounter > 0;
		}
	}
	
	private Dictionary<OuyaSDK.OuyaPlayer, Vector2> m_leftAnalog = new Dictionary<OuyaSDK.OuyaPlayer, Vector2>();
	private Dictionary<OuyaSDK.OuyaPlayer, KeyCodes> m_keyCodes = new Dictionary<OuyaSDK.OuyaPlayer, KeyCodes>();
	private KeyCodes m_allKeyCodes;
	
	
	private static bool sm_onPause = false;
	
	/// <summary>
	/// Implementation for OuyaSDK.IPauseListener. NOTE! Might be called multiple times, so need to set a flag.
	/// </summary>
	public void OuyaOnPause()
	{
#if DEBUG_TEXTS
		Debug.Log(string.Format ("OuyaInputWrapper.OuyaOnPause() sm_onPause: {0}", sm_onPause));
#endif		
		
		AudioListener.pause = true;
		sm_onPause = true;
	}
	/// <summary>
	/// Implementation for OuyaSDK.IResumeListener. NOTE! Might be called multiple times, so need to set a flag.
	/// </summary>
	public void OuyaOnResume()
	{
#if DEBUG_TEXTS
		Debug.Log(string.Format ("OuyaInputWrapper.OuyaOnResume() sm_onPause: {0}", sm_onPause));
#endif		

		if (sm_onPause)
		{
			sm_onPause = false;
			
			AudioListener.pause = false;
			GL.InvalidateState();
		}
	}
	
	void Awake () 
	{ 
#if DEBUG_TEXTS
		Debug.Log(string.Format ("OuyaInputWrapper.Awake(), sm_instance == null => {0}", sm_instance == null));
#endif		
		
		if (sm_instance == null) 
		{ 
			sm_instance = this;
			sm_instance.Initialize();
			DontDestroyOnLoad(gameObject); 
		} 
		else 
			Destroy(gameObject); 
	}
	
	void OnEnable () 
	{ 
#if DEBUG_TEXTS
		Debug.Log(string.Format ("OuyaInputWrapper.OnEnable"));
#endif		
		if (sm_instance == null) 
		{
			sm_instance = this; 
		}
	}
	
	void OnDisable () 
	{ 
#if DEBUG_TEXTS
		Debug.Log(string.Format ("OuyaInputWrapper.OnDisable"));
#endif
	}
	
	private void Initialize()
	{
#if DEBUG_TEXTS
		Debug.Log(string.Format ("OuyaInputWrapper.Initialize"));
#endif
				
		m_allKeyCodes = new KeyCodes(UnityEngine.KeyCode.JoystickButton0);

		OuyaInputManager.OuyaButtonEvent.addButtonEventListener(HandleButtonEvent);
		OuyaSDK.registerPauseListener(this);
		OuyaSDK.registerResumeListener(this);
	}
	
	void Start()
	{	
#if DEBUG_TEXTS
		Debug.Log(string.Format ("OuyaInputWrapper.Start"));
#endif
	}
	
	void OnDestroy()
	{	
#if DEBUG_TEXTS
		Debug.Log(string.Format ("OuyaInputWrapper.OnDestroy"));
#endif
		OuyaInputManager.OuyaButtonEvent.removeButtonEventListener(HandleButtonEvent);
		OuyaInputManager.initKeyStates();
		OuyaSDK.unregisterPauseListener(this);
		OuyaSDK.unregisterResumeListener(this);
	}
	
	void LateUpdate()
	{
		// we need to reset all our button states here, so we don't handle them multiple times
		// NOTE! This means we need to query all input stuff on Update() on every game object
		foreach (KeyCodes codes in m_keyCodes.Values)
		{
			codes.Reset();
		}
		m_allKeyCodes.Reset();
	}
	
	private static Dictionary<OuyaSDK.OuyaPlayer, UnityEngine.KeyCode> sm_playerToKeyBaseMap = 
		new Dictionary<OuyaSDK.OuyaPlayer, UnityEngine.KeyCode>() 
	{
		{ OuyaSDK.OuyaPlayer.player1, UnityEngine.KeyCode.Joystick1Button0 }
		, { OuyaSDK.OuyaPlayer.player2, UnityEngine.KeyCode.Joystick2Button0 }
		, { OuyaSDK.OuyaPlayer.player3, UnityEngine.KeyCode.Joystick3Button0 }
		, { OuyaSDK.OuyaPlayer.player4, UnityEngine.KeyCode.Joystick4Button0 }
	};
	
	void HandleButtonEvent(OuyaSDK.OuyaPlayer player, OuyaSDK.KeyEnum b, OuyaSDK.InputAction bs)
	{
#if DEBUG_TEXTS
		Debug.Log(string.Format ("OuyaInputWrapper.HandleButtonEvent - player: {0}, KeyEnum: {1}, InputAction: {2}"
			, player.ToString(), b.ToString(), bs.ToString()));
#endif		
		if (!m_keyCodes.ContainsKey(player))
		{
			m_keyCodes[player] = new KeyCodes(sm_playerToKeyBaseMap[player]);
		}
		m_keyCodes[player].HandleEvent(b, bs);
		m_allKeyCodes.HandleEvent(b, bs);
	}
	
	private Vector2 convertRawJoystickCoordinates(float x, float y, float deadzoneRadius)
	{
		Vector2 result = new Vector2(x, y); // a class with just two members, int x and int y
		bool isInDeadzone = testIfRawCoordinatesAreInDeadzone(x, y, deadzoneRadius);
		if (isInDeadzone)
		{
			result.x = 0f;
			result.y = 0f;
		}
		return result;
	}
	
	private bool testIfRawCoordinatesAreInDeadzone(float x, float y, float radius)
	{
		float distance = Mathf.Sqrt((x * x) + (y * y));
		return distance < radius;
	}
	
#endif // USE_OUYA

}
