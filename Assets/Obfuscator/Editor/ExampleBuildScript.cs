using UnityEngine;
using UnityEditor;

using System;
using System.IO;
using System.Collections.Generic;


public class BuildScript : EditorWindow
{
    static bool autoRunOption;
    static bool autoShowOption;
    static string savePath;
    static string saveName;

    static string[] GetScenes(BuildTarget bTarget)
    {
        string[] levels = new string[1];
        levels[0] = "Assets/Test/testscene.unity";

        foreach (string lev in levels)
        {
            if (!File.Exists(lev))
                Debug.LogError("Build failed, scene does not exist: "+lev);
        }

        return levels;
    }

    [MenuItem("Build/Single/Webplayer")]
    static void BuildWebplayer()
    {
        BuildWebplayer(false);
    }
    static void BuildWebplayer(bool manualStart)
    { 
        StartBuild(GetScenes(BuildTarget.WebPlayer), "builds/", "mywebplayer.unity3d", BuildTarget.WebPlayerStreamed, false, false);
    }


    [MenuItem("Build/Single/WindowsStandalone")]
    static void BuildWin()
    {
        BuildWin(false);
    }
    static void BuildWin(bool manualStart)
    {
        StartBuild(GetScenes(BuildTarget.StandaloneWindows), "builds/windows/", "mygame.exe", BuildTarget.StandaloneWindows, false, false);
        M2HObfuscator.ObfuscateFile("builds/windows/mygame.exe");
    }

    [MenuItem("Build/Single/MacStandalone")]
    static void BuildMac()
    {
        BuildMac(false);
    }
    static void BuildMac(bool manualStart)
    {
        StartBuild(GetScenes(BuildTarget.StandaloneOSXUniversal), "builds/", "mygameMac.app", BuildTarget.StandaloneOSXUniversal, false, false);
        M2HObfuscator.ObfuscateFile("builds/mygameMac.app");
    }

   
    [MenuItem("Build/Build All")]
    static void BuildALL()
    {
        BuildWebplayer(false);
        BuildWin(false);
        BuildMac(false);
    }


    static void StartBuild(string[] scenes, string path, string name, BuildTarget target, bool autoRun, bool autoShow)
    {
        if (scenes.Length == 0 )
        {
            Debug.LogError("No levels selected to build!");
            return;
        }

        string outputFile = path + name;
        if (outputFile.Length <= 3)
        {
            Debug.LogError("StartBuild cancelled: Invalid build path:" + outputFile);
            return;
        }
       
        string[] folders = path.Split('/');
        for(int i=0;i<folders.Length-1;i++){
            string currentpath = folders[i];
            if (i >0){
                for(int j=i-1;j>=0;j--)
                    currentpath = folders[j]+'/'+currentpath;                
            }
            if (!Directory.Exists(currentpath))
            {
                Directory.CreateDirectory(currentpath);
            }
        }

       

        BuildPipeline.BuildPlayer(scenes, outputFile, target, BuildOptions.None);
        Debug.Log("Built: \"" + outputFile + "\" with " + scenes.Length + " scenes");
    }
    
        
}