
using UnityEngine;
using UnityEditor;
using System.IO;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public class M2HObfuscator : EditorWindow
{
    private static string OBF_MAINFOLDER = "";

	// choose one of these - 2.0.0 seems to work for Android, but AngryAnt's version is the best...
	private static string OBF_PATH = "Obfuscar_AngryAnt/Bin/";
	//private static string OBF_PATH = "Obfuscar_2.0.0/Bin/";
	//private static string OBF_PATH = "Obfuscar_1.5.4/Bin/";

    [MenuItem("Tools/Obfuscator")]
    static void Init()
    {
        if (Application.platform == RuntimePlatform.OSXEditor)
        {
            string file = EditorUtility.OpenFilePanel("Select your Mac/Windows standalone build(s)", EditorPrefs.GetString("ObfuscateTargetFolder", ""), "");
            ObfuscateFile(file);
        }
        else
        {
            string folder = EditorUtility.OpenFolderPanel("Select your Mac/Windows standalone build(s)", EditorPrefs.GetString("ObfuscateTargetFolder", ""), "");
            ObfuscateFolder(folder);
        }

    }

    public static void ObfuscateFile(string selectedTargetFile)
    {

        if (selectedTargetFile.Substring(selectedTargetFile.Length - 4) != ".app")
            selectedTargetFile = selectedTargetFile.Substring(0, selectedTargetFile.LastIndexOf('/'));

        ObfuscateFolder(selectedTargetFile);
    }

    public static void ObfuscateFolder(string selectedTargetFolder)
    {
        string targetFolder = selectedTargetFolder;
        if (targetFolder.Length >= 1 && targetFolder[targetFolder.Length - 1] != '/')
            targetFolder = targetFolder + "/";
        targetFolder = FindFolder(targetFolder, "Managed");
        if (targetFolder == "")
        {
            Debug.LogError("No \"Managed\" folder found to obfuscate!");
            return;
        }
        targetFolder = targetFolder.Replace("\\", "/");
        if (targetFolder[targetFolder.Length - 1] != '/')
            targetFolder = targetFolder + "/";

        UpdateSettingsFile(targetFolder);


        EnableAllDlls(GetObfuscatorFolder() + OBF_PATH); 
        if (Application.platform == RuntimePlatform.OSXEditor)
        {			
            selectedTargetFolder = selectedTargetFolder.Substring(0, selectedTargetFolder.LastIndexOf('/'));
            EditorPrefs.SetString("ObfuscateTargetFolder", selectedTargetFolder);
			string macFilename = "" + (GetObfuscatorFolder()) + "runMac.sh";
			macFilename = macFilename.Substring( macFilename.LastIndexOf("Assets")  );
            System.Diagnostics.Process.Start("/usr/bin/mono", "'" + (GetObfuscatorFolder()) + OBF_PATH + "Obfuscaror.exe' '" + (SettingsFile()) + "'").WaitForExit();
        }
        else
        {            
            EditorPrefs.SetString("ObfuscateTargetFolder", targetFolder);            
            System.Diagnostics.Process.Start("\"" + ObfuscatorFile() + "\"", "\"" + SettingsFile() + "\"").WaitForExit(); 
        }
        DisableAllDlls(GetObfuscatorFolder() + OBF_PATH); 

        File.Delete(targetFolder + "Mapping.txt");
        File.Delete(SettingsFile());
        AssetDatabase.DeleteAsset(SettingsFile());

        Debug.Log("Obfuscated folder: " + targetFolder);
    }

    static void EnableAllDlls(string folder)
    {
        DirectoryInfo dirInfo = new FileInfo(folder).Directory;
        foreach (FileInfo fInfo in dirInfo.GetFiles())
        {
            if (fInfo.Extension == ".DISABLEDdll"){                
                string nam = fInfo.FullName.Replace(".DISABLEDdll", ".dll");
                fInfo.MoveTo(nam);
            }
        }
    }
    static void DisableAllDlls(string folder)
    {
        DirectoryInfo dirInfo = new FileInfo(folder).Directory;
        foreach (FileInfo fInfo in dirInfo.GetFiles())
        {
            if (fInfo.Extension == ".dll")
            {
                string nam = fInfo.FullName.Replace(".dll", ".DISABLEDdll");
                fInfo.MoveTo(nam);
            }
        }
    }
	
    static void UpdateSettingsFile(string targetFolder)
    {
        StreamReader tr = new StreamReader(TemplateSettingsFile());
        string settingsFile = tr.ReadToEnd();

        string pattern = @"""__TARGET_PATH__""";
        string replacement = "\"" + targetFolder + "\"";
        settingsFile = Regex.Replace(settingsFile, pattern, replacement);

        string fileFilter = GetFileFilter(targetFolder);
        if (fileFilter == "")
        {
            Debug.LogError("Error: no files of interest present in the target folder!");
            return;
        }

        settingsFile = settingsFile.Replace("__TARGET_FILES__", fileFilter);

        // close the stream
        tr.Close();

        StreamWriter sr = new StreamWriter(SettingsFile());
        sr.Write(settingsFile);
        sr.Close();
    }

    static string GetFileFilter(string targetFolder)
    {
        DirectoryInfo dirInfo = new FileInfo(targetFolder).Directory;
        string fileFilter = "";
        foreach (FileInfo fInfo in dirInfo.GetFiles())
        {
            /* //No need to obfuscate default stuff, could possibly only mess up/slow down
             if (fInfo.Name.IndexOf("UnityScript") == 0) continue;
             if (fInfo.Name.IndexOf("UnityEngine") == 0) continue;
             if (fInfo.Name.IndexOf("Mono.") == 0) continue;
             if (fInfo.Name.IndexOf("System.") == 0) continue;
             //Signed
             if (fInfo.Name.IndexOf("Boo.Lang") == 0) continue;
             if (fInfo.Name.IndexOf("ICSharpCode.") == 0) continue;
             if (fInfo.Name.IndexOf("mscorlib.") == 0) continue;
             */

            //Only obfuscate the "Assembly" .dlls (Assembly-CSharp.dll etc.)
            if (fInfo.Name.IndexOf(".dll") == -1) continue;
            if (fInfo.Name.IndexOf("Assembly") == -1) continue;
			
			// VILLE:
			//if (fInfo.Name.IndexOf("firstpass") >= 0) continue;
			
            fileFilter += "<Module file=\"$(InPath)" + fInfo.Name + "\">\n\r";
            fileFilter += "<SkipType name=\"^.*\"/>\n\r";
            //fileFilter += "<SkipMethod type=\"public\"  rx=\".*\" />\n\r";
            fileFilter += "<SkipMethod type=\"^.*\"  rx=\".*\" />\n\r";
			
			fileFilter += @"
			<SkipNamespace name=""LitJson"" />
			<SkipType name=""*Ouya*"" skipMethods=""true"" skipFields=""true"" skipEvents=""true"" skipProperties=""true"" skipStringHiding=""true"" />
			<SkipType name=""*xsd_*"" skipMethods=""true"" skipFields=""true"" skipEvents=""true"" skipProperties=""true"" skipStringHiding=""true"" />

			<!-- UnityScript has compiler generated methods with special ""$blah$"" names that need to be skipped. -->
	        <SkipType rx="".*\$.*"" skipMethods=""true""/>
	        <!-- C# has compiler generated iterators for coroutines that need to be skipped. -->
	        <SkipType rx="".*__Iterator.*"" skipMethods=""true""/>
			";
			
            fileFilter += "</Module>\n\r";
			
			/*
			// VILLE: https://gist.github.com/darktable/3942495
			
        fileFilter += "<Module file=\"$(InPath)" + fInfo.Name + "\">\n\r";
		fileFilter += @"
        <!-- Available module rules and supported attributes
        SkipNamespace: name, rx
        SkipType: name, rx, attrib, typeinherits, static, serializable, skipMethods, skipStringHiding, skipFields, skipProperties, skipEvents
        SkipField: name, rx, type, attrib, typeattrib, typeinherits, static, serializable, decorator
        SkipMethod: name, rx, type, attrib, typeattrib, typeinherits, static
        SkipProperty: name, rx, type, attrib, typeattrib
        SkipEvent: name, rx, type, attrib, typeattrib
 
        SkipStringHiding: name, rx, type, attrib, typeattrib
        -->
 
        <!-- AngryBots uses SendMessage(string) a lot. The messages go to ""OnSomething"" methods. -->
        <SkipMethod typeinherits=""UnityEngine.Component"" static=""false"" attrib=""public"" rx=""^On.*$""/>
        
        <!-- You might need this if you're using SendMesssage(string) a lot.
        <SkipMethod typeinherits=""UnityEngine.Component"" static=""false"" attrib=""public"" rx=""^.*$""/>
        -->
        
        <!-- skip special names
        If you're using SendMessage(string), Invoke(string), InvokeRepeating(string), or StartCoroutine(string)
        you should rename the methods being called with underscore before and after the name (e.g.  _YourFunc_)
        so those methods won't get renamed during obfuscation. -->
        <SkipField rx=""^_.+_$""/>
        <SkipMethod rx=""^_.+_$""/>
        <SkipProperty rx=""^_.+_$""/>
        <SkipEvent rx=""^_.+_$""/>
        
        <!-- skip renaming Unity Components and public fields of those classes.
        These are most likely MonoBehaviours. -->
        <SkipType typeinherits=""UnityEngine.Object"" static=""false""/>
        <SkipField typeinherits=""UnityEngine.Object"" static=""false"" attrib=""public""/>
        <SkipField decorator=""UnityEngine.SerializeField""/>
 
        <!-- skip renaming serializable classes and their fields -->
        <SkipType serializable=""true""/>
        <SkipField serializable=""true""/>
        
        <!-- UnityScript has compiler generated methods with special ""$blah$"" names that need to be skipped. -->
        <SkipType rx="".*\$.*"" skipMethods=""true""/>
        <!-- C# has compiler generated iterators for coroutines that need to be skipped. -->
        <SkipType rx="".*__Iterator.*"" skipMethods=""true""/>
 
        <!-- reserved MonoBehaviour events -->
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""Update""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""Awake""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""Start""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""LateUpdate""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""FixedUpdate""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnApplicationFocus""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnApplicationPause""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnApplicationQuit""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnAudioFilterRead""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnBecameInvisible""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnBecameVisible""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnCollisionEnter""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnCollisionExit""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnCollisionStay""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnConnectedToServer""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnControllerColliderHit""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnDestroy""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnDisable""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnDisconnectedFromServer""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnEnable""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnFailedToConnect""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnFailedToConnectToMasterServer""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnGUI""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnJointBreak""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnLevelWasLoaded""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnMasterServerEvent""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnMouseDown""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnMouseDrag""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnMouseEnter""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnMouseExit""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnMouseOver""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnMouseUp""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnMouseUpAsButton""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnNetworkInstantiate""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnParticleCollision""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnPlayerConnected""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnPlayerDisconnected""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnPostRender""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnPreCull""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnPreRender""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnRenderImage""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnRenderObject""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnSerializeNetworkView""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnServerInitialized""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnTriggerEnter""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnTriggerExit""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnTriggerStay""/>
        <SkipMethod typeinherits=""UnityEngine.MonoBehaviour"" name=""OnWillRenderObject""/>
    </Module>";
    */
		}

        return fileFilter;
    }

    //Get the "MANAGED" folder that contains our DLLs
    static string FindFolder(string targetFolder, string searchFolderName)
    {
        return FindFolder(targetFolder, searchFolderName, false);
    }

    static string FindFolder(string targetFolder, string searchFolderName, bool ignoreCurDir)
    {
        if (targetFolder == "") return "";
        DirectoryInfo dirInfo = new FileInfo(targetFolder).Directory;
        // Debug.Log(targetFolder+" _ "+dirInfo.Name+"_ "+searchFolderName);

        if (!ignoreCurDir && dirInfo.Name == searchFolderName) return targetFolder;

        DirectoryInfo[] allDirs = dirInfo.GetDirectories();
        if (allDirs.Length >= 50) { //Debug.LogWarning("Aborted obfuscation, illegal target folder(" + targetFolder + ")?"); 
            return ""; }
        foreach (DirectoryInfo dirInfos in allDirs)
        {
            string result = FindFolder(dirInfos.FullName + "/", searchFolderName);
            result = result.Replace('\\', '/');
            if (result != "") return result;
        }
        return "";
    }


    static string GetObfuscatorFolder()
    {
        //if (OBF_MAINFOLDER != "") return OBF_MAINFOLDER;
        string obfFolder = FindFolder(Application.dataPath, "M2HObfuscator", true);
        if (obfFolder == "")
        {
			OBF_MAINFOLDER = FindFolder(Application.dataPath, "Obfuscator");
        }
        else{
            OBF_MAINFOLDER = obfFolder + "Editor/Obfuscator/";
		}
        if (!File.Exists(OBF_MAINFOLDER + "/settingsTemplate.proj"))
        {
            OBF_MAINFOLDER = FindFolder(OBF_MAINFOLDER, "Obfuscator", true);
        }
        return OBF_MAINFOLDER;
    }

    static string TemplateSettingsFile()
    {
        return GetObfuscatorFolder() + "settingsTemplate.proj";
    }
    static string SettingsFile()
    {
        return GetObfuscatorFolder() + "settings.proj";
    }
    static string ObfuscatorFile()
    {
        return GetObfuscatorFolder() + OBF_PATH + "Obfuscaror.exe";
    }
}
