// NNuke.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>


void convert_to_string(wchar_t *crap, char *buf) {
	
	// convert wide-char to normal char
	while (*buf++ = (char)*crap++);
}

int _tmain(int argc, _TCHAR* argv[])
{
	if (argc != 3) {
		fprintf(stderr, "USAGE: NNuke [file-in] [file-out]\n");
		return -1;
	}

	char nameIn[256];
	convert_to_string(argv[1], nameIn);
	char nameOut[256];
	convert_to_string(argv[2], nameOut);

	FILE *f = fopen(nameIn, "rb");
	if (f == NULL) {
		fprintf(stderr, "Could not open file \"%s\"\n", nameIn);
		return -2;
	}

	fseek(f, 0 , SEEK_END);
	int size = ftell(f);
	unsigned char* buffer = (unsigned char*)malloc(size);

	fseek(f, 0, SEEK_SET);
	fread(buffer, 1, size, f);
	fclose(f);

	int i = 0;
	int nuked = 0;
	for (i = 0; i < size-7; i++) {
		if (buffer[i] == 'N' && buffer[i+1] == 'N' && buffer[i+2] == 'U' && buffer[i+3] == 'K' && buffer[i+4] == 'E' && buffer[i+5]== '_') {
			// GOT IT! NNUKE until 0
			nuked++;
			while (buffer[i] != 0 && i < size) {
				buffer[i++] = 0;
			}
		}
	}

	fprintf(stderr, "NUKED %d strings.\n", nuked);

	f = fopen(nameOut, "wb");
	if (f == NULL) {
		fprintf(stderr, "Could not open file \"%s\" for writing\n", nameOut);
		return -3;
	}

	fwrite(buffer, 1, size, f);
	fclose(f);

	return 0;
}

