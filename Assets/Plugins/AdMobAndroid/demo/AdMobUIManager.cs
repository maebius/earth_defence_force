using UnityEngine;
using System.Collections.Generic;
using Prime31;


public class AdMobUIManager : MonoBehaviourGUI
{
#if UNITY_ANDROID
	void OnGUI()
	{
		beginColumn();


		if( GUILayout.Button( "Init" ) )
		{
			AdMobAndroid.init( "YOUR_APP_ID_HERE" );
		}


		if( GUILayout.Button( "Set Test Devices" ) )
		{
			AdMobAndroid.setTestDevices( new string[] { "6D13FA054BC989C5AC41900EE14B0C1B", "8E2F04DC5B964AFD3BC2D90396A9DA6E", "3BAB93112BBB08713B6D6D0A09EDABA1" } );
		}


		if( GUILayout.Button( "Create Smart Banner" ) )
		{
			// place it on the top
			AdMobAndroid.createBanner( AdMobAndroidAd.smartBanner, AdMobAdPlacement.BottomCenter );
		}


		if( GUILayout.Button( "Create 320x50 banner" ) )
		{
			// place it on the top
			AdMobAndroid.createBanner( AdMobAndroidAd.phone320x50, AdMobAdPlacement.TopCenter );
		}


		if( GUILayout.Button( "Create 300x250 banner" ) )
		{
			// center it on the top
			AdMobAndroid.createBanner( AdMobAndroidAd.tablet300x250, AdMobAdPlacement.BottomCenter );
		}


		if( GUILayout.Button( "Destroy Banner" ) )
		{
			AdMobAndroid.destroyBanner();
		}


		endColumn( true );


		if( GUILayout.Button( "Refresh Ad" ) )
		{
			AdMobAndroid.refreshAd();
		}


		if( GUILayout.Button( "Request Interstitial" ) )
		{
			AdMobAndroid.requestInterstital( "a14de56b4e8babd" );
		}


		if( GUILayout.Button( "Is Interstitial Ready?" ) )
		{
			var isReady = AdMobAndroid.isInterstitalReady();
			Debug.Log( "is interstitial ready? " + isReady );
		}


		if( GUILayout.Button( "Display Interstitial" ) )
		{
			AdMobAndroid.displayInterstital();
		}


		if( GUILayout.Button( "Hide Banner" ) )
		{
			AdMobAndroid.hideBanner( true );
		}


		if( GUILayout.Button( "Show Banner" ) )
		{
			AdMobAndroid.hideBanner( false );
		}

		endColumn();
	}
#endif
}
