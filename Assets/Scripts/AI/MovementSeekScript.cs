using UnityEngine;
using System.Collections;

sealed public class MovementSeekScript : MonoBehaviour 
{
	#region Enums, structs, and inner classes
	sealed public class Steering
	{
		public Vector3 wantedVelocity;
		public Quaternion wantedOrientation;
	}
	#endregion

	#region Delegates And Events
	#endregion

	#region Public members, visible in Unity GUI
	public float AccelerationMax;
	public float SpeedMax;
	public float TurningSpeedAnglesPerSecond;
	
	public DamageTakerScript Target;
	
	public float SeekRadius;
	public bool DampMovementIfNoTarget;
	
	public Vector3 OrientationOffsetAngles;
	#endregion

	#region Public members, not visible to Unity GUI
	[HideInInspector]
	#endregion

	#region Private data members
	private Vector3 m_targetPosition;
	
	private Quaternion m_offsetTargetOrientation;
	#endregion


	#region Unity MonoBehaviour base overrides
		
	// Use this for initialization
	void Start ()
	{
		m_offsetTargetOrientation = Quaternion.Euler(OrientationOffsetAngles);
		
		Update();
	}
	
	// Update is called once per frame
	void Update()
	{
		// still valid?
		if (Target == null)
		{
			// ... try to get new target
			// NOTE! We default here to enemy ...
			Target = LevelScript.Instance.GetClosestEnemyTo(transform.position, SeekRadius);
		}
		
		// just copy our targets position
		if (Target)
		{
			m_targetPosition = Target.transform.position;
		}
		else
		{
			// no valid targets, make target to be straight ahead
			m_targetPosition = transform.position + transform.rotation * (100f*Vector3.up);
		}
	}
	
	void FixedUpdate ()
	{
		Steering steering = NNUKE_GetSteering ();
		
		if (Target == null && DampMovementIfNoTarget)
		{
			steering.wantedVelocity = Vector3.zero;
		}
		
		gameObject.GetComponent<Rigidbody>().velocity = Vector3.MoveTowards(
			gameObject.GetComponent<Rigidbody>().velocity
			, steering.wantedVelocity
			, AccelerationMax * GameStateScript.Instance.NNUKE_GetWorldDeltaTime());
		
		gameObject.transform.localRotation = Quaternion.RotateTowards(
			gameObject.transform.localRotation
			, steering.wantedOrientation * m_offsetTargetOrientation
			, TurningSpeedAnglesPerSecond * GameStateScript.Instance.NNUKE_GetWorldDeltaTime());
	}
	#endregion
		
	#region Public object methods
	
	#endregion
		
	#region Private object methods
	[Obfuscar.Obfuscate]
	private Steering NNUKE_GetSteering()
	{
		Steering steering = new Steering();
		
		Vector3 direction = (m_targetPosition - transform.position);
		direction.Normalize();
		steering.wantedVelocity = SpeedMax * direction;
		
		Quaternion wantedOrientation = Quaternion.LookRotation(direction, Vector3.forward);
		wantedOrientation.x = 0f;
		wantedOrientation.y = 0f;
		
		steering.wantedOrientation = wantedOrientation;
		
		return steering;
		
	}
	#endregion
}
