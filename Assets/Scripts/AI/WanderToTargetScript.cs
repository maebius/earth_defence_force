//#define DEBUG_DRAW
using UnityEngine;
using System.Collections;

sealed public class WanderToTargetScript : MonoBehaviour 
{
	public Transform m_target = null;
	public float m_circleRadiusAtTarget;
	public float m_circleRadiusAtMax;
	public float m_distanceForMaxCircle;
	public float m_wanderRateDegreesPerSecond;
	public float m_updateIntervalSeconds = 0f;
	
	private float m_currentWanderAngleDegrees = 0f;
	private float m_nextUpdateSeconds;
	
	private EnemyBaseScript m_enemy = null;
	
	// Use this for initialization
	void Start () 
	{
		m_enemy = GetComponent<EnemyBaseScript>();
		if (m_target == null && PlayerHomePlanetScript.Instance != null)
		{
			m_target = PlayerHomePlanetScript.Instance.transform;
		}
		m_nextUpdateSeconds = GameStateScript.Instance.NNUKE_GetWorldTime() + m_updateIntervalSeconds;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (m_target == null || m_enemy == null)
		{
			return;
		}
		float timeNow = GameStateScript.Instance.NNUKE_GetWorldTime();
		if (m_nextUpdateSeconds > timeNow)
		{
			return;
		}
		float delta = Mathf.Max (m_updateIntervalSeconds, GameStateScript.Instance.NNUKE_GetWorldDeltaTime());
		
		m_nextUpdateSeconds = timeNow + m_updateIntervalSeconds;
		m_currentWanderAngleDegrees += delta * MathHelpers.Binominial() * m_wanderRateDegreesPerSecond;
		
		m_currentWanderAngleDegrees = MathHelpers.ClampDegrees(m_currentWanderAngleDegrees);
		
		Vector3 dir = m_target.position - transform.position;
		float distance = dir.magnitude;
		dir.Normalize();
		
		float radius  = m_circleRadiusAtMax;
		if (distance < m_distanceForMaxCircle)
		{
			radius -= (m_circleRadiusAtMax - m_circleRadiusAtTarget) 
				* ((m_distanceForMaxCircle - distance)/m_distanceForMaxCircle);
		}
#if DEBUG_DRAW
		Utility.DrawCircleXY(m_target.position, radius, 10, Color.blue, m_updateIntervalSeconds);
#endif
		
		Quaternion q = Quaternion.AngleAxis(m_currentWanderAngleDegrees, Vector3.forward);
		Vector3 newOffset = q * (radius * dir);
		newOffset.z = 0f;
		
		m_enemy.MovementTarget = m_target.position - newOffset;
		
	}
}
