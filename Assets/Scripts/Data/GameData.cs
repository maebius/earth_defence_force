//#define DEBUG_TEXTS

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.IO;

sealed public class GameData : MonoBehaviour
{
	#region Public GUI tweakable members
	public TextAsset DataXml;
	public TextAsset DataXmlSchema;
	#endregion

	#region Private data members	
	private static GameData sm_instance = null;
	private System.Type GameDataType = typeof(xsd_EDF_Data);
	private static xsd_EDF_Data m_data = null;
	#endregion

	#region Public members, not visible to Unity GUI
	[HideInInspector]
	public static bool IsCreated
	{
		get
		{
			return sm_instance != null;
		}
	}
	
	[HideInInspector]
	public static GameData Instance 
	{
		get 
		{
			if (sm_instance == null)
			{
				sm_instance = Object.FindObjectOfType(typeof(GameData)) as GameData;

				if (sm_instance == null)
				{
					Debug.LogError( 
						string.Format (
						"GameData.Instance.get - m_instance is null! We need to have this somewhere"));
				}
			}
			return sm_instance;
		}
	}
	[HideInInspector]
	public xsd_EDF_Data Data 
	{
		get { return m_data; }
	}
	#endregion
	
	#region Unity MonoBehaviour base overrides
	void Awake () 
	{ 
		if (sm_instance == null) 
		{ 
			sm_instance = this;
			DontDestroyOnLoad(gameObject); 
			sm_instance.NNUKE_Initialize();
		} 
		else 
			Destroy(gameObject); 
					
	}
	
	void OnEnable () { if (sm_instance == null) sm_instance = this; }
	void OnDestroy () { if (sm_instance == this) sm_instance = null; }
	#endregion
	
	#region Public object methods
	
	
	[Obfuscar.Obfuscate]
	public string NNUKE_GetSceneName(string worldId, int index)
	{
		xsd_ct_LevelData level = NNUKE_GetLevelData(worldId, index);
		if (level == null)
			return null;
		return level.SceneName;
	}
	
	
	[Obfuscar.Obfuscate]
	public int NNUKE_GetLevelCount(string id)
	{
		xsd_ct_WorldData world = NNUKE_GetWorld(id);
		if (world == null)
			return -1;
		return world.LevelData.Length;
	}	
	
	[Obfuscar.Obfuscate]
	public int NNUKE_GetStarCountFor(string id, int level, int score)
	{
		xsd_ct_LevelData data = NNUKE_GetLevelData(id, level);
		if (data == null)
			return -1;
		if (score >= data.ScoreLimitStar_3) return 3;
		if (score >= data.ScoreLimitStar_2) return 2;
		if (score >= data.ScoreLimitStar_1) return 1;
		return 0;
	}
		
	[Obfuscar.Obfuscate]
	public xsd_ct_GarageSlot NNUKE_GetEntry(string id)
	{
		if (m_data == null || m_data.Equipment == null)
			return null;
		
		foreach (xsd_ct_GarageSlot slot in Data.Equipment)
		{
			if (slot.Id == id)
				return slot;
		}
		return null;
	}	
		
	[Obfuscar.Obfuscate]
	public int NNUKE_GetSlotPropertyIndex(xsd_ct_Equipment eq, string name)
	{
		xsd_ct_GarageSlot slot = NNUKE_GetEntry(eq.Id);
		if (slot == null || slot.Property == null) 
			return -1;
		
		if (slot.Property1 == name) return eq.Property1;
		if (slot.Property2 == name) return eq.Property2;
		
		return 0;
	}
	[Obfuscar.Obfuscate]
	public int NNUKE_GetMaxPropertyIndex(xsd_ct_Equipment eq, string name)
	{
		xsd_ct_GarageSlot slot = NNUKE_GetEntry(eq.Id);
		if (slot == null || slot.Property == null) 
			return -1;

		foreach (xsd_ct_PropertyInfo pi in slot.Property)
		{
			if (pi.Name == name && pi.Level != null)
			{
				return pi.Level.Length;
			}
		}
		return -1;
	}
	
	[Obfuscar.Obfuscate]
	public void NNUKE_SetSlotPropertyIndex(xsd_ct_Equipment eq, string name, int level)
	{
		xsd_ct_GarageSlot slot = NNUKE_GetEntry(eq.Id);
		if (slot == null || slot.Property == null) 
			return;
		
		if (slot.Property1 == name) eq.Property1 = level;
		if (slot.Property2 == name) eq.Property2 = level;
	}
	[Obfuscar.Obfuscate]
	public int NNUKE_GetLevelCost(xsd_ct_Equipment eq, string property, int level)
	{
		xsd_ct_GarageSlot slot = NNUKE_GetEntry(eq.Id);
		if (slot == null || slot.Property == null) 
			return -1;
		
		foreach (xsd_ct_PropertyInfo pi in slot.Property)
		{
			if (pi.Name == property && pi.Level != null)
			{
				foreach (xsd_ct_LevelUpInfo li in pi.Level)
				{
					if (li.Value == level)
						return li.Cost;
				}
			}
		}
		return -1;
	}

	
	[Obfuscar.Obfuscate]
	public xsd_ct_LevelData[] NNUKE_GetLevelData(string worldId)
	{
		xsd_ct_WorldData world = NNUKE_GetWorld(worldId);
		if (world == null)
			return null;
		return world.LevelData;
	}
	
	[Obfuscar.Obfuscate]
	public xsd_ct_LevelData NNUKE_GetLevelData(string worldId, int levelIndex)
	{
		xsd_ct_WorldData world = NNUKE_GetWorld(worldId);
		if (world == null)
			return null;
		
		if (levelIndex < -1 || levelIndex >= world.LevelData.Length)
			return null;
		
		return world.LevelData[levelIndex];
	}
		
	[Obfuscar.Obfuscate]
	public xsd_ct_WorldData NNUKE_GetWorld(string id)
	{
		if (m_data == null || m_data.WorldData == null)
			return null;
		
		foreach (xsd_ct_WorldData world in Data.WorldData)
		{
			if (world.Id == id)
				return world;
		}
		return null;
	}	
	
	#endregion

	#region Private object methods
	
	[Obfuscar.Obfuscate]
	private void NNUKE_Initialize ()
	{
		#if DEBUG_TEXTS
		Debug.Log (string.Format ("GameData.NNUKE_Initialize() - DataXml: {0}", DataXml.name));
		#endif
		
		/*
		if (true)
		{
			NNUKE_CreateDataByCode();
			return;
		}*/
		
		//XmlReader reader = XmlTools.CreateReader(DataXml, DataXmlSchema, GameDataType);
		XmlReader reader = XmlTools.CreateReader(DataXml, null, GameDataType);
		
		if (reader != null)
		{
			#if DEBUG_TEXTS
			Debug.Log (string.Format ("GameData.NNUKE_Initialize() - created XmlReader, calling Deserialize"));
			#endif
			
			m_data = (xsd_EDF_Data) XmlTools.Serializer.Deserialize (reader);
			
			#if DEBUG_TEXTS
			Debug.Log (string.Format ("GameData.NNUKE_Initialize() - Deserialize finished, sm_data != null => {0}, ToString(): {1}"
				, m_data != null, (m_data != null) ? m_data.ToString() : "null"));
			#endif
			
			XmlTools.CleanUpSerializer();
		}
		else
		{			
			Debug.LogError (
				string.Format ("Epic fail! Could not load game data (\"{0}\"), so should bail out.", DataXml));
			
			// epic fail, bail out
			Application.Quit();
		}
	}
	
	[Obfuscar.Obfuscate]
	private void NNUKE_CreateDataHelper(int index, string id, string iconId, string silhId, string locId, string p1, string p2)
	{
		/*
		m_data.Equipment[index] = new xsd_ct_GarageSlot();
		m_data.Equipment[index].Id = id;
		m_data.Equipment[index].IconSilhouetteId = silhId;
		m_data.Equipment[index].Property1 = p1;
		m_data.Equipment[index].Property2 = p2;
		m_data.Equipment[index].IconId = iconId;
		m_data.Equipment[index].LocalizationKey = "menu_garage_select_name_" + locId;
		m_data.Equipment[index].Level = new xsd_ct_SlotInfo[5];
		m_data.Equipment[index].Level[0] = new xsd_ct_SlotInfo();
		m_data.Equipment[index].Level[0].Level = 0;
		m_data.Equipment[index].Level[0].Cost = 1000;
		m_data.Equipment[index].Level[0].Property1 = 0;
		m_data.Equipment[index].Level[0].Property2 = 0;
		m_data.Equipment[index].Level[1] = new xsd_ct_SlotInfo();
		m_data.Equipment[index].Level[1].Level = 1;
		m_data.Equipment[index].Level[1].Cost = 5000;
		m_data.Equipment[index].Level[1].Property1 = 1;
		m_data.Equipment[index].Level[1].Property2 = 1;
		m_data.Equipment[index].Level[2] = new xsd_ct_SlotInfo();
		m_data.Equipment[index].Level[2].Level = 2;
		m_data.Equipment[index].Level[2].Cost = 10000;
		m_data.Equipment[index].Level[2].Property1 = 2;
		m_data.Equipment[index].Level[2].Property2 = 2;
		m_data.Equipment[index].Level[3] = new xsd_ct_SlotInfo();
		m_data.Equipment[index].Level[3].Level = 3;
		m_data.Equipment[index].Level[3].Cost = 20000;
		m_data.Equipment[index].Level[3].Property1 = 3;
		m_data.Equipment[index].Level[3].Property2 = 3;
		m_data.Equipment[index].Level[4] = new xsd_ct_SlotInfo();
		m_data.Equipment[index].Level[4].Level = 4;
		m_data.Equipment[index].Level[4].Cost = 50000;
		m_data.Equipment[index].Level[4].Property1 = 4;
		m_data.Equipment[index].Level[4].Property2 = 4;*/
	}
	
	[Obfuscar.Obfuscate]
	private void NNUKE_CreateDataByCode()
	{
		m_data = new xsd_EDF_Data();
		m_data.InitialCredits = 1000;
		
		m_data.Equipment = new xsd_ct_GarageSlot[7];
		NNUKE_CreateDataHelper(0, "empty", "", "", "", "", "");
		NNUKE_CreateDataHelper(1, "cannon", "cannon_big", "icon_weapon_cannon", "menu_garage_select_name_cannon", "Power", "Rate");
		NNUKE_CreateDataHelper(2, "laser", "laser_big", "icon_weapon_laser", "menu_garage_select_name_laser", "Power", "Range");
		NNUKE_CreateDataHelper(3, "turret", "turret_big", "", "menu_garage_select_name_turret", "Power", "Rate");
		NNUKE_CreateDataHelper(4, "turret_rear", "turret_rear_big", "",  "menu_garage_select_name_rear_turret", "Power", "Rate");
		NNUKE_CreateDataHelper(5, "shockwave", "shockwave_big", "icon_item_shockwave", "menu_garage_select_name_shockwave", "Power", "Speed");
		NNUKE_CreateDataHelper(6, "mine", "mine_big", "icon_item_mine", "menu_garage_select_name_mine", "Power", "Speed");	
		
		m_data.WorldData = new xsd_ct_WorldData[1];
		m_data.WorldData[0] = new xsd_ct_WorldData();
		m_data.WorldData[0].Id = "First 3";
		m_data.WorldData[0].LevelData = new xsd_ct_LevelData[3];
		for (int i = 0; i < 3; i++)
		{
			m_data.WorldData[0].LevelData[i] = new xsd_ct_LevelData();
			m_data.WorldData[0].LevelData[i].SceneName = string.Format("level_0_{0}", i); 
			m_data.WorldData[0].LevelData[i].InitiallyOpen = false; 
			m_data.WorldData[0].LevelData[i].ScoreLimitStar_1 = 15000;
			m_data.WorldData[0].LevelData[i].ScoreLimitStar_2 = 30000;
			m_data.WorldData[0].LevelData[i].ScoreLimitStar_3 = 50000;
		}
		m_data.WorldData[0].LevelData[0].InitiallyOpen = true;
	}
	#endregion	
}