//#define DEBUG_TEXTS

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.IO;

sealed public class SaveData : MonoBehaviour
{
	
#region Public GUI tweakable members
	public bool UnlockAll;
	
	public TextAsset DataXmlSchema;
	public TextAsset DefaultSaveGameXml;
	public TextAsset UnlockAllSaveGameXml;
	
	public string[] m_initiallyOpenWorlds;
	public int[] m_initiallyOpenStartIndex;
	public int[] m_initiallyOpenCount;
#endregion

#region Private data members
	private static SaveData sm_instance = null;	
	private System.Type SaveGameBaseType = typeof(xsd_EDF_Config);
	private const string SAVE_GAME_FILENAME = "save_game.xml";
	private static xsd_EDF_Config m_config = null;
	
	private string m_fullSavePath;
	
	private Texture2D m_texture = null;
	
	private int m_totalScore;
#endregion

#region Public members, not visible to Unity GUI
	private static int sm_nativeResolutionWidth = 0;
	private static int sm_nativeResolutionHeight = 0;
	
	[HideInInspector]
	public static bool IsCreated
	{
		get
		{
			return sm_instance != null;
		}
	}
	[HideInInspector]
	public xsd_EDF_Config Config 
	{
		get { return m_config; }
	}
	[HideInInspector]
	public static SaveData Instance 
	{
		get 
		{
			if (sm_instance == null)
			{
				// as we cannot instantiate MonoBehaviors directy with "new", we need to create
				// gameobject and add a component to that ...
				sm_instance = Object.FindObjectOfType(typeof(SaveData)) as SaveData;

				if (sm_instance == null)
				{
					#if DEBUG_TEXTS
					Debug.LogError( 
						string.Format (
						"SaveData.Instance.get - m_instance is null! We need to have this somewhere"));
					#endif
				}
			}
			return sm_instance;
		}
	}

	[HideInInspector]
	public int TotalScore
	{
		get { return m_totalScore; }
		set { m_totalScore = value; }
	}

	
#endregion
	
#region Unity MonoBehaviour base overrides
	void Awake () 
	{ 
		#if DEBUG_TEXTS
		Debug.Log(string.Format ("SaveData.Awake()"));
		#endif
		if (sm_instance == null) 
		{ 
			sm_instance = this;
			DontDestroyOnLoad(gameObject); 
			sm_instance.NNUKE_Initialize();
		} 
		else 
			Destroy(gameObject); 
	}
	void OnEnable () 
	{ 
		if (sm_instance == null) 
		{
			sm_instance = this; 
		}
	}
	
	public static Dictionary<string, string> sm_resolutionMap = new Dictionary<string, string>();

	void Start () 
	{ 
		if (sm_nativeResolutionHeight == 0)
		{
			sm_nativeResolutionHeight = Screen.currentResolution.height;
			sm_nativeResolutionWidth = Screen.currentResolution.width;
			
			string res = string.Format ("{0} x {1}"
				, sm_nativeResolutionWidth
				, sm_nativeResolutionHeight);
			string key = "menu_settings_resolution_best";
			sm_resolutionMap.Add (key, res);

			res = string.Format ("{0} x {1}"
				, (int) (sm_nativeResolutionWidth / 1.25f)
				, (int) (sm_nativeResolutionHeight / 1.25f));
			key = "menu_settings_resolution_good";
			sm_resolutionMap.Add (key, res);

			res = string.Format ("{0} x {1}"
				, (int) (sm_nativeResolutionWidth / 1.5f)
				, (int) (sm_nativeResolutionHeight / 1.5f));
			key = "menu_settings_resolution_medium";
			sm_resolutionMap.Add (key, res);
		
			res = string.Format ("{0} x {1}"
				, (int) (sm_nativeResolutionWidth / 2f)
				, (int) (sm_nativeResolutionHeight / 2));
			key = "menu_settings_resolution_low";
			sm_resolutionMap.Add (key, res);
		}
		
		#if DEBUG_TEXTS		
		Debug.Log( string.Format ("SaveData.{0} - sm_nativeResolutionWidth/Height: {1}x{2}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, sm_nativeResolutionWidth
			, sm_nativeResolutionHeight));
		#endif

		NNUKE_Refresh();
	}
	
	void OnDestroy () { if (sm_instance == this) sm_instance = null; }
#endregion
	
#region Public object methods
	[Obfuscar.Obfuscate]
	public bool NNUKE_IsNewHighScore(xsd_ct_LevelInfo level, int score)
	{
		if (level == null)
			return false;
		
		return score > level.Highscore;
	}
	
	[Obfuscar.Obfuscate]
	public bool NNUKE_UpdateLocalHighScore(string leaderBoardID, int score)
	{
		if (SaveData.Instance.Config.WorldInfo == null)
			return false;
		
		foreach (xsd_ct_WorldInfo wi in SaveData.Instance.Config.WorldInfo)
		{
			for (int i = 0; i < wi.LevelInfo.Length; i++)
			{
				xsd_ct_LevelData ld = GameData.Instance.NNUKE_GetLevelData(wi.LevelInfo[i].ReferencesTo, i);
				if (!string.IsNullOrEmpty(ld.LeaderboardID) && ld.LeaderboardID == leaderBoardID)
				{
					wi.LevelInfo[i].Highscore = Mathf.Max (wi.LevelInfo[i].Highscore, score);
					return true;
				}
			}
		}

		return false;
	}
	
	[Obfuscar.Obfuscate]
	public bool NNUKE_Load()
	{
		#if UNITY_WEBPLAYER
		Debug.Log(string.Format ("SaveData.Load() with UNITY_WEBPLAYER, saving is currently disabled!"));
		return false;
		#else
		if (NNUKE_DoesSaveExist())
			return NNUKE_LoadSaveGame(m_fullSavePath);
		
		return false;
		#endif
	}
	
	[Obfuscar.Obfuscate]
	public void NNUKE_Save()
	{
#if UNITY_WEBPLAYER
		#if DEBUG_TEXTS
		Debug.Log(string.Format ("SaveData.NNUKE_Save() with UNITY_WEBPLAYER, saving is currently disabled!"));
		#endif
#else
		NNUKE_SaveGame (m_fullSavePath);
#endif
	}
	
	[Obfuscar.Obfuscate]
	public xsd_ct_WorldInfo NNUKE_GetWorld(string name)
	{
		#if DEBUG_TEXTS
		Debug.Log (string.Format ("SaveData.NNUKE_GetWorld(name={0}) - Config != null => {1}, Config.WorldInfo != null => {2}, Config.WorldInfo.Length={3}"
			, name, Config != null
			, (Config != null) ? (Config.WorldInfo != null).ToString() : "null"
			, (Config != null && Config.WorldInfo != null) ? Config.WorldInfo.Length : -1));
		#endif
		
		if (!string.IsNullOrEmpty(name) && Config.WorldInfo != null)
		{
			foreach (xsd_ct_WorldInfo world in Config.WorldInfo)
			{
				if (world.ReferencesTo.Equals(name))
				{
					return world;
				}
			}
		}
#if UNITY_WEBPLAYER
		Debug.Log (string.Format ("SaveData.NNUKE_GetWorld(\"{0}\") not found!", name));
#endif
		return null;
	}
	
	[Obfuscar.Obfuscate]
	public xsd_ct_Equipment NNUKE_GetWeaponPrimary(string id)
	{
		if (!string.IsNullOrEmpty(id) && Config.WeaponPrimary != null)
		{
			foreach (xsd_ct_Equipment weapon in Config.WeaponPrimary)
			{
				if (weapon.Id.Equals(id))
				{
					return weapon;
				}
			}
		}
		return null;
	}
	
	
	[Obfuscar.Obfuscate]
	public xsd_ct_Equipment NNUKE_GetWeaponSecondary(string id)
	{
		if (!string.IsNullOrEmpty(id) && Config.WeaponSecondary != null)
		{
			foreach (xsd_ct_Equipment weapon in Config.WeaponSecondary)
			{
				if (weapon.Id.Equals(id))
				{
					return weapon;
				}
			}
		}
		return null;
	}
	
	[Obfuscar.Obfuscate]
	public xsd_ct_Equipment NNUKE_GetItem(string id)
	{
		if (!string.IsNullOrEmpty(id) && Config.Item != null)
		{
			foreach (xsd_ct_Equipment item in Config.Item)
			{
				if (item.Id.Equals(id))
				{
					return item;
				}
			}
		}
		return null;
	}
	
	[Obfuscar.Obfuscate]
	public xsd_ct_Equipment NNUKE_GetShield(string id)
	{
		if (!string.IsNullOrEmpty(id) && Config.Shield != null)
		{
			foreach (xsd_ct_Equipment item in Config.Shield)
			{
				if (item.Id.Equals(id))
				{
					return item;
				}
			}
		}
		return null;
	}
	
	[Obfuscar.Obfuscate]
	public string NNUKE_GetCurrentSceneName()
	{
		if (string.IsNullOrEmpty(Config.SelectedWorld))
			return null;
		
		string worldName = Config.SelectedWorld;
		xsd_ct_WorldInfo selectedWorld = NNUKE_GetWorld(worldName);
		
		if (selectedWorld == null)
			return null;
		
		return GameData.Instance.NNUKE_GetSceneName(worldName, selectedWorld.SelectedLevel);
	}	
	
	[Obfuscar.Obfuscate]
	public void NNUKE_ValidateCurrentLevel()
	{
		string currentRealScene = Application.loadedLevelName;
		if (NNUKE_GetCurrentSceneName() != currentRealScene)
		{
			foreach (xsd_ct_WorldInfo wi in Config.WorldInfo)
			{
				int levelCount = GameData.Instance.NNUKE_GetLevelCount(wi.ReferencesTo);
				for (int i = 0; i < levelCount; i++)
				{
					string sceneName = GameData.Instance.NNUKE_GetSceneName(wi.ReferencesTo, i); 
					if (sceneName == currentRealScene)
					{
						Config.SelectedWorld = wi.ReferencesTo;
						wi.SelectedLevel = i;
						return;
					}
				}
			}
		}
	}
		
	[Obfuscar.Obfuscate]
	public string NNUKE_GetNextWorldName(int increment)
	{
#if DEBUG_TEXTS
		Debug.Log(string.Format("SaveData.NNUKE_GetNextWorldName () increment: {0}, WorldInfo != null: {1}, length: {2}"
			, increment, Config.WorldInfo != null, (Config.WorldInfo != null) ? Config.WorldInfo.Length : -1 ));
#endif
		
		if (Config.WorldInfo == null)
			return string.Empty;
		
		int index = -1;
		for (int i = 0; i < Config.WorldInfo.Length; i++)
		{
			if (Config.WorldInfo[i].ReferencesTo == Config.SelectedWorld)
			{
				index = i + increment;
				break;
			}
		}
		return (index > -1 && index < Config.WorldInfo.Length) ? 
			Config.WorldInfo[index].ReferencesTo : null;
	}
		
	[Obfuscar.Obfuscate]
	public void NNUKE_ChooseNextLevel()
	{
		string worldName = Config.SelectedWorld;
		xsd_ct_WorldInfo selectedWorld = NNUKE_GetWorld(worldName);
		
		if (selectedWorld == null || selectedWorld.SelectedLevel < 0)
			return;
		
		selectedWorld.SelectedLevel = Mathf.Min (selectedWorld.SelectedLevel + 1, selectedWorld.LastOpenLevel);
	}
	[Obfuscar.Obfuscate]
	public bool NNUKE_IsCurrentLastOpenLevel()
	{
		string worldName = Config.SelectedWorld;
		xsd_ct_WorldInfo selectedWorld = NNUKE_GetWorld(worldName);

		return  selectedWorld.SelectedLevel == selectedWorld.LevelInfo.Length - 1;
	}
	
	[Obfuscar.Obfuscate]
	public void NNUKE_OpenNextLevel()
	{
		string worldName = Config.SelectedWorld;
		xsd_ct_WorldInfo selectedWorld = NNUKE_GetWorld(worldName);
		
		if (selectedWorld == null || selectedWorld.SelectedLevel < 0)
			return;
		
		int nextLevel = Mathf.Min (selectedWorld.SelectedLevel + 1, selectedWorld.LevelInfo.Length - 1);
		selectedWorld.LastOpenLevel = Mathf.Max (nextLevel, selectedWorld.LastOpenLevel);
		if (selectedWorld.LastOpenLevel < selectedWorld.LevelInfo.Length)
			selectedWorld.LevelInfo[selectedWorld.LastOpenLevel].Open = true;
	}
	
	[Obfuscar.Obfuscate]
	public xsd_ct_LevelInfo NNUKE_GetCurrentLevel()
	{
		xsd_ct_WorldInfo world = NNUKE_GetWorld(Config.SelectedWorld);
		if (world == null)
			return null;
		if (world.SelectedLevel >= world.LevelInfo.Length)
			return null;
		
		return world.LevelInfo[world.SelectedLevel];
	}
	
	[Obfuscar.Obfuscate]
	public void UpdateScreenShot ()
	{
		string filename = System.IO.Path.Combine ("level_images", "pah") + ".jpg";
		if (!System.IO.File.Exists (filename)) 
		{
			Application.CaptureScreenshot (filename);
		}
	}
		public Texture2D GetLevelTexture (string levelName)
	{
		string filename = "level_images/" + levelName + ".jpg";
		if (System.IO.File.Exists (filename)) 
		{
			string resourcePath = "file://" + Application.dataPath + "/../" + filename;
			//string resourcePath = "http://images.earthcam.com/ec_metros/ourcams/fridays.jpg";
			Debug.Log (string.Format ("{0}", resourcePath));

			WaitForRequest (resourcePath);
			Debug.Log (string.Format ("phase 2"));
			if (m_texture != null) 
			{
				Debug.Log (string.Format ("texture not null"));
			}

			return m_texture;
		}
		return null;
	}
		
	[Obfuscar.Obfuscate]
	public void NNUKE_NNUKE_AddWorldData(string id, int firstLevelIndex, int levelCount)
	{
		#if DEBUG_TEXTS
		Debug.Log (string.Format ("SaveData.NNUKE_NNUKE_AddWorldData - id: {0}, firstLevelIndex / count: {1} / {2}", id, firstLevelIndex, levelCount));
		#endif
		
		xsd_ct_WorldInfo wi = NNUKE_GetWorld(id);
		
		if (wi != null && wi.LevelInfo.Length > firstLevelIndex)
		{
			#if DEBUG_TEXTS
			Debug.Log (string.Format ("SaveData.NNUKE_NNUKE_AddWorldData : world {0} already exists with overlapping levels, not adding!", id));
			#endif
			return;
		}
		
		GameData gd = GameData.Instance;
		xsd_ct_LevelData[] lds = gd.NNUKE_GetLevelData(id);
		if (lds != null)
		{
			int newSize = firstLevelIndex + levelCount;
			newSize = Mathf.Min (lds.Length, newSize);
				
			bool addAsNew = false;
			bool openFirst = false;
			
			if (wi == null)
			{
				wi = new xsd_ct_WorldInfo();
				wi.ReferencesTo = id;
				wi.LastOpenLevel = 0;
				wi.SelectedLevel = 0;
				wi.LevelInfo = new xsd_ct_LevelInfo[newSize];
				addAsNew = true;
			}
			else
			{
				wi.LevelInfo = Utility.Resize<xsd_ct_LevelInfo>(wi.LevelInfo, levelCount);
				openFirst = 
					wi.LastOpenLevel == firstLevelIndex - 1
					&& wi.LevelInfo[wi.LastOpenLevel].Highscore > 0;
			}
			
			for (int i = firstLevelIndex; i < newSize; i++)
			{
				wi.LevelInfo[i] = new xsd_ct_LevelInfo();
				wi.LevelInfo[i].ReferencesTo =  lds[i].SceneName;
				wi.LevelInfo[i].Open = lds[i].InitiallyOpen;
			}
			if (openFirst)
			{
				wi.LevelInfo[firstLevelIndex].Open = true;
				wi.LastOpenLevel = firstLevelIndex;
			}
			
			if (addAsNew)
				NNUKE_AddWorld(wi);
		}
		else
		{
			#if DEBUG_TEXTS
			Debug.LogError(string.Format ("SaveData.NNUKE_NNUKE_AddWorldData() - could not find {0}", id));
			#endif
		}
	}
		
	[Obfuscar.Obfuscate]
	public void NNUKE_DeleteWorld(xsd_ct_WorldInfo wi, int firstLevelIndex, int levelCount)
	{
		int index = System.Array.IndexOf(Config.WorldInfo, wi);
		if (index > -1 && wi.LevelInfo.Length > firstLevelIndex)
		{
			int newSize = firstLevelIndex;
			if (newSize == 0)
			{
				Utility.RemoveAt<xsd_ct_WorldInfo>(Config.WorldInfo, index);
			}
			else
			{
				Config.WorldInfo[index].LevelInfo =
					Utility.Resize<xsd_ct_LevelInfo>(wi.LevelInfo, newSize - wi.LevelInfo.Length);
			}
		}
	}
	[Obfuscar.Obfuscate]
	public bool NNUKE_CanSelect(string id)
	{
		xsd_ct_Equipment equipment = NNUKE_GetWeaponPrimary(id);
		if (equipment != null)
		{
			int count = 0;
			if (Config.WeaponPrimarySlot_1 == equipment.Id) count++;
			if (Config.WeaponPrimarySlot_2 == equipment.Id) count++;
			if (Config.WeaponPrimarySlot_3 == equipment.Id) count++;
			return count <= 1;
		}
		equipment = NNUKE_GetItem(id);
		if (equipment != null)
		{
			int count = 0;
			if (Config.ItemSlot_1 == equipment.Id) count++;
			if (Config.ItemSlot_2 == equipment.Id) count++;
			return count <= 1;
		}
		equipment = NNUKE_GetWeaponSecondary(id);
		if (equipment != null)
		{
			return true;
		}
		equipment = NNUKE_GetShield(id);
		if (equipment != null)
		{
			return true;
		}
		return false;
	}
	
	[Obfuscar.Obfuscate]
	public void NNUKE_AddWeaponPrimary(xsd_ct_Equipment equipment)
	{
		Config.WeaponPrimary = NNUKE_AddEquipment(Config.WeaponPrimary, equipment);
	}
	
	[Obfuscar.Obfuscate]
	public void NNUKE_AddWeaponSecondary(xsd_ct_Equipment equipment)
	{
		Config.WeaponSecondary = NNUKE_AddEquipment(Config.WeaponSecondary, equipment);
	}
	
	[Obfuscar.Obfuscate]
	public void NNUKE_AddItem(xsd_ct_Equipment equipment)
	{
		Config.Item = NNUKE_AddEquipment(Config.Item, equipment);
	}
	
	[Obfuscar.Obfuscate]
	public void NNUKE_AddShield(xsd_ct_Equipment equipment)
	{
		Config.Shield = NNUKE_AddEquipment(Config.Shield, equipment);
	}
	
	[Obfuscar.Obfuscate]
	public void NNUKE_DeleteWeaponPrimary(xsd_ct_Equipment equipment)
	{
		Config.WeaponPrimary = NNUKE_DeleteEquipment(Config.WeaponPrimary, equipment);
	}
	
	[Obfuscar.Obfuscate]
	public void NNUKE_DeleteWeaponSecondary(xsd_ct_Equipment equipment)
	{
		Config.WeaponSecondary = NNUKE_DeleteEquipment(Config.WeaponSecondary, equipment);
	}
	
	[Obfuscar.Obfuscate]
	public void NNUKE_DeleteItem(xsd_ct_Equipment equipment)
	{
		Config.Item = NNUKE_DeleteEquipment(Config.Item, equipment);
	}
	[Obfuscar.Obfuscate]
	public void NNUKE_DeleteShield(xsd_ct_Equipment equipment)
	{
		Config.Shield = NNUKE_DeleteEquipment(Config.Shield, equipment);
	}
	[Obfuscar.Obfuscate]
	public void NNUKE_RefreshTotalScore(xsd_ct_WorldInfo wi)
	{
		m_totalScore = 0;
		
		if (wi == null || wi.LevelInfo == null)
			return;
		
		foreach (xsd_ct_LevelInfo li in wi.LevelInfo)
		{
			m_totalScore += li.Highscore;
		}
	}
	[Obfuscar.Obfuscate]
	public void NNUKE_Reset()
	{
		if (NNUKE_DoesSaveExist())
		{
			#if DEBUG_TEXTS
			Debug.Log (string.Format ("SaveData.Reset() - m_fullSavePath = {0} exists, trying to delete it", m_fullSavePath));
			#endif
			
			System.IO.File.Delete(m_fullSavePath);
		}
		NNUKE_CreateNewConfig();
		TotalScore = 0;
		NNUKE_Save();
	}
#endregion
	
#region Private object methods
	
	[Obfuscar.Obfuscate]
	private void NNUKE_AddWorld(xsd_ct_WorldInfo wi)
	{
		int newSize = (Config.WorldInfo != null) ? Config.WorldInfo.Length + 1 : 1;
		
		xsd_ct_WorldInfo[] newInfo = new xsd_ct_WorldInfo[newSize];
		if (newSize > 1)
		{
			System.Array.Copy (Config.WorldInfo, newInfo, Config.WorldInfo.Length);
		}
		newInfo[newSize - 1] = wi;
		Config.WorldInfo = newInfo;
	}
		
	[Obfuscar.Obfuscate]
	private xsd_ct_Equipment[] NNUKE_AddEquipment(xsd_ct_Equipment[] table, xsd_ct_Equipment equipment)
	{
		int newSize = (table != null) ? table.Length + 1 : 1;
		xsd_ct_Equipment[] newTable = new xsd_ct_Equipment[newSize];
		if (newSize > 1)
			System.Array.Copy(table, newTable, table.Length);
		newTable[newSize - 1] = equipment;
		return newTable;
	}
		
	[Obfuscar.Obfuscate]
	private xsd_ct_Equipment[] NNUKE_DeleteEquipment(xsd_ct_Equipment[] table, xsd_ct_Equipment equipment)
	{
		int index = System.Array.IndexOf(table, equipment);
		return (index > -1) ? Utility.RemoveAt<xsd_ct_Equipment>(table, index) : table;
	}
		
	[Obfuscar.Obfuscate]
	private void NNUKE_Refresh()
	{
		// NOTE! We do not want to directy access any system from here, they should all 
		// instead fetch the data from here what they want (sound volumes etc.)
		
		// TODO -> consider using deletegates and firing an event here
		SettingsScript.SetResolution(Config.Resolution);
		
		string worldName = Config.SelectedWorld;
		xsd_ct_WorldInfo selectedWorld = NNUKE_GetWorld(worldName);
		if (selectedWorld == null && Config.WorldInfo != null)
		{
			Config.SelectedWorld = Config.WorldInfo[0].ReferencesTo;
		}
		
		NNUKE_RefreshTotalScore(NNUKE_GetWorld(Config.SelectedWorld));

		// NOTE! Need to do this here as NGUI Localization script uses PlayerPref's here directly
		// and we want to intervene right away ...
		string language = Localization.instance.languages[0].name;
		foreach(TextAsset loc in Localization.instance.languages)
		{
			if (loc.name == Config.Language)
			{
				language = Config.Language;
				break;
			}
		}
		Config.Language = language;
		Localization.instance.currentLanguage = Config.Language;
	}
		
	[Obfuscar.Obfuscate]
	private void NNUKE_Initialize ()
	{
		#if DEBUG_TEXTS
		Debug.Log (string.Format ("SaveData.Initialize"));
		#endif
				
		// before even doing this, we want to make sure we have our game data ...
		GameData gameData = GameData.Instance;
			
		m_fullSavePath = System.IO.Path.Combine (Application.persistentDataPath, SAVE_GAME_FILENAME);
		
		// this loads save, and if not existing/okLoad save game with default values
		if (!NNUKE_Load ())
		{
			
			#if DEBUG_TEXTS
			Debug.Log(string.Format ("SaveData.Initialize() - Failed to load, trying to reset"));
			#endif
			
			NNUKE_Reset ();
		}
		
		NNUKE_ValidateConfig();
	}
		
	[Obfuscar.Obfuscate]
	private bool NNUKE_DoesSaveExist()
	{
#if UNITY_WEBPLAYER
#if DEBUG_TEXTS
		Debug.Log(string.Format ("SaveData.NNUKE_DoesSaveExist() with WEBPLAYER, we don't currently support saving!"));
#endif
		return false;
#else
		bool exists = System.IO.File.Exists (m_fullSavePath);
		
		#if DEBUG_TEXTS
		Debug.Log(string.Format ("SaveData.NNUKE_DoesSaveExist() {0} ? {1}", m_fullSavePath, exists));
		#endif
		
		return exists;
#endif
		
	}
		
	[Obfuscar.Obfuscate]
	private void NNUKE_CreateNewConfig()
	{
		NNUKE_CreateNewConfigByLoading();
		NNUKE_ValidateConfig();
	}
	
	[Obfuscar.Obfuscate]
	private void NNUKE_ValidateConfig()
	{
		for (int i = 0; i < m_initiallyOpenWorlds.Length; i++)
		{
			int initiallyOpenCount = m_initiallyOpenCount[i];
			if (Application.platform == RuntimePlatform.IPhonePlayer
				|| Application.platform == RuntimePlatform.OSXPlayer
				|| Application.platform == RuntimePlatform.OSXEditor
				)
			{
				initiallyOpenCount = 10;
			}
			
			NNUKE_NNUKE_AddWorldData(m_initiallyOpenWorlds[i]
				, m_initiallyOpenStartIndex[i]
				, initiallyOpenCount);
		}		
		
		if (OuyaInputWrapper.OuyaEnabled)
		{
			Config.OverscanCompensation = true;
		}
	}
	
	[Obfuscar.Obfuscate]
	private void NNUKE_CreateNewConfigByLoading()
	{
		TextAsset source = (UnlockAll) ? UnlockAllSaveGameXml : DefaultSaveGameXml;

		#if DEBUG_TEXTS
		Debug.Log(string.Format ("SaveData.NNUKE_CreateNewConfigByLoading() - {0}", source.name));
		#endif

		
#if UNITY_WEBPLAYER
		source = UnlockAllSaveGameXml;
#endif
		
		XmlReader reader = XmlTools.CreateReader(source, null, SaveGameBaseType);
		
		if (reader != null)
		{
			#if DEBUG_TEXTS
			Debug.Log(string.Format ("SaveData.NNUKE_CreateNewConfigByLoading() - Trying to Deserialize ... "));
			#endif
			
			m_config = (xsd_EDF_Config) XmlTools.Serializer.Deserialize (reader);
			
			#if DEBUG_TEXTS
			Debug.Log (string.Format ("SaveData.NNUKE_CreateNewConfigByLoading() - Deserialize finished, sm_config != null => {0}, ToString(): {1}"
				, m_config != null, (m_config != null) ? m_config.ToString() : "null"));
			#endif

			XmlTools.CleanUpSerializer();
		}
		else
		{
#if DEBUG_TEXTS
			Debug.LogError (string.Format ("Epic fail! Could not load default save data, so should bail out."));
#endif
			// epic fail, bail out
			Application.Quit();
		}
		
	}
	
	[Obfuscar.Obfuscate]
	private void NNUKE_CreateNewConfigWithCode()
	{
		m_config = new xsd_EDF_Config();
		
		// TODO: take the player name from somewhere
		// - other than that, we should suffice by setting defaul values to schema file -> figure this out if not so!
		
		// TODO: also available worlds should be read from somewhere (look InitializeMappings), and their open-status
		// according to IAP
		// -> these cannot be default values ... some kind of config-file, or server-check is required
		// -> defining here anyhow (at least the very initial values in stand-alone game are ok to be "fixed in code")
		
		/*
		m_config.Credits = 1000;
		m_config.SelectedWorld = "First 3";
		
		m_config.WeaponPrimary = new xsd_ct_Equipment[3];
		m_config.WeaponPrimary[0] = new xsd_ct_Equipment();
		m_config.WeaponPrimary[0].Id = "cannon";
		m_config.WeaponPrimary[0].Level = 0;
		m_config.WeaponPrimary[1] = new xsd_ct_Equipment();
		m_config.WeaponPrimary[1].Id = "laser";
		m_config.WeaponPrimary[1].Level = 0;
		m_config.WeaponPrimarySlot_1 = m_config.WeaponPrimary[0].Id;
		m_config.WeaponPrimarySlot_2 = m_config.WeaponPrimary[1].Id;
		m_config.WeaponPrimarySelected = m_config.WeaponPrimarySlot_1;

		m_config.WeaponSecondary = new xsd_ct_Equipment[2];
		m_config.WeaponSecondary[0] = new xsd_ct_Equipment();
		m_config.WeaponSecondary[0].Id = "turret";
		m_config.WeaponSecondary[0].Level = 0;
		m_config.WeaponSecondary[1] = new xsd_ct_Equipment();
		m_config.WeaponSecondary[1].Id = "turret_rear";
		m_config.WeaponSecondary[1].Level = -1;
		
		m_config.Item = new xsd_ct_Equipment[2];
		m_config.Item[0] = new xsd_ct_Equipment();
		m_config.Item[0].Id = "shockwave";
		m_config.Item[0].Level = 1;
		m_config.Item[1] = new xsd_ct_Equipment();
		m_config.Item[1].Id = "mine";
		m_config.Item[1].Level = -1;
		
		m_config.ItemSlot_1 = m_config.Item[0].Id;*/
	}

	[Obfuscar.Obfuscate]
	private string NNUKE_GetLevelId(int world, int level)
	{
		return string.Format ("{0}{1}_{2}", "level_", world, level);
	}
		
	[Obfuscar.Obfuscate]
	private bool NNUKE_LoadSaveGame (string filename)
	{
		XmlReader reader = XmlTools.InitializeSerializerForLoading(filename, false, DataXmlSchema, SaveGameBaseType);
		if (reader == null)
		{
			#if DEBUG_TEXTS
			Debug.LogError (string.Format ("SaveData.NNUKE_LoadSaveGame(filename={0})) - FAIL! could not create XmlReader", name));
			#endif
			
			return false;
		}
		m_config = (xsd_EDF_Config) XmlTools.Serializer.Deserialize (reader);
		XmlTools.CleanUpSerializer();
		return m_config != null;
	}
	
	[Obfuscar.Obfuscate]
	private void NNUKE_SaveGame (string filename)
	{
		bool ok = XmlTools.Serialize(filename, SaveGameBaseType, m_config);
		#if DEBUG_TEXTS
		Debug.Log (string.Format ("NNUKE_SaveGame to: \"{0}\" result ok: {1}", filename, ok));
		#endif
	}
	
	[Obfuscar.Obfuscate]
	private IEnumerator WaitForRequest (string resourcePath)
	{
		//Debug.Log (string.Format ("call"));
		WWW resource = new WWW (resourcePath);

		//Debug.Log (string.Format ("yield"));
		yield return resource;
		//Debug.Log (string.Format ("yield over"));

		// check for errors
		if (resource.error == null) 
		{
			//Debug.Log ("WWW Ok!: " + resource.text);
		} 
		else 
		{
			//Debug.Log ("WWW Error: " + resource.error);
		}
		m_texture = resource.texture;
	}
#endregion

}
