//#define DEBUG_TEXTS
//#define DEBUG_DRAW
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using System.Collections;
using System.Collections.Generic;

sealed public class AttackWavePathScript : MonoBehaviour 
{
	#region Enums, structs, and inner classes
	private class PathSegment
	{
		public float length;
		public float distanceToBegin;
		public float distanceToEnd;
		public Vector3 direction;
		public MovementPathPointProxyScript start;
		public MovementPathPointProxyScript end;
	}
	#endregion

	#region Delegates And Events
	#endregion

	#region Public members, visible in Unity GUI
	public bool Loop = false;
	public bool Circular = false;
	public bool EnemiesFacePlayer = false;
	
	public float m_allowedSpeedProportion = 1f;
	
	public MovementPathPointProxyScript DynamicProxyPrefab;
	
	public MovementPathPointProxyScript[] PathPoints;

	#endregion

	#region Public members, not visible to Unity GUI
	public Vector3 StartingPoint
	{
		get { return m_startPoint; }
	}
	#endregion

	#region Private data members
	private List<MovementPathPointProxyScript> m_targetProxies = new List<MovementPathPointProxyScript>();
	private MovementPathPointProxyScript m_currentProxy;
	private List<PathSegment> m_pathSegments = new List<PathSegment>();
	
	private int m_currentTargetProxyIndex;
	
	private float m_pathLength;
	private Vector3 m_startPoint;
	private Vector3 m_endPoint;
	
	private int m_previousClosestPointIndex = -1;
	
	#endregion


	#region Unity MonoBehaviour base overrides
	void Awake()
	{
		if (PathPoints.Length > 0)
		{
			foreach (MovementPathPointProxyScript proxy in PathPoints)
			{
				m_targetProxies.Add(proxy);
				// add the wave path offset to the point
				proxy.PathOffset = transform.position;
			}
			NNUKE_InitPath ();
		}
	}
	
	[Obfuscar.Obfuscate]
	private void NNUKE_InitPath()
	{
		m_pathLength = 0f;
		// make a helper structure for calculating stuff faster at run-time
		for (int i = 1; i < m_targetProxies.Count; i++)
		{
			PathSegment segment = new PathSegment();
			segment.start = m_targetProxies[i-1];
			segment.end = m_targetProxies[i];
			segment.direction = (segment.end.Target - segment.start.Target);
			segment.length = segment.direction.magnitude;
			segment.direction.Normalize();
			segment.distanceToBegin = m_pathLength;
			segment.distanceToEnd = segment.distanceToBegin + segment.length;
			
			m_pathSegments.Add(segment);
			
			m_pathLength += segment.length;
		}
		m_startPoint = m_targetProxies[0].Target;
		m_endPoint = m_targetProxies[m_targetProxies.Count - 1].Target;
		
		m_currentTargetProxyIndex = 0;
		m_previousClosestPointIndex = -1;
	}
	
	// Use this for initialization
	void Start ()
	{
		// determine starting point, if no predefined path and we have a dynamic proxy
		if (m_targetProxies.Count == 0)
		{
			if (DynamicProxyPrefab != null)
			{
				// no path, so generating straight line from random (from player), with player as target
				Vector3 target = PlayerHomePlanetScript.Instance.transform.position;
	
				float angleRad = Mathf.PI * Random.Range(0.0f, 2.0f);
				float radius = GamePlayCamera.Instance.SafeSpawnDistance;
				Vector3 generatedFirstProxyPosition = new Vector3(
					target.x + radius * Mathf.Cos(angleRad)
					, target.y + radius * Mathf.Sin(angleRad)
					, target.z);
				
				MovementPathPointProxyScript firstProxy =
					(MovementPathPointProxyScript) Instantiate(DynamicProxyPrefab
						, generatedFirstProxyPosition, Quaternion.identity);
				firstProxy.PathPointOrder = 0;
				
				MovementPathPointProxyScript secondProxy =
					(MovementPathPointProxyScript) Instantiate(DynamicProxyPrefab
						, target, Quaternion.identity);
				secondProxy.PathPointOrder = 1;
	
				m_targetProxies.Add(firstProxy);
				m_targetProxies.Add(secondProxy);
				
				NNUKE_InitPath();
			}
			else
			{
				Debug.LogError("No path defined for AttackWavePath and no Dynamic proxy to randomly generate straight line!");
			}
		}
	}
	
#if UNITY_EDITOR	
	void OnDrawGizmos()
	{
		if (!gameObject.active || PathPoints == null)
			return;
		
#if DEBUG_DRAW
		for (int i = 0; i < PathPoints.Length; i++)
		{
			Utility.DrawCircleXY(PathPoints[i].Target
				, 5f, 4, (PathPoints[i].enabled) ? Color.green : Color.gray);
		}
		Color lineColor = (Selection.activeGameObject == gameObject) ? Color.blue : Color.gray;
		
		for (int i = 1; i < PathPoints.Length; i++)
		{
			Debug.DrawLine(PathPoints[i-1].Target
				, PathPoints[i].Target
				, lineColor);
		}
		if (Circular && PathPoints.Length > 1)
		{
			Debug.DrawLine(PathPoints[PathPoints.Length-1].Target
				, PathPoints[0].Target
				, Color.white);
		}
#endif
	}
#endif
	
	#endregion
	
	#region Public object methods
	[Obfuscar.Obfuscate]
	public Vector3 NNUKE_GetCurrentTargetSmooth(bool dontSkipPoints, float lookAheadTime
		, Vector3 currentPosition, Vector3 currentVelocity
		, int fwdOrBwd, float lookDistance, out bool endOfPath)
	{
		if (m_targetProxies.Count == 1)
		{
			endOfPath = false;
			return m_targetProxies[0].Target;
		}
		
		// current projection
		int firstIndex, secondIndex;
		Vector3 currentProjection = NNUKE_GetProjectionOnPath(dontSkipPoints, currentPosition, fwdOrBwd
			, out firstIndex, out secondIndex);
		// ... convert that to path parameter
		float currentPathParameter = NNUKE_GetPathParameter(currentProjection, firstIndex, secondIndex);
		
		// here we will think the enemy will be, given the velocity and guess time
		Vector3 assumedPosition = currentPosition + lookAheadTime * currentVelocity;
		// get the position where the projected point is on our path
		
		Vector3 assumedProjection = NNUKE_GetProjectionOnPath(dontSkipPoints, assumedPosition, fwdOrBwd
			, out firstIndex, out secondIndex);
		// ... convert that to path parameter
		float assumedPathParameter = NNUKE_GetPathParameter(assumedProjection, firstIndex, secondIndex);
		
		// ... and then let's have a new path parameter that is in wanted direction
		float newPathParameter = assumedPathParameter + lookDistance * fwdOrBwd;

		// NOTE! We need to check if we go so fast that while we would want to go to other direction in the path,
		// because of the speed the lookDistance is to small to "switch the direction"
		if ((newPathParameter > currentPathParameter && fwdOrBwd < 0)
			|| (newPathParameter < currentPathParameter && fwdOrBwd > 0))
		{
			// just use the current as reference ...
			newPathParameter = currentPathParameter + lookDistance * fwdOrBwd;
		}
		
		int assumedSeg = NNUKE_IsOutsidePath(assumedPathParameter);
		endOfPath = ( assumedSeg < 0 && fwdOrBwd < 0) || (assumedSeg > 0 && fwdOrBwd > 0);

		newPathParameter = Mathf.Min (newPathParameter, m_pathLength);
		newPathParameter = Mathf.Max (newPathParameter, 0f);
		
		// convert the wanted path parameter to actual 3d coordinate point
		Vector3 newTarget = NNUKE_GetPositionFromPath(newPathParameter);
		
		/*
		Debug.Log (
			string.Format (
			"currentPos: {0}, curVel: {10}, assumedPos: {1}, fwdOrBwd: {2}, first: {3}, second: {4}, curPath: {5}, assumedPath: {6}, newPath: {7}, newPos: {8}, endOf: {9}"
			, currentPosition, assumedPosition, fwdOrBwd, firstIndex, secondIndex
			, currentPathParameter, assumedPathParameter, newPathParameter, newTarget, endOfPath
			, currentVelocity));
		*/
		return newTarget;
	}

	[Obfuscar.Obfuscate]
	public Vector3 NNUKE_GetCurrentTargetHard(Vector3 currentPosition, Vector3 currentVelocity)
	{
		const float closeEnoughLimit = 5f;
		const float closeEnoughLimitSquared = closeEnoughLimit * closeEnoughLimit;
		MovementPathPointProxyScript currentProxy = m_targetProxies[m_currentTargetProxyIndex];
		
		if ((currentProxy.transform.position - currentPosition).sqrMagnitude < closeEnoughLimitSquared)
		{
			m_currentTargetProxyIndex++;
		}
		m_currentTargetProxyIndex = Mathf.Min (m_currentTargetProxyIndex, m_targetProxies.Count-1);
		
		return m_targetProxies[m_currentTargetProxyIndex].Target;
	}
	
	#endregion
		
	#region Private object methods
	[Obfuscar.Obfuscate]
	private int NNUKE_IsOutsidePath(float pathParameter)
	{
		const float OFFSET_MARGIN = 0.01f;
		
		// returns 
		// 0 if on path
		// -1 if outside the beginning of path
		// +1 if past the path
		
		return (pathParameter < -OFFSET_MARGIN) 
				? -1 : ((pathParameter > m_pathLength + OFFSET_MARGIN)
					? 1 : 0);
	}
		
	
	[Obfuscar.Obfuscate]
	private float NNUKE_GetPathParameter(Vector3 point, int firstIndex, int secondIndex)
	{
		float nowOnSegment = MathHelpers.GetProportion(point
			, m_targetProxies[firstIndex].Target
			, m_targetProxies[secondIndex].Target);
		
		bool closerToStart;
		bool inside = MathHelpers.IsProjectionOnLine(point
			, m_targetProxies[firstIndex].Target
			, m_targetProxies[secondIndex].Target
			, out closerToStart);
		
		float factor = (!inside && closerToStart) ? -1f : 1f;
		
		return m_pathSegments[firstIndex].distanceToBegin 
			+ factor * nowOnSegment * m_pathSegments[firstIndex].length;
	}
	
	
	[Obfuscar.Obfuscate]
	private Vector3 NNUKE_GetProjectionOnPath(bool dontSkipPoints, Vector3 position, int fwdOrBwd
		, out int firstIndex, out int secondIndex)
	{
		firstIndex = NNUKE_GetClosestPathControlPoint(position);
		
		#if DEBUG_TEXTS
		Debug.Log (string.Format ("NNUKE_GetProjectionOnPath - previousClosest: {0}, closest: {1}"
			, m_previousClosestPointIndex, firstIndex));
		#endif
		
		secondIndex = NNUKE_GetSecondClosestPathControlPoint(position, firstIndex);
		// need to switch?
		if (firstIndex > secondIndex)
		{
			Utility.Swap(ref firstIndex, ref secondIndex);
		}
		// are we trying to skip a point? don't allow that 
		if (dontSkipPoints && m_previousClosestPointIndex != -1 
			&& (firstIndex > m_previousClosestPointIndex + 1 || firstIndex < m_previousClosestPointIndex))
		{
			firstIndex = m_previousClosestPointIndex;
			secondIndex = NNUKE_GetSecondClosestPathControlPoint(position, firstIndex);
			if (firstIndex > secondIndex)
			{
				Utility.Swap(ref firstIndex, ref secondIndex);
			}
		}
		
		m_previousClosestPointIndex = firstIndex;
		
		Vector3 direction = m_targetProxies[secondIndex].Target -
			m_targetProxies[firstIndex].Target;
		direction.Normalize();
		
		Vector3 projectedPoint = MathHelpers.ProjectPointToLine(position
			, m_targetProxies[firstIndex].Target
			, direction);
		#if DEBUG_TEXTS
		Debug.Log (string.Format ("assumedPosition: {0}, projectedPoint: {1}"
			, assumedPosition, projectedPoint));
		#endif
		
		// projection not on actual line?
		bool closerToStart;
		if (!MathHelpers.IsProjectionOnLine(projectedPoint
			, m_targetProxies[firstIndex].Target
			, m_targetProxies[secondIndex].Target
			, out closerToStart))
		{
			bool newCalculation = false;
			if (fwdOrBwd > 0 &&	secondIndex < m_targetProxies.Count - 1)
			{
				newCalculation = true;
				firstIndex++;
				secondIndex++;
			}
			else if (fwdOrBwd < 0 && firstIndex > 0)
			{
				newCalculation = true;
				firstIndex--;
				secondIndex--;
			}
			if (newCalculation)
			{
				// take the next ones ...
				direction = m_targetProxies[secondIndex].Target - m_targetProxies[firstIndex].Target;
				direction.Normalize();
				// ... and project to those
				projectedPoint = MathHelpers.ProjectPointToLine(position
					, m_targetProxies[firstIndex].Target
					, direction);
			}
		}
		return projectedPoint;
	}
	
	[Obfuscar.Obfuscate]
	private Vector3 NNUKE_GetPositionFromPath(float pathParameter)
	{
		int v = NNUKE_IsOutsidePath(pathParameter);
		
		if (v < 0) return m_startPoint;
		if (v > 1) return m_endPoint;
		
		for (int i = 0; i < m_pathSegments.Count; i++)
		{
			if (m_pathSegments[i].distanceToEnd >= pathParameter)
				return m_pathSegments[i].start.Target 
					+ (pathParameter - m_pathSegments[i].distanceToBegin) * m_pathSegments[i].direction;
		}
		
		Debug.LogError(
			string.Format ("AttackWavePathScript.NNUKE_GetPositionFromPath() - no point found for parameter: {0} - should not happen!"
			, pathParameter));

		return Vector3.zero;
	}	
	
	[Obfuscar.Obfuscate]
	private int NNUKE_GetClosestPathControlPoint(Vector3 position)
	{
		int closestIndex = 0;
		float closestSquaredDistance = (m_targetProxies[0].Target - position).sqrMagnitude;
		for (int i = 1; i < m_targetProxies.Count; i++)
		{
			MovementPathPointProxyScript point = m_targetProxies[i];
			float squaredDistance = (point.Target - position).sqrMagnitude;
			if (squaredDistance < closestSquaredDistance)
			{
				closestIndex = i;
				closestSquaredDistance = squaredDistance;
			}
		}
		return closestIndex;
	}
	[Obfuscar.Obfuscate]
	private int NNUKE_GetSecondClosestPathControlPoint(Vector3 position, int closestIndex)
	{
		if (m_targetProxies.Count == 1)
			return 0;
		if (closestIndex == 0)
			return 1;
		if (closestIndex == m_targetProxies.Count - 1)
			return m_targetProxies.Count - 2;
		
		float prevSquaredDistance = (m_targetProxies[closestIndex - 1].Target - position).sqrMagnitude;
		float nextSquaredDistance = (m_targetProxies[closestIndex + 1].Target - position).sqrMagnitude;
		
		return (prevSquaredDistance < nextSquaredDistance) ? closestIndex - 1 : closestIndex + 1;
	}
	
	[Obfuscar.Obfuscate]
	private static int NNUKE_CompareProxies(MovementPathPointProxyScript x, MovementPathPointProxyScript y)
	{
		if (x == null)
		{
			if (y == null)
			{
				// If x is null and y is null, they're equal.
				return 0;
			}
			else
			{
				// If x is null and y is not null, y is greater.
				return -1;
			}
		}
		else
		{
			// If x is not null... 
			if (y == null)
			// ...and y is null, x is greater.
			{
				return 1;
			}
			else
			{
				// ...and y is not null, THEN the actual comparison
				return (x.PathPointOrder > y.PathPointOrder) ? 1 : 
					(x.PathPointOrder < y.PathPointOrder) ? -1 : 0;
			}
		}
	}
	
	#endregion
}
