using UnityEngine;
using System.Collections;
using System.Collections.Generic;

sealed public class AttackWaveScript : MonoBehaviour 
{
	#region Public GUI tweakable members
	public EnemyBaseScript[] EnemyPrefabs;
	public int[] EnemyCounts;

	public float LeadInTimeSeconds;
	public bool IncludedInLevel;
	public bool DontStartBeforePreviousFinished;
	public int Order;
	
	public int m_pathToJumpAfterLast = -1;
	
	public AttackWavePathScript[] PathPrefabs;
	
	public bool m_constant = false;
	
	#endregion
	
	#region Delegates And Events
	public delegate void AttackWaveLaunched(AttackWaveScript sender);
	public event AttackWaveLaunched Launched;
	public delegate void AttackWaveEnded(AttackWaveScript sender);
	public event AttackWaveEnded Ended;
	#endregion
	
	#region Public members, not visible in Unity GUI
	[HideInInspector]
	public float ActiveInSeconds
	{
		get { return m_activeInSeconds; }
	}
	[HideInInspector]
	public bool Active;
	[HideInInspector]
	public float ClosestEnemyFactor
	{
		get { return m_closestEnemyFactor; }
	}
	#endregion
	
	#region Private data members
	private List<DamageTakerScript> m_enemies = new List<DamageTakerScript>();
	private float m_activeInSeconds;
	
	private List<AttackWavePathScript> m_paths = new List<AttackWavePathScript>();
	private int m_currentPathIndex;
	private EnemyFormationBaseScript m_formation;

	/// <summary>
	/// Our bonus spawner in level, if available.
	/// </summary>
	private BonusSpawnerScript m_bonusSpawner;
	
	private float m_closestEnemyFactor;
	#endregion
		
	#region Unity MonoBehaviour base overrides
		
	// Use this for initialization
	void Start ()
	{
		// this needs to be set as 1, so that before spawn we do not get alarm ...
		m_closestEnemyFactor = 1f;
		
		m_bonusSpawner = (BonusSpawnerScript) FindObjectOfType(typeof(BonusSpawnerScript));
		
		foreach (AttackWavePathScript prefab in PathPrefabs)
		{
			AttackWavePathScript aws = (AttackWavePathScript) Instantiate(prefab);
			m_paths.Add (aws);
		}
		
		m_formation = GetComponent<EnemyFormationBaseScript>();

		int count = 0;
		for (int index = 0; index < EnemyPrefabs.Length; index++)
		{
			count += EnemyCounts[index];
		}
		m_formation.MemberCount = count;
		
		// assign first one 
		m_currentPathIndex = 0;
		m_formation.WavePath = (m_currentPathIndex < m_paths.Count) ? m_paths[m_currentPathIndex] : null;
	}
	void Update ()
	{
		if (!Active)
			return;
		
		if (m_activeInSeconds >= 0)
		{
			m_activeInSeconds -= GameStateScript.Instance.NNUKE_GetWorldDeltaTime();
			if (m_activeInSeconds < 0)
			{
				BeginWave();
			}
		}		
		else if (m_enemies.Count == 0)
		{
			EndWave ();
		}
		else
		{
			// check closest to player
			float factor = 1f;
			foreach (DamageTakerScript enemy in m_enemies)
			{
				if (enemy != null && enemy.RadarDot != null)
					factor = Mathf.Min (enemy.RadarDot.FactorFromPlayer, factor);
			}
			m_closestEnemyFactor = factor;
		}
	}

	#endregion
		
	#region Public object methods
	[Obfuscar.Obfuscate]
	public void Process ()
	{
		if (!Active || m_enemies.Count == 0 || !m_formation.Finalized)
			return;
		
		m_formation.NNUKE_UpdateTarget();
	}
	
	[Obfuscar.Obfuscate]
	public void DoStartCountDown()
	{
		if (m_formation != null)
			m_formation.Finalized = false;
		
		Active = true;
		m_activeInSeconds = LeadInTimeSeconds;
	}
	
	public void DamageTakerDied(DamageTakerScript enemy)
	{
		m_enemies.Remove(enemy);
	}
	
	[Obfuscar.Obfuscate]
	public AttackWavePathScript GetNextPath()
	{
		m_currentPathIndex = m_currentPathIndex + 1;
		if (m_currentPathIndex >= m_paths.Count)
		{
			m_currentPathIndex = (m_pathToJumpAfterLast > -1)
				? m_pathToJumpAfterLast
				: m_paths.Count - 1;
		}
		return m_paths[m_currentPathIndex];
	}
	
	#endregion
		
	#region Private object methods
	[Obfuscar.Obfuscate]
	private void BeginWave()
	{
		m_enemies.Clear();
		m_closestEnemyFactor = 1f;
		m_currentPathIndex = 0;
		m_formation.WavePath = (m_currentPathIndex < m_paths.Count) ? m_paths[m_currentPathIndex] : null;
		
		if (LevelScript.Instance.Outcome != LevelScript.Result.Ongoing)
		{
			Active = false;
			return;
		}
		
		if (m_currentPathIndex >= m_paths.Count)
			return;
		
		m_formation.transform.position = m_paths[m_currentPathIndex].StartingPoint;
		for (int index = 0; index < EnemyPrefabs.Length; index++)
		{
			EnemyBaseScript prefab = EnemyPrefabs[index];
			for (int i = 0; i < EnemyCounts[index]; i++)
			{
				DoSpawnEnemy(prefab);
			}
		}
		m_formation.NNUKE_DoFinalize();
		
		if (Launched != null)
			Launched(this);
		
	}
	[Obfuscar.Obfuscate]
	private void EndWave()
	{
		Active = false;
		if (Ended != null)
			Ended(this);
	}
	
	[Obfuscar.Obfuscate]
	private EnemyBaseScript DoSpawnEnemy(EnemyBaseScript enemyPrefab)
	{
		// Create thea actual enemy		
		EnemyBaseScript enemyClone = (EnemyBaseScript) Instantiate(
			enemyPrefab
			, Vector3.zero
			, Quaternion.identity);
		
		// these entities want to know about the enemy activities ...
		// NOTE! DamageTakerScript itself assigns listeners to PlayerHomePlanet,
		// and Enemy's Awake assigns listeners to Level
		DamageTakerScript damageTaker = enemyClone.GetComponentInChildren<DamageTakerScript>();
		damageTaker.Died += new DamageTakerScript.DamageTakerDied(DamageTakerDied);
		damageTaker.CountAsPartOfAttackWave = true;
		
		// NOTE! We want to give some variation to the weight of the enemy, so that
		// if they're stupid enough to start pushing each other, they don't dead lock so easily
		if (enemyClone.gameObject.GetComponent<Rigidbody>())
		{
			float originalMass = enemyClone.gameObject.GetComponent<Rigidbody>().mass;
			float newMass = originalMass * (1f + 0.2f * MathHelpers.Binominial());
			enemyClone.gameObject.GetComponent<Rigidbody>().mass = newMass;
		}
		
		if (m_bonusSpawner)
			damageTaker.Died += new DamageTakerScript.DamageTakerDied(m_bonusSpawner.DamageTakerDied);

		m_formation.NNUKE_DoFormationEntry(enemyClone, m_paths[m_currentPathIndex].StartingPoint);
		m_enemies.Add(damageTaker);
		
		return enemyClone;
	}	
#endregion
}
