//#define DEBUG_DRAW

using UnityEngine;
using System.Collections;

sealed public class EnemyBaseScript : MonoBehaviour {
	
	#region Public members, visible in Unity GUI
	[HideInInspector]
	public Vector3 MovementTarget
	{
		set 
		{
			m_movementTarget = value;
			m_distanceSquaredToTarget = (transform.position - m_movementTarget).sqrMagnitude;
		}
		get
		{
			return m_movementTarget;
		}
	}
	[HideInInspector]
	public DamageTakerScript DamageTaker
	{
		get { return m_damageTaker; }
	}
	[HideInInspector]
	public float DistanceToTargetSquared
	{
		get { return m_distanceSquaredToTarget; }
	}
	[HideInInspector]
	public Vector3 FacingTarget;
	[HideInInspector]
	public float AllowedSpeedProportion = 1f;
	#endregion
	
	#region Private data members
	private Vector3 m_movementTarget;
	private float m_distanceSquaredToTarget;
	private float m_originalSpeed;
	
	private DamageTakerScript m_damageTaker;

	#endregion 
	
	#region Delegates And Events

	#endregion
	
	#region Unity MonoBehaviour base overrides
	
	public void Awake()
	{
		// level wants to know about us living and dying ...
		m_damageTaker = GetComponentInChildren<DamageTakerScript>();
		LevelScript level = (LevelScript) GameObject.FindObjectOfType(typeof(LevelScript));
		if (level)
		{
			m_damageTaker.Spawned += new DamageTakerScript.DamageTakerSpawned(level.DamageTakerSpawned);
			m_damageTaker.Died += new DamageTakerScript.DamageTakerDied(level.DamageTakerDied);
		}
	}
	
	// Use this for initialization
	public void Start () 
	{
		if (PlayerHomePlanetScript.Instance != null)
			MovementTarget = PlayerHomePlanetScript.Instance.transform.position;
	}
	
	public void FixedUpdate()
	{
		// HACK! We're dealing with velocities here which is not wise with physics engine,
		// should go with forces instead ...

		#if DEBUG_DRAW
		Utility.DrawCircleXY(MovementTarget, 12f, 10, Color.gray);
		#endif

		// go max to target, but clamp
		Vector3 targetLocation = MovementTarget;
		Vector3 direction = (targetLocation - transform.position);
						
		Vector3 wantedVelocity = Vector3.zero;

		direction.Normalize();
		wantedVelocity = new Vector3(m_damageTaker.Speed * direction.x, m_damageTaker.Speed * direction.y, 0f);
		wantedVelocity *= AllowedSpeedProportion;
		
		Rigidbody rb = gameObject.GetComponent<Rigidbody>();
		float dt = GameStateScript.Instance.NNUKE_GetWorldDeltaTime();
		
		rb.velocity = Vector3.MoveTowards(rb.velocity, wantedVelocity, m_damageTaker.Acceleration * dt);
		
		// make sure we stay on z = 0
		Vector3 pos = gameObject.transform.position;
		pos.z = 0f;
		gameObject.transform.position = pos;
		
		// update orientation
		
		Vector3 facingDir = (FacingTarget - transform.position);
		facingDir.Normalize();
		Quaternion wantedOrientation = Quaternion.LookRotation(facingDir, Vector3.forward);
		wantedOrientation.x = 0f;
		wantedOrientation.y = 0f;
		
		// HACK! we should set forces here, or at least velocities, not directly zeroing
		// the physics engine nad setting the dir ourselves ... collisions with enemies look funny now ...
		rb.angularVelocity = Vector3.zero;
		
		gameObject.transform.rotation = Quaternion.RotateTowards(
			gameObject.transform.rotation
			, wantedOrientation
			, m_damageTaker.TurningSpeedAnglesPerSecond * dt);
	}
	#endregion
	
}
