//#define DEBUG_TEXTS
//#define DEBUG_DRAW

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

sealed public class EnemyFormationBaseScript : MonoBehaviour 
{
	public enum FormationType
	{
		Column
		, Row
		, Fuzz
		, Cross
		, Circle
	};
	
	private class FormationEntry
	{
		public EnemyBaseScript enemy;
		public int memberIndex;
		public Vector3 initialOffset;
		public Vector3 currentOffset;
		public Vector3 initialDirection;
	}
	
	#region Public GUI tweakable members
	public FormationType Configuration;
	public float InitialDistanceFromEachOther;
	public float FinalDistanceFromEachOther;
	public bool CloseDistanceWhenApproaching;
	
	public bool RelativeToPathInsteadPlayer;
	public bool OverrideFacingToPath;
	public bool OverrideFacingToPlayer;
	
	public bool ChangeFacingAfterPath = false;
	
	public bool CloseInToCenter = false;
	
	public bool m_dontSkipPathPoints = true;
	#endregion
	
	#region Public members, not visible in Unity GUI
	[HideInInspector]
	public bool Finalized
	{
		get { return m_finalized; }
		set { m_finalized = value; }
	}
	
	[HideInInspector]
	public Vector3 AveragePosition
	{
		get { return m_averagePosition; }
	}
	[HideInInspector]
	public int MemberCount
	{
		set { m_memberCount = value; }
		get { return m_memberCount; }
	}
	[HideInInspector]
	public AttackWavePathScript WavePath
	{
		set { m_pathScript = value; }
		get { return m_pathScript; }
	}
	[HideInInspector]
	public float LiveDistanceFromEachOther
	{
		get 
		{ 			
			
			/*
			Debug.Log (
				string.Format (
				"LiveDistanceFromEachOther - formPos: {0}, m_initialFormationDistance: {1}, m_distanceDelta: {2}, LiveDistanceFactorFromInitial: {3}"
				, transform.position, m_initialFormationDistance, m_distanceDelta, LiveDistanceFactorFromInitial));
			*/
			
			return (CloseDistanceWhenApproaching) ? 
				FinalDistanceFromEachOther + 
					(LiveDistanceFactorFromInitial * m_distanceDelta)
				: InitialDistanceFromEachOther; 
		}
	}
	public float LiveDistanceFactorFromInitial
	{
		get 
		{ 			
			Vector3 reference = (PlayerHomePlanetScript.Instance != null) ?
				PlayerHomePlanetScript.Instance.transform.position : m_lastPlayerPosition;
			
			return ((reference - transform.position).magnitude) / m_initialFormationDistance; 
		}
	}
	#endregion
	
	#region Private data members
	private Dictionary<EnemyBaseScript, FormationEntry> m_formationEntries = 
		new Dictionary<EnemyBaseScript, FormationEntry>();
	private List<FormationEntry> m_formations = new List<FormationEntry>();
	private List<EnemyBaseScript> m_enemies = new List<EnemyBaseScript>();
	
	private float m_initialFormationDistance;
	private AttackWaveScript m_attackWaveScript;
	private AttackWavePathScript m_pathScript;
	private Vector3 m_averagePosition;
	private int m_memberCount;
	
	private Vector3 m_formationVelocity;
	
	private float m_distanceDelta;
	private Vector3 m_lastVelocityUpdatePosition;
	private float m_lastVelocityUpdateTimeSeconds;
	private float m_nextVelocityUpdateTimeSeconds;
	private static float sm_velocityUpdateIntervalSeconds = 0.25f;
	private static float sm_recalculateFormationPositionsDelta = 0.75f;
	
	// This far we check in the current path for the next target, when current position is first projected to the
	// path
	private float m_lookDistance = 10f;
	// Are we currently going path forward (1) or backwards (-1) ?
	private int m_forwardOrBackward = 1;
	// how much in future we predict formations movement when taking target on path?
	private float m_lookAheadTimeSeconds = 1f;
	
	private float m_lastRecalculateTimeSeconds = -1f;
	
	private Vector3 m_lastPlayerPosition = Vector3.zero;
	private Vector3 m_previousTarget = Vector3.zero;
	
	private bool m_finalized = false;
	#endregion
		
	#region Unity MonoBehaviour base overrides
	void Awake()
	{
		m_attackWaveScript = GetComponent<AttackWaveScript>();
		m_finalized = false;
	}
	
	// Use this for initialization
	void Start ()
	{
		m_distanceDelta = InitialDistanceFromEachOther - FinalDistanceFromEachOther;
	}
	#endregion
		
	#region Public object methods
	[Obfuscar.Obfuscate]	
	public void NNUKE_UpdateTarget ()
	{
		// We do not perform these calculates in Update, as we do not want to run this every frame
		
		if (!m_finalized)
			return;
		
		bool endOfPath = false;
		
		// take the ideal target for formation from the path
		
		Vector3 formationTarget = (m_pathScript == null)
			? m_previousTarget
			: m_pathScript.NNUKE_GetCurrentTargetSmooth(m_dontSkipPathPoints,
				m_lookAheadTimeSeconds, transform.position, m_formationVelocity
				, m_forwardOrBackward, m_lookDistance, out endOfPath);
		
		if (m_pathScript != null && endOfPath && m_pathScript.Circular)
		{
			NNUKE_ChangePathFacing();

			formationTarget = m_pathScript.NNUKE_GetCurrentTargetSmooth(false
				, m_lookAheadTimeSeconds, m_pathScript.StartingPoint, m_formationVelocity
				, m_forwardOrBackward, m_lookDistance, out endOfPath);
		}
		
		if (m_lastRecalculateTimeSeconds + sm_recalculateFormationPositionsDelta < GameStateScript.Instance.NNUKE_GetWorldTime()
			&& NNUKE_CountOfMembersThatAreFartherAwayFromTarget(5f) > 0)
		{
			NNUKE_RecalculateFormationPositions();
			m_lastRecalculateTimeSeconds = GameStateScript.Instance.NNUKE_GetWorldTime();
		}
		
		Vector3 playerPos = (PlayerHomePlanetScript.Instance != null) ?
			PlayerHomePlanetScript.Instance.transform.position : m_lastPlayerPosition;
		if (PlayerHomePlanetScript.Instance != null)
			m_lastPlayerPosition = playerPos;
		
		// First pass, before setting targets, calculate some info
		
		Vector3 sum = Vector3.zero;
		
		EnemyBaseScript closestToTarget = null;
		float closestDistSqr = float.MaxValue;
		
		float closestDstSqr = float.MaxValue;
		Vector3 closestDir = Vector3.zero;
		
		foreach (EnemyBaseScript enemy in m_enemies)
		{
			if (enemy == null)
				continue;
			
			Vector3 dif = formationTarget - enemy.transform.position;
			float distSqr = dif.sqrMagnitude;
			if (distSqr < closestDistSqr)
			{
				closestToTarget	= enemy;
				closestDistSqr = distSqr;
				closestDir = dif;
			}
			
			sum = sum + enemy.transform.position;
		
			#if DEBUG_DRAW
			Utility.DrawCircleXY(enemy.MovementTarget, 12f, 10, Color.gray);
			#endif
		}

		// modify target calculations based on previous info
		if (m_enemies.Count > 0)
		{
			m_averagePosition = (1f / m_enemies.Count) * sum;
		}
		
		transform.position = AveragePosition;
		
		const float targetDistanceLimit = 50f;
		
		// reset formation position close to the enemy that is closest to the target,
		// so that the formation does not get stuck ... happens if the formation is thought to be in the
		// actual average position of its members ..
		if (m_formations.Count > 1 && closestToTarget != null && closestDistSqr > targetDistanceLimit)
		{
			closestDir.Normalize();
			transform.position = closestToTarget.transform.position + targetDistanceLimit * closestDir;
		}
		
		// second pass, actually set targets to enemies
		Vector3 direction = (RelativeToPathInsteadPlayer) 
			? (formationTarget - transform.position)
				: (playerPos - transform.position);
		direction.Normalize();
		
		foreach (EnemyBaseScript enemy in m_enemies)
		{
			if (enemy == null)
				continue;

			enemy.AllowedSpeedProportion = m_pathScript.m_allowedSpeedProportion;
			
			// enemy's target position is the formations target plus the offset
			// that amounts from the enemy's position in the formation
			enemy.MovementTarget = formationTarget + NNUKE_GetLiveOffset(enemy, direction);
			
			if (OverrideFacingToPath) 
				enemy.FacingTarget = enemy.MovementTarget;
			else if (OverrideFacingToPlayer)
				enemy.FacingTarget = m_lastPlayerPosition;
			else
				enemy.FacingTarget = (m_pathScript.EnemiesFacePlayer)
					? m_lastPlayerPosition : enemy.MovementTarget;
			
			#if DEBUG_DRAW
			Utility.DrawCircleXY(enemy.MovementTarget, 3f, 10, Color.cyan);
			Debug.DrawLine(enemy.transform.position, enemy.MovementTarget, Color.cyan);
			#endif
		}
		
		// TODO: tämä ei toimi tarpeeksi hyvin, johtuen varmaan ylipäätään
		// formation targetin asettamisesta, joten toistaiseksi pois käytöstä
		/*
		if (m_enemies.Count > 1)
		{
			float deltaSqr = farthestDstSqr - closestDstSqr;
			deltaSqr = Mathf.Max (deltaSqr, 1f);
			// assign allowed speeds so that formation somewhat sticks together
			foreach (EnemyBaseScript enemy in m_enemies)
			{
				enemy.AllowedSpeedProportion = 0.5f + 
					0.5f * (enemy.DistanceToTargetSquared - closestDstSqr) / deltaSqr;
			}
		}*/
		
		// post processing, record some info for next iterations
		
		// we update here the velocity of the formation only every now and then, as doing it in
		// successive frames would fluctuate too much
		float now = GameStateScript.Instance.NNUKE_GetWorldTime();
		if (m_nextVelocityUpdateTimeSeconds < now)
		{
			float delta = now - m_lastVelocityUpdateTimeSeconds;
			m_formationVelocity =  (1f/delta)*(transform.position - m_lastVelocityUpdatePosition);
				
			m_lastVelocityUpdatePosition = transform.position;
			m_lastVelocityUpdateTimeSeconds = now;
			m_nextVelocityUpdateTimeSeconds = now + sm_velocityUpdateIntervalSeconds;
		}
		
		if (endOfPath && !m_pathScript.Circular)
		{
			NNUKE_ChangePathFacing();
			
			 if (m_pathScript.Loop)
				m_forwardOrBackward *= -1;
			 else 
			{
				// this might return the same path, in which case 
				// the enemies "are free to do whatever they want"
				m_pathScript = m_attackWaveScript.GetNextPath();
			}
		}
		m_previousTarget = formationTarget;

		#if DEBUG_DRAW
		Utility.DrawCircleXY(formationTarget, 1.5f, 10, Color.red);
		Utility.DrawCircleXY(transform.position, 5f, 10, Color.blue);
		Utility.DrawCircleXY(AveragePosition, 7.5f, 10, Color.white);
		Debug.DrawLine(transform.position, transform.position + 20f * direction, Color.green);
		#endif
	}

	[Obfuscar.Obfuscate]
	public void NNUKE_DoFinalize()
	{
		m_memberCount = m_enemies.Count;
		
		foreach (EnemyBaseScript enemy in m_enemies)
		{
			FormationEntry entry = m_formationEntries[enemy];
			Vector3 offset = NNUKE_GetInitialFormationLivePosition(enemy, entry.memberIndex, entry.initialDirection);
		
			entry.initialOffset = offset;
			entry.currentOffset = offset;
			
			// TODO: jälkimmäinen haara ei ole järkevä!
			if (m_pathScript != null)
				enemy.transform.position = m_pathScript.StartingPoint + offset;
			else
				enemy.transform.position = offset;
		}
		
		NNUKE_UpdateTarget();
		
		m_lastVelocityUpdatePosition = transform.position;
		m_lastVelocityUpdateTimeSeconds = GameStateScript.Instance.NNUKE_GetWorldTime();
		m_nextVelocityUpdateTimeSeconds = m_lastVelocityUpdateTimeSeconds;
		m_initialFormationDistance = 
			(PlayerHomePlanetScript.Instance.transform.position - transform.position).magnitude;
		
		if (Configuration == FormationType.Circle && CloseInToCenter)
		{
			m_initialFormationDistance = InitialDistanceFromEachOther;
		}
		m_finalized = true;
	}
	
	[Obfuscar.Obfuscate]
	public Vector3 NNUKE_GetInitialOffset(EnemyBaseScript enemy)
	{
		return m_formationEntries[enemy].initialOffset;
	}
	
	[Obfuscar.Obfuscate]
	public int NNUKE_CountOfMembersThatAreFartherAwayFromTarget(float limit)
	{
		int result = 0;
		float limitSquared = Mathf.Pow(limit, 2);
		
		foreach (KeyValuePair<EnemyBaseScript, FormationEntry> kv in m_formationEntries)
		{
			if (kv.Key == null || kv.Value == null)
				continue;
			
			FormationEntry fe = kv.Value;
			Vector3 formationEntryPosition = transform.position + fe.currentOffset;
			if ((kv.Key.transform.position - formationEntryPosition).sqrMagnitude > limitSquared)
			{
				result++;
			}
		}
		return result;
	}
	
	/// <summary>
	/// Dynamically tries to re-assign all the positions from formation to enemies,
	/// so that the enemies get their currently closest spot.
	/// </summary>
	[Obfuscar.Obfuscate]
	public void NNUKE_RecalculateFormationPositions()
	{
		// clear old helper
		m_formationEntries.Clear ();
		
		// this holds our enemies that have been already assigned by this routine
		List<EnemyBaseScript> assignedEnemies = new List<EnemyBaseScript>();
		
		// this keeps formation "tight" -> fills places from ground up
		int biggestAcceptedFormationIndex = m_enemies.Count - 1;
		
		// we calculate things through the formation slots, so we go through them,
		// and try to assign enemies closest to occupy them
		for (int i = 0; i < m_formations.Count; i++)
		{
			if (m_formations[i].memberIndex > biggestAcceptedFormationIndex)
				continue;
			
			// take this formation slots actual position
			FormationEntry fe = m_formations[i];
			
			Vector3 formationEntryPosition = transform.position + fe.currentOffset;

			// then loop through all (available, that is, not assigned) enemies
			// and assign the one that is closest to this slot
			float closestSquared = float.MaxValue;
			EnemyBaseScript assigned = null;
			foreach (EnemyBaseScript enemy in m_enemies)
			{
				if (!assignedEnemies.Contains(enemy))
				{
					float distanceSquared = (enemy.transform.position - formationEntryPosition).sqrMagnitude;
					if (distanceSquared < closestSquared)
					{
						closestSquared = distanceSquared;
						assigned = enemy;
					}
				}
			}
			// assigned can be null, then this slot does not have enemy ...
			fe.enemy = assigned;
			if (assigned != null)
			{
				// remember what we did
				assignedEnemies.Add (assigned);
				m_formationEntries[fe.enemy] = fe;
			}
		}
	}
	
	[Obfuscar.Obfuscate]
	public Vector3 NNUKE_GetLiveOffset(EnemyBaseScript enemy, Vector3 direction)
	{
		if (!m_formationEntries.ContainsKey(enemy))
		{
			// this might happen if enemy just got killed, but not yet removed from our info ...
			return Vector3.zero;
		}
		
		m_formationEntries[enemy].currentOffset = NNUKE_GetRelativeFormationLivePosition(
			LiveDistanceFromEachOther
			, enemy
			, m_formationEntries[enemy].memberIndex
			, direction);
		
		return m_formationEntries[enemy].currentOffset;
	}
	
	[Obfuscar.Obfuscate]
	public void DamageTakerDied(DamageTakerScript damageTaker)
	{
		EnemyBaseScript enemy = damageTaker.transform.parent.GetComponent<EnemyBaseScript>();
		if (m_formationEntries.ContainsKey(enemy))
		{
			FormationEntry entry = m_formationEntries[enemy];
			m_formations.Remove(entry);
			m_formationEntries.Remove(enemy);
		}
		m_enemies.Remove (enemy);
	}

	[Obfuscar.Obfuscate]
	private void NNUKE_ChangePathFacing()
	{
		if (ChangeFacingAfterPath)
		{
			if (OverrideFacingToPath)
			{
				OverrideFacingToPath = false;
				OverrideFacingToPlayer = true;
			}
			else if (OverrideFacingToPlayer)
			{
				OverrideFacingToPlayer = false;
				OverrideFacingToPath = true;
			}
		}
	}
	
	[Obfuscar.Obfuscate]
	private Vector3 NNUKE_GetRelativeFormationLivePosition(float distanceFromEach, EnemyBaseScript enemy, int member, Vector3 direction)
	{
		Vector3 offset = Vector3.zero;
		
		if (member == 0 && Configuration != FormationType.Circle)
			return offset;
		
		Vector3 up = distanceFromEach * direction;
		Vector3 down = -1f*up;
		
		Vector3 left = Vector3.Cross(Vector3.back, direction);
		left.Normalize();
		left = distanceFromEach * left;
		Vector3 right = -1f*left;
		
		int sidePosition = 1 + ((member-1) / 2);
		
		switch (Configuration)
		{
			case FormationType.Column:
			{
				// NOTE! this if we want the first one in the center
				//offset = sidePosition * ((member % 2 == 0) ? up : down);
				// this, if first to lead
				offset = member * down;
				break;
			}
			case FormationType.Row:
			{
				offset = sidePosition * ((member % 2 == 0) ? left : right);
				break;
			}
			case FormationType.Fuzz:
			{
				offset = LiveDistanceFactorFromInitial * m_formationEntries[enemy].initialOffset;
				break;
			}
			case FormationType.Circle:
			{	
			 	Vector3 axis = Vector3.Cross(left, up);
				offset = Quaternion.AngleAxis(360f * member / m_memberCount, axis) * left;
			
				/*
				Debug.Log (
					string.Format (
					"NNUKE_GetRelativeFormationLivePosition - distanceFromEach: {0}, direction: {1}, left: {2}, offset: {3} (time: {4})"
					, distanceFromEach, direction, left, offset, Time.time));
			 	*/
				break;
			}
			case FormationType.Cross:
			{	
				int magnitude = 1 + Mathf.FloorToInt (member / 4);
				switch (member % 4)
				{
					case 1:
					{
						offset = magnitude * left;
						offset = offset + magnitude * up;
						break;
					}
					case 2:
					{
						offset = magnitude * right;
						offset = offset + magnitude * up;
						break;
					}
					case 3:
					{
						offset = magnitude * left;
						offset = offset + magnitude * down;
						break;
					}
					case 0:
					{
						offset = (magnitude-1) * right;
						offset = offset + (magnitude-1) * down;
						break;
					}
					default:
					{
						break;
					}
					break;
				}
				break;
			}
			default:
			{
				break;
			}
		}			
		return offset;
	}
	
	[Obfuscar.Obfuscate]
	private Vector3 NNUKE_GetInitialFormationLivePosition(EnemyBaseScript enemy, int member, Vector3 direction)
	{
		Vector3 offset = Vector3.zero;
		
		switch (Configuration)
		{
		case FormationType.Circle:
		case FormationType.Cross:
			offset = NNUKE_GetRelativeFormationLivePosition(InitialDistanceFromEachOther , enemy, member, direction);
			break;
		case FormationType.Column:
		case FormationType.Row:
			offset = NNUKE_GetRelativeFormationLivePosition(InitialDistanceFromEachOther , enemy, member, direction);
			break;
		case FormationType.Fuzz:
			offset = MathHelpers.RandomVectorOffset(InitialDistanceFromEachOther, member);
			break;
		default:
			break;
		}
			
		return offset;
	}
	
	[Obfuscar.Obfuscate]
	public void NNUKE_DoFormationEntry(EnemyBaseScript enemy, Vector3 pathStartingPoint)
	{		
		FormationEntry entry = new FormationEntry();
		entry.enemy = enemy;
		entry.memberIndex = m_enemies.Count;
		
		DamageTakerScript damageTaker = enemy.GetComponentInChildren<DamageTakerScript>();
		if (damageTaker)
			damageTaker.Died += new DamageTakerScript.DamageTakerDied(DamageTakerDied);
		
		bool endOfPath;
		Vector3 formationTarget = m_pathScript.NNUKE_GetCurrentTargetSmooth(m_dontSkipPathPoints
			, m_lookAheadTimeSeconds, pathStartingPoint, m_formationVelocity
			, m_forwardOrBackward, m_lookDistance, out endOfPath);
		
		Vector3 direction = (RelativeToPathInsteadPlayer) 
			? (formationTarget - pathStartingPoint)
				: (PlayerHomePlanetScript.Instance.transform.position - pathStartingPoint);
		
		if (direction.sqrMagnitude < 1f)
		{
			direction = Vector3.up;
		}
		// jos pisteitä on pathissa vain yksi, ja formation on circle, ja target playerissa,
		// tulee directionista nolla ja kaikkien vihollisten alotuspositio nollaksi ...
		
		direction.Normalize();
		entry.initialDirection = direction;
		
		m_enemies.Add (enemy);
		m_formations.Add(entry);
		m_formationEntries.Add(enemy, entry);
	}
	#endregion
		
	#region Private object methods
	#endregion
}
