using UnityEngine;
using System.Collections;

sealed public class EnemyShootingScript : MonoBehaviour 
{
	#region Enums, structs, and inner classes
	
	#endregion

	#region Delegates And Events
	#endregion

	#region Public members, visible in Unity GUI
	public bool Enabled;
	public int GroupCount;
	public float GroupIntervalSeconds;
	public float GroupIntervalRandomSeconds = 1f;
	public bool GroupMembersToSameDirection;
	public float FireRateSeconds;
	
	public float AccuracyPercentage;
	public float AccuracyRadiusInPlayerMultiples;
	
	public float LimitToStopDegrees = 180f;
	
	public float StartOffsetFwd;
	public float StartOffsetSide;
	public float StartOffsetUp;
	public float FiringVizDepthOffset;
	
	public ProjectileScript ProjectilePrefab;
	public GameObject FiringVisualizationPrefab;
	public float FiringVizOffset;

	#endregion

	#region Public members, not visible to Unity GUI
	
	#endregion

	#region Private data members
	private float m_nextShootingTimeSeconds;
	private int m_shootingsLeftInThisGroup;
	
	private float m_targetDeviationUnit;
	private bool m_changeTarget;
	private Vector3 m_previousTarget;
	#endregion


	#region Unity MonoBehaviour base overrides
	// Use this for initialization
	void Start ()
	{
		m_nextShootingTimeSeconds = GameStateScript.Instance.NNUKE_GetWorldTime() + GroupIntervalSeconds;
		m_shootingsLeftInThisGroup = GroupCount;
		
		m_targetDeviationUnit = AccuracyRadiusInPlayerMultiples * PlayerHomePlanetScript.Instance.GetCollideRadius();
		m_changeTarget = true;
	}
	
	// Update is called once per frame
	void Update ()
	{
		float now = GameStateScript.Instance.NNUKE_GetWorldTime();
		if (now > m_nextShootingTimeSeconds)
		{
			ExecuteShoot();
			m_shootingsLeftInThisGroup--;
			if (m_shootingsLeftInThisGroup > 0)
			{
				m_nextShootingTimeSeconds = now + FireRateSeconds;
				if (GroupMembersToSameDirection)
					m_changeTarget = false;
			}
			else
			{
				m_changeTarget = true;
				m_nextShootingTimeSeconds = now + GroupIntervalSeconds + Random.Range(0f, GroupIntervalRandomSeconds);
				m_shootingsLeftInThisGroup = GroupCount;
			}
		}
	}
	#endregion
	
	#region Public object methods
	public void ProjectileDepleted(ProjectileScript source)
	{
		
	}
	#endregion
	
	#region Private object methods
	[Obfuscar.Obfuscate]
	private void ExecuteShoot()
	{
		if (ProjectilePrefab == null 
			|| PlayerHomePlanetScript.Instance == null
			|| PlayerHomePlanetScript.Instance.CheckIfDead())
			return;
		
		Vector3 target = m_previousTarget;
		Vector3 direction;
		
		Vector3 startingPoint = transform.position;
		
		Quaternion q = transform.rotation;
		q.x = 0f; q.y = 0f;
		startingPoint += StartOffsetFwd * (transform.rotation * Vector3.down);
		startingPoint += StartOffsetSide * (transform.rotation * Vector3.left);
		startingPoint += StartOffsetUp * (transform.rotation * Vector3.forward);
		
		Quaternion startingRotation = transform.rotation;
		
		if (m_changeTarget)
		{
			target = PlayerHomePlanetScript.Instance.transform.position;
			direction = target - startingPoint;
			direction.Normalize();
			Vector3 leftVector = Vector3.Cross(direction, Vector3.forward);
			leftVector.Normalize ();
			
			float deviation = 
				(1f - AccuracyPercentage) * Random.Range(-1f, 1f) * m_targetDeviationUnit;
			
			target = target + deviation * leftVector;
		}		
		if (Enabled)
		{
			direction = target - startingPoint;
			direction.Normalize ();
			
			if (MathHelpers.IsAngleBetweenBiggerThan(transform.rotation * Vector3.down, direction, LimitToStopDegrees))
				return;
			
			// instantiate bullet
			ProjectileScript bulletClone = (ProjectileScript) Instantiate(
				ProjectilePrefab
				, startingPoint
				, startingRotation);
			bulletClone.Owner = ProjectileScript.OwnerType.Enemy;
				
			MovementSeekScript seeker = bulletClone.GetComponent<MovementSeekScript>();
			if (seeker)
			{
				seeker.Target = PlayerHomePlanetScript.Instance.GetComponent<DamageTakerScript>();
				Vector3 newDir = startingPoint - transform.position;
				newDir.Normalize();
				bulletClone.gameObject.GetComponent<Rigidbody>().velocity = newDir * bulletClone.Speed;
			}
			else
			{
				bulletClone.gameObject.GetComponent<Rigidbody>().velocity = direction * bulletClone.Speed;
			}
			
			bulletClone.Depleted += new ProjectileScript.ProjectileDepleted(ProjectileDepleted);
			
			if (FiringVisualizationPrefab)
			{
				GameObject vizClone = (GameObject) Instantiate(
					FiringVisualizationPrefab
					, startingPoint + FiringVizOffset * direction + FiringVizDepthOffset * Vector3.forward
					, startingRotation * FiringVisualizationPrefab.transform.localRotation);
				vizClone.active = true;
			}
		}
		
		m_previousTarget = target;
	}
	#endregion

}
