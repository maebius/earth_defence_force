//#define DEBUG_DRAW

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

using System.Collections;

sealed public class MovementPathPointProxyScript : MonoBehaviour 
{
	#region Public members, visible in Unity GUI
	public AttackWaveScript AttackWave;
	public int PathPointOrder;
	#endregion

	#region Public members, not visible to Unity GUI
	public Vector3 Target
	{
		get 
		{ 
			Vector3 scaled = transform.localScale;
			if (transform.parent)
			{
				scaled = new Vector3(
					scaled.x*transform.parent.localScale.x * transform.position.x
					, scaled.y*transform.parent.localScale.y * transform.position.y
					, scaled.z*transform.parent.localScale.z * transform.position.z);
			}
			return scaled + m_pathOffset + sm_offset; 
		}
	}
	public Vector3 PathOffset
	{
		set { m_pathOffset = value; }
	}
	#endregion

	#region Private data members
	private static Vector3 sm_offset = new Vector3(0f, 0f, 0f);
	private Vector3 m_pathOffset = new Vector3(0f, 0f, 0f);
	#endregion


	#region Unity MonoBehaviour base overrides

#if UNITY_EDITOR
	void OnDrawGizmos()
	{
		if (!gameObject.active 
			|| (transform.parent && transform.parent.gameObject && !transform.parent.gameObject.active))
			return;
		
#if DEBUG_DRAW
		Handles.Label(Target, string.Format ("{0}{1}", transform.parent.name, transform.name));
		Utility.DrawCircleXY(Target
			, (Selection.activeGameObject == gameObject) ? 4f : 2.5f
			, 4
			, (Selection.activeGameObject == gameObject) ? Color.red : Color.gray);
#endif
	}
#endif
	
	#endregion
}
