using UnityEngine;
using System.Collections;

sealed public class MathHelpers 
{
	[Obfuscar.Obfuscate]
	public static float Binominial()
	{
		return Random.Range (-1f, 1f);
	}
	
	[Obfuscar.Obfuscate]
	public static bool Chance(float value)
	{
		return Random.Range(0f, 1f) < value;
	}
	
	[Obfuscar.Obfuscate]
	public static Color InterpolateColor(Color src, Color trg, float ce)
	{
		return new Color(
			Mathf.Lerp (src.r, trg.r, ce)
			, Mathf.Lerp (src.g, trg.g, ce)
			, Mathf.Lerp (src.b, trg.b, ce)
			, Mathf.Lerp (src.a, trg.a, ce));
	}
	
	[Obfuscar.Obfuscate]
	public static float ClampDegrees(float degrees)
	{
		if (degrees < 0f) degrees += 360f * Mathf.CeilToInt(degrees/-360f);
		if (degrees > 360f) degrees -= 360f * Mathf.FloorToInt(degrees/360f);
		return degrees;
	}
	public static float ClampRadians(float radians)
	{
		if (radians < 0f) radians += 2f*Mathf.PI * Mathf.CeilToInt(radians/(-2f*Mathf.PI));
		if (radians > 2f*Mathf.PI) radians -= 2f*Mathf.PI * Mathf.FloorToInt(radians/(2f*Mathf.PI));
		return radians;
	}
	
	[Obfuscar.Obfuscate]
	public static Vector3 RandomVectorOffset(float radialOffset, int multiple)
	{
		float angleRad = Random.Range(0, 2f*Mathf.PI);
		return new Vector3(
				radialOffset * multiple * Mathf.Cos (angleRad)
				, radialOffset * multiple * Mathf.Sin (angleRad)
				, 0f);
	}
	
	[Obfuscar.Obfuscate]
	public static bool HeadingAwayFromTarget(Vector3 myPos, Vector3 targetPos, Vector3 myVelocity)
	{
		Vector3 direction = targetPos - myPos;
		return IsAngleBetweenBiggerThan(direction, myVelocity, 90f);
	}

	[Obfuscar.Obfuscate]
	public static bool IsAngleBetweenBiggerThan(Vector3 v1, Vector3 v2, float limitDegrees)
	{
		v1.Normalize();
		v2.Normalize();
		float dot = Vector3.Dot(v1, v2);
		return Mathf.Acos(dot) > (limitDegrees * Mathf.Deg2Rad);
	}
	
	[Obfuscar.Obfuscate]
	public static Vector3 ProjectPointToLine(Vector3 point, Vector3 lineStart, Vector3 lineDirection)
	{
		float projected = Vector3.Dot(lineDirection, point - lineStart);
		return lineStart + lineDirection * projected;
	}
	
	[Obfuscar.Obfuscate]
	public static bool IsProjectionOnLine(Vector3 projectedPoint, Vector3 lineStart, Vector3 lineEnd, out bool closerToStart)
	{
		// in order the projection to be on line (instead of the continuing line after the start/end),
		// the distance from both needs to be at most the line length
		float squaredLength = (lineEnd - lineStart).sqrMagnitude;
		float squaredDistanceFromStart = (projectedPoint - lineStart).sqrMagnitude;
		float squaredDistanceFromEnd = (projectedPoint - lineEnd).sqrMagnitude;
		
		closerToStart = squaredDistanceFromStart < squaredDistanceFromEnd;
		
		return squaredDistanceFromStart <= squaredLength && squaredDistanceFromEnd <= squaredLength;
	}
	
	[Obfuscar.Obfuscate]
	public static float GetProportion(Vector3 projectedPoint, Vector3 lineStart, Vector3 lineEnd)
	{
		float length = (lineEnd - lineStart).magnitude;
		float distanceFromStart = (projectedPoint - lineStart).magnitude;
		
		//Debug.Log (string.Format ("projectedPoint: {0}, lineStart: {1}, lineEnd: {2}", projectedPoint, lineStart, lineEnd));
		
		return distanceFromStart / length;
	}
}
