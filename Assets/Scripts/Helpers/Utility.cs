//#define DEBUG_TEXTS
//#define DEBUG_DRAW

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
#if UNITY_EDITOR
using UnityEditor;
#endif

public static class Utility
{
	#if UNITY_EDITOR
	[Obfuscar.Obfuscate]
	public static bool IsPrefab(GameObject go)
	{
		if (Application.isEditor)		
			return PrefabUtility.GetPrefabParent(go) == null 
				&& PrefabUtility.GetPrefabObject(go) != null;
		
		return false;
	}
	#endif
	
	[Obfuscar.Obfuscate]
	public static void Swap(ref int val1, ref int val2)
	{
		int temp = val1;
		val1 = val2;
		val2 = temp;
	}
	
	[Obfuscar.Obfuscate]
	public static void SetState(GameObject obj, bool state, bool includeChildren = true)
	{
		if (obj != null)
		{			
			if (includeChildren) NGUITools.SetActiveChildren(obj, state);
			NGUITools.SetActive(obj, state);
		}
		else
		{
			#if DEBUG_TEXTS			
			UnityEngine.Debug.LogWarning(string.Format ("Utility.SetState() object is null!"));
			#endif
		}
	}
	
	[Obfuscar.Obfuscate]
	public static void ResetTweens(GameObject root)
	{
		UITweener[] tweens = root.transform.GetComponentsInChildren<UITweener>(true);
		foreach (UITweener tween in tweens)
		{
			tween.enabled = true;
			tween.Reset();
		}
	}
	
	public static T[] RemoveAt<T>(this T[] source, int index)
	{
		T[] dest = new T[source.Length - 1];
		if( index > 0 )
			System.Array.Copy(source, 0, dest, 0, index);
		if( index < source.Length - 1 )
			System.Array.Copy(source, index + 1, dest, index, source.Length - index - 1);
		return dest;
	}

	public static T[] Resize<T>(this T[] source, int additionalCount)
	{
		int newSize = source.Length + additionalCount;
		T[] dest = new T[newSize];
		int lastIndex = (newSize > source.Length) ? source.Length : newSize;
		System.Array.Copy(source, 0, dest, 0, lastIndex);
		return dest;
	}
	
	[Obfuscar.Obfuscate]
	public static string GetMethodName(int frame)
	{
		StackTrace stackTrace = new StackTrace();
		StackFrame stackFrame = stackTrace.GetFrame(frame);
		if (stackFrame == null)
			return string.Empty;
		MethodBase methodBase = stackFrame.GetMethod();
		return methodBase.Name;
	}
	
	[Obfuscar.Obfuscate]
	public static string GetTypeName(int frame)
	{
		StackTrace stackTrace = new StackTrace();
		StackFrame stackFrame = stackTrace.GetFrame(frame);
		if (stackFrame == null)
			return string.Empty;
		MethodBase methodBase = stackFrame.GetMethod();
		return methodBase.GetType().ToString();
	}
	
	[Obfuscar.Obfuscate]
	public static void CheckDate()
	{
		System.DateTime expirationDate = new System.DateTime(2013, 2, 21);
		// TODO: ota tämä koko funkka pois! ja alla oleva falsetus myös
		if (false && System.DateTime.Now > expirationDate)
		{
			#if DEBUG_TEXTS			
			UnityEngine.Debug.LogError(string.Format ("EXPIRED"));
			#endif
			
			Application.Quit();
		}
	}
	
	[Obfuscar.Obfuscate]
	public static void CollectAllVisualGUIElements(Transform root, ref List<GameObject> collection)
	{
		UILabel[] labels = root.GetComponentsInChildren<UILabel>(true);
		UISlicedSprite[] ssprites = root.GetComponentsInChildren<UISlicedSprite>(true);
		UISprite[] sprites = root.GetComponentsInChildren<UISprite>(true);
		
		foreach (UILabel e in labels)
		{
			collection.Add (e.gameObject);
		}
		foreach (UISlicedSprite e in ssprites)
		{
			collection.Add (e.gameObject);
		}
		foreach (UISprite e in sprites)
		{
			collection.Add (e.gameObject);
		}
	}
	
	[Obfuscar.Obfuscate]
	public static DamageTakerScript GetDamageTaker(GameObject obj)
	{
		DamageTakerScript taker = obj.GetComponent<DamageTakerScript>();
		if (taker == null) 
		{
			taker = obj.GetComponentInChildren <DamageTakerScript>();
		}
		return taker;
	}
	
	[Obfuscar.Obfuscate]
	public static GameObject GetChildRecursive(GameObject root, string name, bool includeInactive = false)
	{
		Transform[] ts = root.transform.GetComponentsInChildren<Transform>(includeInactive);
		foreach (Transform t in ts)
		{
			if (t.gameObject.name == name)
			{
				return t.gameObject;
			}
		}
		#if DEBUG_TEXTS			
		UnityEngine.Debug.LogError(string.Format ("Utility.GetChildRecursive() - could not find: {0} under {1} (at time {2})"
				, name, root.name, Time.time));
		#endif		
		return null;
	}
	
	[Obfuscar.Obfuscate]
	public static GameObject GetChild(GameObject root, string name)
	{
		Transform t = root.transform.FindChild(name);
		if (t == null)
		{
			#if DEBUG_TEXTS
			UnityEngine.Debug.LogError(string.Format ("Utility.GetChild() - could not find: {0} under {1} (at time {2})"
				, name, root.name, Time.time));
			#endif
			return null;
		}
		return t.gameObject;
	}
	
	[Obfuscar.Obfuscate]
	public static GameObject GetObject(string name)
	{
		GameObject obj = GameObject.Find (name);
		if (obj == null)
		{
			#if DEBUG_TEXTS
			UnityEngine.Debug.LogError(string.Format ("Utility.GetObject() - could not find: {0} (at time {1})"
				, name, Time.time));
			#endif
		}
		return obj;
	}
	
	[Obfuscar.Obfuscate]
	public static string Location()
	{
		// 0 would be this function, we want 1 as that is where the call came from
		return "0: " + Location(0) + "- 1: " + Location(1) + " - 2: " + Location (2);
	}

	[Obfuscar.Obfuscate]
	public static string Location(int frame)
	{
		StackTrace stackTrace = new StackTrace();
		StackFrame stackFrame = stackTrace.GetFrame(frame);
		if (stackFrame == null)
			return string.Empty;
		MethodBase methodBase = stackFrame.GetMethod();
		return methodBase.GetType().ToString() + "." + methodBase.Name;
	}
	
	[Obfuscar.Obfuscate]
	static public float VectorToAngleRad(Vector3 vector)
	{
		float rad = 0f;
		if (vector.x < 0f) 
		{
			if (vector.y < 0f) 
				rad = Mathf.PI + Mathf.Atan (vector.y / vector.x);
			else
				rad = Mathf.PI + Mathf.Atan (vector.y / vector.x);
		}
		else
			rad = Mathf.Atan (vector.y / vector.x);
		if (rad < 0f) rad += 2f*Mathf.PI;
		else if (rad > 2f*Mathf.PI) rad -= 2f*Mathf.PI;
		return rad;
	}
	
	[Obfuscar.Obfuscate]
	static public void ScaleSprite(ref UISprite sp, bool scale)
	{
		UIAtlas.Sprite sprite = sp.GetAtlasSprite();
		float sw = sprite.inner.width;
		float sh = sprite.inner.height;
		#if UNITY_IPHONE
		if (scale)
			sh *= 0.5f;
		#endif
		sp.transform.localScale = new Vector3(
			sw * sp.atlas.texture.width
			, sh * sp.atlas.texture.height
			, 1f);
	}
	
#if DEBUG_DRAW
	static public void DrawCircleXY(Vector3 center, float radius, uint segments, Color color, float duration = 0f)
	{
		if (!UnityEngine.Debug.isDebugBuild)
			return;

		float angle = 0.0f;
		float deltaAngle = 2.0f * (float) System.Math.PI / segments;
		Vector3 startPoint = center;
		startPoint.x += radius;
		Vector3 endPoint = center;
		for (uint i = 0; i < segments; i++)
		{
			angle += deltaAngle;
			endPoint.x = center.x + radius * (float) System.Math.Cos(angle); 
			endPoint.y = center.y + radius * (float) System.Math.Sin(angle); 
			if (duration > 0f)
			{
				UnityEngine.Debug.DrawLine(startPoint, endPoint, color, duration);
			}
			else
			{
				UnityEngine.Debug.DrawLine(startPoint, endPoint, color);
			}
			startPoint = endPoint;
		}
	}
#endif
}
