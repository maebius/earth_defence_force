using UnityEngine;
using System.Collections;

sealed public class ItemBaseScript : MonoBehaviour 
{
	#region Delegates And Events
	// A delegate type for hooking up change notifications.
	public delegate void ItemDepleted(ItemBaseScript sender);
	// An event that clients can use to be notified whenever item depletes.
	public event ItemDepleted Depleted;
	#endregion

	#region Public members, visible in Unity GUI
	public float[] PowerLevels;
	public float[] SpeedLevels;
	public float[] DurationLevels;
	
	public float TimeLeftSeconds
	{
		get { return m_liveTimeSeconds; }
	}
	#endregion

	#region Public members, not visible to Unity GUI
	[HideInInspector]
	public float Power;
	[HideInInspector]
	public float Speed;
	[HideInInspector]
	public float Duration;
	#endregion

	#region Private data members
	private float m_liveTimeSeconds;
	#endregion


	#region Unity MonoBehaviour base overrides
		
	// Use this for initialization
	void Start ()
	{
		m_liveTimeSeconds = Duration;
	}

	// Update is called once per frame
	void Update ()
	{
		if (DurationLevels.Length > 0)
		{
			m_liveTimeSeconds -= GameStateScript.Instance.NNUKE_GetWorldDeltaTime();
			if (m_liveTimeSeconds < 0)
			{
				DoDepleteMyself();
			}
		}
	}
	#endregion
		
	#region Private object methods
	private void DoDepleteMyself()
	{
		if (Depleted != null)
			Depleted(this);
		
		Destroy (gameObject);
	}
	#endregion
}
