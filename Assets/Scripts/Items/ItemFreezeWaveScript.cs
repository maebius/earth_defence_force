using UnityEngine;
using System.Collections;
using System.Collections.Generic;


sealed public class ItemFreezeWaveScript : MonoBehaviour 
{
	#region Private data members
	private List<DamageTakerScript> m_influencingObjects = new List<DamageTakerScript>();
	private List<ProjectileScript> m_influencingProjectiles = new List<ProjectileScript>();
	
	private ItemBaseScript m_base;
	#endregion

	#region Unity MonoBehaviour base overrides
	void Awake()
	{
		m_base = GetComponent<ItemBaseScript>();
		m_base.Depleted += new ItemBaseScript.ItemDepleted(ItemDepleted);
	}
	
	void OnTriggerEnter (Collider hit)
	{
		DamageTakerScript taker = hit.gameObject.GetComponent<DamageTakerScript>();

		if (taker && (taker.IsEnemy() || taker.IsAsteroid())) 
		{
			if (m_influencingObjects.Contains(taker))
				return;
			
			m_influencingObjects.Add (taker);
			taker.Speed = m_base.Power * taker.OriginalSpeed;
			
			return;
		}
		
		ProjectileScript proj = hit.gameObject.GetComponent<ProjectileScript>();
		if (proj && proj.Owner == ProjectileScript.OwnerType.Enemy)
		{
			if (m_influencingProjectiles.Contains(proj))
				return;
			
			m_influencingProjectiles.Add (proj);
			proj.GetComponent<Rigidbody>().velocity = m_base.Power * proj.GetComponent<Rigidbody>().velocity;
		}
	}

	void OnTriggerExit (Collider hit)
	{
		DamageTakerScript taker = hit.gameObject.GetComponent<DamageTakerScript>();
		if (taker) 
		{
			if (m_influencingObjects.Contains(taker))
			{
				taker.Speed = taker.OriginalSpeed;
			}
		}
	}
	
	public void ItemDepleted(ItemBaseScript item)
	{
		foreach (DamageTakerScript taker in m_influencingObjects)
		{
			if (taker == null)
				continue;
			
			taker.Speed = taker.OriginalSpeed;
		}
	}
	
	#endregion
}
