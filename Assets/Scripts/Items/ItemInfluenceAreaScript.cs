using UnityEngine;
using System.Collections;

sealed public class ItemInfluenceAreaScript : MonoBehaviour 
{

	#region Public members, visible in Unity GUI
	public float Speed;
	public float MaximumScale;
	
	public float RotateSpeedDegreesPerSecond;
	
	[HideInInspector]
	public float AdditionaGrow
	{
		get { return m_currentGrowFactor; }
	}
	
	#endregion

	#region Public members, not visible to Unity GUI
	#endregion

	#region Private data members
	private Vector3 m_originalScale;
	private Vector3 m_maximumScale;
	
	private ItemBaseScript m_base;
	
	private float m_timeToShrink;
	
	private float m_currentGrowFactor;
	
	private SoundScript m_sound;
	#endregion


	#region Unity MonoBehaviour base overrides
	void Awake()
	{
		m_base = GetComponent<ItemBaseScript>();
		
		m_sound = GetComponent<SoundScript>();
	}
	
	// Use this for initialization
	void Start ()
	{
		m_originalScale = transform.localScale;
		m_maximumScale = MaximumScale * m_originalScale;
		m_maximumScale.y = m_originalScale.y;
			
		m_base.Depleted += new ItemBaseScript.ItemDepleted(ItemDepleted);
		
		m_timeToShrink = MaximumScale / Speed;
	}

	// Update is called once per frame
	void Update ()
	{
		float addition = Speed * GameStateScript.Instance.NNUKE_GetWorldDeltaTime();
		if (m_base.TimeLeftSeconds < m_timeToShrink)
		{
			addition *= -1f;
		}
		
		Vector3 newScale = new Vector3(
			transform.localScale.x + addition
			, m_originalScale.y
			, transform.localScale.z + addition);
		
		newScale.x = Mathf.Min (newScale.x, m_maximumScale.x);
		newScale.z = Mathf.Min (newScale.z, m_maximumScale.z);
		newScale.x = Mathf.Max (m_originalScale.x, newScale.x);
		newScale.z = Mathf.Max (m_originalScale.z, newScale.z);

		m_currentGrowFactor = (newScale.x - m_originalScale.x) / (m_maximumScale.x - m_originalScale.x);
		
		if (m_sound)
		{			
			m_sound.SetVolume(m_currentGrowFactor);
		}
		
		transform.localScale = newScale;
		
		transform.RotateAround(Vector3.forward
			, Mathf.Deg2Rad * RotateSpeedDegreesPerSecond * GameStateScript.Instance.NNUKE_GetWorldDeltaTime());
	}
	
	public void ItemDepleted(ItemBaseScript item)
	{
	}
	
	#endregion
}
