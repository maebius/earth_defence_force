using UnityEngine;
using System.Collections;
using System.Collections.Generic;


sealed public class ItemInvulnearabilityScript : MonoBehaviour 
{
	#region Public data members
	public GameObject HitVisualizationPrefab;
	public AudioClip m_soundHit;
	#endregion
	
	#region Unity MonoBehaviour base overrides
	void Start ()
	{
		PlayerShieldGeneratorScript.Instance.SetShieldEnabled(false);
	}
	
	void OnDestroy()
	{
		PlayerShieldGeneratorScript.Instance.SetShieldEnabled(true);
	}
	
	void OnTriggerEnter (Collider hit)
	{
		DamageTakerScript taker = hit.gameObject.GetComponent<DamageTakerScript>();
		if (taker && !taker.IsPlayer()) 
		{
			// kill him! we're invincible!
			taker.DoGiveDamage(taker.Energy + 1f, DamageTakerScript.DamageType.PlayerShield, hit.transform.position);
			return;
		}
		
		HandleProjectile(hit.gameObject.GetComponent<ProjectileScript>(), hit.transform.position);
	}
	#endregion
	
	#region Private object methods
	private void HandleProjectile(ProjectileScript projectile, Vector3 pos)
	{
		if (projectile != null)
		{
			SoundManagerScript.PlayOneTime (gameObject, m_soundHit);
			 if (HitVisualizationPrefab)
			{
				GameObject obj = (GameObject) Instantiate(
					HitVisualizationPrefab
					, pos
					, transform.rotation);
				obj.active = true;
			}

			// don't react (otherwise than audiovisually above) to own bullets ...
			if (projectile.Owner != ProjectileScript.OwnerType.PlayerPrimary 
				&& projectile.Owner != ProjectileScript.OwnerType.PlayerSecondary)
			{
				projectile.DepleteMyself();
			}
		}
	}
	#endregion
}
