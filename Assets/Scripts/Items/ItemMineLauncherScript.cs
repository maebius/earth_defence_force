using UnityEngine;
using System.Collections;

sealed public class ItemMineLauncherScript : MonoBehaviour 
{
	public ProjectileScript ProjectilePrefab;
	public GameObject SpawnVizualisation;
	
	public float Spread;
	public float InitialSpeed;
	public float OffsetFactor;
	
	private ItemBaseScript m_base;
	
	void Awake()
	{
		m_base = GetComponent<ItemBaseScript>();
	}
	
	// Use this for initialization
	void Start () 
	{
		if (PlayerHomePlanetScript.Instance == null)
			return;
		
		// take weapon system primary slot as a base for spawning
		Vector3 basePos = WeaponSystemScript.Instance.GetPrimarySlotPosition();
		Vector3 dir = basePos - PlayerHomePlanetScript.Instance.transform.position;
		basePos += OffsetFactor * dir;
		Vector3 left = Vector3.Cross(dir, Vector3.forward);
		left.Normalize();
		
		// NOTE! This can't be modified unless ItemSystemScript is
		// also notified how many mines we launch ... so that it can keep book
		int launchCount = 1;
		
		float start = -0.5f * (launchCount-1) * Spread;
		for (int i = 0; i < launchCount; i++)
		{
			Vector3 spawnOffset = start * left;
			start += Spread;
			
			Vector3 spawnPosition = basePos + spawnOffset;
			Vector3 direction = spawnPosition - PlayerHomePlanetScript.Instance.transform.position;
			direction.Normalize();

			if (SpawnVizualisation)
			{
				GameObject obj = (GameObject) Instantiate(
					SpawnVizualisation
					, spawnPosition
					, Quaternion.identity);
				obj.active = true;
			}
			
			ProjectileScript bulletClone = (ProjectileScript) Instantiate(
				ProjectilePrefab
				, spawnPosition
				, Quaternion.identity);
		
			MovementSeekScript ms = bulletClone.GetComponent<MovementSeekScript>();
			ms.SpeedMax = m_base.Speed;
			ms.AccelerationMax = 0.5f*m_base.Speed;
			
			bulletClone.gameObject.GetComponent<Rigidbody>().velocity = direction * InitialSpeed;
		}
		
		// we destroy ourselves now as our only use was to spawn the items above
		Destroy(gameObject);
	}
}
