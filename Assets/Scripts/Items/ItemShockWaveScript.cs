using UnityEngine;
using System.Collections;
using System.Collections.Generic;

sealed public class ItemShockWaveScript : MonoBehaviour 
{
	#region Public members, visible in Unity GUI
	public GameObject VisualizationPrefab;
	public int VisualizationCount;
	#endregion

	#region Public members, not visible to Unity GUI
	#endregion

	#region Private data members
	private Vector3 m_originalScale;
	
	private List<GameObject> m_objectsHit = new List<GameObject>();
	private List<VisualizerScript> m_visualizations = new List<VisualizerScript>();
	
	private ItemBaseScript m_base;
	#endregion

	#region Unity MonoBehaviour base overrides
	void Awake()
	{
		m_base = GetComponent<ItemBaseScript>();
	}
	
	// Use this for initialization
	void Start ()
	{
		m_originalScale = transform.localScale;
		if (VisualizationPrefab)
		{
			float deltaAngle = (VisualizationCount > 0) ? 2f * Mathf.PI / VisualizationCount : 0f;
			float angle = 0f;

			// make a circle of visualizations, all going radially outwards
			for (int i = 0; i < VisualizationCount; i++)
			{
				GameObject go =  (GameObject) Instantiate(
					VisualizationPrefab
					, transform.position
					, transform.rotation);
				
				VisualizerScript viz = go.GetComponent<VisualizerScript>();
				
				viz.transform.localScale = new Vector3(
					viz.transform.localScale.x * transform.localScale.x
					, viz.transform.localScale.y * transform.localScale.y
					, viz.transform.localScale.z * transform.localScale.z);
				
				viz.Direction = new Vector3(Mathf.Cos (angle), Mathf.Sin (angle), 0f);
				viz.Speed = 0.5f*m_base.Speed;
				angle += deltaAngle;
				
				m_visualizations.Add (viz);
			}
		}
	}
	
	void OnDestroy()
	{
		for (int i = 0; i < m_visualizations.Count; i++)
		{
			if (m_visualizations[i] != null && m_visualizations[i].gameObject != null)
				Destroy (m_visualizations[i].gameObject);
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		GetComponent<Renderer>().material.SetFloat("_GlobalTime", Time.time);
		
		float addition = m_base.Speed * GameStateScript.Instance.NNUKE_GetWorldDeltaTime();
		transform.localScale = new Vector3(
			transform.localScale.x + addition
			, m_originalScale.y
			, transform.localScale.z + addition);
	}
	
	void OnTriggerEnter (Collider hit)	
	{
		DamageTakerScript taker = Utility.GetDamageTaker(hit.gameObject);

		if (taker && taker.GetComponent<PlayerHomePlanetScript>() == null) 
		{
			if (m_objectsHit.Contains(hit.gameObject))
				return;
			
			m_objectsHit.Add (hit.gameObject);
			
			taker.DoGiveDamage(m_base.Power, DamageTakerScript.DamageType.PlayerBullet, hit.transform.position);
		}
	}

	#endregion
		
}
