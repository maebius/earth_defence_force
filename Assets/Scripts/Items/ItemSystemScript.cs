using UnityEngine;
using System.Collections;
using System.Collections.Generic;

sealed public class ItemSystemScript : MonoBehaviour 	
{
	#region Public GUI tweakable members
	public GameObject Root;
	
	public string[] ExistingItemIds;
	public ItemBaseScript[] ExistingItems;
	#endregion
	
	#region Public members, not visible in GUI
	[HideInInspector]
	public static ItemSystemScript Instance
	{
		get { return sm_instance; }
	}
	[HideInInspector]
	public xsd_ct_Equipment[] m_configs;
	#endregion
	
	#region Private data members
	private static ItemSystemScript sm_instance = null;
	
	private Transform m_parent;
	private List<ItemBaseScript> m_spawnedItems = new List<ItemBaseScript>();

	private bool m_operational;
	
	private ItemBaseScript[] m_items;
	
	#endregion


	#region Unity MonoBehaviour base overrides
	// Use this for early initialization
	void Awake ()
	{
		sm_instance = this;
		m_parent = Utility.GetChildRecursive(Root, "PrimarySlot", true).transform;
	}
		
	// Use this for initialization
	void Start ()
	{
		// make sure we are where player is
		transform.position = PlayerHomePlanetScript.Instance.transform.position;
		
		PlayerHomePlanetScript.Instance.Died += new PlayerHomePlanetScript.PlayerDied(PlayerDied);
		
		xsd_ct_Equipment config_1 =  SaveData.Instance.NNUKE_GetItem(SaveData.Instance.Config.ItemSlot_1);
		xsd_ct_Equipment config_2 =  SaveData.Instance.NNUKE_GetItem(SaveData.Instance.Config.ItemSlot_2);
		
		m_configs = new xsd_ct_Equipment[]
		{
			config_1, config_2
		};
		
		m_items = new ItemBaseScript[]
		{
			(config_1 != null) ?  NNUKE_GetItemPrefab(config_1.Id) : null
			, (config_2 != null) ?  NNUKE_GetItemPrefab(config_2.Id) : null
		};
		
		m_operational = true;
	}
	
	#endregion
		
	#region Public object methods
	[Obfuscar.Obfuscate]
	public bool UseItem(int itemIndex)
	{
		if (!m_operational)
			return false;
		
		if (itemIndex > -1 && itemIndex < 2)
		{
			ItemBaseScript prefab = m_items[itemIndex];
			if (prefab == null)
				return false;
			
			xsd_ct_Equipment config = m_configs[itemIndex];
			if (config.Number > 0)
			{
				config.Number--;
				SpawnItem(prefab, config);
				return true;
			}
		}
		return false;
	}
	
	public void PlayerDied(PlayerHomePlanetScript player)
	{
		m_operational = false;
	}
	
	public void ItemDepleted(ItemBaseScript item)
	{
		m_spawnedItems.Remove (item);
	}

	#endregion
		
	#region Private object methods
	[Obfuscar.Obfuscate]
	private ItemBaseScript NNUKE_GetItemPrefab(string id)
	{
		if (string.IsNullOrEmpty(id))
			return null;
		int index = 0;
		for ( ; index < ExistingItemIds.Length; index++)
		{
			if (ExistingItemIds[index].Equals(id))
				break;
		}
		if (index < ExistingItemIds.Length && index < ExistingItems.Length)
			return ExistingItems[index];
		
		return null;
	}
	
	[Obfuscar.Obfuscate]
	private void SpawnItem(ItemBaseScript prefab, xsd_ct_Equipment config)
	{
		GameData gd = GameData.Instance;
		ItemBaseScript clone = null;
		if (prefab != null)
		{
			clone =
				(ItemBaseScript) Instantiate(
					prefab
					, m_parent.position
					, m_parent.rotation);
			
			if (clone.PowerLevels.Length > 0) 
				clone.Power = clone.PowerLevels[gd.NNUKE_GetSlotPropertyIndex(config, "Power")];
			if (clone.SpeedLevels.Length > 0) 
				clone.Speed = clone.SpeedLevels[gd.NNUKE_GetSlotPropertyIndex(config, "Speed")];
			if (clone.DurationLevels.Length > 0) 
				clone.Duration = clone.DurationLevels[gd.NNUKE_GetSlotPropertyIndex(config, "Duration")];
			
			clone.Depleted += new ItemBaseScript.ItemDepleted(ItemDepleted);
			m_spawnedItems.Add (clone);
		}
	}
	#endregion
}
