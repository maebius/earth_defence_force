using UnityEngine;
using System.Collections;
using System.Collections.Generic;

sealed public class BonusSpawnerScript : MonoBehaviour 
{
	#region Enums, structs, and inner classes
	#endregion

	#region Delegates And Events
	#endregion

	#region Public members, visible in Unity GUI
	public int CreditsAfterAsteroidsDestroyed;
	public int HealthAfterAsteroidsDestroyed;
	
	public int CreditsAfterAsteroidsVariation;
	public int HealthAfterAsteroidsVariation;
	
	public int CreditsAfterEnemiesDestroyed;
	public int HealthAfterEnemiesDestroyed;
	
	public int CreditsAfterEnemiesVariation;
	public int HealthAfterEnemiesVariation;

	public DamageTakerScript HealthPackPrefab;
	public DamageTakerScript CreditPackPrefab;
	
	/// <summary>
	/// What visualization we use for the moment of spawn, if any.
	/// </summary>
	public GameObject SpawningVisualizationPrefab;

	/// <summary>
	/// What sound we play when we spawn object, if any.
	/// </summary> 
	public AudioClip m_soundSpawn = null;
	
	public bool m_randomSpawning;
	public float m_randomSpawningIntervalSeconds;
	
	private int m_healthFromAsteroidsCount;
	private int m_healthFromEnemiesCount;

	private int m_creditsFromAsteroidsCount;
	private int m_creditsFromEnemiesCount;

	#endregion

	#region Public members, not visible to Unity GUI
	#endregion

	#region Private data members
	private int m_asteroidsDestroyed = 0;
	private int m_enemiesDestroyed = 0;
	
	private List<DamageTakerScript> m_currentLiveBonuses = new List<DamageTakerScript>();
	
	private float m_nextRandomCheckTime;
	#endregion


	#region Unity MonoBehaviour base overrides
		
	// Use this for initialization
	void Start ()
	{
		m_asteroidsDestroyed = 0;
		m_enemiesDestroyed = 0;
		
		CalculateNext(ref m_healthFromAsteroidsCount, HealthAfterAsteroidsDestroyed, HealthAfterAsteroidsVariation);
		CalculateNext(ref m_healthFromEnemiesCount, HealthAfterEnemiesDestroyed, HealthAfterEnemiesVariation);
		CalculateNext(ref m_creditsFromAsteroidsCount, CreditsAfterAsteroidsDestroyed, CreditsAfterAsteroidsVariation);
		CalculateNext(ref m_creditsFromEnemiesCount, CreditsAfterEnemiesDestroyed, CreditsAfterEnemiesVariation);
		
		m_nextRandomCheckTime = GameStateScript.Instance.NNUKE_GetWorldTime() + m_randomSpawningIntervalSeconds;
	}

	// Update is called once per frame
	void Update ()
	{
		if (m_randomSpawning 
			&& m_currentLiveBonuses.Count == 0 // this makes sure we don't check in next iteration until zero again
			&& GameStateScript.Instance.NNUKE_GetWorldTime() > m_nextRandomCheckTime)
		{
			float distance = Random.Range (150f, 350f);
			//float distance = Random.Range (75f, 78f);
			float r = Random.Range (0f, 2f*Mathf.PI);
			Vector3 pos = distance * new Vector3(Mathf.Cos (r), Mathf.Sin(r), 0f);
			if (Random.Range (0f, 1f) < 0.5f)
			{
				SpawnHealthPack(pos);
			}
			else
			{
				SpawnCreditPack(pos);
			}
		}
	}
	#endregion
		
	#region Public object methods
	public void DamageTakerDied(DamageTakerScript source)
	{
		// did player kill this one?
		if (source.DiedBecauseOf == DamageTakerScript.DamageType.PlayerBullet
			|| source.DiedBecauseOf == DamageTakerScript.DamageType.PlayerShield)
		{
			// don't react if too close ...
			if (RadarControlScript.Instance != null 
				&& !RadarControlScript.Instance.IsOutsideRadarArea(source.transform.position, 1500f))
			{
				//Debug.Log (string.Format ("DamageTakerDied WAS NOT OUTSIDE OF RADAR AREA!"));
				return;
			}
			
			if (source.GetComponent<AsteroidScript>())
			{
				m_asteroidsDestroyed++;
				
				m_healthFromAsteroidsCount--;
				
				if (m_healthFromAsteroidsCount <= 0)
				{
					SpawnHealthPack(source.transform.position);
					CalculateNext(ref m_healthFromAsteroidsCount, HealthAfterAsteroidsDestroyed, HealthAfterAsteroidsVariation);
					return;
				}
				
				m_creditsFromAsteroidsCount--;
				if (m_creditsFromAsteroidsCount <= 0)
				{
					SpawnCreditPack(source.transform.position);
					CalculateNext(ref m_creditsFromAsteroidsCount, CreditsAfterAsteroidsDestroyed, CreditsAfterAsteroidsVariation);
					return;
				}
			}
			else if (source.transform.parent && source.transform.parent.GetComponent<EnemyBaseScript>())
			{
				m_enemiesDestroyed++;
				
				m_healthFromEnemiesCount--;
				
				if (m_healthFromEnemiesCount <= 0)
				{
					SpawnHealthPack(source.transform.position);
					CalculateNext(ref m_healthFromEnemiesCount, HealthAfterEnemiesDestroyed, HealthAfterEnemiesVariation);
					return;
				}
				
				m_creditsFromEnemiesCount--;
				if (m_creditsFromEnemiesCount <= 0)
				{
					SpawnCreditPack(source.transform.position);
					CalculateNext(ref m_creditsFromEnemiesCount, CreditsAfterEnemiesDestroyed, CreditsAfterEnemiesVariation);
					return;
				}
			}
		}
	}

	#endregion
		
	#region Private object methods
	[Obfuscar.Obfuscate]
	private void CalculateNext(ref int nextLimit, int baseCount, int variation)
	{
		nextLimit = baseCount + Random.Range (0, variation);
	}
	
	[Obfuscar.Obfuscate]
	private void SpawnHealthPack(Vector3 position)
	{
		Spawn (HealthPackPrefab, position);
	}
	[Obfuscar.Obfuscate]
	private void SpawnCreditPack(Vector3 position)
	{
		Spawn (CreditPackPrefab, position);		
	}
	[Obfuscar.Obfuscate]
	private void Spawn(DamageTakerScript prefab, Vector3 position)
	{
		if (PlayerHomePlanetScript.Instance == null
			|| GameStateScript.Instance.State != GameStateScript.GameState.InGameLive)
			return;
		
		// Create thea actual enemy
		DamageTakerScript clone = (DamageTakerScript) Instantiate(
			prefab
			, position
			, Quaternion.identity);
		
		if (SpawningVisualizationPrefab)
		{
			GameObject vizClone = (GameObject) Instantiate(
				SpawningVisualizationPrefab
				, position
				, clone.transform.rotation);
			vizClone.active = true;
		}
		
		if (m_soundSpawn)
		{
			SoundManagerScript.PlayOneTime (clone.gameObject, m_soundSpawn);
		}
		m_currentLiveBonuses.Add (clone);
		
		clone.Died += new DamageTakerScript.DamageTakerDied(BonusDespawned);
	}
	
	private void BonusDespawned(DamageTakerScript bonus)
	{
		if (m_currentLiveBonuses.Count == 1)
			m_nextRandomCheckTime = GameStateScript.Instance.NNUKE_GetWorldTime() + m_randomSpawningIntervalSeconds;
		
		m_currentLiveBonuses.Remove(bonus);
	}
	
	#endregion
}
