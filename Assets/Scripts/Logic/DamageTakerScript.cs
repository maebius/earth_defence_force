using UnityEngine;
using System.Collections;

sealed public class DamageTakerScript : MonoBehaviour 
{
	#region Enums, structs, and inner classes
	public enum DamageType
	{
		None
		, PlayerBullet
		, PlayerShield
		, PlayerCollision
		, EnemyBullet
		, EnemyCollision
	}
	#endregion

	#region Delegates And Events
	// A delegate type for hooking up change notifications.
	public delegate void DamageTakerSpawned(DamageTakerScript sender);
	// An event that clients can use to be notified whenever enemy dies
	public event DamageTakerSpawned Spawned;

	// A delegate type for hooking up change notifications.
	public delegate void DamageTakerDied(DamageTakerScript sender);
	// An event that clients can use to be notified whenever enemy dies
	public event DamageTakerDied Died;
	#endregion

	#region Public members, visible in Unity GUI
	public float Speed;
	public float TurningSpeedAnglesPerSecond;
	public float Acceleration;
	
	public float EnergyInitial;
	public float Energy;
	public bool EnergyReduces = true;
	public float DamageToPlayerFromCollision;
	public bool PositionalDamageVisualizations = false;
	public float VisualizationOffset;
	
	public bool GiveExplosionForce = true;
	
	public int ScoreToPlayer;
	public float EnergyToPlayer;
	public int CreditsToPlayer;
	
	public RadarDotScript RadarVisualizationPrefab;
	public GameObject HitVisualizationPrefab;
	public GameObject DeathVisualizationPrefab;

	public GameObject RepulsorPrefab;

	public AudioClip m_hitSound;
	public AudioClip m_dieSound;
	
	public bool m_disableCollidersInDeath = false;
	
	[HideInInspector]
	public bool m_playerHandled;
	
	[HideInInspector]
	public RadarDotScript RadarDot
	{
		get { return m_myVisualizationInRadar; }
	}

	[HideInInspector]
	public float OriginalSpeed
	{
		set { m_originalSpeed = value; }
		get { return m_originalSpeed; }
	}

	[HideInInspector]
	public DamageType DiedBecauseOf
	{
		get { return m_diedBecauseOf; }
	}
	[HideInInspector]
	public float EnergyNormalized
	{
		get { return Mathf.Min(1f, Energy / EnergyInitial); }
	}
	[HideInInspector]
	public bool CountAsPartOfAttackWave;
	#endregion

	#region Public members, not visible to Unity GUI
	#endregion

	#region Private data members
	private float m_originalSpeed;
	
	private DamageType m_diedBecauseOf;
	private RadarDotScript m_myVisualizationInRadar;
	
	private ShatterTool m_shatter;
	
	private float m_lastHitVisualisationTime = -1f;
	
	private HealthBarScript m_healthBar;
	
	private float m_shatterTimeSeconds;
	
	#endregion

	#region Unity MonoBehaviour base overrides
	// Use this for early initialization
	void Awake ()
	{
		CountAsPartOfAttackWave = false;
		m_shatter = GetComponent<ShatterTool>();
		m_healthBar = GetComponent<HealthBarScript>();
		
		m_playerHandled = false;
		
		m_originalSpeed = Speed;
	}
	// Use this for initialization
	void Start ()
	{
		// player wants to know about us ... unless we are player
		if (GetComponent<PlayerHomePlanetScript>() == null)
		{
			Spawned += new DamageTakerScript.DamageTakerSpawned(
				PlayerHomePlanetScript.Instance.DamageTakerSpawned);
			Died += new DamageTakerScript.DamageTakerDied(
				PlayerHomePlanetScript.Instance.DamageTakerDied);
		}
		else if (PlayerHomePlanetScript.Instance != null)
		{
			Died += new DamageTakerScript.DamageTakerDied(
				PlayerHomePlanetScript.Instance.DidDie);
		}
		
		Energy = EnergyInitial;
		
		if (RadarVisualizationPrefab && RadarControlScript.Instance != null)
		{
			m_myVisualizationInRadar = (RadarDotScript) Instantiate(
				RadarVisualizationPrefab
				, transform.position
				, Quaternion.identity);
			
			m_myVisualizationInRadar.Follows = gameObject;
		}
		
		if (Spawned != null)
			Spawned(this);
	}
	
	void OnDestroy()
	{
		//Debug.Log (string.Format ("DamageTakerScript.OnDestroy() name: {0}", gameObject.name));
		
		if (m_myVisualizationInRadar)
			Destroy(m_myVisualizationInRadar.gameObject);
	}
	
	#endregion
		
	#region Public object methods
	public bool IsPlayer()
	{
		PlayerHomePlanetScript player = gameObject.GetComponent<PlayerHomePlanetScript>();
		return player != null;
	}
	public bool IsEnemy()
	{
		return (gameObject.transform.parent != null 
			&& gameObject.transform.parent.GetComponent<EnemyBaseScript>() != null);
	}
	public bool IsAsteroid()
	{
		return gameObject.GetComponent<AsteroidScript>() != null;
	}
	
	public void MakeDisappear()
	{
		// this "kills" the object without triggering any visuals or points etc.
		m_diedBecauseOf = DamageType.None;
		Energy = 0f;
		if (Died != null)
		{
			Died(this);
		}
		GameObject parent = (transform.parent) ? transform.parent.gameObject : null;
		Destroy (gameObject);
		if (parent) Destroy(parent);
	}
	
	
	/// <summary>
	/// Gives damage to the enemy. If energy goes below zero, will notify the listeners, and destroys itself.
	/// </summary>
	/// <param name='factor'>
	/// Factor.
	/// </param>
	[Obfuscar.Obfuscate]
	public void DoGiveDamage(float factor, DamageType type, Vector3 hitPosition)
	{
		if (gameObject == null)
			return;
		
		if (EnergyReduces)
		{
			Energy -= factor;
			if (m_healthBar != null)
			{
				m_healthBar.EnergyChanged(EnergyNormalized);
			}
		}
		
		if (Energy <= 0f && m_dieSound)
		{
			SoundManagerScript.PlayOneTime(transform.position, m_dieSound);
		}
		else if (Energy > 0f && m_hitSound)
		{
			SoundManagerScript.PlayOneTime(transform.position, m_hitSound);
		}
		
		float now = GameStateScript.Instance.NNUKE_GetWorldTime();
		if (Energy <= 0f)
		{
			m_diedBecauseOf = type;
			
			// notify listeners
			if (Died != null)
			{
				Died(this);
			}
			
			// do we make some last death visualization?
			if (DeathVisualizationPrefab)
			{
				GameObject obj = (GameObject) Instantiate(
					DeathVisualizationPrefab
					, transform.position
					, transform.rotation);
				
				obj.active = true;
				if (obj.GetComponent<Rigidbody>() && gameObject.GetComponent<Rigidbody>())
				{
					obj.GetComponent<Rigidbody>().velocity = gameObject.GetComponent<Rigidbody>().velocity;
				}
			}
			if (m_shatter && !m_shatter.IsLastGeneration)
			{
				m_shatter.Shatter(transform.position);
			}
			else
			{
				// do I have a parent? if I do, remember it so we can destroy it also ...
				GameObject parent = (transform.parent) ? transform.parent.gameObject : null;
				Destroy (gameObject);
				if (parent) Destroy(parent);
			}
		}
		else if (HitVisualizationPrefab && now > m_lastHitVisualisationTime + 0.5f)
		{
			Vector3 pos = (PositionalDamageVisualizations) 
				? hitPosition 
				: (transform.position + VisualizationOffset * (transform.rotation * Vector3.down));
			
			GameObject obj = (GameObject) Instantiate(
				HitVisualizationPrefab
				, pos
				, transform.rotation);
			obj.active = true;
			if (obj.GetComponent<Rigidbody>() && gameObject.GetComponent<Rigidbody>())
			{
				obj.GetComponent<Rigidbody>().velocity = gameObject.GetComponent<Rigidbody>().velocity;
			}
			m_lastHitVisualisationTime = now;
		}
	}
	
	public void PostSplit(GameObject[] objects)
	{
		//Debug.Log (string.Format ("PlayerHomePlanetScript.PostSplit - count: {0}", objects.Length));
		
		if (GiveExplosionForce)
		{
			foreach (GameObject obj in objects)
			{
				float angle = Random.Range(0f, 2f*Mathf.PI);
				if (obj.GetComponent<Rigidbody>())
				{
					obj.GetComponent<Rigidbody>().AddRelativeForce(Random.Range(100f, 500f) * 
						new Vector3(Mathf.Cos (angle), Mathf.Sin (angle), 0f));
				}
			}
		}
		
		if (m_disableCollidersInDeath)
		{
			GetComponent<Collider>().enabled = false;
			foreach (GameObject obj in objects)
			{
				obj.GetComponent<Collider>().enabled = false;
			}
		}
		
		if (RepulsorPrefab)
		{
			// this needs to kill itself ... will do as has LifeTimeScript currently attached
			Instantiate(RepulsorPrefab, transform.position, Quaternion.identity);
		}
	}
	
	#endregion
}
