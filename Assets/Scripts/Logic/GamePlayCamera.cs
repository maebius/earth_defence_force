//#define DEBUG_TEXTS

using UnityEngine;
using System.Collections;

sealed public class GamePlayCamera : MonoBehaviour 
{
	#region Public GUI tweakable members
	public float SpeedRPS;
	public float TiltAngleDegrees;	
	public bool RotateWorld;

	// variables for spawn location and velocity
	public float SpawnRadiusPadding;
	public float DespawnSafetyOffset;

	#endregion
	
	#region Public members not visible in GUI.
	[HideInInspector]
	public Camera UsedCamera;
	[HideInInspector]
	public Vector3 CameraUp;
	[HideInInspector]
	public Vector3 CameraLook;
	[HideInInspector]
	public Vector3 CameraLeft;
	[HideInInspector]
	public float SafeSpawnDistance;
	[HideInInspector]
	public float AngleRadians
	{
		get { return m_cameraRadians; }
	}
	[HideInInspector]
	public Camera ActualCamera;
	
	[HideInInspector]
	public static GamePlayCamera Instance
	{
		get { return sm_instance; }
	}

	[HideInInspector]
	public Vector3 LookNonTilted;
	#endregion

	#region Private data members
	private static GamePlayCamera sm_instance = null;

	private Plane[] m_frustrumPlanes;	
	private Vector3 m_initialCameraPosition;
	private Vector3 m_cameraRotateOrigin;
	private Vector3 m_cameraLeft;
	private Vector3 m_cameraLook;
	private float m_cameraRotationRadius;
	
	private float m_speedInputRadPerSecond;
	private float m_speedMaxRadPerSecond;
	
	private float m_cameraRadians;

	#endregion
		
	#region Unity MonoBehaviour base overrides
	void Awake()
	{
		if (sm_instance != null)
		{
			Debug.LogError (string.Format ("GamePlayCamera.Awake() - WE HAVE ALREADY INSTANCE! Should not, solve this."));
		}
		
		sm_instance = this;
		m_speedInputRadPerSecond = 0f;
		m_speedMaxRadPerSecond = 360f * SpeedRPS * Mathf.Deg2Rad;

		m_cameraRadians = 0f;
		ActualCamera = gameObject.GetComponent<Camera>();
		
		m_initialCameraPosition = ActualCamera.transform.position;
		
	}
	
	void Start()
	{
		Vector3 playerPos = PlayerHomePlanetScript.Instance.transform.position;
		m_cameraRotateOrigin = new Vector3(transform.position.x, playerPos.y, m_initialCameraPosition.z);
		m_cameraRotationRadius = (m_initialCameraPosition - m_cameraRotateOrigin).magnitude;
		
		m_frustrumPlanes = GeometryUtility.CalculateFrustumPlanes(ActualCamera);
		SafeSpawnDistance = ExecuteCalculateSpawnDistance();
	}
	
	// Update is called once per frame
	void Update ()
	{
		float deltaRadians = m_speedInputRadPerSecond * GameStateScript.Instance.NNUKE_GetWorldDeltaTime();
		
		m_cameraRadians += deltaRadians;
		
		ExecuteCameraMovement();
		
		// this needs to come right after camera movement as otherwise will look jaggy
		
		WeaponSystemScript.Instance.ExecuteMovement();
	}
	#endregion
		
	#region Public object methods
	#endregion
		
	#region Private object methods

	[Obfuscar.Obfuscate]
	private float ExecuteCalculateSpawnDistance()
	{
		float max = 0f;
		for (int i = 0; i < m_frustrumPlanes.Length; i++)
		{
			max = Mathf.Max (max, m_frustrumPlanes[i].distance);
		}
		// HACK calculate real point just outside of view frustrum at certain depth
		return 0.5f*max + SpawnRadiusPadding;
	}
	
	[Obfuscar.Obfuscate]
	public bool IsObjectInPlayField(Collider collider)
	{
		Vector3 vp = ActualCamera.WorldToViewportPoint(collider.transform.position);
		return (vp.x > -0.1f && vp.x < 1.1f && vp.y > -0.1f && vp.y < 1.1f);
	}

	[Obfuscar.Obfuscate]
	public void SetCameraSpeed(float normalizedSpeed)
	{
		#if DEBUG_TEXTS
		Debug.Log (string.Format (
			"GamePlayCamera.SetCameraSpeed() - normalizedSpeed: {0}, m_speedMaxRadPerSecond: {1}"
				, normalizedSpeed, m_speedMaxRadPerSecond));
		#endif
		
		m_speedInputRadPerSecond = m_speedMaxRadPerSecond * normalizedSpeed;
		
	}
	
	[Obfuscar.Obfuscate]
	public bool NeedToFlip()
	{
		return ActualCamera.transform.position.x < m_cameraRotateOrigin.x;
	}
	
	[Obfuscar.Obfuscate]
	private void ExecuteCameraMovement()
	{
		if (PlayerHomePlanetScript.Instance == null || PlayerHomePlanetScript.Instance.CheckIfDead())
			return;
		
		Vector3 playerPos = PlayerHomePlanetScript.Instance.transform.position;
		
		if (RotateWorld && ActualCamera != null)
		{
			if (ActualCamera.orthographic)
			{
				ActualCamera.transform.Translate(playerPos);
			}
			else
			{
				// we want to rotate our camera on a sphere that is a constant distance from the target,
				// rotating around it -> basically this is a circle on z-plane
				
				// first, set the position
				ActualCamera.transform.position = 
					new Vector3(m_cameraRotateOrigin.x + m_cameraRotationRadius * Mathf.Cos (m_cameraRadians)
						, m_cameraRotateOrigin.y + m_cameraRotationRadius * Mathf.Sin (m_cameraRadians)
						, m_cameraRotateOrigin.z);
			}
	
			CameraUp = m_cameraRotateOrigin - ActualCamera.transform.position;
			
			CameraLook = playerPos - ActualCamera.transform.position;
			CameraLeft = Vector3.Cross(CameraLook, CameraUp);
			
			if (ActualCamera.orthographic)
			{
				// TODO: jos pelikamera ortografinen ...
			}
			else
			{
				// look at the target
				ActualCamera.transform.LookAt(playerPos, CameraUp);
				
				LookNonTilted = ActualCamera.transform.forward;
				LookNonTilted.z = 0f;
				LookNonTilted.Normalize();
				
				// tilt the camera a bit so that we're not forced to have the target in the middle of the screen
				ActualCamera.transform.RotateAround(CameraLeft, TiltAngleDegrees * Mathf.Deg2Rad);
			}	
			
			CameraLeft.Normalize();
			CameraLook.Normalize();
			CameraUp.Normalize();
			
			if (ActualCamera.orthographic)
			{
				ActualCamera.transform.Translate(-1f * playerPos);
			}
		}
	}
	
	#endregion	
}
