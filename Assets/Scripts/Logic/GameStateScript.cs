using UnityEngine;
using System.Collections;

sealed public class GameStateScript : MonoBehaviour 
{
	
	/// <summary>
	/// Game state. In what states we can be in-game. Updating logic is different for reach states.
	/// </summary>
	public enum GameState
	{
		NotInGame,
		InGameLive,
		InGamePause,
		InGamePlayerDead
	};
	
	#region Public GUI tweakable members	
	public InfoBoxScript m_infoBox;
	#endregion
	
	#region Public members, not visible in GUI		
	[HideInInspector]
	public static GameStateScript Instance
	{
		get { return sm_instance; }
	}
	[HideInInspector]
	public GameStateScript.GameState State
	{
		get { return m_gameState; }
	}	
	private GameState m_gameState = GameState.NotInGame;
	
	[HideInInspector]		
	public bool ShootingState
	{
		set
		{
			if (WeaponSystemScript.Instance != null)
			{
				WeaponSystemScript.Instance.Shoot(value);
			}
		}
	}
	
	[HideInInspector]		
	public bool InputDisabled
	{
		set { m_inputDisabled = value; }
		get { return m_inputDisabled; }
	}
	
	[HideInInspector]	
	public float PlayTime;
	
	#endregion
	
	#region Private data members
	private static GameStateScript sm_instance = null;

	private float m_dontAcceptInputTime = -1f;
	private bool m_inputDisabled = false;
	
	#endregion 

	#region Unity MonoBehaviour base overrides
	// Use this for early initialization
	void Awake ()
	{
		// make sure we have our persistent save data initialized
		SaveData saveData = SaveData.Instance;
		sm_instance = this;
	}

	// Use this for initialization
	void Start () 
	{
		m_gameState = GameState.InGameLive;
		PlayTime = NNUKE_GetWorldTime();
		m_inputDisabled = false;
		
		PlayerHomePlanetScript.Instance.Died += new PlayerHomePlanetScript.PlayerDied(PlayerDied);
		
	}
	void OnDestroy()
	{
		m_gameState = GameState.NotInGame;
		sm_instance = null;
	}
	
	[Obfuscar.Obfuscate]
	private void UpdateTimeScale()
	{
		switch (m_gameState)
		{
		case GameState.InGameLive:
		case GameState.InGamePlayerDead:
			Time.timeScale = 1f;
			break;
		case GameState.InGamePause:
			Time.timeScale = 0f;
			break;
		default:
			break;
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		UpdateTimeScale();
	}
	
	#endregion
	
	#region Public object methods
	[Obfuscar.Obfuscate]
	public void ShowInfoBox()
	{
		m_infoBox.Show(
			new string[] {
				"menu_ingame_help_topic"
			}
			, new string[] {
				"menu_ingame_help_info_1"
			}
			, 1f
		);
		
		SetToPause();
		InputDisabled = true;
		
		if (AssignGamepadButtonsScript.Instance != null)
		{
			AssignGamepadButtonsScript.Instance.AssignButtons(true);
		}
	}

	[Obfuscar.Obfuscate]
	public bool DontAcceptInput()
	{
		return NNUKE_GetWorldTime() < m_dontAcceptInputTime || m_inputDisabled;
	}
	[Obfuscar.Obfuscate]
	public void SetDontAcceptInputTime(float deltaTime)
	{
		m_dontAcceptInputTime = NNUKE_GetWorldTime() + deltaTime;
		ShootingState = false;
	}
	
	[Obfuscar.Obfuscate]
	public void ReturnToPlay()
	{
		m_gameState = GameStateScript.GameState.InGameLive;
		
		MusicManagerScript.Instance.Pause (false);
	
		UpdateTimeScale();
	}
	[Obfuscar.Obfuscate]
	public void SetToPause()
	{
		m_gameState = GameStateScript.GameState.InGamePause;
		ShootingState = false;
		UpdateTimeScale();
		
		MusicManagerScript.Instance.Pause(true);
	}
	
	public void PlayerDied(PlayerHomePlanetScript player)
	{
		m_gameState = GameState.InGamePlayerDead;
	}
	
	[Obfuscar.Obfuscate]
	public float NNUKE_GetWorldTime()
	{
		return Time.time;
	}

	[Obfuscar.Obfuscate]
	public float NNUKE_GetWorldDeltaTime()
	{
		return Time.deltaTime;
	}

	#endregion
	
	#region Private object methods
	#endregion
}