//#define DEBUG_TEXT
//#define DEBUG_DRAW

#if UNITY_EDITOR || UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN || UNITY_WEBPLAYER || UNITY_FLASH
#define USE_MOUSE
#endif

#if UNITY_IPHONE || UNITY_ANDROID
#define USE_TOUCH
#endif

using UnityEngine;
using System.Collections;

sealed public class InputRotateScript : MonoBehaviour 
{
	#region Public members, visible in Unity GUI
	public Camera UsedCamera;
	public Collider ColliderWheel;
	
	public GameObject m_glowSprite;
	public GameObject m_projectionsSprite;
	
	public bool m_sticky;
	#endregion

	#region Public static members
	public static bool sm_gotSomeRaycasts = false;
	#endregion

	#region Private data members
	private bool m_takingInput;
	
	private Vector3 m_currentInputPoint;
	private Vector3 m_previousInputPoint;
	private int m_currentInputPointCount;
	
	// in view port space
	private float m_stickyDistanceLimit = 0.1f;
	private float m_stickyDistanceLimitSquared;
	
	private const float ROTATE_RADIANS_MAX = 10f * Mathf.Deg2Rad;
	
	#endregion

	#region Unity MonoBehaviour base overrides
	void Awake()
	{
		m_stickyDistanceLimitSquared = m_stickyDistanceLimit * m_stickyDistanceLimit;
		Utility.SetState (m_glowSprite, false, false);
	}
	
	// Update is called once per frame
	void Update ()
	{
#if USE_TOUCH
		if (Application.isEditor || OuyaInputWrapper.UseGamepad)
			return;
		
		if (GameStateScript.Instance.InputDisabled)
		{
			if (m_takingInput)
				HandleLastInputPosition(Vector3.zero);
			return;
		}
		if (GameStateScript.Instance.State != GameStateScript.GameState.InGameLive)
			return;
		
		UpdateTouchScreenInput();
#endif
	}
		
#if USE_MOUSE
	public void OnMouseDown()
	{
		sm_gotSomeRaycasts = false;
		HandleFirstInputPosition(Input.mousePosition);
	}
	
	public void OnMouseUp()
	{
		HandleLastInputPosition(Input.mousePosition);
		sm_gotSomeRaycasts = false;
	}
	
	public void OnMouseDrag()
	{
		sm_gotSomeRaycasts = false;
		HandleInputPosition(Input.mousePosition);
		if (!sm_gotSomeRaycasts)
		{
			HandleLastInputPosition(Input.mousePosition);
		}
	}
#endif
	
	#endregion
		
	#region Private object methods
	
	[Obfuscar.Obfuscate]
	private bool ExecuteGUIElementRaycasting(Vector3 inputPoint)
	{
		#if DEBUG_DRAW
		Utility.DrawCircleXY(inputPoint, 0.05f, 10, Color.green);
		Debug.DrawRay(inputPoint, 10f*Vector3.forward, Color.red);
		#endif
		
		Ray ray = UsedCamera.ScreenPointToRay(inputPoint);
		
		#if DEBUG_DRAW
		Utility.DrawCircleXY(ray.origin, 0.05f, 10, Color.green);
		Debug.DrawRay(ray.origin, 10f*ray.direction, Color.red);
		#endif
		
		#if DEBUG_TEXT
		Debug.Log (string.Format ("InputRotateScript.ExecuteGUIElementRaycasting ray: {0} - {1}, colWheel pos: {3} (time {2})"
			, ray.origin, ray.direction, Time.time, ColliderWheel.transform.position));
		#endif
		
		bool hitGui = false;
		
		RaycastHit[] hits = Physics.RaycastAll(ray);
		
		foreach (RaycastHit hit in hits)
		{
			#if DEBUG_TEXT
			Debug.Log (string.Format ("ExecuteGUIElementRaycasting collided to: {0} (time: {1})", hit.collider.gameObject.name, Time.time));
			#endif
			
			if (hit.collider == ColliderWheel)
			{	
				m_currentInputPointCount++;

				m_previousInputPoint = m_currentInputPoint;
				m_currentInputPoint = hit.point;
				if (m_currentInputPointCount > 1)
				{
					ExecuteWheel();
				}
				hitGui = true;
			}
		}
		if (!hitGui && m_sticky && m_currentInputPointCount > 1)
		{
			Vector3 rayOrigin = new Vector3(ray.origin.x, ray.origin.y, ColliderWheel.transform.position.z);
			float distanceSquared = (m_currentInputPoint - rayOrigin).sqrMagnitude;
			
			#if DEBUG_TEXT
			Debug.Log (
				string.Format (
					"ExecuteGUIElementRaycasting ray origin: {0} m_currentInputPoint: {1} distanceSquared: {2} , limit: {3} (time: {4})"
				, rayOrigin,m_currentInputPoint, distanceSquared, m_stickyDistanceLimitSquared, Time.time));
			#endif
			
			if (distanceSquared < m_stickyDistanceLimitSquared)
			{
				m_previousInputPoint = m_currentInputPoint;
				m_currentInputPoint = rayOrigin;
				ExecuteWheel();
				hitGui = true;
			}
		}

		return hitGui;
	}
	
	[Obfuscar.Obfuscate]
	private void ExecuteWheel()
	{
		Vector3 previousDirection = m_previousInputPoint - ColliderWheel.transform.position;
		Vector3 direction = m_currentInputPoint - ColliderWheel.transform.position;
		float prevAngleRad = Utility.VectorToAngleRad (previousDirection);
		float angleRad = Utility.VectorToAngleRad (direction);
				
		#if DEBUG_TEXT
		Debug.Log (string.Format ("ExecuteWheel dir x/y: {0} / {1} (prevDir x/y: {2} / {3}  (time: {4})"
		, direction.x, direction.y
		, previousDirection.x, previousDirection.y
		, Time.time
		));
		#endif
			
		if (angleRad > 0f && angleRad < 0.5f * Mathf.PI && prevAngleRad > Mathf.PI) 
			prevAngleRad = prevAngleRad - 2f*Mathf.PI;
		if (angleRad > 1.5f*Mathf.PI && prevAngleRad > 0f && prevAngleRad < 0.5f*Mathf.PI) 
			prevAngleRad = prevAngleRad + 2f*Mathf.PI;
		
		float angleDeltaRad = angleRad - prevAngleRad;
		if (angleDeltaRad > ROTATE_RADIANS_MAX)
			angleDeltaRad = ROTATE_RADIANS_MAX;
		else if (angleDeltaRad < -ROTATE_RADIANS_MAX)
			angleDeltaRad = -ROTATE_RADIANS_MAX;
		
		// normalize
		m_projectionsSprite.transform.RotateAround(Vector3.forward, -angleDeltaRad);
		m_glowSprite.transform.RotateAround(Vector3.forward, angleDeltaRad);
	
		#if DEBUG_TEXT
		Debug.Log (string.Format ("ExecuteWheel angleRad: {0} (prevRad: {1}), delta: {2} (time: {3})"
			, angleRad
			, prevAngleRad
			, angleDeltaRad
			, Time.time
		));
		#endif
			
		GamePlayCamera.Instance.SetCameraSpeed(angleDeltaRad / ROTATE_RADIANS_MAX);
	}
	
	[Obfuscar.Obfuscate]
	private void HandleFirstInputPosition(Vector3 inputPosition)
	{
		// do normal screen-to-world conversion
		m_takingInput = true;
		m_currentInputPointCount = 0;
		HandleInputPosition(inputPosition);
	}
	
	[Obfuscar.Obfuscate]
	private void HandleLastInputPosition(Vector3 inputPosition)
	{
		m_currentInputPointCount = 0;
		m_takingInput = false;
		
		GamePlayCamera.Instance.SetCameraSpeed(0f);
	}
	
	[Obfuscar.Obfuscate]
	private void HandleInputPosition(Vector3 inputPosition)
	{
		if (ExecuteGUIElementRaycasting(inputPosition))
		{
			sm_gotSomeRaycasts = true;
		}
	}
	
#if USE_TOUCH
	
	[Obfuscar.Obfuscate]
	private void UpdateTouchScreenInput()
	{
		int touchCount = 0;
		
		sm_gotSomeRaycasts = false;
		
		foreach (Touch touch in Input.touches) 
		{
			if (touch.phase != TouchPhase.Ended && touch.phase != TouchPhase.Canceled)
			{
				touchCount++;
				
				if (m_takingInput)
					HandleInputPosition(touch.position);
				else
				{
					HandleFirstInputPosition(touch.position);
				}
			}
		}
		
		if ((touchCount == 0 && m_takingInput) || !sm_gotSomeRaycasts)
		{
			HandleLastInputPosition(m_previousInputPoint);
		}
	}
#endif

	
#endregion
}
