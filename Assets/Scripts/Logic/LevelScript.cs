//#define ENABLE_LUMOS

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

sealed public class LevelScript : MonoBehaviour 
{
	#region Enums, structs, and inner classes
	
	public enum Result
	{
		Ongoing,
		Failure,
		Success
	}
	#endregion

	#region Delegates And Events
	public delegate void LevelStarted(LevelScript sender);
	public event LevelStarted Started;
	public delegate void LevelEnded(LevelScript sender);
	public event LevelEnded Ended;
	#endregion

	#region Public members, visible in Unity GUI
	public int m_bonusScoreFromFullEnergy;
	public int m_bonusScoreFromAllEnemies;
	public int m_bonusScoreFromAccuracy;
	
	public bool m_isNeverEnding;
	
	public bool m_calculateLevelPercentageFromEnergy;
	
	#endregion

	#region Public members, not visible to Unity GUI
	[HideInInspector]
	public Result Outcome;

	[HideInInspector]
	public int TotalCountOfEnemiesInAllWaves
	{ 
		get { return m_totalCountOfEnemiesInAllWaves; }
	}
	[HideInInspector]
	public int EnemiesDied
	{
		get { return m_enemiesDied; }
	}
	[HideInInspector]
	public float StartTime
	{
		get { return m_levelStartTime; }
	}
	[HideInInspector]
	public float StopTime
	{
		get { return m_levelStopTime; }
	}
	[HideInInspector]
	public int CurrentWaveOrder
	{
		get { return m_currentWaveOrder; }
	}
	
	public static LevelScript Instance
	{
		get { return m_instance; }
	}
	
	#endregion

	#region Private data members
	private static LevelScript m_instance = null;
	
	private List<AttackWaveScript> m_activeAllAttackWaves = new List<AttackWaveScript>();
	private List<AttackWaveScript> m_activeNormalAttackWaves = new List<AttackWaveScript>();

	private List<AttackWaveScript> m_normalAttackWaves = new List<AttackWaveScript>();
	private List<AttackWaveScript> m_constantAttackWaves = new List<AttackWaveScript>();

	private List<DamageTakerScript> m_enemiesAlive = new List<DamageTakerScript>();
	private HashSet<int> m_uniqueOrders = new HashSet<int>();
	
	private int m_currentWaveOrder;
	private int m_handledWaveOrdersCount;
	private int m_totalCountOfEnemiesInAllWaves;
	private int m_enemiesDied;
	
	private float m_levelStartTime;
	private float m_levelStopTime;
	
	private int m_scheduledAttackWaveIndex;
	
	#endregion


	#region Unity MonoBehaviour base overrides
	void Awake()
	{
		// ensure level has been assigned properly
		// NOTE! mostly this is an issue if running from Editor, directly some scene
		SaveData.Instance.NNUKE_ValidateCurrentLevel();
		
		// this will be our only alive level at any given moment, so store it as singleton
		m_instance = this;
		
		m_levelStartTime = GameStateScript.Instance.NNUKE_GetWorldTime();
	}
	
	// Use this for initialization
	void Start ()
	{
		// we want to know when player dies
		PlayerHomePlanetScript.Instance.Died += new PlayerHomePlanetScript.PlayerDied(PlayerDied);
		
		Ended += new LevelEnded(PlayerHomePlanetScript.Instance.LevelEnded);
	
		GameStateScript.Instance.InputDisabled = false;
		Outcome = Result.Ongoing;
		
		m_enemiesDied = 0;
		m_totalCountOfEnemiesInAllWaves = 0;
		
		// we take all attackwaves, and will start the first in order
		AttackWaveScript[] waves = FindObjectsOfType(typeof(AttackWaveScript)) as AttackWaveScript[];
		int smallestOrder = int.MaxValue;
		foreach (AttackWaveScript attackWave in waves)
		{
			if (attackWave.IncludedInLevel)
			{
				if (attackWave.m_constant)
				{
					// "constant" attack wave, these start again over and over again,
					// until normal level progression is finished
					
					m_constantAttackWaves.Add (attackWave);
				}
				else
				{
					// normal attack wave, taken into account on level progression
					// (e.g. when there have ended, level ends)
					
					m_normalAttackWaves.Add(attackWave);
					smallestOrder = Mathf.Min (smallestOrder, attackWave.Order);
					m_uniqueOrders.Add (attackWave.Order);
					
					foreach (int eCount in attackWave.EnemyCounts)
						m_totalCountOfEnemiesInAllWaves += eCount;
				}
			}
		}
		m_scheduledAttackWaveIndex = 0;
		StartAttackWavesOfOrder(smallestOrder);
		
		foreach (AttackWaveScript wave in m_constantAttackWaves)
		{
			wave.Launched += new AttackWaveScript.AttackWaveLaunched(
					PlayerActionsScript.Instance.AttackWaveLaunched);
			wave.Ended += new AttackWaveScript.AttackWaveEnded(AttackWaveEnded);
			wave.DoStartCountDown();
			
			m_activeAllAttackWaves.Add (wave);
		}
		
		if (Started != null)
			Started(this);
		
		MusicManagerScript.Instance.PlayClip("keoma_level_01", MusicManagerScript.SwitchMode.Instant);
	}
	
	public void PlayerDied(PlayerHomePlanetScript player)
	{
		Outcome = Result.Failure;

		EndLevel();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (SaveData.Instance.Config.ShowInGameHelp)
		{
			SaveData.Instance.Config.ShowInGameHelp = false;
			SaveData.Instance.NNUKE_Save();
			
			GameStateScript.Instance.ShowInfoBox();
		}
		
		UpdateAlarmLevel();

		if (Outcome != Result.Ongoing)
		{
			return;
		}
		
		if (GameStateScript.Instance.State == GameStateScript.GameState.InGamePause)
		{
			return;
		}
		
		// run out of normal attack waves?
		if (m_activeNormalAttackWaves.Count == 0)
		{
			int nextWaveOrder = GetNextOrder(m_currentWaveOrder);
			
			if (nextWaveOrder > -1)
			{
				StartAttackWavesOfOrder(nextWaveOrder);
			}
			else if (!m_isNeverEnding)
			{
				Outcome = Result.Success;
				EndLevel();
			}
		}
		
		if (m_activeAllAttackWaves.Count == 0)
			return;
		
		// normal attack wave scheduling, only run one per frame
		// to reduce congestion
		if (m_scheduledAttackWaveIndex >= m_activeAllAttackWaves.Count)
			m_scheduledAttackWaveIndex = 0;
		
		m_activeAllAttackWaves[m_scheduledAttackWaveIndex].Process();
		m_scheduledAttackWaveIndex++;

	}
	#endregion
		
	#region Public object methods
	public void AttackWaveEnded(AttackWaveScript wave)
	{
		if (wave.m_constant)
		{
			// start again 
			wave.DoStartCountDown();
		}
		else
		{
			m_activeAllAttackWaves.Remove (wave);
			m_activeNormalAttackWaves.Remove (wave);
		}
	}
	public void DamageTakerSpawned(DamageTakerScript enemy)
	{
		m_enemiesAlive.Add(enemy);
	}
	
	
	public void DamageTakerDied(DamageTakerScript source)
	{
		m_enemiesAlive.Remove(source);
		if (source.CountAsPartOfAttackWave)
			m_enemiesDied++;
	}

		[Obfuscar.Obfuscate]
	public float GetCompletionPercentage()
	{
		if (m_calculateLevelPercentageFromEnergy)
		{
			float totalInitialEnergy = 0.001f; // just to bypass possible zero-division
			float totalCurrentEnergy = 0f;
			foreach (DamageTakerScript enemy in m_enemiesAlive)
			{
				totalInitialEnergy += enemy.EnergyInitial;
				totalCurrentEnergy += enemy.Energy;
			}
			return 1f - (totalCurrentEnergy / totalInitialEnergy);
		}
		else
		{
			float divider = Mathf.Max (1f, (float) TotalCountOfEnemiesInAllWaves);
			return EnemiesDied / divider;
		}
	}
	
	[Obfuscar.Obfuscate]
	public DamageTakerScript GetClosestEnemyTo(Vector3 position, float seekRadius)
	{
		if (m_enemiesAlive.Count == 0)
			return null;
		
		DamageTakerScript closest = m_enemiesAlive[0];
		float closestSquaredDistance = (closest.transform.position - position).sqrMagnitude;
		for (int i = 1; i < m_enemiesAlive.Count; i++)
		{
			DamageTakerScript enemy = m_enemiesAlive[i];
			float squaredDistance = (enemy.transform.position - position).sqrMagnitude;
			if (squaredDistance < closestSquaredDistance)
			{
				closest = enemy;
				closestSquaredDistance = squaredDistance;
			}
		}
		return (closestSquaredDistance < seekRadius*seekRadius) ? closest : null;
	}	
	#endregion
	
	#region Private object methods
	[Obfuscar.Obfuscate]
	private void UpdateAlarmLevel()
	{
		// get the closest enemy and set alarm factor
		float alarmFactor = 1f;
		foreach (AttackWaveScript aw in m_activeAllAttackWaves)
		{
			alarmFactor = Mathf.Min (alarmFactor, aw.ClosestEnemyFactor);
		}
		if (PlayerHomePlanetScript.Instance != null)
			PlayerHomePlanetScript.Instance.SetAlarmFactor(alarmFactor);
	}
	
	[Obfuscar.Obfuscate]
	private void EndLevel()
	{
		GameStateScript.Instance.ShootingState = false;

		m_levelStopTime = GameStateScript.Instance.NNUKE_GetWorldTime();
		PlayerHomePlanetScript.Instance.PlayTime = m_levelStopTime - m_levelStartTime;
		
		SaveData data = SaveData.Instance;
		string worldName = data.Config.SelectedWorld;
		xsd_ct_WorldInfo selectedWorld = data.NNUKE_GetWorld(worldName);
		int level = selectedWorld.SelectedLevel;
		
		#if ENABLE_LUMOS
		Lumos.Event(string.Format ("LevelEnd - World {0} Level {1} - Result {2}"
			, worldName, level, Outcome.ToString()));
		#endif
			
		if (Outcome == Result.Success || m_isNeverEnding)
		{
			// bonus from energy
			float energyAsPct = PlayerHomePlanetScript.Instance.DamageTaker.EnergyNormalized;
			energyAsPct = Mathf.Max(energyAsPct, 0f);
			int bonus = (int) (m_bonusScoreFromFullEnergy * energyAsPct);
			PlayerHomePlanetScript.Instance.BonusHealth = bonus;
			PlayerHomePlanetScript.Instance.Score += bonus;
			
			// bonus from killed enemies ratio
			bonus = 0;
			if (PlayerHomePlanetScript.Instance.EnemiesEncountered > 0)
			{
				bonus = (int) 
					(1f * m_bonusScoreFromAllEnemies
						* PlayerHomePlanetScript.Instance.EnemiesKilled 
						/ PlayerHomePlanetScript.Instance.EnemiesEncountered);
				bonus = Mathf.Max (0, bonus);
			}
			PlayerHomePlanetScript.Instance.BonusEnemies = bonus;
			PlayerHomePlanetScript.Instance.Score += bonus;
			PlayerHomePlanetScript.Instance.Score = Mathf.Max (PlayerHomePlanetScript.Instance.Score, 0);
			
			// bonus from firing accuracy
			bonus = 0;
			if (PlayerHomePlanetScript.Instance.BulletsFired > 0)
			{
				bonus = (int) 
					(1f * m_bonusScoreFromAccuracy
						* PlayerHomePlanetScript.Instance.BulletsHit 
						/ PlayerHomePlanetScript.Instance.BulletsFired);
				bonus = Mathf.Max (0, bonus);
			}
			PlayerHomePlanetScript.Instance.BonusBullets = bonus;
			PlayerHomePlanetScript.Instance.Score += bonus;
			
			// cache the obtained star-count, as this is used later
			PlayerHomePlanetScript.Instance.StarCountForScore = GameData.Instance.NNUKE_GetStarCountFor(
				selectedWorld.ReferencesTo, level
				, PlayerHomePlanetScript.Instance.Score);
			
			// update save data
			SaveData.Instance.Config.Credits = 
				SaveData.Instance.Config.Credits + PlayerHomePlanetScript.Instance.Credits;
			PlayerHomePlanetScript.Instance.MadeNewHighScore = 
				ScoresManagerScript.Instance.UpdateHighScoreCurrent(PlayerHomePlanetScript.Instance.Score);
			
			// open next level (also, save at this point)
			SaveData.Instance.NNUKE_OpenNextLevel();
			SaveData.Instance.NNUKE_Save();
		}
		else if (Outcome == Result.Failure)
		{
			// in this case we let the player keep the earned credits ...
			SaveData.Instance.Config.Credits = 
				SaveData.Instance.Config.Credits + PlayerHomePlanetScript.Instance.Credits;
			SaveData.Instance.NNUKE_Save();
		}

		if (Ended != null)
			Ended(this);
	}
	
	[Obfuscar.Obfuscate]
	private int GetNextOrder(int currentOrder)
	{
		int smallestOfTheBiggest = int.MaxValue;
		bool found = false;
		foreach (AttackWaveScript wave in m_normalAttackWaves)
		{
			if (wave.Order > currentOrder && wave.Order < smallestOfTheBiggest)
			{
				smallestOfTheBiggest = wave.Order;
				found = true;
			}
		}
		return (found) ? smallestOfTheBiggest : -1;
	}

	[Obfuscar.Obfuscate]
	private void StartAttackWavesOfOrder(int order)
	{
		m_currentWaveOrder = order;
		
		foreach (AttackWaveScript wave in m_normalAttackWaves)
		{
			if (wave.Order == m_currentWaveOrder)
			{
				wave.Launched += new AttackWaveScript.AttackWaveLaunched(
					PlayerActionsScript.Instance.AttackWaveLaunched);
				
				// we want to know when the wave ends
				wave.Ended += new AttackWaveScript.AttackWaveEnded(AttackWaveEnded);
				m_activeNormalAttackWaves.Add (wave);
				m_activeAllAttackWaves.Add (wave);
				wave.DoStartCountDown();
			}
		}
		m_handledWaveOrdersCount++;
	}
	#endregion
}
