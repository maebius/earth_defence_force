using UnityEngine;
using System.Collections;

sealed public class LifeTimeScript : MonoBehaviour 
{
	#region Public members, visible in Unity GUI
	public float LifeTimeSeconds;
	
	/// <summary>
	/// What visualization we use for the moment of destroction, if any.
	/// </summary>
	public GameObject VisualizationPrefab;
	
	/// <summary>
	/// What sound we play when we destroy object, if any.
	/// </summary> 
	public AudioClip m_sound;

	#endregion

	#region Private data members
	private float m_endMyLifeTimeSeconds;
	#endregion


	#region Unity MonoBehaviour base overrides
		
	// Use this for initialization
	void Start ()
	{
		m_endMyLifeTimeSeconds = GameStateScript.Instance.NNUKE_GetWorldTime() + LifeTimeSeconds;
	}

	// Update is called once per frame
	void Update ()
	{
		if (GameStateScript.Instance.NNUKE_GetWorldTime() > m_endMyLifeTimeSeconds)
		{
			if (VisualizationPrefab)
			{
				GameObject vizClone = (GameObject) Instantiate(
					VisualizationPrefab
					, transform.position
					, transform.rotation);
				vizClone.active = true;
			
			
				if (m_sound)
				{
					SoundManagerScript.PlayOneTime (vizClone.gameObject, m_sound);
				}
			}
			DamageTakerScript dts = gameObject.GetComponent<DamageTakerScript>();
			if (dts)
			{
				dts.MakeDisappear();
			}
			else
			{
				Destroy(gameObject);
			}
		}
	}
	#endregion
		
}
