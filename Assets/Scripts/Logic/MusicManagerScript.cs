//#define DEBUG_TEXTS

using UnityEngine;
using System.Collections;

sealed public class MusicManagerScript : MonoBehaviour 
{
	#region Enums, structs, and inner classes
	public enum SwitchMode
	{
		Instant
		, CrossFade
		, WaitCurrentEnd
	}
	#endregion

	#region Delegates And Events
	#endregion

	#region Public members, visible in Unity GUI
	public AudioClip[] MusicClips;
	public float[] MusicVolumes;
	public float CrossFadeSeconds;
	
	public bool Enabled
	{
		set 
		{
			m_enabled = value;
			m_currentSource.enabled = m_enabled;
			m_nextSource.enabled = m_enabled;
		}
		get { return m_enabled; }
	}
	
	private const float MUTE_LEVEL_LIMIT = 0.01f;
	private bool m_muted = false;
	private float m_masterVolume = 1f;
	private float m_previousMasterVolume = 1f;

	public bool Muted
	{
		set 
		{
			MasterVolume = (m_muted) ? m_previousMasterVolume : 0f;
		}
		get { return m_muted; }
	}
	
	public float MasterVolume
	{
		set 
		{
			m_previousMasterVolume = m_masterVolume;
			
			m_muted = value <= MUTE_LEVEL_LIMIT;
			m_masterVolume = (m_muted) ? 0f : value;
			m_masterVolume = Mathf.Min (m_masterVolume, 1f);
			
			m_currentSource.volume = MasterVolume;
			m_nextSource.volume = MasterVolume;
		
			SaveData.Instance.Config.MusicVolume = m_masterVolume;
		}
		get
		{
			return m_masterVolume;
		}
	}
	#endregion

	#region Private data members
	private static MusicManagerScript m_instance = null;
	
	private AudioSource m_currentSource;
	private AudioSource m_nextSource;
	
	private bool m_currentPaused = false;
	private bool m_nextPaused = false;
	
	private float m_crossFadeTime;
	private bool m_enabled;
	private bool m_doingCrossFade;
	
	private float m_targetVolume;
	
	#endregion
	
	#region Public members, not visible to Unity GUI
	[HideInInspector]
	public static MusicManagerScript Instance 
	{
		get 
		{
			if (m_instance == null)
			{
				// as we cannot instantiate MonoBehaviors directy with "new", we need to create
				// gameobject and add a component to that ...
				m_instance = Object.FindObjectOfType(typeof(MusicManagerScript)) as MusicManagerScript;

				if (m_instance == null)
				{
					// the name for the object is "arbitrary", could be anything ...
					GameObject go = new GameObject("_MusicManagerScript");
					DontDestroyOnLoad(go);
					m_instance = go.AddComponent<MusicManagerScript>();
				}
			}
			return m_instance;
		}
	}
	#endregion
	
	
	#region Unity MonoBehaviour base overrides
	void Awake () 
	{ 
		#if DEBUG_TEXTS
		Debug.Log(string.Format ("MusicManagerScript.Awake()"));
		#endif 
		
		if (m_instance == null) 
		{ 
			m_instance = this;
			DontDestroyOnLoad(gameObject); 
			m_instance.Initialize();
		} 
		else 
			Destroy(gameObject);
	}
	
	void OnApplicationPause(bool pause) 
	{
		#if DEBUG_TEXTS
		Debug.Log(string.Format ("MusicManagerScript.OnApplicationPause(pause = {0})", pause));
		#endif 
		
		Pause (pause);
	}
	
	void OnApplicationFocus(bool focus) 
	{
		#if DEBUG_TEXTS
		Debug.Log(string.Format ("MusicManagerScript.OnApplicationFocus(focus = {0})", focus));
		#endif 
		
		Pause (!focus);
	}
	
	void OnApplicationQuit() 
	{
		Pause(true);
	}
	
	#endregion

	#region Public object methods
	
	[Obfuscar.Obfuscate]
	public void StopAll()
	{
		m_currentSource.Stop();
		m_nextSource.Stop ();
	}
	[Obfuscar.Obfuscate]
	public void Pause(bool flag)
	{
		if (flag)
		{
			if (m_currentSource != null && m_currentSource.isPlaying)
			{
				m_currentSource.Pause();
				m_currentPaused = true;
			}
			if (m_nextSource != null && m_nextSource.isPlaying)
			{
				m_nextSource.Pause();
				m_nextPaused = true;
			}
		}
		else
		{
			if (m_currentSource != null && m_currentPaused)
			{
				m_currentSource.Play ();
			}
			if (m_nextSource != null && m_nextPaused)
			{
				m_nextSource.Play ();
			}
			m_currentPaused = false;
			m_nextPaused = false;
		}
	}

	[Obfuscar.Obfuscate]
	public void PlayClip(string id, SwitchMode mode, bool loop = true)
	{
		if (!Enabled)
			return;
		
		for (int index = 0; index < MusicClips.Length; index++)
		{
			if (MusicClips[index].name == id)
			{
				PlayClip (index, mode, loop);
				return;
			}
		}
		#if DEBUG_TEXTS
		Debug.LogWarning(string.Format ("MusicManagerScript.PlayClip() - did not find clip: {0}!", name));
		#endif 
	}
	
	[Obfuscar.Obfuscate]
	public void PlayClip(int index, SwitchMode mode, bool loop = true)
	{
		if (!Enabled)
			return;
		
		if (index < 0 && index >= MusicClips.Length)
		{
			return;
		}
		
		if (m_currentSource.isPlaying 
			&& ((!m_doingCrossFade && MusicClips[index].name == m_currentSource.clip.name) 
				|| m_doingCrossFade && MusicClips[index].name == m_nextSource.clip.name))
		{
			return;
		}
		if (GameStateScript.Instance != null
			&& GameStateScript.Instance.State == GameStateScript.GameState.InGamePause)
			return;

		m_targetVolume = (index < MusicVolumes.Length) ? MusicVolumes[index] : 1f;
		m_targetVolume *= MasterVolume;
		
		// need to terminate possible coroutines ...
		StopCoroutine("DoCrossFade");
		StopCoroutine("WaitForEnd");
		
		m_doingCrossFade = false;
		
		switch (mode)
		{
		case SwitchMode.CrossFade:
			m_crossFadeTime = CrossFadeSeconds;
			PlayClip (MusicClips[index], true, loop);
			break;
		case SwitchMode.Instant:
			PlayClip (MusicClips[index], false, loop);
			break;
		case SwitchMode.WaitCurrentEnd:
			if (m_currentSource.isPlaying)
			{
				m_nextSource.clip = MusicClips[index];
				m_nextSource.loop = loop;
				StartCoroutine("WaitForEnd");
			}
			else
			{
				m_currentSource.clip = MusicClips[index];
				m_currentSource.volume = m_targetVolume;
				m_currentSource.loop = loop;
				m_currentSource.Play();
			}
			break;
		default:
			break;
		}
	}
	
	#endregion
	
	#region Private object methods
	[Obfuscar.Obfuscate]
	private void Initialize()
	{
		#if DEBUG_TEXTS
		Debug.Log(string.Format ("MusicManagerScript.Initialize() audio != null => {0}", audio != null));
		#endif
		
		m_enabled = true;
		AudioSource[] sources = GetComponents<AudioSource>();
		if (sources.Length != 2)
		{
			Debug.LogError(string.Format ("MusicManagerScript.Start() we should have 2 audiosources! (now: {0})"
				, sources.Length));
		}
		m_currentSource = sources[0];
		m_nextSource = sources[1];
		
		MasterVolume = SaveData.Instance.Config.MusicVolume;
	}
	
	[Obfuscar.Obfuscate]
	private void PlayClip(AudioClip clip, bool fade, bool loop)
	{
		if (GameStateScript.Instance != null
			&& GameStateScript.Instance.State == GameStateScript.GameState.InGamePause)
			return;

		#if DEBUG_TEXTS
		Debug.Log(string.Format ("MusicManagerScript.PlayClip() clipName={0}, fade={1}", clip.name, fade));
		#endif
		
		if (fade)
		{
			m_nextSource.clip = clip;
			m_nextSource.volume = 0f;
			m_nextSource.loop = loop;
			m_doingCrossFade = true;
			StartCoroutine("DoCrossFade");
		}
		else
		{
			if (m_nextSource.isPlaying)
			{
				m_nextSource.Stop ();
			}
			if (m_currentSource.isPlaying)
			{
				m_currentSource.Stop();
			}
			m_currentSource.clip = clip;
			m_currentSource.volume = m_targetVolume;
			m_currentSource.loop = loop;
			
			m_currentSource.Play();
		}
	}
	private IEnumerator WaitForEnd()
	{
		/*
		Debug.Log(string.Format ("MusicManagerScript.WaitForEnd() from: {0}, to: {1}"
			, (m_currentSource.clip) ? m_currentSource.clip.name : ""
			, (m_nextSource.clip) ? m_nextSource.clip.name : ""));
		 */
		
		const float fastCrossFade = 0.06f;
		float crossFadeTime = m_currentSource.clip.length - fastCrossFade;
		m_crossFadeTime = fastCrossFade;
		
		while ((GameStateScript.Instance != null && GameStateScript.Instance.State == GameStateScript.GameState.InGamePause)
			|| (m_currentSource.time < crossFadeTime && m_currentSource.isPlaying))
		{
			yield return new WaitForSeconds(0.02f);
		}
		m_doingCrossFade = true;
		StartCoroutine("DoCrossFade");
		
		StopCoroutine("WaitForEnd");
	}
	
	private IEnumerator DoCrossFade()
	{
		/*
		Debug.Log(string.Format ("MusicManagerScript.DoCrossFade() from: {0}, to: {1} in {2}"
			, (m_currentSource.clip) ? m_currentSource.clip.name : ""
			, (m_nextSource.clip) ? m_nextSource.clip.name : ""
			, CrossFadeSeconds));
		*/	
		float timeCounter = 0f;
		
		if (GameStateScript.Instance != null
			&& GameStateScript.Instance.State == GameStateScript.GameState.InGamePause) 
		{
			yield return new WaitForSeconds(0.02f);
		}
		
		m_nextSource.volume = 0f;
		m_nextSource.Play();
		
		float currentVolume = m_currentSource.volume;
		
		while (timeCounter < m_crossFadeTime)
		{
			timeCounter += Time.deltaTime;
			
			float pct = timeCounter/m_crossFadeTime;
			m_nextSource.volume = m_targetVolume * pct;
			
			m_currentSource.volume = currentVolume * (1f - pct);
		
			yield return new WaitForSeconds(0.02f);
		}
		m_nextSource.volume = m_targetVolume;
		m_currentSource.volume = 0f;
		m_currentSource.Stop ();
		
		// switch
		AudioSource temp = m_currentSource;
		m_currentSource = m_nextSource;
		m_nextSource = temp;
		m_doingCrossFade = false;
		
		StopCoroutine("DoCrossFade");
	}
	
	#endregion
}
