//#define FORCE_TOUCH
//#define USE_DEBUG_FUNCTIONS
#if UNITY_EDITOR || UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN || UNITY_WEBPLAYER || UNITY_FLASH
#define USE_MOUSE
#endif

#if FORCE_TOUCH || UNITY_IPHONE || UNITY_ANDROID
#define USE_TOUCH
#endif

using UnityEngine;
using System.Collections;

sealed public class RadarControlScript : MonoBehaviour 
{
	#region Public GUI tweakable members
	public Camera UsedCamera;
	public Collider Radar;
	
	public float GamepadSpeed;
	#endregion
		
	#region Public members, not visible in GUI	
	[HideInInspector]
	public static RadarControlScript Instance
	{
		get { return m_instance; }
	}
	#endregion
	
	#region Private data members
	private static RadarControlScript m_instance = null;
	
	private bool m_takingInput;

	private Vector3 m_currentInputPoint;
	private Vector3 m_previousInputPoint;
	private int m_currentInputPointCount;
	
	private float m_radiusSquared;
	
	#endregion
		
	#region Unity MonoBehaviour base overrides
	// Use this for early initialization
	void Awake ()
	{
		m_instance = this;
	}
	
	// Use this for late initialization
	void Start ()
	{	
		m_takingInput = false;
		m_currentInputPointCount = 0;
		
		m_radiusSquared = Radar.bounds.extents.x*Radar.bounds.extents.x;
	}

	// Update is called once per frame
	void Update ()
	{
		if (GameStateScript.Instance.InputDisabled)
		{
			if (m_takingInput)
				HandleLastInputPosition(Vector3.zero);
			return;
		}
		
#if USE_TOUCH
		UpdateTouchScreenInput();
#endif
		
#if USE_DEBUG_FUNCTIONS
		if (m_takingInput)
		{
			// PLACE_HOLDER visualizing player touch input
			DrawCircleXY(m_currentInputPoint, 5f, 10, Color.red);
		}
#endif
	}
	
#if USE_MOUSE
	public void OnMouseDown()
	{
		//Debug.Log (string.Format ("RadarControlScript.OnMouseDown : {0}", Input.mousePosition));
		HandleFirstInputPosition(Input.mousePosition);
	}
	
	public void OnMouseUp()
	{
		HandleLastInputPosition(Input.mousePosition);
	}
	
	public void OnMouseDrag()
	{
		HandleInputPosition(Input.mousePosition);
	}
#endif
	
	#endregion
		
	#region Public object methods
	[Obfuscar.Obfuscate]
	public float GetRadarRadiusMax()
	{
		return Radar.bounds.extents[0];
	}
	
	[Obfuscar.Obfuscate]
	public void ResetRadarTarget()
	{
		PlayerShieldGeneratorScript.Instance.ResetShieldTarget();
	}
	
	[Obfuscar.Obfuscate]
	public void SetRadarTarget(float x, float y)
	{
		Vector3 vel = new Vector3(x, -y, 0f);
		// There's a half PI offset in player camera ...
		Quaternion q = Quaternion.AngleAxis(Mathf.Rad2Deg * GamePlayCamera.Instance.AngleRadians + 90f, Vector3.forward);
		vel = q * vel;
		vel.z = 0f;
		vel.Normalize();
		
		Vector3 newTarget = 
			PlayerShieldGeneratorScript.Instance.GetCurrentPosition() 
			+ GamepadSpeed * vel;
		
		if (IsOutsideRadarArea(newTarget))
		{
			newTarget = Radar.bounds.center + Radar.bounds.extents.x * vel;
		}
		PlayerShieldGeneratorScript.Instance.SetShieldTarget(newTarget);
	}
	
	public bool IsOutsideRadarArea(Vector3 position, float offsetSquared = 0f)
	{
		float squaredMagnitude = (position - Radar.bounds.center).sqrMagnitude;
		//Debug.Log (string.Format ("RadarControlScript.IsOutsideRadarArea - squareMag: {0}, radiusSquared {1}", squaredMagnitude, m_radiusSquared));
		return (squaredMagnitude - offsetSquared > m_radiusSquared);
	}
	
	#endregion
		
	#region Private object methods
	[Obfuscar.Obfuscate]
	private bool ExecuteGUIElementRaycasting()
	{
		Ray ray = UsedCamera.ScreenPointToRay(m_currentInputPoint);
		
		//Debug.Log (string.Format ("RadarControlScript.ExecuteGUIElementRaycasting - Ray: {0} / {1}", ray.origin, ray.direction));

		bool hitGui = false;
		
		RaycastHit[] hits = Physics.RaycastAll(ray);
		foreach (RaycastHit hit in hits)
		{
			//Debug.Log (string.Format ("RadarControlScript.ExecuteGUIElementRaycasting - hit to: {0}", hit.collider.gameObject.name));
			
			if (hit.collider == Radar)
			{
				PlayerShieldGeneratorScript.Instance.SetShieldTarget(hit.point);
				hitGui = true;
			}
		}
		return hitGui;
	}
	
	[Obfuscar.Obfuscate]
	private void HandleFirstInputPosition(Vector3 inputPosition)
	{
		// do normal screen-to-world conversion
		m_currentInputPointCount = 0;
		m_takingInput = true;
		HandleInputPosition(inputPosition);
	}
	
	[Obfuscar.Obfuscate]
	private void HandleLastInputPosition(Vector3 inputPosition)
	{
		m_takingInput = false;
	}
	
	[Obfuscar.Obfuscate]
	private void HandleInputPosition(Vector3 inputPosition)
	{
		if (UsedCamera == null)
			return;
		
		/*
		Debug.Log (string.Format ("RadarControlScript.HandleInputPosition - input: {0}, world: {1}, view: {2}"
			, inputPosition
			, UsedCamera.ScreenToWorldPoint(inputPosition)
			, UsedCamera.ScreenToViewportPoint(inputPosition)));
		*/
		
		m_currentInputPointCount++;
		if (m_currentInputPointCount == 2)
			m_previousInputPoint = m_currentInputPoint;
		
		m_currentInputPoint = inputPosition;
		
		ExecuteGUIElementRaycasting();
	}
	
#if USE_TOUCH
	[Obfuscar.Obfuscate]
	private void UpdateTouchScreenInput()
	{
		int touchCount = 0;
		foreach (Touch touch in Input.touches) 
		{
			if (touch.phase != TouchPhase.Ended && touch.phase != TouchPhase.Canceled)
			{
				touchCount++;
				if (m_takingInput)
					HandleInputPosition(touch.position);
				else
				{
					HandleFirstInputPosition(touch.position);
				}
			}
		}
		if (touchCount == 0 && m_takingInput)
		{
			HandleLastInputPosition(m_previousInputPoint);
		}
	}
#endif


	
#endregion
}
