using UnityEngine;
using System.Collections;

sealed public class SpawnedObjectScript : MonoBehaviour 
{
	#region Delegates And Events
	// A delegate type for hooking up change notifications.
	public delegate void SpawnedObjectDestroyed(SpawnedObjectScript sender);
	// An event that clients can use to be notified whenever object is destroyed
	public event SpawnedObjectDestroyed Destroyed;

	#endregion


	#region Unity MonoBehaviour base overrides
	void OnDestroy()
	{
		if (Destroyed != null)
		{
			Destroyed(this);
		}
	}
	#endregion
		
}
