
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

sealed public class SpawnerScript : MonoBehaviour 
{
	#region Public members, visible in Unity GUI
	public int ConcurrentMaximumCount;
	
	/// <summary>
	/// How much in scale we vary the spawned objects. The amount is random binominial times this value.
	/// </summary>
	public float ScaleVariation;
	/// <summary>
	/// How often we check if we spawn more objects or not.
	/// </summary>
	public float SpawnDeltaTimeSeconds;
	/// <summary>
	/// How much variation we add to the spawn check time, the amount is random ranging from zero to this value.
	/// </summary>
	public float SpawnVariationTimeSeconds;
	/// <summary>
	/// What is the probability (with 1, we always spawn) that we spawn something when checking.
	/// </summary>
	public float SpawnProbability;
	/// <summary>
	/// The minimum to spawn when check to spawn goes through.
	/// </summary>
	public int SpawnAmountMin;
	/// <summary>
	/// The maximum to spawn, so actual amount is min + (max-min)*random.
	/// </summary>
	public int SpawnAmountMax;
	/// <summary>
	/// How far from in enemys forward direction we spawn the objects
	/// </summary>
	public float SpawnOffsetRadiusFwd;
	/// <summary>
	/// How far each object is spawned from each other. Note that the objects are spawned in line, 
	/// and the middle point of the line is SpawnOffsetRadiusFwd away from the spawner.
	/// </summary>
	public float SpawnOffsetRadiusSide;
	
	/// <summary>
	/// The prefab used for spawned objects. NOTE! We probably should have several, so we would have different
	/// kinds of objects.
	/// </summary>
	public SpawnedObjectScript SpawnedObjectPrefab;
	
	/// <summary>
	/// What visualization we use for the moment of spawn, if any.
	/// </summary>
	public GameObject SpawningVisualizationPrefab;

	/// <summary>
	/// What sound we play when we spawn object, if any.
	/// </summary> 
	public AudioClip m_spawnSound;
	
	#endregion

	#region Public members, not visible to Unity GUI
	#endregion

	#region Private data members
	/// <summary>
	/// Our currently existing objects.
	/// </summary>
	private List<SpawnedObjectScript> m_objects = new List<SpawnedObjectScript>();
	/// <summary>
	/// When to check the next time if we want to spawn objects.
	/// </summary>
	private float m_nextSpawnTimeCheck;
	/// <summary>
	/// Helper to calculate count variation for objects we want to spawn, basically max-min.
	/// </summary>
	private int m_spawnCountDelta;

	#endregion


	#region Unity MonoBehaviour base overrides
		
	// Use this for initialization
	void Start ()
	{
		m_spawnCountDelta = SpawnAmountMax - SpawnAmountMin + 1;
		InitNextCheckTime();
	}

	// Update is called once per frame
	void Update ()
	{
		if (GameStateScript.Instance.NNUKE_GetWorldTime() > m_nextSpawnTimeCheck
			&& PlayerHomePlanetScript.Instance != null)
		{
			if (MathHelpers.Chance(SpawnProbability))
			{
				DoSpawnObjects();
			}
			
			InitNextCheckTime();
		}
	}
	#endregion
		
	#region Public object methods
	public void ObjectDestroyed(SpawnedObjectScript sender)
	{
		m_objects.Remove(sender);
	}
	#endregion
		
	#region Private object methods
	
	[Obfuscar.Obfuscate]
	private void InitNextCheckTime()
	{
		m_nextSpawnTimeCheck = GameStateScript.Instance.NNUKE_GetWorldTime()
			+ SpawnDeltaTimeSeconds + MathHelpers.Binominial() * SpawnVariationTimeSeconds;
		
	}
	
	[Obfuscar.Obfuscate]
	private void DoSpawnObjects()
	{
		int count = SpawnAmountMin + Random.Range (0, m_spawnCountDelta);
		count = Mathf.Min (count, ConcurrentMaximumCount - m_objects.Count);
		
		Vector3	direction = transform.rotation * Vector3.down;
		Vector3 side = transform.rotation * Vector3.left;
		direction.Normalize();
		side.Normalize();
		
		side *= SpawnOffsetRadiusSide;
		
		Vector3 start = transform.position
			+ (SpawnOffsetRadiusFwd * direction)
			- (0.5f * (count-1) * side);
		
		for (int i = 0; i < count; i++)
		{
			Spawn (new Vector3(start.x, start.y, 0f));
			
			start += side;
		}
	}
	
	[Obfuscar.Obfuscate]
	private void Spawn(Vector3 location)
	{
		SpawnedObjectScript clone = (SpawnedObjectScript) 
			Instantiate(
				SpawnedObjectPrefab
				, location
				, transform.rotation);
		
		float scalingFactor = 1f + ScaleVariation * Random.Range(0f, 1f);
		clone.transform.localScale = scalingFactor * clone.transform.localScale;
		clone.Destroyed += new SpawnedObjectScript.SpawnedObjectDestroyed(ObjectDestroyed);
		
		if (clone.GetComponent<Rigidbody>() && gameObject.GetComponent<Rigidbody>())
		{
			clone.GetComponent<Rigidbody>().velocity = gameObject.GetComponent<Rigidbody>().velocity;
		}
		
		if (SpawningVisualizationPrefab)
		{
			GameObject vizClone = (GameObject) Instantiate(
				SpawningVisualizationPrefab
				, location
				, clone.transform.rotation);
			vizClone.active = true;
		}
		
		if (m_spawnSound)
		{
			SoundManagerScript.PlayOneTime (clone.gameObject, m_spawnSound);
		}
		
		m_objects.Add (clone);
	}
	#endregion
}
