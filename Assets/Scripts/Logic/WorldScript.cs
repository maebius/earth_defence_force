using UnityEngine;
using System.Collections;
using System.Collections.Generic;

sealed public class WorldScript : MonoBehaviour 
{
	#region Enums, structs, and inner classes
	#endregion

	#region Delegates And Events
	#endregion

	#region Public members, visible in Unity GUI
	public string Name;
	public LevelScript[] Levels;
	#endregion

	#region Public members, not visible to Unity GUI
	[HideInInspector]	
	public int SelectedLevel;
	
	#endregion

	#region Private data members
	#endregion


	#region Unity MonoBehaviour base overrides
		
	// Use this for initialization
	void Start ()
	{
		SelectedLevel = -1;
	}

	// Update is called once per frame
	void Update ()
	{
		
	}
	#endregion
		
	#region Public object methods
	#endregion
		
	#region Private object methods
	#endregion
}
