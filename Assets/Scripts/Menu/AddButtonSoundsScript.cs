//#define DEBUG_TEXTS

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

sealed public class AddButtonSoundsScript : MonoBehaviour 
{
	public AudioClip m_buttonDownSound;
	public AudioClip m_buttonUpSound;
	
	public GameObject[] m_dontOperateThese;
	
	static public AddButtonSoundsScript Instance
	{
		get { return sm_instance; }
	}
	
	private HashSet<UIButtonMessage> m_operatedButtons = new HashSet<UIButtonMessage>();
	private List<UIButtonSound> m_sounds = new List<UIButtonSound>();
	private HashSet<GameObject> m_dontOperate = new HashSet<GameObject> ();
	
	private static AddButtonSoundsScript sm_instance = null;
	
	void Awake()
	{
		#if DEBUG_TEXTS
		Debug.Log(string.Format ("AddButtonSoundsScript.Awake()"));
		#endif
		
		foreach (GameObject go in m_dontOperateThese)
		{
			m_dontOperate.Add(go);
		}
		sm_instance = this;
	}
	
	void OnEnable () 
	{ 
		AddSounds ();
	}
	
	void OnDestroy()
	{
		sm_instance = null;
	}
	
	[Obfuscar.Obfuscate]
	public void AddSounds(bool reUseExisting = true)
	{
		UIButtonMessage[] objs = (UIButtonMessage[]) GameObject.FindSceneObjectsOfType(typeof(UIButtonMessage));

#if DEBUG_TEXTS
		Debug.Log (string.Format (
			"AddButtonSoundsScript.AddSounds(), buttonCount: {0}", objs.Length));
#endif
		
		foreach (UIButtonMessage button in objs)
		{
			if (m_operatedButtons.Contains(button) || m_dontOperate.Contains(button.gameObject))
				continue;

			int nextIndex = 0;
			// we have a sound already (or many)?
			if (!reUseExisting)
			{
				UIButtonSound[] sounds = button.transform.GetComponentsInChildren<UIButtonSound>(true);
				nextIndex = sounds.Length;
			}
			
			nextIndex = ApplyOrMakeSound(nextIndex, button, m_buttonDownSound, UIButtonSound.Trigger.OnPress);
			nextIndex = ApplyOrMakeSound(nextIndex, button, m_buttonUpSound, UIButtonSound.Trigger.OnRelease);
			
			m_operatedButtons.Add (button);
		}
	}
	
	[Obfuscar.Obfuscate]
	private int ApplyOrMakeSound(int startIndex, UIButtonMessage button
		, AudioClip clip, UIButtonSound.Trigger trigger)
	{
		UIButtonSound selected = null;
		UIButtonSound[] sounds = button.GetComponents<UIButtonSound>();
		
		int nextIndex = startIndex + 1;
		if (sounds == null || sounds.Length <= startIndex)
		{
			selected = button.gameObject.AddComponent<UIButtonSound>();
			if (sounds == null)
				nextIndex = 1;
			else
				nextIndex = sounds.Length+1;
		}
		else
		{
			selected = sounds[startIndex];
		}
		
		selected.audioClip = clip;
		selected.trigger = trigger;
		
		if (!m_sounds.Contains(selected))
			m_sounds.Add (selected);
		
		return nextIndex;
	}
		
	
}
