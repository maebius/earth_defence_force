//#define DEBUG_TEXTS

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

sealed public class AddSelectionColorToButtonsScript : MonoBehaviour 
{
	
	public bool OperateLabels;
	public bool OperateSprites;
	
	public string SkipSpritesIncludingString;
	
	public float IgnoreFocusSeconds = 0.2f;
	
	public GameObject[] DontOperateThese;
	
	public UnityEngine.Color LabelHoverColor;
	public UnityEngine.Color LabelPressedColor;
	public UnityEngine.Color LabelDisabledColor;
	public float LabelDuration;

	public UnityEngine.Color BgHoverColor;
	public UnityEngine.Color BgPressedColor;
	public UnityEngine.Color BgDisabledColor;
	public float BgDuration;
	
	private GameObject m_objectToGetInitialFocus = null;
	private UICamera m_initialFocusCamera = null;
	private bool m_resetInitialSelection;
	
	private List<UILabel> m_labels = new List<UILabel>();
	private List<UISlicedSprite> m_sprites = new List<UISlicedSprite>();
	
	private List<UnityEngine.KeyCode> m_activateKeys = new List<UnityEngine.KeyCode>();
	
	private HashSet<GameObject> m_dontOperate = new HashSet<GameObject> ();
	
	private HashSet<UIButton> m_operatedButtons = new HashSet<UIButton>();
	private HashSet<UIButtonColor> m_operatedButtonColors = new HashSet<UIButtonColor>();
	
	private float m_acceptFocusTimeSeconds;
	
	private bool m_addColors;
	
	private static AddSelectionColorToButtonsScript sm_instance = null;
	public static AddSelectionColorToButtonsScript Instance
	{
		get 
		{
			return sm_instance;
		}
	}
	
	void Awake()
	{
		sm_instance = this;
		
		// NOTE! This needs to be done first as later in the method we already call AddColorsToButtons, which depends
		// on this
		m_dontOperate.Clear();
		foreach (GameObject obj in DontOperateThese)
		{
			m_dontOperate.Add (obj);
		}

		m_resetInitialSelection = false;
		
		m_addColors = true;
		
#if UNITY_IPHONE
		m_addColors = false;
		m_resetInitialSelection = true;
#endif
#if UNITY_ANDROID
		if (!OuyaInputWrapper.UseGamepad)
		{
			m_addColors = false;
			m_resetInitialSelection = true;
		}
#endif
		if (Application.isEditor)
		{
			m_addColors = true;
		}
		if (m_addColors)
		{
			AddColors ();
		}
		
		UICamera[] cameras = (UICamera[]) GameObject.FindObjectsOfType(typeof(UICamera));
		foreach (UICamera cam in cameras)
		{
			if (cam.submitKey0 != KeyCode.None)
			{
				m_activateKeys.Add (cam.submitKey0);
			}
			if (cam.submitKey1 != KeyCode.None)
			{
				m_activateKeys.Add (cam.submitKey1);
			}
		}
	}
	
	void Start()
	{
		//Debug.Log (string.Format ("scriptAddSelectionColorToButtons.Start() - m_resetInitialSelection: {0} (scene: {1}), UICamera.MouseUsed: {2}, selected: {3}"
		//	, m_resetInitialSelection, Application.loadedLevelName, UICamera.MouseUsed, (UICamera.selectedObject != null) ? UICamera.selectedObject.gameObject.name : ""));
		
		m_objectToGetInitialFocus = UICamera.selectedObject;
		m_initialFocusCamera = UICamera.current;
		
		if (m_resetInitialSelection && OuyaInputWrapper.UseGamepad)
		{
			UICamera.selectedObject = null;
		}
		m_acceptFocusTimeSeconds = Time.time + IgnoreFocusSeconds;
	}
	
	void Update()
	{
		/*
		Debug.Log (string.Format (
			"selectedObject: {0}, m_activateKeys.Count: {1}, m_objectToGetInitialFocus: {2}, UICamera.MouseUsed: {3}"
			, (UICamera.selectedObject != null), m_activateKeys.Count, m_objectToGetInitialFocus != null, UICamera.MouseUsed));
		*/
		
		if (UICamera.MouseUsed && UICamera.selectedObject != null)
		{
			// if this is a text or popup-list input, then we don't want to reset
			UIInput textInput = UICamera.selectedObject.GetComponent<UIInput>();
			UIPopupList listInput = UICamera.selectedObject.GetComponent<UIPopupList>();
			if (textInput == null && listInput == null)
				UICamera.selectedObject = null;
		}
			
		if (UICamera.selectedObject == null && Time.time > m_acceptFocusTimeSeconds)
		{
			bool activate = false;
			foreach (KeyCode code in m_activateKeys)
			{
				activate = code != KeyCode.None && OuyaInputWrapper.GetKeyDown(code);
				if (activate)
					break;
			}
			
			//Debug.Log (string.Format ("scriptAddSelectionColorToButtons.Update() - activate: {0}", activate));
			
			if (activate)
			{
				// can we return to the previous?
				GameObject focus = UICamera.previousNonNullSelection;
				if (focus == null || !NGUITools.GetActive(focus)) focus = m_objectToGetInitialFocus;
				if (focus != null && (focus.GetComponent<UIInput>() != null || focus.GetComponent<UISlider>() != null))
				{
					UICamera.skipFirstSubmitOnSelected = true;
				}
				UICamera.selectedObject = focus;
			}
		}
	}
	
	[Obfuscar.Obfuscate]
	public void ResetColors()
	{
		if (m_addColors)
		{
			foreach (UILabel label in m_labels)
			{
				label.color = LabelPressedColor;
			}
			foreach (UISlicedSprite sprite in m_sprites)
			{
				sprite.color = BgPressedColor;
			}
		}
		m_acceptFocusTimeSeconds = Time.time + IgnoreFocusSeconds;
	}
	
	[Obfuscar.Obfuscate]
	public void	AddColors()
	{
		if (m_addColors)
		{
			AddColorsToButtons();
			AddColorsToButtonColors();
		}
	}
	
	[Obfuscar.Obfuscate]
	private void AddColorsToButtons()
	{
		UIButton[] objs = (UIButton[]) GameObject.FindSceneObjectsOfType(typeof(UIButton));

#if DEBUG_TEXTS
		Debug.Log (string.Format (
			"scriptAddSelectionColorToButtons.AddColorsToButtons(), buttonCount: {0}", objs.Length));
#endif
		
		foreach (UIButton button in objs)
		{
			// already handled?
			if (m_operatedButtons.Contains(button))
				continue;
			
			int nextIndex = 0;
			// we have a label (or many)?
			UILabel[] labels = button.transform.GetComponentsInChildren<UILabel>(true);
			if (OperateLabels && labels != null)
			{
				foreach (UILabel label in labels)
				{
					// do we want to skip?
					if (m_dontOperate.Contains(label.gameObject))
						continue;
					
					nextIndex = ApplyOrMakeColorTween(nextIndex, button, label.gameObject
						, LabelHoverColor, LabelPressedColor, LabelDisabledColor, LabelDuration);
				
					m_labels.Add(label);
				}
			}
			// we have a background sprite (or many)?
			UISlicedSprite[] sprites = button.transform.GetComponentsInChildren<UISlicedSprite>(true);
			if (OperateSprites && sprites != null)
			{
				foreach (UISlicedSprite sprite in sprites)
				{
					// do we want to skip?
					if (m_dontOperate.Contains(sprite.gameObject))
						continue;
					
					if (!string.IsNullOrEmpty(SkipSpritesIncludingString) 
						&& sprite.gameObject.name.Contains(SkipSpritesIncludingString))
						continue;
					
					nextIndex = ApplyOrMakeColorTween(nextIndex, button, sprite.gameObject
						, BgHoverColor, BgPressedColor, BgDisabledColor, BgDuration);
					
					m_sprites.Add(sprite);
				}
			}
			
			m_operatedButtons.Add (button);
		}
	}
	
	[Obfuscar.Obfuscate]
	private int ApplyOrMakeColorTween(int startIndex, UIButton button, GameObject obj
		, Color hoverColor, Color pressedColor, Color disabledColor, float duration)
	{
		//Debug.Log (string.Format ("scriptAddSelectionColorToButtons.ApplyOrMakeColorTween() - button-name: {0}, obj-name: {1}, startIndex: {2}, m_dontOperate.Count: {3}"
		//			, button.gameObject.name, obj.gameObject.name, startIndex, m_dontOperate.Count));
		
		UIButton selectedUIBC = null;
		UIButton[] colors = button.GetComponents<UIButton>();
		
		int nextIndex = startIndex + 1;
		if (colors == null || colors.Length <= startIndex)
		{
			selectedUIBC = button.gameObject.AddComponent<UIButton>();
			if (colors == null)
				nextIndex = 1;
			else
				nextIndex = colors.Length+1;
		}
		else
		{
			selectedUIBC = colors[startIndex];
		}
		
		selectedUIBC.tweenTarget = obj;
		selectedUIBC.hover = hoverColor;
		selectedUIBC.pressed = pressedColor;
		selectedUIBC.defaultColor = button.defaultColor;
		selectedUIBC.disabledColor = disabledColor;
		selectedUIBC.duration = duration;
		selectedUIBC.enabled = true;
		
		return nextIndex;
	}
		
	[Obfuscar.Obfuscate]
	private void AddColorsToButtonColors()
	{
		UIButtonColor[] objs = (UIButtonColor[]) GameObject.FindObjectsOfType(typeof(UIButtonColor));

#if DEBUG_TEXTS
		Debug.Log (string.Format (
			"scriptAddSelectionColorToButtons.AddColorsToButtons(), buttonCount: {0}", objs.Length));
#endif
		
		foreach (UIButtonColor button in objs)
		{
			// already handled?
			if (m_operatedButtonColors.Contains(button))
				continue;
			
			int nextIndex = 0;
			// we have a label (or many)?
			UILabel[] labels = button.transform.GetComponentsInChildren<UILabel>(true);
			if (OperateLabels && labels != null)
			{
				foreach (UILabel label in labels)
				{
					// do we want to skip?
					if (m_dontOperate.Contains(label.gameObject))
						continue;
					
					nextIndex = ApplyOrMakeColorTween(nextIndex, button, label.gameObject
						, LabelHoverColor, LabelPressedColor, LabelDuration);
				
					m_labels.Add(label);
				}
			}
			// we have a background sprite (or many)?
			UISlicedSprite[] sprites = button.transform.GetComponentsInChildren<UISlicedSprite>(true);
			if (OperateSprites && sprites != null)
			{
				foreach (UISlicedSprite sprite in sprites)
				{
					// do we want to skip?
					if (m_dontOperate.Contains(sprite.gameObject))
						continue;
					
					if (!string.IsNullOrEmpty(SkipSpritesIncludingString) 
						&& sprite.gameObject.name.Contains(SkipSpritesIncludingString))
						continue;
					
					nextIndex = ApplyOrMakeColorTween(nextIndex, button, sprite.gameObject
						, BgHoverColor, BgPressedColor, BgDuration);
					
					m_sprites.Add(sprite);
				}
			}
			
			m_operatedButtonColors.Add (button);
		}
	}
	
	[Obfuscar.Obfuscate]
	private int ApplyOrMakeColorTween(int startIndex, UIButtonColor button, GameObject obj
		, Color hoverColor, Color pressedColor, float duration)
	{
		//Debug.Log (string.Format ("scriptAddSelectionColorToButtons.ApplyOrMakeColorTween() - button-name: {0}, obj-name: {1}, startIndex: {2}, m_dontOperate.Count: {3}"
		//			, button.gameObject.name, obj.gameObject.name, startIndex, m_dontOperate.Count));
		
		UIButtonColor selectedUIBC = null;
		UIButtonColor[] colors = button.GetComponents<UIButtonColor>();
		
		int nextIndex = startIndex + 1;
		if (colors == null || colors.Length <= startIndex)
		{
			selectedUIBC = button.gameObject.AddComponent<UIButtonColor>();
			if (colors == null)
				nextIndex = 1;
			else
				nextIndex = colors.Length+1;
		}
		else
		{
			selectedUIBC = colors[startIndex];
		}
		
		selectedUIBC.tweenTarget = obj;
		selectedUIBC.hover = hoverColor;
		selectedUIBC.pressed = pressedColor;
		selectedUIBC.defaultColor = button.defaultColor;
		selectedUIBC.duration = duration;
		selectedUIBC.enabled = true;
		
		return nextIndex;
	}
}
