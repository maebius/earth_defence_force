//#define DEBUG_TEXTS

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

sealed public class AddTextEffectScript : MonoBehaviour 
{
	public UILabel[] m_dontOperateThese;
	
	public UILabel.Effect m_effect;
	public Vector2 m_distance;
	public Color m_color;
	
	private HashSet<UILabel> m_operatedLabels = new HashSet<UILabel>();
	private HashSet<UILabel> m_dontOperate = new HashSet<UILabel> ();
	
	void Awake()
	{
		#if DEBUG_TEXTS
		Debug.Log(string.Format ("AddTextEffectScript.Awake()"));
		#endif
		foreach (UILabel go in m_dontOperateThese)
		{
			m_dontOperate.Add(go);
		}
	}
	
	void OnEnable () 
	{ 
		AddEffects ();
	}
	
	[Obfuscar.Obfuscate]
	private void AddEffects()
	{
		UILabel[] objs = (UILabel[]) GameObject.FindSceneObjectsOfType(typeof(UILabel));

#if DEBUG_TEXTS
		Debug.Log (string.Format (
			"AddTextEffectScript.AddEffects(), labelCount: {0}", objs.Length));
#endif
		
		foreach (UILabel obj in objs)
		{
			// already handled or not wanted?
			if (m_operatedLabels.Contains(obj) || m_dontOperate.Contains(obj))
				continue;
			
			obj.effectStyle = m_effect;
			obj.effectDistance = m_distance;
			obj.effectColor = m_color;
			
			m_operatedLabels.Add (obj);
		}
	}
}
