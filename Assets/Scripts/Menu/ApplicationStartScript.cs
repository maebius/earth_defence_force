
using UnityEngine;
using System.Collections;

sealed public class ApplicationStartScript : MonoBehaviour 
{
	void Start() 
	{
		
#if UNITY_IPHONE || UNITY_ANDROID
		Application.runInBackground = false;
#endif
	}
}
