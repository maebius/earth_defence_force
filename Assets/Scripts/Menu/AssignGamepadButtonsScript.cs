using UnityEngine;
using System.Collections;
using System.Collections.Generic;

sealed public class AssignGamepadButtonsScript : MonoBehaviour 
{
	public bool m_assignEmpty = true;
	public bool m_requireOverlap = false;
	public bool m_forceSelectFirst = false;
	public bool m_useBoundingBoxAsStart = true;
	public bool m_useDistanceLimit = true;
	public float m_distanceLimitSquared = 4000000f;
	public bool m_assignSliderHorizontal = true;
	public bool m_assignSliderVertical = true;
	
	public UIButtonKeys[] m_skipThese;
	
	private static AssignGamepadButtonsScript sm_instance = null;
	public static AssignGamepadButtonsScript Instance
	{
		get 
		{
			return sm_instance;
		}
	}
	
	protected enum Direction
	{
		Left
		, Right
		, Down
		, Up
	}
	
	private List<UIButtonKeys> m_skip = new List<UIButtonKeys>();
	private List<UIButtonKeys> m_allKeys = new List<UIButtonKeys>();
	
	void Awake()
	{
		// we don't wanna do this anywhere else than in Ouya currently ....
		if (!OuyaInputWrapper.UseGamepad)
		{
			DestroyObject(gameObject);
			sm_instance = null;
			return;
		}
		
		sm_instance = this;
		m_skip.AddRange (m_skipThese);

		FindButtons ();
	}
	
	// Use this for initialization
	void Start () 
	{
		AssignButtons();
	}
	
	[Obfuscar.Obfuscate]
	private void FindButtons()
	{
		m_allKeys.Clear();
		
		// this is insanely slow and should not be called every frame!
		UIButtonKeys[] keys = (UIButtonKeys[]) GameObject.FindSceneObjectsOfType(typeof(UIButtonKeys));
		m_allKeys.AddRange(keys);
	}
	
	[Obfuscar.Obfuscate]
	public void AssignButtons(bool forceReInit = false)
	{
		SafeZoneManagerScript.ApplySafeZone();
		
		if (m_allKeys.Count == 0 || forceReInit)
		{
			FindButtons();
		}
		
		if (m_allKeys.Count == 0)
			return;
		
		UIButtonKeys first = null;
		UIButtonKeys firstWithSelection = null;
		
		foreach (UIButtonKeys key in m_allKeys)
		{
			// skip the ones we explicitly have defined, and inactive (both wholly and the ones that have collider disabled)
			if (m_skip.Contains(key) || !key.gameObject.active ||
				(key.gameObject.GetComponent<Collider>() != null && !key.gameObject.GetComponent<Collider>().enabled))
				continue;
			
			if (first == null) first = key;
			if (firstWithSelection == null && key.startsSelected) firstWithSelection = key;
			
			UIButtonKeys candinate = null;
			bool notSlider = (key.GetComponent<UISlider>() == null);

			if (notSlider || m_assignSliderVertical)
			{
				candinate = GetClosest(key, m_allKeys, Direction.Down, true);
				if (candinate == null && !m_requireOverlap) candinate = GetClosest(key, m_allKeys, Direction.Down, false);
				
				if (candinate != null || m_assignEmpty) key.selectOnDown = candinate;
				
				candinate = GetClosest(key, m_allKeys, Direction.Up, true);
				if (candinate == null && !m_requireOverlap) candinate = GetClosest(key, m_allKeys, Direction.Up, false);
				if (candinate != null || m_assignEmpty) key.selectOnUp = candinate;
			}
			
			if (notSlider || m_assignSliderHorizontal)
			{
				candinate = GetClosest(key, m_allKeys, Direction.Right, true);
				if (candinate == null && !m_requireOverlap) candinate = GetClosest(key, m_allKeys, Direction.Right, false);
				if (candinate != null || m_assignEmpty) key.selectOnRight = candinate;
			
				candinate = GetClosest(key, m_allKeys, Direction.Left, true);
				if (candinate == null && !m_requireOverlap) candinate = GetClosest(key, m_allKeys, Direction.Left, false);
				if (candinate != null || m_assignEmpty) key.selectOnLeft = candinate;
			}
		}
		
		if (m_forceSelectFirst)
		{
			if (!IsValidSelection(UICamera.selectedObject) && !IsValidSelection(UICamera.previousNonNullSelection))
			{
				if (firstWithSelection != null)
				{
					UICamera.selectedObject = firstWithSelection.gameObject;
				}
				else if (first != null)
				{
					UICamera.selectedObject = first.gameObject;
				}
			}
		}
	}
	
	[Obfuscar.Obfuscate]
	private bool IsValidSelection(GameObject obj)
	{
		if (obj == null)
			return false;
		UIButtonKeys keys = obj.GetComponent<UIButtonKeys>();
		if (keys == null)
			return false;
		if (!keys.gameObject.active)
			return false;
		if (keys.gameObject.GetComponent<Collider>() == null || !keys.gameObject.GetComponent<Collider>().enabled)
			return false;
		
		return true;
	}
	
	[Obfuscar.Obfuscate]
	private UIButtonKeys GetClosest(UIButtonKeys key
		, List<UIButtonKeys> keys
		, Direction dir
		, bool requireOverlap)
	{
		Vector3 myPos = key.transform.position;
		Bounds myBounds = key.GetComponent<Collider>().bounds;
		
		float myLeft = myPos.x;
		float myRight = myPos.x;
		float myTop = myPos.y;
		float myBottom = myPos.y;
		if (m_useBoundingBoxAsStart)
		{
			myLeft -= myBounds.extents.x;
			myRight += myBounds.extents.x;
			myTop += myBounds.extents.y;
			myBottom -= myBounds.extents.y;
		}
		
		UIButtonKeys closestKeys = null;
		float closestSqrDistance = float.MaxValue;
		
		foreach (UIButtonKeys k in keys)
		{
			// skip myself or inactive
			if (k == key || !k.gameObject.active || 
				(k.gameObject.GetComponent<Collider>() != null && !k.gameObject.GetComponent<Collider>().enabled)) continue;
			
			Vector3 pos = k.transform.position;
			
			// early exit if totally in wrong direction
			if (dir == Direction.Down && pos.y > myBottom)
				continue;
			else if (dir == Direction.Up && pos.y < myTop)
				continue;
			else if (dir == Direction.Left && pos.x > myLeft)
				continue;
			else if (dir == Direction.Right && pos.x < myRight)
				continue;
			
			if (!requireOverlap || DoOverlap(myBounds, k.GetComponent<Collider>().bounds, dir))
			{
				float sqrDist = (myPos - pos).sqrMagnitude;
				if (sqrDist < closestSqrDistance && (!m_useDistanceLimit || sqrDist < m_distanceLimitSquared))
				{
					closestSqrDistance = sqrDist;
					closestKeys = k;
				}
			}
		}
		return closestKeys;
	}
	
	[Obfuscar.Obfuscate]
	private bool DoOverlap(Bounds myBounds, Bounds hisBounds, Direction dir)
	{
		bool overlaps = false;
		// make copies, so not to modify the originals
		Bounds mb = new Bounds(myBounds.center, myBounds.size);
		Bounds b = new Bounds(hisBounds.center, hisBounds.size);
		if (dir == Direction.Down || dir == Direction.Up)
		{
			// we are in the same horizontal segment?
			mb.center = new Vector3(mb.center.x, 0f, 0f);
			b.center = new Vector3(b.center.x, 0f, 0f);
			overlaps = mb.Intersects(b);
		}
		else
		{
			// we are in the same vertical segment?
			mb.center = new Vector3(0f, mb.center.y, 0f);
			b.center = new Vector3(0f, b.center.y, 0f);
			overlaps = mb.Intersects(b);
		}
		return overlaps;
	}
}
