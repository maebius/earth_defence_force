using UnityEngine;
using System.Collections;

sealed public class ChangeSceneScript : MonoBehaviour 
{
	public bool ChangeAfterDelay;
	public float DelayToSwitchSceneSeconds;
	public string SceneNameToSwitch;
	
	private float m_timeToSwithScene;
	// Use this for initialization
	void Start () 
	{
		m_timeToSwithScene = Time.time + DelayToSwitchSceneSeconds;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (ChangeAfterDelay && Time.time > m_timeToSwithScene) 
		{
			DoChangeScene();
		}
	}
	
	void DoChangeScene()
	{
		Application.LoadLevel(SceneNameToSwitch);
	}
}
