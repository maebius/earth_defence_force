//#define DEBUG_TEXTS

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

sealed public class DisableCollidersManagerScript : MonoBehaviour 
{
	private List<DisableCollidersScript> m_disablers = new List<DisableCollidersScript>();
	
	void Awake()
	{
		#if DEBUG_TEXTS		
		Debug.Log(string.Format ("DisableCollidersManagerScript.Awake()"));
		#endif
		
		DisableCollidersScript[] objects = (DisableCollidersScript[]) 
			GameObject.FindSceneObjectsOfType(typeof(DisableCollidersScript));
		
		m_disablers.AddRange(objects);
	}
	
	// Use this for initialization
	void OnEnable () 
	{
		#if DEBUG_TEXTS		
		Debug.Log(string.Format ("DisableCollidersManagerScript.OnEnable() - Found scripts: {0}", m_disablers.Count));
		#endif
		
		// NOTE! This script needs to be executed after DisableCollidersScript, so that
		// this setting of those object instances to Active happens after their OnEnable has
		// been already run -> this is the intentio of this whole manager so that we
		// initially do not need to run the disabling, only later in scene
		
		foreach (DisableCollidersScript dcs in m_disablers)
		{
			dcs.Active = true;
		}
	}	
}
