//#define DEBUG_TEXTS

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

sealed public class DisableCollidersScript : MonoBehaviour 
{
	public static void ActivateColliderDisablers()
	{
		DisableCollidersScript[] objects = (DisableCollidersScript[]) GameObject.FindSceneObjectsOfType(typeof(DisableCollidersScript));
		foreach (DisableCollidersScript sdc in objects)
		{
			sdc.Active = true;
		}
	}
	public static void ReInitialize()
	{
		DisableCollidersScript[] objects = (DisableCollidersScript[]) GameObject.FindSceneObjectsOfType(typeof(DisableCollidersScript));
		foreach (DisableCollidersScript sdc in objects)
{
			sdc.m_reInitialize = true;
		}
	}
	
	public Collider[] CollidersToSkip;
	public bool Active = false;
	public bool UseInitialState = false;
	// If this is true, then we disable the collider if it has the same Layer mask than What is marked here in Layers
	public bool UseLayers;
	
	public LayerMask Layers;
	
	private Dictionary<Collider, bool> m_colliders = new Dictionary<Collider, bool>();
	private HashSet<Collider> m_notToOperate = new HashSet<Collider>();
	private bool m_reInitialize = false;

	void Awake()
	{
		#if DEBUG_TEXTS
		Debug.Log(string.Format ("DisableCollidersScript.Awake()"));
		#endif

		foreach (Collider col in CollidersToSkip)
		{
			m_notToOperate.Add (col);
			//Debug.Log(string.Format ("scriptDisableButtons.Awake() - buttonToSkip: {0}", button.name));
		}
		FindColliders();
	}
		
	void OnEnable()
	{
		if (Active)
		{
			SetCollisionState(false);
		}
	}
	void OnDisable()
	{
		if (Active)
		{
			SetCollisionState(true);
		}
	}
	
	private void FindColliders()
	{
		m_colliders.Clear ();
		Collider[] objects = (Collider[]) GameObject.FindSceneObjectsOfType(typeof(Collider));
		foreach (Collider c in objects)
		{
			// don't add if we dont want to
			if (!m_notToOperate.Contains(c) && (!UseLayers ||  (((1 << c.gameObject.layer) & Layers) != 0)))
			{
				m_colliders.Add (c, c.enabled);
			}
		}
		m_reInitialize = false;
	}
	
	[Obfuscar.Obfuscate]
	private void SetCollisionState(bool flag)
	{
		if (m_reInitialize)
			FindColliders();

		foreach (KeyValuePair<Collider, bool> col in m_colliders)
		{
#if DEBUG_TEXTS
				Debug.Log(string.Format ("DisableCollidersScript.SetCollisionState() - myName: {3}, flag: {0}, name: {1}, origState: {2}"
				, flag, (col.Key != null) ? col.Key.gameObject.name : "null", col.Value, gameObject.name));
#endif
			
			if (col.Key)
				col.Key.enabled = flag && (!UseInitialState || col.Value);
		}
		
		if (AssignGamepadButtonsScript.Instance != null)
		{
			AssignGamepadButtonsScript.Instance.AssignButtons(true);
		}
	}
}
