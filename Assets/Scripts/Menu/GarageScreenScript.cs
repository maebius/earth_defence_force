//#define DEBUG_TEXTS

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum GarageButtonType
{
	None
	, WeaponPrimary_01
	, WeaponPrimary_02
	, WeaponPrimary_03
	, WeaponAUX_L_01
	, WeaponAUX_L_02
	, WeaponAUX_R_01
	, WeaponAUX_R_02
	, Item_01
	, Item_02
	, Shield
}

sealed public class GarageScreenScript : MonoBehaviour 
{
	#region Enums, structs, and inner classes
	
	private class GarageButton
	{
		public GameObject root;
		public UILabel label;
		public UISprite iconSprite;
		public UISprite bgSprite;
		
		public ItemLevels property_1;
		public ItemLevels property_2;
	}
	
	#endregion

	#region Public members, not visible to Unity GUI
	public bool m_shieldEnabled;
	public bool m_secondaryFirstEnabled;
	public bool m_secondarySecondEnabled;
	
	public GameObject m_playButton;
	public GameObject m_backButton;
	public GameObject m_shopButton;
	public GameObject m_helpButton;
	public GameObject m_closeButton;
	
	public InfoBoxScript m_infoBox;
	public GarageSlotSelectorScript m_slotSelector;
	public GameObject m_helpRoot;
	public GameObject[] m_helpHide;
	
	#endregion

	#region Private data members
	private Dictionary<GarageButtonType, GarageButton> m_selectionButtons =
		new Dictionary<GarageButtonType, GarageButton>();

	private UILabel m_credits;

	private GameObject m_previousSelection = null;
	
	private List<GameObject> m_toEnableAfterHelp = new List<GameObject>();
	
	#endregion

	#region Unity MonoBehaviour base overrides
	// Use this for early initialization
	void Awake ()
	{
		#if DEBUG_TEXTS		
		Debug.Log( string.Format ("GarageScreenScript.{0}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()));
		#endif

		InitSlotConfigReference(GarageButtonType.WeaponPrimary_01, "Weapon - 01");
		InitSlotConfigReference(GarageButtonType.WeaponPrimary_02, "Weapon - 02");
		InitSlotConfigReference(GarageButtonType.WeaponPrimary_03, "Weapon - 03");
		InitSlotConfigReference(GarageButtonType.WeaponAUX_L_01, "Aux L - 01", m_secondaryFirstEnabled);
		InitSlotConfigReference(GarageButtonType.WeaponAUX_L_02, "Aux L - 02", m_secondarySecondEnabled);
		InitSlotConfigReference(GarageButtonType.WeaponAUX_R_01, "Aux R - 01", m_secondaryFirstEnabled);
		InitSlotConfigReference(GarageButtonType.WeaponAUX_R_02, "Aux R - 02", m_secondarySecondEnabled);
		InitSlotConfigReference(GarageButtonType.Item_01, "Item - 01");
		InitSlotConfigReference(GarageButtonType.Item_02, "Item - 02");
		InitSlotConfigReference(GarageButtonType.Shield, "Shield", m_shieldEnabled);
		
		m_credits = Utility.GetObject ("valueCredits").GetComponent<UILabel>();
		
		Utility.SetState (m_helpRoot, false, false);

		MusicManagerScript.Instance.PlayClip("clip_03_loop_2", MusicManagerScript.SwitchMode.CrossFade);
	}
	
	// Use this for initialization
	void Start ()
	{
		#if DEBUG_TEXTS		
		Debug.Log( string.Format ("GarageScreenScript.{0}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()));
		#endif

				
		ValidateConfiguration();
		
		UpdateAll();
		
		DisableCollidersScript.ReInitialize();
		if (AssignGamepadButtonsScript.Instance != null)
		{
			AssignGamepadButtonsScript.Instance.AssignButtons(true);
		}
		if (OuyaInputWrapper.UseGamepad)
			m_previousSelection = UICamera.selectedObject;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (SaveData.Instance.Config.ShowGarageHelp)
		{
			OnMessageInfo();
			SaveData.Instance.Config.ShowGarageHelp = false;
			SaveData.Instance.NNUKE_Save ();
		}
	}
	#endregion
		
	#region Public object methods
	[Obfuscar.Obfuscate]
	public void SetButtonsEnabled(bool flag)
	{
		m_playButton.GetComponent<Collider>().enabled = flag;
		m_backButton.GetComponent<Collider>().enabled = flag;
		
		m_shopButton.GetComponent<Collider>().enabled = flag;
		m_helpButton.GetComponent<Collider>().enabled = flag;
		
		foreach (KeyValuePair<GarageButtonType, GarageButton> keyValue in m_selectionButtons)
		{
			keyValue.Value.root.GetComponent<Collider>().enabled = flag;
		}
		
		DisableCollidersScript.ReInitialize();
		
		if (AssignGamepadButtonsScript.Instance != null)
		{
			AssignGamepadButtonsScript.Instance.AssignButtons(true);
		}
		if (flag && m_previousSelection != null)
		{
			UICamera.selectedObject = m_previousSelection;
		}
	}
	
	public void OnMessageBackFromHelp()
	{
		foreach (GameObject go in m_toEnableAfterHelp)
		{
			Utility.SetState(go, true, false);
		}
		m_toEnableAfterHelp.Clear ();

		Utility.SetState (m_helpRoot, false, false);
		UpdateAll();
		
		SetButtonsEnabled(true);
	}
	
	public void OnMessageBackFromInfo()
	{
		foreach (GameObject go in m_helpHide)
		{
			if (go != null && go.active)
			{
				Utility.SetState(go, false, false);
				m_toEnableAfterHelp.Add (go);
			}
		}
		Utility.SetState (m_helpRoot, true, false);
		
		if (OuyaInputWrapper.UseGamepad)
			UICamera.selectedObject = m_closeButton;
		if (AssignGamepadButtonsScript.Instance != null)
		{
			AssignGamepadButtonsScript.Instance.AssignButtons(true);
		}
	}
	
	public void OnMessageInfo()
	{
		if (OuyaInputWrapper.UseGamepad)
			m_previousSelection = UICamera.selectedObject;

		m_infoBox.Show(
			new string[] {
				"menu_help_text_garage_title_1"
			}
			, new string[] {
				"menu_help_text_garage_1"
			}
			, SaveData.Instance.Config.ShowGarageHelp ? 1f : 0f
		);
	}
	
	public void OnMessageBack()
	{
		SaveData.Instance.NNUKE_Save();
		Application.LoadLevel("level_screen");
	}
	
	public void OnMessagePlay()
	{
		ValidateConfiguration();

		string sceneName = SaveData.Instance.NNUKE_GetCurrentSceneName();
		if (!string.IsNullOrEmpty(sceneName))
		{
			// save, as our config might have changed
			SaveData.Instance.NNUKE_Save();
			
			MusicManagerScript.Instance.StopAll();
			
			Application.LoadLevel(sceneName);
		}
	}
	public void OnMessageStore()
	{
		OnlineStoreSelectorScript.sm_sceneToGoBack = "garage";
		Application.LoadLevel("iap_store");
	}
	
	public void OnMessageInitItems()
	{
		ValidateConfiguration();
	}

	public void OnMessageGarageSelectWeapon1()
	{
		DoSelectionForSlot(GarageButtonType.WeaponPrimary_01);
	}
	public void OnMessageGarageSelectWeapon2()
	{
		DoSelectionForSlot(GarageButtonType.WeaponPrimary_02);
	}
	public void OnMessageGarageSelectWeapon3()
	{
		DoSelectionForSlot(GarageButtonType.WeaponPrimary_03);
	}
	public void OnMessageGarageSelectLeftAuxWeapon1()
	{
		DoSelectionForSlot(GarageButtonType.WeaponAUX_L_01);
	}
	public void OnMessageGarageSelectLeftAuxWeapon2()
	{
		DoSelectionForSlot(GarageButtonType.WeaponAUX_L_02);
	}
	public void OnMessageGarageSelectRightAuxWeapon1()
	{
		DoSelectionForSlot(GarageButtonType.WeaponAUX_R_01);
	}
	public void OnMessageGarageSelectRightAuxWeapon2()
	{
		DoSelectionForSlot(GarageButtonType.WeaponAUX_R_02);
	}
	public void OnMessageGarageSelectItem1()
	{
		DoSelectionForSlot(GarageButtonType.Item_01);
	}
	public void OnMessageGarageSelectItem2()
	{
		DoSelectionForSlot(GarageButtonType.Item_02);
	}
	public void OnMessageGarageSelectShield()
	{
		DoSelectionForSlot(GarageButtonType.Shield);
	}
	
	#endregion
		
	#region Private object methods
	[Obfuscar.Obfuscate]
	private void UpdateAll()
	{
		UpdateTexts();
		UpdateWeaponIcons(new bool[] {true, true, true});
		UpdateItemIcons(new bool[] {true, true});
		UpdateAuxWeaponIcons(new bool[] {true, true, true, true});
		UpdateItemShield();
	}
		
	[Obfuscar.Obfuscate]
	private void DoSelectionForSlot(GarageButtonType buttonType)
	{
		#if DEBUG_TEXTS
		Debug.Log(string.Format ("GarageScreenScript.DoSelectionForSlot - type: {0}, selectedObject: {1}"
			, buttonType, (UICamera.selectedObject != null) ? UICamera.selectedObject.name : "null"));
		#endif
		
		if (OuyaInputWrapper.UseGamepad)
			m_previousSelection = UICamera.selectedObject;
		
		// first, validate that "previous" state is ok (that we do not let items that are not bought
		// linger in selections
		ValidateConfiguration();
		
		if (m_slotSelector != null)
		{
			m_slotSelector.UpdateSelectorItems(buttonType);
		}
		
		SetButtonsEnabled(false);
		if (OuyaInputWrapper.UseGamepad)
			UICamera.selectedObject = Utility.GetObject ("buttonSelect");
	}
	
	[Obfuscar.Obfuscate]
	private void InitSlotConfigReference(GarageButtonType buttonType, string id, bool isEnabled = true)
	{
		const string baseId = "button";
		const string labelId = "/Animation/label";
		const string iconSpriteId = "/Animation/iconRoot/icon";
		const string bgSpriteId = "/Animation/bgRoot/icon";
		
		if (isEnabled)
		{		
			m_selectionButtons[buttonType] = new GarageButton
			{ 
				root = Utility.GetObject(string.Format("{0}{1}", baseId, id))
				, label = Utility.GetObject (string.Format("{0}{1}{2}", baseId, id, labelId)).GetComponent<UILabel>()
				, iconSprite = Utility.GetObject (string.Format("{0}{1}{2}", baseId, id, iconSpriteId)).GetComponent<UISprite>()
				, bgSprite = Utility.GetObject (string.Format("{0}{1}{2}", baseId, id, bgSpriteId)).GetComponent<UISprite>()
			};
			m_selectionButtons[buttonType].property_1 = new ItemLevels();
			m_selectionButtons[buttonType].property_2 = new ItemLevels();
			m_selectionButtons[buttonType].property_1.Initialize(string.Format ("{0}{1}", baseId, id), "property_1", true);
			m_selectionButtons[buttonType].property_2.Initialize(string.Format ("{0}{1}", baseId, id), "property_2", true);
		}
		else
		{
			GameObject root = Utility.GetObject(string.Format("{0}{1}", baseId, id));
			if (root != null)
			{
				Utility.SetState(root, false, true);
			}
		}
	}
	
	[Obfuscar.Obfuscate]
	private void UpdateSlot(GarageButtonType slot, xsd_ct_Equipment equipment)
	{
		bool exists = equipment != null && equipment.Available;
		
		m_selectionButtons[slot].label.text = (exists)? equipment.Id : "?";
		
		NGUITools.SetActiveSelf(m_selectionButtons[slot].label.gameObject, !exists);
		NGUITools.SetActiveSelf(m_selectionButtons[slot].iconSprite.gameObject, exists);
		
		if (equipment != null && equipment.Available)
		{
			xsd_ct_GarageSlot s = GameData.Instance.NNUKE_GetEntry(equipment.Id);
			if (s != null)
			{
				GarageButton button = m_selectionButtons[slot];
				
				button.iconSprite.spriteName = s.IconId;
				button.iconSprite.enabled = !string.IsNullOrEmpty(s.IconId);
				
				#if DEBUG_TEXTS
				Debug.Log(string.Format ("GarageScreenScript.UpdateSlot - slot: {3}, icon: {0}, enabled: {1}, time: {2}"
					, s.IconId, button.iconSprite.enabled, Time.time, slot.ToString()));
				#endif
				
				if (button.iconSprite.enabled)
				{
					button.iconSprite.color = Color.white;
					Utility.ScaleSprite(ref button.iconSprite, true);
				}
				
				if (s.CountBased)
				{
					// NOTE! As the label is common for both levels, we need to set it to both ... 
					m_selectionButtons[slot].property_1.SetLevel(-1, equipment.Number.ToString(), true);
					m_selectionButtons[slot].property_2.SetLevel(-1, equipment.Number.ToString(), true);
				}
				else
				{
					m_selectionButtons[slot].property_1.SetLevel(equipment.Property1, string.Empty
						, string.IsNullOrEmpty(s.Property1) || equipment.Number > -1);
					m_selectionButtons[slot].property_2.SetLevel(equipment.Property2, string.Empty
						,  string.IsNullOrEmpty(s.Property2) || equipment.Number > -1);
				}
			}
		}
		else
		{
			m_selectionButtons[slot].property_1.SetLevel(-1, string.Empty, true);
			m_selectionButtons[slot].property_2.SetLevel(-1, string.Empty, true);
		}
		
		UpdateTexts();
	}
	
	[Obfuscar.Obfuscate]
	public void UpdateWeaponIcons(bool[] flags)
	{
		if (flags[0])
		{
			xsd_ct_Equipment w1 = SaveData.Instance.NNUKE_GetWeaponPrimary(SaveData.Instance.Config.WeaponPrimarySlot_1);
			UpdateSlot(GarageButtonType.WeaponPrimary_01, w1);
		}
		if (flags[1])
		{
			xsd_ct_Equipment w2 = SaveData.Instance.NNUKE_GetWeaponPrimary(SaveData.Instance.Config.WeaponPrimarySlot_2);
			UpdateSlot(GarageButtonType.WeaponPrimary_02, w2);
		}
		if (flags[2])
		{
			xsd_ct_Equipment w3 = SaveData.Instance.NNUKE_GetWeaponPrimary(SaveData.Instance.Config.WeaponPrimarySlot_3);
			UpdateSlot(GarageButtonType.WeaponPrimary_03, w3);
		}
		
	}
	
	[Obfuscar.Obfuscate]
	public void UpdateAuxWeaponIcons(bool[] flags)
	{
		if (flags[0] && m_secondaryFirstEnabled)
		{
			xsd_ct_Equipment l_1 = SaveData.Instance.NNUKE_GetWeaponSecondary(SaveData.Instance.Config.WeaponSecondarySlotLeft_1);
			UpdateSlot(GarageButtonType.WeaponAUX_L_01, l_1);
		}
		if (flags[1] && m_secondarySecondEnabled)
		{
			xsd_ct_Equipment l_2 = SaveData.Instance.NNUKE_GetWeaponSecondary(SaveData.Instance.Config.WeaponSecondarySlotLeft_2);
			UpdateSlot(GarageButtonType.WeaponAUX_L_02, l_2);
		}
		if (flags[2] && m_secondaryFirstEnabled)
		{
			xsd_ct_Equipment r_1 = SaveData.Instance.NNUKE_GetWeaponSecondary(SaveData.Instance.Config.WeaponSecondarySlotRight_1);
			UpdateSlot(GarageButtonType.WeaponAUX_R_01, r_1);
		}
		if (flags[3] && m_secondarySecondEnabled)
		{
			xsd_ct_Equipment r_2 = SaveData.Instance.NNUKE_GetWeaponSecondary(SaveData.Instance.Config.WeaponSecondarySlotRight_2);
			UpdateSlot(GarageButtonType.WeaponAUX_R_02, r_2);
		}
	}
	
	[Obfuscar.Obfuscate]
	public void UpdateItemShield()
	{
		if (!m_shieldEnabled)
			return;
		
		xsd_ct_Equipment shield = SaveData.Instance.NNUKE_GetShield(SaveData.Instance.Config.ShieldSlot);
		UpdateSlot(GarageButtonType.Shield, shield);
	}
	
	[Obfuscar.Obfuscate]
	public void UpdateItemIcons(bool[] flags)
	{
		if (flags[0])
		{
			xsd_ct_Equipment i1 = SaveData.Instance.NNUKE_GetItem(SaveData.Instance.Config.ItemSlot_1);
			UpdateSlot(GarageButtonType.Item_01, i1);
		}
		if (flags[1])
		{
			xsd_ct_Equipment i2 = SaveData.Instance.NNUKE_GetItem(SaveData.Instance.Config.ItemSlot_2);
			UpdateSlot(GarageButtonType.Item_02, i2);
		}
	}
	
	[Obfuscar.Obfuscate]
	private bool IsSelectionOk(xsd_ct_Equipment equipment)
	{
		return equipment != null && equipment.Available && equipment.Number != 0; 
	}
	
	[Obfuscar.Obfuscate]
	private bool ValidateConfiguration()
	{
		bool changed = false;
		xsd_ct_Equipment w1 = SaveData.Instance.NNUKE_GetWeaponPrimary(SaveData.Instance.Config.WeaponPrimarySlot_1);
		if (!IsSelectionOk(w1)) 
		{
			SaveData.Instance.Config.WeaponPrimarySlot_1 = "";
			changed = true;
		}
		xsd_ct_Equipment w2 = SaveData.Instance.NNUKE_GetWeaponPrimary(SaveData.Instance.Config.WeaponPrimarySlot_2);
		if (!IsSelectionOk(w2)) 
		{
			SaveData.Instance.Config.WeaponPrimarySlot_2 = "";
			changed = true;
		}
		xsd_ct_Equipment w3 = SaveData.Instance.NNUKE_GetWeaponPrimary(SaveData.Instance.Config.WeaponPrimarySlot_3);
		if (!IsSelectionOk(w3)) 
		{
			SaveData.Instance.Config.WeaponPrimarySlot_3 = "";
			changed = true;
		}
		xsd_ct_Equipment al1 = SaveData.Instance.NNUKE_GetWeaponSecondary(SaveData.Instance.Config.WeaponSecondarySlotLeft_1);
		if (!IsSelectionOk(al1)) 
		{
			SaveData.Instance.Config.WeaponSecondarySlotLeft_1 = "";
			changed = true;
		}
		xsd_ct_Equipment al2 = SaveData.Instance.NNUKE_GetWeaponSecondary(SaveData.Instance.Config.WeaponSecondarySlotLeft_2);
		if (!IsSelectionOk(al2)) 
		{
			SaveData.Instance.Config.WeaponSecondarySlotLeft_2 = "";
			changed = true;
		}
		xsd_ct_Equipment ar1 = SaveData.Instance.NNUKE_GetWeaponSecondary(SaveData.Instance.Config.WeaponSecondarySlotRight_1);
		if (!IsSelectionOk(ar1)) 
		{
			SaveData.Instance.Config.WeaponSecondarySlotRight_1 = "";
			changed = true;
		}
		xsd_ct_Equipment ar2 = SaveData.Instance.NNUKE_GetWeaponSecondary(SaveData.Instance.Config.WeaponSecondarySlotRight_2);
		if (!IsSelectionOk(ar2)) 
		{
			SaveData.Instance.Config.WeaponSecondarySlotRight_2 = "";
			changed = true;
		}
		xsd_ct_Equipment i1 = SaveData.Instance.NNUKE_GetItem(SaveData.Instance.Config.ItemSlot_1);
		if (!IsSelectionOk(i1)) 
		{
			SaveData.Instance.Config.ItemSlot_1 = "";
			changed = true;
		}
		xsd_ct_Equipment i2 = SaveData.Instance.NNUKE_GetItem(SaveData.Instance.Config.ItemSlot_2);
		if (!IsSelectionOk(i2)) 
		{
			SaveData.Instance.Config.ItemSlot_2 = "";
			changed = true;
		}
		xsd_ct_Equipment shield = SaveData.Instance.NNUKE_GetShield(SaveData.Instance.Config.ShieldSlot);
		if (!IsSelectionOk(shield)) 
		{
			SaveData.Instance.Config.ShieldSlot = "";
			changed = true;
		}
		return changed;
	}
		
	[Obfuscar.Obfuscate]
	private void UpdateTexts()
	{
		m_credits.text = string.Format("{0}", SaveData.Instance.Config.Credits);
	}

	#endregion
}
