//#define DEBUG_TEXTS

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

sealed public class GarageSlotSelectorScript : MonoBehaviour 
{
	#region Public members, visible in Unity GUI
	public Color ButtonLabelDisabled;
	public Color ButtonBgSpriteDisabled;
	
	public Color SelectedIconColor;
	public Color NotSelectedIconColor;
	
	public Color SelectedLabelColor;
	public Color NotSelectedLabelColor;
	
	public UIDraggablePanel m_draggablePanel;
	public UIGrid m_grid;
	
	public UILabel m_title;
	
	public GarageScreenScript m_garageScreen;
		
	public GameObject m_root;
	
	public AudioClip m_buyAudio;
	public AudioClip m_upgradeAudio;
	#endregion

	private class SlotChangeItem
	{
		public GameObject root;
		public UIButtonKeys buttonKeys;
		public UILabel labelNew;
		public UILabel labelName;
		public UILabel labelLevel;
		public UILabel labelLevelValue;

		public UILocalize localizeNew;
		public UILocalize localizeName;
		public UILocalize localizeLevel;

		public UISprite icon;
		public UISlicedSprite bgIcon;
		
		public xsd_ct_Equipment equipment;
	}
	
	private GameObject m_buyButton;
	private GameObject m_selectButton;

	private UILabel m_buyButtonLabel;
	private UILabel m_selectButtonLabel;
	private UILabel m_currentNextCost;
	
	private UILabel m_countTopic;
	private UILabel m_countValue;
		
	private UISlicedSprite m_buyButtonBgSprite;
	private UISlicedSprite m_selectButtonBgSprite;

	private UILocalize m_countLocale;
	private UILocalize m_currentSelectionLocale;
	private UILocalize m_buyButtonLocale;
	private UILocalize m_priceTopicLocale;
	private UILocalize m_infoLocale;

	private Color m_originalButtonLabelColor;
	private Color m_originalButtonSpriteColor;

	private int m_currentSlotSelection;
	private int m_initialSlotSelection;
	private GameObject m_previousSelection = null;

	private List<SlotChangeItem> m_slotChangeItems = new List<SlotChangeItem>();

	private Dictionary<GarageButtonType, xsd_ct_Equipment[]> m_buttontypeMap = 
		new Dictionary<GarageButtonType, xsd_ct_Equipment[]>();

	private List<string> m_selectionEntries = new List<string>();

	private xsd_ct_Equipment m_currentEquipment;
	private xsd_ct_GarageSlot m_currentSlot;

	private GarageButtonType m_doingSelectionForSlot;
	private int m_placesActive;
	
	private ItemLevels m_property_1_level = new ItemLevels();
	private ItemLevels m_property_2_level = new ItemLevels();
	
	private string m_selectedProperty;
	
	private float m_propertyButtonNotSelectedAlpha;

	#region Unity MonoBehaviour base overrides
	// Use this for early initialization
	void Awake ()
	{
		#if DEBUG_TEXTS		
		Debug.Log( string.Format ("GarageSlotSelectorScript.{0}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()));
		#endif
		
		m_buttontypeMap[GarageButtonType.None] = null;
		m_buttontypeMap[GarageButtonType.Item_01] = SaveData.Instance.Config.Item;
		m_buttontypeMap[GarageButtonType.Item_02] = SaveData.Instance.Config.Item;
		m_buttontypeMap[GarageButtonType.Shield] = SaveData.Instance.Config.Shield;
		m_buttontypeMap[GarageButtonType.WeaponPrimary_01] = SaveData.Instance.Config.WeaponPrimary;
		m_buttontypeMap[GarageButtonType.WeaponPrimary_02] = SaveData.Instance.Config.WeaponPrimary;
		m_buttontypeMap[GarageButtonType.WeaponPrimary_03] = SaveData.Instance.Config.WeaponPrimary;
		m_buttontypeMap[GarageButtonType.WeaponAUX_L_01] = SaveData.Instance.Config.WeaponSecondary;
		m_buttontypeMap[GarageButtonType.WeaponAUX_L_02] = SaveData.Instance.Config.WeaponSecondary;
		m_buttontypeMap[GarageButtonType.WeaponAUX_R_01] = SaveData.Instance.Config.WeaponSecondary;
		m_buttontypeMap[GarageButtonType.WeaponAUX_R_02] = SaveData.Instance.Config.WeaponSecondary;
		
		InitSlotChangeItemReference();
		
		m_selectButton = Utility.GetObject ("buttonSelect");
		m_buyButton = Utility.GetObject ("buttonBuy");
		
		m_buyButtonLabel = Utility.GetObject ("buttonBuy/animation/label").GetComponent<UILabel>();
		m_selectButtonLabel = Utility.GetObject ("buttonSelect/animation/label").GetComponent<UILabel>();
		m_buyButtonBgSprite = Utility.GetObject ("buttonBuy/animation/bgRoot/bgSprite").GetComponent<UISlicedSprite>();
		m_selectButtonBgSprite = Utility.GetObject ("buttonSelect/animation/bgRoot/bgSprite").GetComponent<UISlicedSprite>();
	
		m_originalButtonLabelColor = m_selectButtonLabel.color;
		m_originalButtonSpriteColor = m_buyButtonBgSprite.color;
		
		m_currentNextCost = Utility.GetObject ("valuePrice").GetComponent<UILabel>();
		m_buyButtonLocale = Utility.GetObject("buttonBuy/animation/label").GetComponent<UILocalize>();
		m_priceTopicLocale = Utility.GetObject("labelPrice").GetComponent<UILocalize>();
		m_infoLocale = Utility.GetObject("labelInfo").GetComponent<UILocalize>();
		
		m_countTopic = Utility.GetObject ("labelCount").GetComponent<UILabel>();
		m_countValue = Utility.GetObject ("valueCount").GetComponent<UILabel>();
		
		m_countLocale = m_countTopic.GetComponent<UILocalize>();
		
		m_selectButton = Utility.GetObject ("buttonSelect");
		m_buyButton = Utility.GetObject ("buttonBuy");
		
		m_property_1_level.Initialize("selectorProperty_1", "levels", true);
		m_property_2_level.Initialize("selectorProperty_2", "levels", true);		
		
		SetSlotSelectorPlacesActive(0);
	}
	#endregion
	
	#region Public interface

	[Obfuscar.Obfuscate]
	public void UpdateSelectorItems(GarageButtonType buttonType)
	{
		#if DEBUG_TEXTS		
		Debug.Log( string.Format ("GarageSlotSelectorScript.{0} - buttontype: {1}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString(), buttonType));
		#endif
		
		xsd_ct_Equipment[] equipment = m_buttontypeMap[buttonType];
		if (equipment == null)
			return;
		
		m_currentEquipment = null;
		m_currentSlot = null;
		m_currentSlotSelection = 0;
		
		m_doingSelectionForSlot = buttonType;
		m_selectionEntries.Clear ();
		
		for (int index = 0 ; index < equipment.Length; index++)
		{
			xsd_ct_Equipment config = equipment[index];
			if (config == null) continue;
			
			xsd_ct_GarageSlot eq = GameData.Instance.NNUKE_GetEntry(config.Id);

			if (eq == null) continue;
			
			m_selectionEntries.Add (config.Id);
			
			SlotChangeItem item  = m_slotChangeItems[index];
			item.equipment = config;
			
			item.localizeName.key = eq.LocalizationKey;
			item.localizeName.Localize();

			item.icon.spriteName = eq.IconId;
			if (string.IsNullOrEmpty(eq.IconId))
			{
				item.icon.enabled = false;
			}
			else
			{
				item.icon.enabled = true;
				
				Utility.ScaleSprite(ref item.icon, true);
			}
		}
		SetSlotSelectorPlacesActive(equipment.Length);

		GetCurrentEquipment(buttonType, out m_currentEquipment);
		
		if (m_currentEquipment != null)
		{
			m_currentSlot = GameData.Instance.NNUKE_GetEntry(m_currentEquipment.Id);
			
			for (int index = 0; index < equipment.Length; index++)
			{
				if (equipment[index] == m_currentEquipment)
				{
					m_currentSlotSelection = index;
					break;
				}
			}
		}
		
		m_initialSlotSelection = m_currentSlotSelection;

		
		UpdateInfoTexts(true);
	}
	[Obfuscar.Obfuscate]
	public void UpdateSlots(bool focus)
	{
		int i = 0;
		foreach (SlotChangeItem item  in m_slotChangeItems)
		{
			// not necessarily yet initialized
			if (item.equipment == null)
				continue;
			
			if (item.equipment.Available)
			{
				if (item.labelLevel != null) 
					item.labelLevel.enabled = true;
			}
			else
			{
				if (item.labelLevel != null) 
					item.labelLevel.enabled = false;
				if (item.labelLevelValue != null) 
					item.labelLevelValue.enabled = false;
			}
			if (item.labelNew != null)
				item.labelNew.enabled = (!item.equipment.Available) && item.equipment.Id != "empty";
			
			bool selected = m_currentEquipment == item.equipment;
			
			if (item.icon != null)
				item.icon.color = (selected) ? SelectedIconColor : NotSelectedIconColor;
			if (item.labelName != null)
				item.labelName.color = (selected) ? SelectedLabelColor : NotSelectedLabelColor;
			
			// focus on this
			if (focus && selected && OuyaInputWrapper.UseGamepad)
			{
				UICamera.selectedObject = item.root;
				// NOTE! This also changes the color, which in turn annoyingly goes away when we tween
				// away from the object ...
				
				// also scroll to this
				ScrollTo(i, item.root.name);
			}
			i++;
		}
		
		if (AssignGamepadButtonsScript.Instance != null)
		{
			AssignGamepadButtonsScript.Instance.AssignButtons(true);
		}
	}
	
	#endregion
	
	#region Button event listeners

	public void OnMessageProperty1()
	{
		if (m_currentSlot != null)
			m_selectedProperty = m_currentSlot.Property1;
		
		m_property_1_level.SetButtonAlpha(1f);
		m_property_2_level.ResetButtonAlpha();
		
		UpdateInfoTexts(false);
	}
	
	public void OnMessageProperty2()
	{
		if (m_currentSlot != null)
			m_selectedProperty = m_currentSlot.Property2;

		m_property_2_level.SetButtonAlpha(1f);
		m_property_1_level.ResetButtonAlpha();
		
		UpdateInfoTexts(false);
	}
	
	public void OnMessageSlotSelectorCancel()
	{
		ApplyPlayerSelection(m_initialSlotSelection);
		m_selectionEntries.Clear ();
		SetSlotSelectorPlacesActive(0);
		m_garageScreen.SetButtonsEnabled(true);
	}
	
	public void OnMessageSlotSelectorSelect()
	{
		#if DEBUG_TEXTS		
		Debug.Log( string.Format ("GarageSlotSelectorScript.{0}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()));
		#endif

		ApplyPlayerSelection(m_currentSlotSelection, true);
		m_selectionEntries.Clear ();
		SetSlotSelectorPlacesActive(0);
		m_garageScreen.SetButtonsEnabled(true);
	}
	
	public void OnMessageSlotSelectorBuy()
	{
		if (m_currentEquipment == null)
			return;
		
		if (m_currentSlot == null)
			return;
		
		GameData gd = GameData.Instance;
		int credits = SaveData.Instance.Config.Credits;
		int cost = 0;

		if (m_currentSlot.CountBased)
		{
			cost = m_currentSlot.Cost;
			if (cost > credits)
				return;
			m_currentEquipment.Number++;
		}
		else
		{
			if (m_currentEquipment.Available)
			{
				int levelIndex = gd.NNUKE_GetSlotPropertyIndex(m_currentEquipment, m_selectedProperty);
				if (levelIndex > -1) levelIndex++;
				cost = gd.NNUKE_GetLevelCost(m_currentEquipment, m_selectedProperty, levelIndex);
				if (cost > credits)
					return;
				gd.NNUKE_SetSlotPropertyIndex(m_currentEquipment, m_selectedProperty, levelIndex);
			}
			else
			{
				cost = m_currentSlot.Cost;
				if (cost > credits)
					return;
				if (!string.IsNullOrEmpty(m_currentSlot.Property1))
					m_currentEquipment.Property1 = 0;
				if (!string.IsNullOrEmpty(m_currentSlot.Property2))
					m_currentEquipment.Property2 = 0;
			}
		}

		m_currentEquipment.Available = true;
		SaveData.Instance.Config.Credits = credits - cost;
		ApplyPlayerSelection(m_currentSlotSelection);
		
		// something happened that we want to remember ...
		SaveData.Instance.NNUKE_Save();
		UpdateInfoTexts(true);
	}
	
	
	public void OnMessageSlotSelectorItem_00()
	{
		SlotSelectionMade(0);
	}
	public void OnMessageSlotSelectorItem_01()
	{
		SlotSelectionMade(1);
	}
	public void OnMessageSlotSelectorItem_02()
	{
		SlotSelectionMade(2);
	}
	public void OnMessageSlotSelectorItem_03()
	{
		SlotSelectionMade(3);
	}
	public void OnMessageSlotSelectorItem_04()
	{
		SlotSelectionMade(4);		
	}
	public void OnMessageSlotSelectorItem_05()
	{
		SlotSelectionMade(5);		
	}
	public void OnMessageSlotSelectorItem_06()
	{
		SlotSelectionMade(6);
	}
	public void OnMessageSlotSelectorItem_07()
	{
		SlotSelectionMade(7);
	}
	public void OnMessageSlotSelectorItem_08()
	{
		SlotSelectionMade(8);
	}
	public void OnMessageSlotSelectorItem_09()
	{
		SlotSelectionMade(9);
	}	
	#endregion
	
	#region Private interface
			
	[Obfuscar.Obfuscate]
	private void SlotSelectionMade(int index)
	{
		//Debug.Log(string.Format ("GarageScreenScript.SlotSelectionMade - index: {0}", index));
		
		m_selectedProperty = string.Empty;
		m_currentSlotSelection = index;
		ApplyPlayerSelection(m_currentSlotSelection);
		UpdateInfoTexts(true);
		
		m_property_1_level.ResetButtonAlpha();
		m_property_2_level.ResetButtonAlpha();

		if (AssignGamepadButtonsScript.Instance != null)
		{
			AssignGamepadButtonsScript.Instance.AssignButtons();
		}
	}
	[Obfuscar.Obfuscate]
	private void SetSlotSelectorPlacesActive(int amount)
	{
		m_placesActive = amount;
		
		#if DEBUG_TEXTS		
		Debug.Log( string.Format ("GarageSlotSelectorScript.{0} - active:{1}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString(), amount));
		#endif
		bool isOn = amount > 0;

		if (isOn && OuyaInputWrapper.UseGamepad)
		{
			m_previousSelection = UICamera.selectedObject;
			UICamera.selectedObject = null;
		}
		
		NGUITools.SetActiveChildren(m_root, isOn);
		NGUITools.SetActive(m_root, isOn);
		
		UIButtonKeys previousButtonKeys = null;
		for (int i = 0; i < m_slotChangeItems.Count; i++)
		{
			SlotChangeItem item  = m_slotChangeItems[i];
			isOn = i < amount;
			
			if (previousButtonKeys)
			{
				item.buttonKeys.selectOnLeft = (isOn) ? previousButtonKeys : null;
				previousButtonKeys.selectOnRight = (isOn) ? item.buttonKeys : null;
			}
			
			NGUITools.SetActiveChildren(item.root, isOn);
			NGUITools.SetActive(item.root, isOn);
			
			previousButtonKeys = item.buttonKeys;
		}
	
		if (m_draggablePanel && amount > 0)
		{
			m_draggablePanel.ResetPosition();
		}
		
		if (!isOn && OuyaInputWrapper.UseGamepad)
		{
			UICamera.selectedObject = m_previousSelection;
		}
	}
	
	[Obfuscar.Obfuscate]
	private void InitSlotChangeItemReference()
	{
		string baseId = "panelItems/grid";
		
		for (int index = 0; index < 10; index++)
		{
			string b = string.Format ("{0}/Item {1:00}", baseId, index);
		
			GameObject item = Utility.GetObject (b);
			
			if (item == null)
				return;
			
			GameObject name = Utility.GetObject (string.Format("{0}/labelName", b));
			GameObject nnew = Utility.GetObject (string.Format("{0}/labelNew", b));
			GameObject level = Utility.GetObject (string.Format("{0}/labelLevel", b));
			GameObject levelValue = Utility.GetObject (string.Format("{0}/labelLevelValue", b));
			GameObject sprite = Utility.GetObject (string.Format("{0}/iconRoot/sprite", b));
			GameObject bgSprite = Utility.GetObject (string.Format("{0}/bgSprite", b));
			
			m_slotChangeItems.Add ( new SlotChangeItem
				{ 
					root = item
					, buttonKeys = item.GetComponent<UIButtonKeys>()
					, labelNew = (nnew) ? nnew.GetComponent<UILabel>() : null
					, labelName = (name) ? name.GetComponent<UILabel>() : null
					, labelLevel = (level) ? level.GetComponent<UILabel>() : null
					, labelLevelValue = (levelValue) ? levelValue.GetComponent<UILabel>() : null
					, localizeNew = (nnew) ? nnew.GetComponent<UILocalize>() : null
					, localizeName = (name) ? name.GetComponent<UILocalize>() : null
					, localizeLevel = (level) ? level.GetComponent<UILocalize>() : null
					, icon = (sprite) ? sprite.GetComponent<UISprite>() : null
					, bgIcon = (bgSprite) ? bgSprite.GetComponent<UISlicedSprite>() : null
				}
			);
		}
		
		if (AddButtonSoundsScript.Instance != null)
		{
			AddButtonSoundsScript.Instance.AddSounds();
		}
	}
		
	
	
	[Obfuscar.Obfuscate]
	private void ShowLevelUp()
	{
		bool canBuy = !m_currentEquipment.Available;
		
		if (canBuy)
			m_selectedProperty = string.Empty;
		
		bool canUpgrade = !canBuy && m_currentSlot.Property != null;
		
		GameData gd = GameData.Instance;
		int level =  gd.NNUKE_GetSlotPropertyIndex(m_currentEquipment, m_selectedProperty);
		int maxLevel = gd.NNUKE_GetMaxPropertyIndex(m_currentEquipment, m_selectedProperty);
		
		bool maxedOut = canUpgrade && level >= maxLevel - 1;
		bool canSelect = canUpgrade && SaveData.Instance.NNUKE_CanSelect(m_currentEquipment.Id);
		
		bool canAfford = false;
		int cost = 0;
		
		int credits = SaveData.Instance.Config.Credits;
		if (canBuy)
		{
			cost = m_currentSlot.Cost;
		}
		else if (canUpgrade && !maxedOut)
		{
			cost = gd.NNUKE_GetLevelCost(m_currentEquipment, m_selectedProperty, level + 1);
		}
		canAfford = (cost <= credits);
	
		SetSelectButtonState(canSelect);
		
		if (canUpgrade)
		{
			if (maxedOut)
			{
				m_buyButtonLocale.key = "menu_garage_select_empty";
				m_priceTopicLocale.key = "menu_garage_select_maxed";
				
				m_buyButton.GetComponent<UIButtonSound>().audioClip = null;
			}
			else
			{
				m_buyButtonLocale.key = "menu_garage_select_upgrade";
				m_priceTopicLocale.key = "menu_garage_select_price_levelup";
				
				m_buyButton.GetComponent<UIButtonSound>().audioClip = m_upgradeAudio;
			}
		}
		else if (canBuy)
		{
			m_buyButtonLocale.key = "menu_garage_select_buy";
			m_priceTopicLocale.key = "menu_garage_select_price_buy";

			m_buyButton.GetComponent<UIButtonSound>().audioClip = m_buyAudio;
		}
		else
		{
			m_buyButtonLocale.key = "menu_garage_select_empty";
			m_priceTopicLocale.key = "menu_garage_select_select";

			m_buyButton.GetComponent<UIButtonSound>().audioClip = null;
		}
		
		if (!canBuy && string.IsNullOrEmpty(m_selectedProperty))
		{
			m_priceTopicLocale.key = "menu_garage_select_property";
		}
		
		m_property_1_level.SetButtonState(canUpgrade);
		m_property_2_level.SetButtonState(canUpgrade);
		

		m_currentNextCost.enabled =  !maxedOut;
		m_currentNextCost.text  = string.Format ("{0}", (!maxedOut) ? cost.ToString() : "-");

		m_buyButtonLocale.Localize();
		m_priceTopicLocale.Localize();
		
		// NOTE! This after above localization
		SetBuyButtonState((canUpgrade || canBuy) && canAfford && !maxedOut);
		
		// level up parameter visualizations
			
		int p1_value = -1, p2_value = -1;
		
		if (!string.IsNullOrEmpty(m_currentSlot.Property1))
		{
			p1_value =  m_currentEquipment.Property1;
		}
		if (!string.IsNullOrEmpty(m_currentSlot.Property2))
		{
			p2_value =  m_currentEquipment.Property2;
		}
		
		m_property_1_level.SetLevel (p1_value
			, (canBuy) ? string.Empty : m_currentSlot.LocalizationKeyProperty1
			, string.IsNullOrEmpty(m_currentSlot.Property1) || m_selectedProperty != m_currentSlot.Property1);
		m_property_2_level.SetLevel (p2_value
			, (canBuy) ? string.Empty : m_currentSlot.LocalizationKeyProperty2
			, string.IsNullOrEmpty(m_currentSlot.Property2) || m_selectedProperty != m_currentSlot.Property2);
		
		if (m_selectedProperty == m_currentSlot.Property1)
			m_property_1_level.SetLabelColor(Color.white);
		else
			m_property_1_level.ResetLabelColor();

		if (m_selectedProperty == m_currentSlot.Property2)
			m_property_2_level.SetLabelColor(Color.white);
		else
			m_property_2_level.ResetLabelColor();
		
		float p1_alpha = (m_selectedProperty == m_currentSlot.Property1) ? 1f : 0.5f;
		float p2_alpha = (m_selectedProperty == m_currentSlot.Property2) ? 1f : 0.5f;
		m_property_1_level.SetAlpha (p1_alpha, 0, p1_value, false);
		m_property_2_level.SetAlpha (p2_alpha, 0, p2_value, false);
	}
	
	[Obfuscar.Obfuscate]
	private void HideLevelUp()
	{
		m_property_1_level.SetLevel (-2, string.Empty, true);
		m_property_2_level.SetLevel (-2, string.Empty, true);
		
		m_property_1_level.SetButtonState(false);
		m_property_2_level.SetButtonState(false);
		m_currentNextCost.enabled =  false;
	}
	
	[Obfuscar.Obfuscate]
	private void ShowItemCount()
	{
		bool canSelect = 
			m_currentEquipment.Number > 0
			&& SaveData.Instance.NNUKE_CanSelect(m_currentEquipment.Id);
		
		bool canBuy = m_currentEquipment.Id != "empty";
	
		bool canAfford = false;
		int cost = 0;
		
		int credits = SaveData.Instance.Config.Credits;
		if (canBuy)
		{
			cost = m_currentSlot.Cost;
			canAfford = (cost <= credits);
			
			m_buyButtonLocale.key = "menu_garage_select_buy";
			m_priceTopicLocale.key = "menu_garage_select_price_buy";
			m_buyButton.GetComponent<UIButtonSound>().audioClip = m_buyAudio;
		}
		else
		{
			m_buyButtonLocale.key = "menu_garage_select_empty";
			m_priceTopicLocale.key = "menu_garage_select_empty";
			m_buyButton.GetComponent<UIButtonSound>().audioClip = null;
		}
	

		m_currentNextCost.enabled = canBuy;
		m_currentNextCost.text  = string.Format ("{0}", (canBuy) ? cost.ToString() : "-");
		
		m_countLocale.key = "menu_garage_current_count";
		m_countLocale.Localize();
		
		m_countTopic.enabled = true;
		m_countValue.enabled = true;
		
		m_countValue.text = string.Format ("{0}", m_currentEquipment.Number);
		
		m_buyButtonLocale.Localize();
		m_priceTopicLocale.Localize();

		// NOTE! These after above localizations
		SetBuyButtonState(canAfford);
		SetSelectButtonState(canSelect);
	}
	
	private void SetBuyButtonState(bool flag)
	{
		m_buyButton.GetComponent<Collider>().enabled = flag;
		m_buyButtonLabel.color = (flag) ? m_originalButtonLabelColor : ButtonLabelDisabled;
		m_buyButtonBgSprite.color = (flag) ? m_originalButtonSpriteColor : ButtonBgSpriteDisabled;
		m_buyButtonBgSprite.enabled = !string.IsNullOrEmpty(m_buyButtonLabel.text);
	}
	
	private void SetSelectButtonState(bool flag)
	{
		m_selectButton.GetComponent<Collider>().enabled = flag;
		m_selectButtonLabel.color = (flag) ? m_originalButtonLabelColor : ButtonLabelDisabled;
		m_selectButtonBgSprite.color = (flag) ? m_originalButtonSpriteColor : ButtonBgSpriteDisabled;
		m_selectButtonBgSprite.enabled = !string.IsNullOrEmpty(m_selectButtonLabel.text);
	}
	
	private void HideItemCount()
	{
		m_currentNextCost.enabled =  false;

		m_countTopic.enabled = false;
		m_countValue.enabled = false;
		
		m_buyButtonLocale.key = "menu_garage_select_empty";
		m_priceTopicLocale.key = "menu_garage_select_empty";
		m_buyButtonLocale.Localize();
		m_priceTopicLocale.Localize();
		
	}
	
	[Obfuscar.Obfuscate]
	private void UpdateInfoTexts(bool focus)
	{
		m_title.text = string.Format("{0}", SaveData.Instance.Config.Credits);
		
		bool isEmpty = m_currentEquipment == null || m_currentSlot == null || m_currentEquipment.Id == "empty";
		Utility.SetState(m_infoLocale.gameObject, false, false);
		
		if (isEmpty)
		{
			HideLevelUp();
			HideItemCount();
			
			m_priceTopicLocale.key = "menu_garage_select_select";
			m_priceTopicLocale.Localize();
			m_priceTopicLocale.enabled = true;
			
			Utility.SetState(m_infoLocale.gameObject, true, false);
			m_infoLocale.key = "menu_garage_select_info_empty";
			m_infoLocale.Localize();
				
			SetSelectButtonState(true);
			SetBuyButtonState(false);
		}
		else if (m_currentEquipment.Number > -1)
		{
			HideLevelUp();
			ShowItemCount();
		}
		else
		{
			HideItemCount();
			ShowLevelUp();
		}
		
		UpdateSlots(focus);
	}
	
	private void OnGridItemSelected(GameObject item)
	{
		int i = 0;
		for ( ; i < m_grid.transform.GetChildCount(); i++) 
		{
			GameObject obj = m_grid.transform.GetChild(i).gameObject;
			if (item == obj) 
			{
				break;
			}
		}
		if (i < m_grid.transform.GetChildCount())
		{
			ScrollTo (i, item.name);
		}
	}
	
	[Obfuscar.Obfuscate]
	private void ScrollTo(int index, string name) 
	{
		if (m_draggablePanel == null || m_grid == null)
			return;
		
		if (!OuyaInputWrapper.UseGamepad || (!OuyaInputWrapper.OuyaEnabled && UICamera.MouseUsed))
			return;
		
		if (m_draggablePanel.horizontalScrollBar == null || !m_draggablePanel.shouldMoveHorizontally)
			return;
		
		float div = (m_placesActive > 1) ? (m_placesActive-1) : 1f;
		float scrollTo = index / div;
		if (index > 0 && index < m_placesActive -1)
			scrollTo += (0.5f / m_placesActive);
		
#if DEBUG_TEXTS
		Debug.Log(
			string.Format ("GarageSlotSelectorScript.ScrollTo() name: {0}, index: {1}, scrollTo: {2}, childCount: {3}, placesActive: {4}"
			, name, index, scrollTo, m_grid.transform.GetChildCount(), m_placesActive));
#endif
		
		m_draggablePanel.relativePositionOnReset = new Vector2(scrollTo, 0f);
		m_draggablePanel.ResetPosition();
	}
	
	
	[Obfuscar.Obfuscate]
	private bool GetCurrentEquipment(GarageButtonType buttonType, out xsd_ct_Equipment equipment)
	{
		bool found = true;
		switch (buttonType)
		{
		case GarageButtonType.WeaponPrimary_01:
			equipment = SaveData.Instance.NNUKE_GetWeaponPrimary(SaveData.Instance.Config.WeaponPrimarySlot_1);
			break;
		case GarageButtonType.WeaponPrimary_02:
			equipment = SaveData.Instance.NNUKE_GetWeaponPrimary(SaveData.Instance.Config.WeaponPrimarySlot_2);
			break;
		case GarageButtonType.WeaponPrimary_03:
			equipment = SaveData.Instance.NNUKE_GetWeaponPrimary(SaveData.Instance.Config.WeaponPrimarySlot_3);
			break;
		case GarageButtonType.Item_01:
			equipment = SaveData.Instance.NNUKE_GetItem(SaveData.Instance.Config.ItemSlot_1);
			break;
		case GarageButtonType.Item_02:
			equipment = SaveData.Instance.NNUKE_GetItem(SaveData.Instance.Config.ItemSlot_2);
			break;
		case GarageButtonType.Shield:
			equipment = SaveData.Instance.NNUKE_GetShield(SaveData.Instance.Config.ShieldSlot);
			break;
		case GarageButtonType.WeaponAUX_L_01:
			equipment = SaveData.Instance.NNUKE_GetWeaponSecondary(SaveData.Instance.Config.WeaponSecondarySlotLeft_1);
			break;
		case GarageButtonType.WeaponAUX_L_02:
			equipment = SaveData.Instance.NNUKE_GetWeaponSecondary(SaveData.Instance.Config.WeaponSecondarySlotLeft_2);
			break;
		case GarageButtonType.WeaponAUX_R_01:
			equipment = SaveData.Instance.NNUKE_GetWeaponSecondary(SaveData.Instance.Config.WeaponSecondarySlotRight_1);
			break;
		case GarageButtonType.WeaponAUX_R_02:
			equipment = SaveData.Instance.NNUKE_GetWeaponSecondary(SaveData.Instance.Config.WeaponSecondarySlotRight_2);
			break;
		default:
			equipment = null;
			found = false;
			break;
		}
		return found;
	}
	
	[Obfuscar.Obfuscate]
	private void ApplyPlayerSelection(int index, bool setOtherSlotsIfEmpty = false)
	{
		#if DEBUG_TEXTS		
		Debug.Log( string.Format ("GarageSlotSelectorScript.{0} - index: {1}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString(), index));
		#endif
		
		if (index < 0 || index >= m_selectionEntries.Count)
			return;
		
		bool w1 = false, w2 = false, w3 = false;
		bool i1 = false, i2 = false;
		bool al1 = false, al2 = false, ar1 = false, ar2 = false;
		bool s = false;
		
		switch (m_doingSelectionForSlot)
		{
		case GarageButtonType.WeaponPrimary_01:
			w1 = true; w2 = true; w3 = true;
			SaveData.Instance.Config.WeaponPrimarySlot_1 = m_selectionEntries[index];
			m_currentEquipment = SaveData.Instance.NNUKE_GetWeaponPrimary(m_selectionEntries[index]);
			break;
		case GarageButtonType.WeaponPrimary_02:
			w1 = true; w2 = true; w3 = true;
			SaveData.Instance.Config.WeaponPrimarySlot_2 = m_selectionEntries[index];
			m_currentEquipment = SaveData.Instance.NNUKE_GetWeaponPrimary(m_selectionEntries[index]);
			break;
		case GarageButtonType.WeaponPrimary_03:
			w1 = true; w2 = true; w3 = true;
			SaveData.Instance.Config.WeaponPrimarySlot_3 = m_selectionEntries[index];
			m_currentEquipment = SaveData.Instance.NNUKE_GetWeaponPrimary(m_selectionEntries[index]);
			break;
		case GarageButtonType.Item_01:
			i1 = true; i2 = true;
			SaveData.Instance.Config.ItemSlot_1 = m_selectionEntries[index];
			m_currentEquipment = SaveData.Instance.NNUKE_GetItem(m_selectionEntries[index]);
			break;
		case GarageButtonType.Item_02:
			i1 = true; i2 = true;
			SaveData.Instance.Config.ItemSlot_2 = m_selectionEntries[index];
			m_currentEquipment = SaveData.Instance.NNUKE_GetItem(m_selectionEntries[index]);
			break;
		case GarageButtonType.Shield:
			s = true;
			SaveData.Instance.Config.ShieldSlot = m_selectionEntries[index];
			m_currentEquipment = SaveData.Instance.NNUKE_GetShield(m_selectionEntries[index]);
			break;
		case GarageButtonType.WeaponAUX_L_01:
			al1 = true; al2 = true; ar1 = true; ar2 = true;
			SaveData.Instance.Config.WeaponSecondarySlotLeft_1 = m_selectionEntries[index];
			if (setOtherSlotsIfEmpty) 
				SetAllEmptyAuxWeapons(m_selectionEntries[index]);
			m_currentEquipment = SaveData.Instance.NNUKE_GetWeaponSecondary(m_selectionEntries[index]);
			break;
		case GarageButtonType.WeaponAUX_L_02:
			al1 = true; al2 = true; ar1 = true; ar2 = true;
			SaveData.Instance.Config.WeaponSecondarySlotLeft_2 = m_selectionEntries[index];
			if (setOtherSlotsIfEmpty) 
				SetAllEmptyAuxWeapons(m_selectionEntries[index]);
			m_currentEquipment = SaveData.Instance.NNUKE_GetWeaponSecondary(m_selectionEntries[index]);
			break;
		case GarageButtonType.WeaponAUX_R_01:
			al1 = true; al2 = true; ar1 = true; ar2 = true;
			SaveData.Instance.Config.WeaponSecondarySlotRight_1 = m_selectionEntries[index];
			if (setOtherSlotsIfEmpty) 
				SetAllEmptyAuxWeapons(m_selectionEntries[index]);
			m_currentEquipment = SaveData.Instance.NNUKE_GetWeaponSecondary(m_selectionEntries[index]);
			break;
		case GarageButtonType.WeaponAUX_R_02:
			al1 = true; al2 = true; ar1 = true; ar2 = true;
			SaveData.Instance.Config.WeaponSecondarySlotRight_2 = m_selectionEntries[index];
			if (setOtherSlotsIfEmpty) 
				SetAllEmptyAuxWeapons(m_selectionEntries[index]);
			m_currentEquipment = SaveData.Instance.NNUKE_GetWeaponSecondary(m_selectionEntries[index]);
			break;
		default:
			m_currentEquipment = null;
			break;
		}
		if (m_currentEquipment != null)
		{
			m_currentSlot = GameData.Instance.NNUKE_GetEntry(m_selectionEntries[index]);
		}
		
		m_garageScreen.UpdateWeaponIcons(new bool[] { w1, w2, w3 });
		m_garageScreen.UpdateItemIcons(new bool[] { i1, i2 });
		m_garageScreen.UpdateAuxWeaponIcons(new bool[] { al1, al2, ar1, ar2 });
		if (s) 
			m_garageScreen.UpdateItemShield();
	}
	
	[Obfuscar.Obfuscate]
	private void SetAllEmptyAuxWeapons(string entryId)
	{
		if (string.IsNullOrEmpty(SaveData.Instance.Config.WeaponSecondarySlotRight_1))
		{
			SaveData.Instance.Config.WeaponSecondarySlotRight_1 = entryId;
		}
		if (string.IsNullOrEmpty(SaveData.Instance.Config.WeaponSecondarySlotRight_2))
		{
			SaveData.Instance.Config.WeaponSecondarySlotRight_2 = entryId;
		}
		if (string.IsNullOrEmpty(SaveData.Instance.Config.WeaponSecondarySlotLeft_1))
		{
			SaveData.Instance.Config.WeaponSecondarySlotLeft_1 = entryId;
		}
		if (string.IsNullOrEmpty(SaveData.Instance.Config.WeaponSecondarySlotLeft_2))
		{
			SaveData.Instance.Config.WeaponSecondarySlotLeft_2 = entryId;
		}
	}
	
	#endregion
}
