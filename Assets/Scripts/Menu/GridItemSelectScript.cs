//#define DEBUG_TEXTS

using UnityEngine;
using System.Collections;

sealed public class GridItemSelectScript : MonoBehaviour 
{
	public GameObject m_handler;
	public string m_handlerFunction;
	
	public void OnSelect()
	{
		#if DEBUG_TEXTS
		Debug.Log(string.Format ("GridItemSelectScript.OnSelect() name: {0}", gameObject.name));
		#endif
		
		if (m_handler)
		{
			m_handler.SendMessage(m_handlerFunction, gameObject, SendMessageOptions.DontRequireReceiver);
		}
	}
}
