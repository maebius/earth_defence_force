using UnityEngine;
using System.Collections;
using System.Collections.Generic;

sealed public class InfoBoxScript : MonoBehaviour 
{

	#region Public members, visible in Unity GUI
	public GameObject m_root;
	public UILocalize m_title;
	public UILocalize m_text;
	
	public UILabel m_pageCount;
	public UILabel m_pageDivider;
	public UILabel m_pageCurrent;
	
	public string m_nextBackSpriteName;
	public string m_closeSpriteName;
	
	public GameObject m_prevButton;
	public GameObject m_nextButton;
	public GameObject m_cancelButton;
	
	public GameObject m_closeHandler;
	public string m_closeHandlerFunction;
	
	
	public AudioClip m_soundAppears;
	#endregion

	#region Private members
	private List<string> m_titleLocalizations = new List<string>();
	private List<string> m_textLocalizations = new List<string>();
	private int m_currentIndex;
	
	private UISprite m_nextIcon;
	#endregion
	
	#region Unity MonoBehaviour base overrides
	void Awake()
	{
		m_nextIcon = Utility.GetChildRecursive(m_nextButton, "icon", true).GetComponent<UISprite>();
		
		Utility.SetState(m_root, false, false);
	}
	
	public void Show(string[] titleIds, string[] localizationIds, float delay) 
	{
		m_titleLocalizations.Clear ();
		m_textLocalizations.Clear ();
		m_titleLocalizations.AddRange(titleIds);
		m_textLocalizations.AddRange(localizationIds);

		DisableCollidersScript.ReInitialize();

		m_pageCount.text = string.Format ("{0}", localizationIds.Length);
		
		m_currentIndex = 0;
		
		TweenScale[] scales = m_root.GetComponentsInChildren<TweenScale>(true);
		foreach (TweenScale ts in scales)
		{
			ts.delay = delay;
		}
		TweenAlpha[] alphas = m_root.GetComponentsInChildren<TweenAlpha>(true);
		foreach (TweenAlpha ta in alphas)
		{
			ta.delay = delay;
		}
		
		Utility.SetState(m_root, true, false);

		UpdateText(m_currentIndex);
		
		SoundManagerScript.PlayOneTime (m_root.gameObject, m_soundAppears, delay);
	
		if (AssignGamepadButtonsScript.Instance != null)
		{
			AssignGamepadButtonsScript.Instance.AssignButtons(true);
		}
	}
	#endregion
	
	#region Button handlers
	void OnButtonMessageClose()
	{
		CloseMyself ();
	}
	
	void OnButtonMessageNext()
	{
		if (m_currentIndex >= m_textLocalizations.Count - 1)
		{
			CloseMyself();
			return;
		}
		UpdateText(m_currentIndex + 1);
	}
	
	void OnButtonMessagePrevious()
	{
		if (m_currentIndex < 1)
			return;

		UpdateText(m_currentIndex - 1);
	}	
	#endregion

	#region Private methods
	private void CloseMyself()
	{
		Utility.ResetTweens(m_root);
		Utility.SetState(m_root, false, false);
		if (m_closeHandler)
		{
			m_closeHandler.SendMessage(m_closeHandlerFunction, SendMessageOptions.DontRequireReceiver);
		}
	}
	
	private void UpdateText(int index)
	{
		m_currentIndex = index;

		m_title.key = (index < m_titleLocalizations.Count) 
			? m_titleLocalizations[index] : m_titleLocalizations[m_titleLocalizations.Count-1] ;
		m_text.key = m_textLocalizations[index];

		m_pageCurrent.text = string.Format ("{0}", m_currentIndex + 1);
		
		if (m_currentIndex == 0 && OuyaInputWrapper.UseGamepad)
		{
			UICamera.selectedObject = m_nextButton;
		}
		
		Utility.SetState(m_prevButton, m_currentIndex > 0, true);
		Utility.SetState(m_cancelButton, m_textLocalizations.Count > 1, true);
		
		Utility.SetState(m_pageCount.gameObject, m_textLocalizations.Count > 1, true);
		Utility.SetState(m_pageDivider.gameObject, m_textLocalizations.Count > 1, true);
		Utility.SetState(m_pageCurrent.gameObject, m_textLocalizations.Count > 1, true);
		
		if (m_nextIcon != null)
		{
			m_nextIcon.spriteName = (m_currentIndex >= m_textLocalizations.Count - 1) 
				? m_closeSpriteName : m_nextBackSpriteName;
		}
		
		m_text.Localize();
		m_title.Localize();
		
		if (AssignGamepadButtonsScript.Instance != null)
		{
			AssignGamepadButtonsScript.Instance.AssignButtons(true);
		}
	}
	
	#endregion
}
