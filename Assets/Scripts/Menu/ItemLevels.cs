using UnityEngine;
using System.Collections;

sealed public class ItemLevels
{
	private UISlicedSprite[] levelOutlines = new UISlicedSprite[5];
	private UISlicedSprite[] levelValues = new UISlicedSprite[5];
	private Color[] outlineColors = new Color[5];
	private Color[] levelColors = new Color[5];
	
	private GameObject root;
	
	private UIButton button;
	private UISlicedSprite buttonSprite;
	private float buttonOriginalAlpha;
	
	private UILabel label = null;
	private UILocalize labelLocalization = null;
	
	private Color labelOriginalColor;
	
	private const string levelOutline = "outlineRoot/icon";
	private const string levelValue = "valueRoot/icon";
	private const string topic = "label";

	public void Initialize(string baseId, string id, bool useLabel)
	{
		#if DEBUG_TEXTS		
		Debug.Log( string.Format ("GarageScreenScript.{0}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()));
		#endif
		
		root = Utility.GetObject(baseId);
		
		if (useLabel)
		{
			GameObject obj = Utility.GetObject (string.Format("{0}/{1}", baseId, topic));
			if (obj != null)
			{
				label = obj.GetComponent<UILabel>();
				labelLocalization =  obj.GetComponent<UILocalize>();
				labelOriginalColor = label.color;
			}
		}
		
		GameObject bobj = Utility.GetChildRecursive(root, "buttonSelect", true);
		if (bobj != null)
		{
			button = bobj.GetComponent<UIButton>();
			buttonSprite = bobj.GetComponentInChildren<UISlicedSprite>();
			buttonOriginalAlpha = buttonSprite.alpha;
		}
		
		for (int i = 0; i < 5; i++)
		{
			GameObject outlineObj = 
				Utility.GetObject (string.Format("{0}/{1}/{2}/{3}", baseId, id, i, levelOutline));
			GameObject valueObj =
				Utility.GetObject (string.Format("{0}/{1}/{2}/{3}", baseId, id, i, levelValue));
			
			levelOutlines[i] = outlineObj.GetComponent<UISlicedSprite>();
			levelValues[i] = valueObj.GetComponent<UISlicedSprite>();
			outlineColors[i] = levelOutlines[i].color;
			levelColors[i] = levelValues[i].color;
		}
	}
	[Obfuscar.Obfuscate]
	public void SetState(bool state)
	{
		Utility.SetState(root, state, false);
	}
	
	[Obfuscar.Obfuscate]
	public void SetButtonState(bool state)
	{
		if (button != null)
		{
			button.GetComponent<Collider>().enabled = state;
			
			if (buttonSprite != null)
				Utility.SetState(buttonSprite.gameObject, state, true);
		}
		if (!state)
		{
			ResetLabelColor();
		}
	}
	[Obfuscar.Obfuscate]
	public void SetButtonAlpha(float a)
	{
		if (buttonSprite != null)
		{
			buttonSprite.alpha = a;
		}
	}
	[Obfuscar.Obfuscate]
	public void ResetButtonAlpha()
	{
		if (buttonSprite != null)
		{
			buttonSprite.alpha = buttonOriginalAlpha;
		}
	}
	
	[Obfuscar.Obfuscate]
	public void SetLabelColor(Color color)
	{
		if (label != null)
		{
			label.color = color;
		}
	}
	[Obfuscar.Obfuscate]
	public void ResetLabelColor()
	{
		if (label != null)
		{
			label.color = labelOriginalColor;
		}
	}
	[Obfuscar.Obfuscate]
	public void SetAlpha(float alpha, int start, int stop, bool includeOutline = false)
	{
		for (int i = start; i <= stop && i < levelValues.Length; i++)
		{
			levelValues[i].color = new Color(
				levelColors[i].r
				, levelColors[i].g
				, levelColors[i].b
				, alpha);
			
			if (includeOutline)
			{
				levelOutlines[i].color = new Color(
					outlineColors[i].r
					, outlineColors[i].g
					, outlineColors[i].b
					, alpha);
			}
		}
	}
	
	[Obfuscar.Obfuscate]
	public void SetLevel(int level, string textOrLoc = "", bool hideOutline = false)
	{
		bool hasLabel = !string.IsNullOrEmpty(textOrLoc);
		
		if (label != null)
			NGUITools.SetActive(label.gameObject, hasLabel);
		else
			hasLabel = false;
		
		SetButtonState(level > -1);
		
		if (labelLocalization != null)
		{
			labelLocalization.key = textOrLoc;
			labelLocalization.Localize();
		}
		else if (label != null)
		{
			label.text = textOrLoc;
		}	
		
		for (int i = 0; i < levelValues.Length; i++)
		{
			levelValues[i].color = levelColors[i];
			levelOutlines[i].color = outlineColors[i];
			
			GameObject obj = levelValues[i].gameObject;
			NGUITools.SetActive(obj, (i <= level));
			
			obj = levelOutlines[i].gameObject;
			NGUITools.SetActive(obj, !hideOutline && (level > -2));
		}
		/*
		Utility.SetState(root
			, !hideOutline || hasLabel || level > -1
			, false);*/
	}
}
