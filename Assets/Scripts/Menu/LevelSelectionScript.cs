//#define DEBUG_TEXTS

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

sealed public class LevelSelectionScript : MonoBehaviour 
{
	
	// NOTE! All the button manipulation (what level is open and what not)
	// happens in StarRatingScript ...
	
	#region Enums, structs, and inner classes
	#endregion

	#region Delegates And Events
	#endregion

	#region Public members, visible in Unity GUI
	public UIButton m_worldBackButton;
	public UIButton m_worldNextButton;
	
	public UILabel m_worldLabel;
	
	public StarRatingScript m_levels;
	#endregion

	#region Public members, not visible to Unity GUI
	#endregion

	#region Private data members
	#endregion

	#region Unity MonoBehaviour base overrides
	void Awake()
	{
		MusicManagerScript.Instance.PlayClip("clip_03_loop", MusicManagerScript.SwitchMode.CrossFade);
	}
	
	void Start()
	{
#if DEBUG_TEXTS
		Debug.Log(string.Format("LevelSelectionScript.Start ()"));
#endif
		UpdateWorld();

#if DEBUG_TEXTS
		Debug.Log(string.Format("LevelSelectionScript.Start () - exit"));
#endif	
	}
	
	#endregion
		
	#region Public object methods

	public void OnMessageStory()
	{
		Application.LoadLevel("story_screen");
	}
	
	public void OnMessageStore()
	{
		OnlineStoreSelectorScript.sm_sceneToGoBack = "level_screen";
		Application.LoadLevel("iap_store");
	}
	
	public void OnMessageWorldLeft()
	{
		string newWorld = SaveData.Instance.NNUKE_GetNextWorldName(-1);
		if (!string.IsNullOrEmpty(newWorld)) SaveData.Instance.Config.SelectedWorld = newWorld;
		UpdateWorld();
	}
	public void OnMessageWorldRight()
	{
		string newWorld = SaveData.Instance.NNUKE_GetNextWorldName(1);
		if (!string.IsNullOrEmpty(newWorld)) SaveData.Instance.Config.SelectedWorld = newWorld;
		UpdateWorld();
	}
	
	public void OnMessageLevel_01()
	{
		SetLevel(0);
	}
	
	public void OnMessageLevel_02()
	{
		SetLevel(1);
	}
	public void OnMessageLevel_03()
	{
		SetLevel(2);
	}
	public void OnMessageLevel_04()
	{
		SetLevel(3);
	}
	public void OnMessageLevel_05()
	{
		SetLevel(4);
	}
	public void OnMessageLevel_06()
	{
		SetLevel(5);
	}
	public void OnMessageLevel_07()
	{
		SetLevel(6);
	}
	public void OnMessageLevel_08()
	{
		SetLevel(7);
	}
	public void OnMessageLevel_09()
	{
		SetLevel(8);
	}
	public void OnMessageLevel_10()
	{
		SetLevel(9);
	}
	public void OnMessageBack()
	{
		Application.LoadLevel("main_menu");
	}
	
	#endregion
		
	#region Private object methods
	
	private void UpdateWorld()
	{
#if DEBUG_TEXTS
		Debug.Log(string.Format("LevelSelectionScript.UpdateWorld ()"));
#endif
		
		string prevWorld = SaveData.Instance.NNUKE_GetNextWorldName(-1);
		string nextWorld = SaveData.Instance.NNUKE_GetNextWorldName(1);
		
#if DEBUG_TEXTS
		Debug.Log(string.Format("LevelSelectionScript.UpdateWorld () . prevWorld: {0}, nextWorld: {1}"
			, prevWorld, nextWorld));
#endif
		
		Utility.SetState(m_worldBackButton.gameObject, !string.IsNullOrEmpty(prevWorld));
		Utility.SetState(m_worldNextButton.gameObject, !string.IsNullOrEmpty(nextWorld));

		if (AssignGamepadButtonsScript.Instance != null)
		{
			AssignGamepadButtonsScript.Instance.AssignButtons();
		}
		
		if (UICamera.selectedObject == m_worldBackButton.gameObject
			&& string.IsNullOrEmpty(prevWorld)
			&& OuyaInputWrapper.UseGamepad)
		{
			UICamera.selectedObject = m_worldNextButton.gameObject;
		}
		if (UICamera.selectedObject == m_worldNextButton.gameObject
			&& string.IsNullOrEmpty(nextWorld)
			&& OuyaInputWrapper.UseGamepad)
		{
			UICamera.selectedObject = m_worldBackButton.gameObject;
		}
		
		string worldLabel = Localization.Localize("menu_title_world");
		string worldId = SaveData.Instance.Config.SelectedWorld;
		string localizedText = worldLabel + " " + worldId;
		m_worldLabel.text = localizedText;
		
		m_levels.InitItems();
	}
	
	
	private void SetLevel(int levelIndex)
	{
		SaveData data = SaveData.Instance;
		string worldId = data.Config.SelectedWorld;
		xsd_ct_WorldInfo selectedWorld = data.NNUKE_GetWorld(worldId);
		if (selectedWorld == null)
			return;
		selectedWorld.SelectedLevel = levelIndex;

#if DEBUG_TEXTS
		Debug.Log(string.Format("LevelSelectionScript.SetLevel (levelIndex={0}) GOING TO GARAGE", levelIndex));
#endif
		
		Application.LoadLevel("garage");
	}
	
	#endregion
}
