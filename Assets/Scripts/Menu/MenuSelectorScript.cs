//#define DEBUG_TEXTS
//#define ENABLE_LUMOS

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

sealed public class MenuSelectorScript : MonoBehaviour 
{
	#region Public members, visible in Unity GUI
	public StatusBoxScript m_statusBox;
	
	public GameObject m_buttonExit;
	#endregion

	#region Private members
	private SocialLayerManagerScript.StateChanged m_fbHandler = null;
	private TwitterManagerScript.StateChanged m_twitterHandler = null;
	#endregion
	
	#region Unity MonoBehaviour base overrides
	void Awake()
	{
		IAPManagerScript iap = 	IAPManagerScript.Instance;
		if (iap != null)
		{
			iap.m_bindings.NNUKE_DoValidateAll(false);
		}
		
		#if UNITY_IPHONE
		Utility.SetState(m_buttonExit, false, true);
		#endif
		
		// make sure we have our persistent save data initialized
		SaveData saveData = SaveData.Instance;

		MusicManagerScript.Instance.PlayClip("intro", MusicManagerScript.SwitchMode.CrossFade);
	}
	void Start()
	{
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("MenuSelectorScript.{0} (time: {1}) - NUKE_GetIAPItem(0) == null => {2}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, Time.time
			, IAPManagerScript.Instance.NNUKE_GetIAPItem(0) == null));
		#endif

		IAPManagerScript iap = IAPManagerScript.Instance;
		if (iap != null && iap.NNUKE_GetIAPItem(0) == null)
		{
			iap.NNUKE_GetProductData();
		}
	}
	
	#endregion
	
	#region Button callbacks
	public void OnMessagePlay()
	{
		#if ENABLE_LUMOS
		Lumos.Event("Menu/Main/Play");
		#endif
		
		if (SaveData.Instance.Config.ShowStory)
		{
			SaveData.Instance.Config.ShowStory = false;
			SaveData.Instance.NNUKE_Save ();
			
			Application.LoadLevel("story_screen");
		}
		else
		{
			Application.LoadLevel("level_screen");
		}
	}
	public void OnMessageStore()
	{
		OnlineStoreSelectorScript.sm_sceneToGoBack = "main_menu";
		Application.LoadLevel("iap_store");
	}
	
	public void OnMessageSettings()
	{
		#if ENABLE_LUMOS
		Lumos.Event("Menu/Main/Settings");
		#endif
		Application.LoadLevel("settings");
	}
	public void OnMessageAbout()
	{
		#if ENABLE_LUMOS
		Lumos.Event("Menu/Main/About");
		#endif
		Application.LoadLevel("about");
	}
	public void OnMessageScore()
	{
		#if ENABLE_LUMOS
		Lumos.Event("Menu/Main/Score");
		#endif
		Application.LoadLevel("scores");
	}
	
	public void OnMessageQuit()
	{
		StartCoroutine("CleanupAndQuit");
	}
	
	public void OnMessageFacebook()
	{
		#if ENABLE_LUMOS
		Lumos.Event("Menu/Main/Facebook");
		#endif

		if (SocialLayerManagerScript.GetInstance() != null)
		{
			m_statusBox.Show(
				Localization.Localize("menu_statusbox_title_fb")
				, Localization.Localize("menu_statusbox_processing")
				, 20.0f);

			if (m_fbHandler == null) 
			{
				m_fbHandler = new SocialLayerManagerScript.StateChanged(FacebookChanged);
			}
			SocialLayerManagerScript.GetInstance().Changed += m_fbHandler;

			string message = Localization.Localize("menu_twitter_message_1");
			message += " " + System.DateTime.Now + ". ";
			message += Localization.Localize("menu_twitter_message_2");

			SocialLayerManagerScript.OpenDialog(
				Localization.Localize("menu_fb_url")
				, Localization.Localize("menu_fb_url_caption")
				, Localization.Localize("menu_fb_image_url")
				, Localization.Localize("menu_fb_image_caption")
				, message
				);
		}
		else 
		{
			FacebookChanged(SocialLayerManagerScript.RequestState.Fail);
		}
	}

	public void OnMessageTwitter()
	{
		#if ENABLE_LUMOS
		Lumos.Event("Menu/Main/Twitter");
		#endif
		
		if (TwitterManagerScript.GetInstance() != null) 
		{
			m_statusBox.Show(
				Localization.Localize("menu_statusbox_title_twitter")
				, Localization.Localize("menu_statusbox_processing")
				, 20.0f);
			
			if (m_twitterHandler == null) 
			{
				m_twitterHandler = new TwitterManagerScript.StateChanged(TwitterChanged);
			}
			TwitterManagerScript.GetInstance().Changed += m_twitterHandler;
			
			string message = Localization.Localize("menu_twitter_message_1");
			message += " " + System.DateTime.Now + ". ";
			message += Localization.Localize("menu_twitter_message_2");
			
			TwitterManagerScript.Post(message);
		}
		else 
		{
			TwitterChanged(TwitterManagerScript.RequestState.Fail);
		}
	}
	#endregion
	
	#region Private methods
	private IEnumerator CleanupAndQuit()
	{
		#if DEBUG_TEXTS	
		Debug.Log (string.Format ("MenuSelectorScript.CleanupAndQuit"));
		#endif

		MusicManagerScript.Instance.Pause(true);
		AudioListener.pause = true;
		AudioListener.volume = 0f;
		
		float exitTime = Time.time + 0.5f;
		
		while (exitTime > Time.time)
		{
			yield return new WaitForSeconds(0.2f);
		}
		
		StopCoroutine ("CleanupAndQuit");
		
		Application.Quit();
	}
	#endregion
	
	#region Event listener callbacks
	private void FacebookChanged(SocialLayerManagerScript.RequestState state) 
	{
		#if DEBUG_TEXTS	
		Debug.Log (string.Format ("MenuSelectorScript.FacebookChanged - state: {0}, time: {1}", state, Time.time));
		#endif
		
		StatusBoxScript.ShowFacebookStatus(m_statusBox, state, m_fbHandler);
	}
	
	private void TwitterChanged(TwitterManagerScript.RequestState state) 
	{
		#if DEBUG_TEXTS	
		Debug.Log (string.Format ("MenuSelectorScript.TwitterChanged - state: {0}, time: {1}", state, Time.time));
		#endif

		StatusBoxScript.ShowTwitterStatus(m_statusBox, state, m_twitterHandler);
		
	}
	#endregion
}