//#define DEBUG_TEXTS

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

sealed public class OnlineStoreSelectorScript : MonoBehaviour 
{
	// this is static as this script/object is used in two locations, and those two locations (level and garage screens)
	// set this to the value that they want we come back to after exiting the store
	public static string sm_sceneToGoBack;
	
	#region Public Unity Editor visible members
	public SlotSelectorScript m_slots;
	public GameObject m_noItemsRoot;
	public StatusBoxScript m_statusBox;
	public GameObject m_buyButton;
	public GameObject m_restoreButton;
	public GameObject m_backButton;
	#endregion
	
	#region Private members
	private int m_itemCount;
	private int m_selectedIndex;
	
	private IAPManagerScript.StateChanged m_iapStateHandler = null;
	#endregion
	
	#region Unity MonoBehaviour base overrides
	// Use this for early initialization
	void Awake ()
	{
		#if DEBUG_TEXTS		
		Debug.Log( string.Format ("OnlineStoreSelectorScript.{0}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()));
		#endif
		
	}
	
	void Start()
	{
		#if DEBUG_TEXTS		
		Debug.Log( string.Format ("OnlineStoreSelectorScript.{0} - IAPManagerScript.Instance != null: {1}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, IAPManagerScript.Instance != null));
		#endif
		
		#if ENABLE_LUMOS
		Lumos.Event(string.Format ("Online store entered"));
		#endif

		#if !UNITY_IPHONE
		m_buyButton.transform.position = new Vector3(
			m_buyButton.transform.position.x
			, 0.5f * (m_buyButton.transform.position.y + m_restoreButton.transform.position.y)
			, m_buyButton.transform.position.z);
		Utility.SetState(m_restoreButton, false, true);
		Destroy (m_restoreButton);
		#endif
		
		// we want to know about all purchases happening, so we can update the gui accordingly
		m_iapStateHandler = new IAPManagerScript.StateChanged(IAPStateChanged);
		IAPManagerScript.Instance.Changed += m_iapStateHandler;
		
		// fetch all items, NOTE! -> can't be in Awake as SlotSelecter needs to do stuff in its Awake also
		m_itemCount = 0;
		while (UpdateItem (m_itemCount))
		{
			m_itemCount++;
		}
		
		if (m_itemCount == 0 && IAPManagerScript.Instance != null)
		{
			IAPManagerScript.Instance.NNUKE_GetProductData();
		}
		
		Utility.SetState(m_noItemsRoot, m_itemCount == 0, true);
		
		m_slots.FinalizeSlots();
		
		if (AssignGamepadButtonsScript.Instance != null)
			AssignGamepadButtonsScript.Instance.AssignButtons(true);
	}
	
	void OnDestroy()
	{
		#if DEBUG_TEXTS		
		Debug.Log( string.Format ("OnlineStoreSelectorScript.{0}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()));
		#endif
		
		IAPManagerScript.Instance.Changed -= m_iapStateHandler;
	}

	#endregion
	
	#region Private methods.
	private void OnMessageBuy(int index)
	{
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("OnlineStoreSelectorScript.OnMessageBuy index: {0}", index ));
		#endif
		
		IAPData item = IAPManagerScript.Instance.NNUKE_GetIAPItem(index);
		if (item == null)
			return;
		
		m_statusBox.Show(
			Localization.Localize("menu_statusbox_title_store")
			, Localization.Localize("menu_statusbox_processing")
			, 20.0f);
		
		m_selectedIndex = index;
		
		IAPManagerScript.Instance.NNUKE_DoTransaction(item.productId, 1);
	}
	
	private void OnMessageBack()
	{
		if (string.IsNullOrEmpty(sm_sceneToGoBack))
		{
			sm_sceneToGoBack = "level_screen";
		}
		
		Application.LoadLevel(sm_sceneToGoBack);
	}
	
	private void OnMessageRestore()
	{
		#if DEBUG_TEXTS
		Debug.Log( "OnlineStoreSelectorScript.OnMessageRestore" );
		#endif
		#if UNITY_IPHONE
		IAPManagerScript.Instance.NNUKE_RestoreCompletedTransactions();
		#endif
	}
	
	private bool UpdateItem(int index)
	{
		IAPData item = IAPManagerScript.Instance.NNUKE_GetIAPItem(index);

		#if DEBUG_TEXTS		
		Debug.Log( string.Format ("OnlineStoreSelectorScript.{0} - index: {1}, item != null: {2}, contents: {3}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, index, item != null, (item != null) ? item.ToString() : "null"));
		#endif
		
		if (item == null)
			return false;
		
		bool bought = IAPManagerScript.Instance.NNUKE_HasBought(item.productId);
		bool canBuy = item.consumable || !bought;
		
		string statusLocId = (canBuy) ? "menu_iap_buy" : "menu_iap_already_bought";
		
		string description = 
			(!string.IsNullOrEmpty(item.descriptionLocalizationId))
				? Localization.Localize(item.descriptionLocalizationId)
				: item.description;
		
		m_slots.UpdateSelectorItem(index
			, item.productId
			, item.title
			, Localization.Localize(statusLocId)
			, (canBuy) ? item.formattedPrice : ""
			, description
			, item.spriteName
			, false
			, canBuy
			);
		
		return true;
	}
	
	private void IAPStateChanged(IAPManagerScript.RequestState state) 
	{
		#if DEBUG_TEXTS
			Debug.Log( string.Format ("OnlineStoreSelectorScript.{0} state={1}"
				, System.Reflection.MethodBase.GetCurrentMethod().ToString()
				, state
				));
		#endif
		
		if (state != IAPManagerScript.RequestState.None) 
		{
			string result = "";
			switch (state)
			{
			case IAPManagerScript.RequestState.Processing:
				result = "menu_statusbox_processing";
				break;
			case IAPManagerScript.RequestState.OK_Purchase:
				result = "menu_statusbox_purchase_success";
				break;
			case IAPManagerScript.RequestState.FAIL_Purchase:
				result = "menu_statusbox_purchase_failed";
				break;
			case IAPManagerScript.RequestState.CANCEL_Purchase:
				result = "menu_statusbox_purchase_cancelled";
				break;
			case IAPManagerScript.RequestState.OK_Restore:
				result = "menu_statusbox_restore_succesfull";
				break;
			case IAPManagerScript.RequestState.FAIL_Restore:
				result = "menu_statusbox_restore_failed";
				break;
			case IAPManagerScript.RequestState.OK_GetItems:
				result = "menu_statusbox_connection_ok";
				break;
			case IAPManagerScript.RequestState.FAIL_GetItems:
				result = "menu_statusbox_connection_failed";
				break;
			default:
				break;
			}
			
			if (!string.IsNullOrEmpty(result))
			{
				m_statusBox.Show(
					Localization.Localize("menu_statusbox_title_store")
					, Localization.Localize(result));
			}
			
			UpdateItem(m_selectedIndex);
			
			if (AssignGamepadButtonsScript.Instance != null)
			{
				AssignGamepadButtonsScript.Instance.AssignButtons(true);
			}
			if (OuyaInputWrapper.UseGamepad)
			{
				UICamera.selectedObject = m_backButton.gameObject;
			}
		}
	}
	#endregion
	
}
