using UnityEngine;
using System.Collections;
using System.Collections.Generic;

sealed public class PauseMenuScript : MonoBehaviour 
{
	#region Public GUI tweakable members
	public GameObject m_root;
	
	public GameObject[] m_ouyaRoots;
	public GameObject[] m_rightSideRoots;
	public GameObject[] m_leftSideRoots;
	
	public AudioClip m_soundAppears;
	
	#endregion
		
	#region Private data members
	private List<UITweener> m_tweens = new List<UITweener>();
	#endregion
		
	#region Unity MonoBehaviour base overrides

	void Awake()
	{
		UnityEngine.Object[] objs = GameObject.FindObjectsOfType(typeof(UITweener));
		foreach (UnityEngine.Object obj in objs)
		{
			m_tweens.Add ((UITweener) obj);
		}
		
		Utility.SetState(m_root, false, false);
	}
	
	#endregion
		
	#region Public object methods
	public void OnMessageEnter()
	{
		Utility.SetState(m_root, true, true);
		SetVisiblity();
		
		GameStateScript.Instance.SetToPause();
		GameStateScript.Instance.InputDisabled = true;
		
		if (AssignGamepadButtonsScript.Instance != null)
		{
			AssignGamepadButtonsScript.Instance.AssignButtons(true);
		}
		
		SoundManagerScript.PlayOneTime (gameObject, m_soundAppears, 0f);
	}

	public void OnMessageResume()
	{
		GameStateScript.Instance.SetDontAcceptInputTime(0.2f);
		GameStateScript.Instance.InputDisabled = false;
		
		Utility.SetState (m_root, false, false);
		GameStateScript.Instance.ReturnToPlay();
		
		foreach (UITweener tween in m_tweens)
		{
			tween.enabled = true;
			tween.Reset();
		}
	}
	
	public void OnMessageRestart()
	{
		GameStateScript.Instance.SetDontAcceptInputTime(0.2f);
		
		Application.LoadLevel(SaveData.Instance.NNUKE_GetCurrentSceneName());
	}
	
	public void OnMessageBack()
	{
		// this is needed so that we get the time scale back to normal
		GameStateScript.Instance.ReturnToPlay();
		Application.LoadLevel("garage");
	}
	
	#endregion
	
	#region Private object methods
	private void SetVisiblity()
	{
		if (OuyaInputWrapper.UseGamepad)
		{
			foreach (GameObject o in m_rightSideRoots)
			{
				Utility.SetState(o, false, true);
			}
			foreach (GameObject o in m_leftSideRoots)
			{
				Utility.SetState(o, false, true);
			}
		}
		else
		{
			foreach (GameObject o in m_ouyaRoots)
			{
				Utility.SetState(o, false, true);
			}
			
			GameObject[] disable = (SaveData.Instance.Config.Controls.ToLowerInvariant().Contains("right"))
				? m_leftSideRoots : m_rightSideRoots;

			foreach (GameObject o in disable)
			{
				Utility.SetState(o, false, true);
			}

			// hide turn-info (as with dpad we don't need one)?
			GameObject mi = Utility.GetObject("movementInfo");
			if (mi != null)
			{
				Utility.SetState (mi
					, SaveData.Instance.Config.Controls.ToLowerInvariant().Contains("analog")
					, true);
			}
		}
	}
	
	#endregion		
}
