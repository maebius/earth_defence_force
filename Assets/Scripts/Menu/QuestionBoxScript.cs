using UnityEngine;
using System.Collections;
using System.Collections.Generic;

sealed public class QuestionBoxScript : MonoBehaviour 
{
	#region Public members, visible in Unity GUI
	public GameObject m_root;
	public UILocalize m_title;
	public UILocalize m_text;
	
	public GameObject m_okButton;
	public GameObject m_cancelButton;
	
	public GameObject m_okHandler;
	public string m_okFunction;
	
	public GameObject m_cancelHandler;
	public string m_cancelFunction;
	
	public AudioClip m_soundAppears;
	#endregion
	
	#region Unity MonoBehaviour base overrides
	void Awake()
	{
		Utility.SetState(m_root, false, false);
	}
	
	public void Show(string titleLocalizationKey, string textLocalizationKey, bool showCancel = true) 
	{
		DisableCollidersScript.ReInitialize();

		Utility.SetState(m_root, true, true);
		
		m_title.key = titleLocalizationKey;
		m_text.key = textLocalizationKey;
		
		UICamera.selectedObject = m_cancelButton;
		
		Utility.SetState(m_cancelButton, showCancel, true);
		
		m_text.Localize();
		m_title.Localize();
		
		if (AssignGamepadButtonsScript.Instance != null)
		{
			AssignGamepadButtonsScript.Instance.AssignButtons(true);
		}

		// is there a delay?
		float delay = 0f;
		TweenScale ts = m_root.GetComponentInChildren<TweenScale>();
		if (ts != null)
		{
			delay = ts.delay;
		}
		
		SoundManagerScript.PlayOneTime (gameObject, m_soundAppears, delay);
	}
	#endregion
	
	#region Button handlers
	void OnButtonMessageCancel()
	{
		Utility.ResetTweens(m_root);
		CloseMyself(m_cancelHandler, m_cancelFunction);
	}
	
	void OnButtonMessageOk()
	{
		Utility.ResetTweens(m_root);
		CloseMyself(m_okHandler, m_okFunction);
	}
	
	#endregion

	#region Private methods
	private void CloseMyself(GameObject handler, string handlerFunction)
	{
		DisableCollidersScript.ReInitialize();
		
		Utility.SetState(m_root, false, false);
		if (handler)
		{
			handler.SendMessage(handlerFunction, SendMessageOptions.DontRequireReceiver);
		}
		
		if (AssignGamepadButtonsScript.Instance != null)
		{
			AssignGamepadButtonsScript.Instance.AssignButtons(true);
		}
	}
	
	#endregion

}
