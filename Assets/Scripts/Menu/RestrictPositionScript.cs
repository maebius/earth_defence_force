using UnityEngine;
using System.Collections;

sealed public class RestrictPositionScript : MonoBehaviour 
{
	public bool RestrictX;
	public bool RestrictY;
	public bool RestrictZ;
	
	public float RestrictionX;
	public float RestrictionY;
	public float RestrictionZ;
	
	// Update is called once per frame
	void Update () 
	{
		Vector3 currentPos = transform.localPosition;
		
		transform.localPosition = new Vector3(
			(RestrictX) ? RestrictionX : currentPos.x
			, (RestrictY) ? RestrictionY : currentPos.y
			, (RestrictZ) ? RestrictionZ : currentPos.z);
	}
}
