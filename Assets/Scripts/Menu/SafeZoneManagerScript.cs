using UnityEngine;
using System.Collections;
using System.Collections.Generic;

sealed public class SafeZoneManagerScript : MonoBehaviour 
{
	public float m_addRelativeOffsetX;
	public float m_addRelativeOffsetY;
	
	public float m_scaleManualHeight = 1f;
	
	public static void ApplySafeZone()
	{
		if (sm_instance == null)
			return;
		
		if (SaveData.Instance.Config.OverscanCompensation)
		{
			sm_instance.AddAnchorOffsets(
				sm_instance.m_addRelativeOffsetX
				, sm_instance.m_addRelativeOffsetY);
			sm_instance.ScaleRootManualHeight(sm_instance.m_scaleManualHeight);
		}
		else
		{
			sm_instance.Reset();
		}
	}
	
	private static SafeZoneManagerScript sm_instance = null;
	public static SafeZoneManagerScript Instance
	{
		get 
		{
			return sm_instance;
		}
	}
	
	private Dictionary<UIRoot, int> m_handledRoots = new Dictionary<UIRoot, int>();
	private Dictionary<UIAnchor, Vector2> m_handledAnchors = new Dictionary<UIAnchor, Vector2>();
	
	void Awake()
	{
		if (sm_instance != null)
		{
			DestroyImmediate(sm_instance);
		}
		
		DontDestroyOnLoad(gameObject);
		sm_instance = this;
	}
	
	void OnEnable()
	{
		m_handledRoots.Clear ();
		m_handledAnchors.Clear ();
	}

	[Obfuscar.Obfuscate]
	private void Reset()
	{
		foreach (KeyValuePair<UIRoot, int> kvp in m_handledRoots)
		{
			kvp.Key.manualHeight = kvp.Value;
		}		
		m_handledRoots.Clear ();
		
		foreach (KeyValuePair<UIAnchor, Vector2> kvp in m_handledAnchors)
		{
			kvp.Key.relativeOffset = kvp.Value;
		}		
		m_handledAnchors.Clear ();
	}
	
	[Obfuscar.Obfuscate]
	private void ScaleRootManualHeight(float scale)
	{
		UIRoot[] objs = (UIRoot[]) GameObject.FindSceneObjectsOfType(typeof(UIRoot));
		foreach (UIRoot root in objs)
		{
			if (m_handledRoots.ContainsKey(root))
				continue;
			int original = root.manualHeight;
			root.manualHeight = (int) (root.manualHeight * scale);
			m_handledRoots.Add (root, original);
		}
	}
	
	[Obfuscar.Obfuscate]
	private void AddAnchorOffsets(float inputOffsetX, float inputOffsetY)
	{
		UIAnchor[] objs = (UIAnchor[]) GameObject.FindSceneObjectsOfType(typeof(UIAnchor));

		foreach (UIAnchor anchor in objs)
		{
			if (m_handledAnchors.ContainsKey(anchor))
				continue;
			
			Vector2 original = anchor.relativeOffset;
			
			// our offsetX/Y are in whole screen viewport (1,1), so we need possibly to scale
			Rect viewPort = anchor.uiCamera.rect;
			float offsetY = inputOffsetX / viewPort.height;
			float offsetX = inputOffsetY / viewPort.width;
			
			switch (anchor.side)
			{
			case UIAnchor.Side.Bottom:
				anchor.relativeOffset.y += offsetY;
				break;
			case UIAnchor.Side.Top:
				anchor.relativeOffset.y -= offsetY;
				break;
			case UIAnchor.Side.Left:
				anchor.relativeOffset.x += offsetX;
				break;
			case UIAnchor.Side.Right:
				anchor.relativeOffset.x -= offsetX;
				break;
			case UIAnchor.Side.BottomLeft:
				anchor.relativeOffset.x += offsetX;
				anchor.relativeOffset.y += offsetY;
				break;
			case UIAnchor.Side.TopLeft:
				anchor.relativeOffset.y -= offsetY;
				anchor.relativeOffset.x += offsetX;
				break;
			case UIAnchor.Side.TopRight:
				anchor.relativeOffset.x -= offsetX;
				anchor.relativeOffset.y -= offsetY;
				break;
			case UIAnchor.Side.BottomRight:
				anchor.relativeOffset.x -= offsetX;
				anchor.relativeOffset.y += offsetY;
				break;
			default:
				// for UIAnchor.Side.Center we don't wanna do anything 
				break;
			}
			m_handledAnchors.Add (anchor, original);
		}
	}
}
