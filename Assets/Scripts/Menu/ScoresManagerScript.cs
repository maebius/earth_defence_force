//#define DEBUG_TEXTS

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using UnityEngine.SocialPlatforms;
#if UNITY_IPHONE
using UnityEngine.SocialPlatforms.GameCenter;
#endif

sealed public class ScoreData
{
	public string leaderBoardID;
	public int score;
	public int rank;
	public int bestScore;
	public int scoreCount;
}

sealed public class ScoresManagerScript : MonoBehaviour 
{
	#region Enums, structs, and inner classes
	public enum RequestState
	{
		None
		, Processing
		, ShowOk
		, GetScoresOk
		, GetScoresFail
		, LoadFriendsOk
		, LoadFriendsFail
		, SubmitScoreOk
		, SubmitScoreFail
		, AchievementsOk
		, AuthenticateFail
		, AuthenticateOk
	};
	#endregion
	
	#region Delegates And Events
	public delegate void StateChanged(RequestState state);
	public event StateChanged Changed;
	#endregion
	
	#region Public data members.
	[HideInInspector]
	public static bool LeaderboardsEnabled
	{
		get 
		{
			return !Application.isEditor &&
				(Application.platform == RuntimePlatform.IPhonePlayer
					|| Application.platform == RuntimePlatform.OSXPlayer);
		}
	}
	
	[HideInInspector]
	public static ScoresManagerScript Instance 
	{
		get 
		{
			if (sm_instance == null)
			{
				// as we cannot instantiate MonoBehaviors directy with "new", we need to create
				// gameobject and add a component to that ...
				sm_instance = Object.FindObjectOfType(typeof(ScoresManagerScript)) as ScoresManagerScript;
				if (sm_instance == null)
				{
					#if DEBUG_TEXTS
					Debug.LogError( 
						string.Format (
						"ScoresManagerScript.Instance.get - sm_instance is null! We need to have this somewhere"));
					#endif
				}
			}
			return sm_instance;
		}
	}
	[HideInInspector]
	public bool ScoresLoaded 
	{
		get { return m_scoreData.Count > 0; }
	}
	[HideInInspector]
	public string CurrentUserId
	{
		get { return Social.localUser.id; }
	}
	
	[HideInInspector]
	public Dictionary<string, ScoreData> ScoreData
	{
		get { return m_scoreData; }
	}
	#endregion
	
	#region Private data members
	private static ScoresManagerScript sm_instance = null;
	
	private bool m_showPending = false;
	
	private RequestState State
	{
		set 
		{
			/*
			#if DEBUG_TEXTS
			Debug.Log( string.Format ("ScoresManagerScript.{0} state={1}, has listeners: {2}"
				, System.Reflection.MethodBase.GetCurrentMethod().ToString()
				, value
				, Changed != null));
			#endif
			*/
			
			m_state = value;
			if (Changed != null)
			{
				Changed(m_state);
			}
		}
	}
	
	private RequestState m_state = RequestState.None;
	
	private List<string> m_leaderBoardsToLoad = new List<string>();
	private Dictionary<string, ScoreData> m_scoreData = new Dictionary<string, ScoreData>();
	
	private List<KeyValuePair<string, int>> m_scoresToSendQueue  = new List<KeyValuePair<string, int>>();
	
	#endregion
	
	#region Unity MonoBehaviour base overrides
	void Awake () 
	{
		/*
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("ScoresManagerScript.{0} - Social.localUser.id: {1}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, Social.localUser.id));
		#endif
		*/
		
		if (sm_instance == null) 
		{ 
			sm_instance = this;
			DontDestroyOnLoad(gameObject);
			
			if (IsInitialized())
			{
				// we want to have scores instantly available when player enters scores screen
				// so we try to load these as a first thing
				LoadScores();
			}
			else
			{
				Initialize();
			}
		} 
		else 
			Destroy(gameObject); 
		
	}
	#endregion
	
	#region Public interface
	[Obfuscar.Obfuscate]
	public bool IsInitialized()
	{
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("ScoresManagerScript.IsInitialized"));
		#endif
		
		return !string.IsNullOrEmpty(Social.localUser.id) 
			&& Social.localUser.id != "0"
			&& Social.localUser.authenticated;
	}
	
	[Obfuscar.Obfuscate]
	public void Clear()
	{
		SaveData.Instance.Config.LeaderboardCheck = true;
		m_scoreData.Clear ();
	}
	
	[Obfuscar.Obfuscate]
	public bool GetScoreData(string leaderboard, ref int score, ref int best, ref int rank, ref int scoreCount)
	{
		/*
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("ScoresManagerScript.{0} has {1}: {2}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, leaderboard
			, m_scoreData.ContainsKey(leaderboard)
			));
		#endif
		*/
		
		if (!m_scoreData.ContainsKey(leaderboard))
			return false;
		
		ScoreData sd = m_scoreData[leaderboard];
		score = sd.score;
		best = sd.bestScore;
		rank = sd.rank;
		scoreCount = sd.scoreCount;
		
		/*
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("ScoresManagerScript.{0} score: {1}, best: {2}, rank: {3}, count: {4}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, score, best, rank, scoreCount));
		#endif
		*/
		
		return true;
	}
	
	[Obfuscar.Obfuscate]
	public bool UpdateHighScoreCurrent(int score)
	{
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("ScoresManagerScript.{0} score: {1},"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, score));
		#endif
		
		string world = SaveData.Instance.Config.SelectedWorld;
		xsd_ct_WorldInfo wi = SaveData.Instance.NNUKE_GetWorld(world);
		int level = wi.SelectedLevel;
		
		bool newHigh = UpdateHighScore(score, world, level, true);
		if (newHigh)
		{
			UpdateSending();
		}
		
		return newHigh;
	}
	
	private void ValidateLevelOpen(int index, xsd_ct_WorldInfo wi, xsd_ct_LevelInfo li)
	{
		// this function is used to make sure that if we sync scores from Leaderboards,
		// we accordingly also open the levels with scores (and also one beyond)
		// -> it would be funny to have scores (thus, having played) from levels but not be able to 
		// play them ...
		
		li.Open = true;
		wi.LastOpenLevel = Mathf.Max (wi.LastOpenLevel, index);
		
		int next = index + 1;
		if (wi.LevelInfo.Length > next)
		{
			wi.LastOpenLevel = next;
			wi.LevelInfo[next].Open = true;
		}
	}
	
	private bool UpdateHighScore(int score, string worldId, int level, bool updateTotal, bool forceSend = true)
	{
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("ScoresManagerScript.{0} score: {1}, worldId: {2}, level: {3}, updateTotal: {4}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, score, worldId, level, updateTotal));
		#endif
		
		xsd_ct_WorldInfo wi = SaveData.Instance.NNUKE_GetWorld(worldId);
		
		if (wi == null)
		{
			#if DEBUG_TEXTS
			Debug.LogWarning( string.Format ("ScoresManagerScript.{0} could not find world: {1}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, worldId));
			#endif
	
			return false;
		}
		xsd_ct_WorldData wd = GameData.Instance.NNUKE_GetWorld(wi.ReferencesTo);
		xsd_ct_LevelData ld = GameData.Instance.NNUKE_GetLevelData(
			worldId, level);
		
		xsd_ct_LevelInfo li = SaveData.Instance.NNUKE_GetCurrentLevel();
		bool newHigh = SaveData.Instance.NNUKE_IsNewHighScore(li, score);
		
		if ((forceSend || newHigh) && !string.IsNullOrEmpty(ld.LeaderboardID) && score > 0)
		{
			// tell leaderboards about this ... (no matter if it's the highest)
			m_scoresToSendQueue.Add (new KeyValuePair<string, int> 
				(ld.LeaderboardID, score));
			// ... also we clear our previous scores so that we know there's new to be fetched
			m_scoreData.Clear ();
			
			li.HighscorePrevious = li.Highscore;
			li.Highscore = score;
			li.HighscoreDate = System.DateTime.Now;
			// NOTE! This makes sure that the level is open, if 
			// we have started locally the game again but are able to get high score from leaderboards
			ValidateLevelOpen(level, wi, li);
			
			if (newHigh)
			{
				SaveData.Instance.NNUKE_RefreshTotalScore(wi);
			}
			if (updateTotal && !string.IsNullOrEmpty(wd.LeaderboardID))
			{
				m_scoresToSendQueue.Add (new KeyValuePair<string, int> 
					(wd.LeaderboardID, SaveData.Instance.TotalScore));
			}
		}
		return newHigh;
	}
	
	public bool IsCurrentUserLeaderboardUser()
	{
		return SaveData.Instance.Config.PlayerID == CurrentUserId;
	}
	
	public void ShowUI()
	{
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("ScoresManagerScript.{0}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()));
		#endif
	
		State = RequestState.Processing;
	
		if (IsInitialized())
		{
			State = RequestState.ShowOk;
			m_showPending = false;
			Social.ShowLeaderboardUI();
		}
		else
		{
			m_showPending = true;
			Initialize ();
		}
	}
	public void LoadScores()
	{
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("ScoresManagerScript.{0}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()));
		#endif
		
		RefreshAllLeaderboards();
	}
	
	public void LoadAchievements()
	{
		Social.LoadAchievements(ProcessLoadedAchievements);
	}
	
	public void LoadFriends()
	{
		Social.localUser.LoadFriends(ProcessLoadedFriends);
	}
	
	public void UpdateAllLocalScoresFromGameCenter(bool overwrite)
	{
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("ScoresManagerScript.{0} "
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()));
		#endif
		
		m_scoresToSendQueue.Clear ();
		
		foreach (xsd_ct_WorldInfo wi in SaveData.Instance.Config.WorldInfo)
		{
			string worldId = wi.ReferencesTo;
			xsd_ct_WorldData wd = GameData.Instance.NNUKE_GetWorld(worldId);
			bool totalUpdate = false;
			for (int i = 0; i < wi.LevelInfo.Length; i++)
			{
				totalUpdate = false;
				xsd_ct_LevelInfo li = wi.LevelInfo[i];
				xsd_ct_LevelData ld = GameData.Instance.NNUKE_GetLevelData(worldId, i);
		
				int score = li.Highscore;
				RefreshDataBetweenLocalAndGC(ld.LeaderboardID, overwrite, ref score);
				li.Highscore = score;
				if (score > 0)
				{
					// NOTE! This makes sure that the level is open, if 
					// we have started locally the game again but are able to get high score from leaderboards
					ValidateLevelOpen(i, wi, li);
				}
			}
			
			int totalScore = SaveData.Instance.TotalScore;
			RefreshDataBetweenLocalAndGC(wd.LeaderboardID, overwrite, ref totalScore);
			SaveData.Instance.TotalScore = totalScore;
		}
		
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("ScoresManagerScript.{0} - number of local scores bigger than GC: {1}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, m_scoresToSendQueue.Count));
		#endif
		
		if (m_scoresToSendQueue.Count > 0)
		{
			// need to clear scores so we know they have changed
			m_scoreData.Clear ();
			State = RequestState.Processing;
			UpdateSending();
		}
		SaveData.Instance.NNUKE_Save();
	}
	
	public void SendAllScoresToGameCenter()
	{
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("ScoresManagerScript.{0} "
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()));
		#endif
		
		m_scoresToSendQueue.Clear ();
		
		foreach (xsd_ct_WorldInfo wi in SaveData.Instance.Config.WorldInfo)
		{
			string worldId = wi.ReferencesTo;
			xsd_ct_WorldData wd = GameData.Instance.NNUKE_GetWorld(worldId);
			
			for (int i = 0; i < wi.LevelInfo.Length; i++)
			{
				xsd_ct_LevelInfo li = wi.LevelInfo[i];
				xsd_ct_LevelData ld = GameData.Instance.NNUKE_GetLevelData(worldId, i);
				if (li.Open && li.Highscore > 0)
					m_scoresToSendQueue.Add (new KeyValuePair<string, int>(
						ld.LeaderboardID, li.Highscore));
			}
			if (SaveData.Instance.TotalScore > 0)
			{
				m_scoresToSendQueue.Add (new KeyValuePair<string, int> 
					(wd.LeaderboardID, SaveData.Instance.TotalScore));
			}
		}
		
		m_scoreData.Clear();
		State = RequestState.Processing;
		UpdateSending ();
	}
	
	#endregion
	
	#region Private object methods.
	// NOTE! This is allowed to be called only once (unless logging out)
	private void Initialize () 
	{
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("ScoresManagerScript.{0} - LeaderboardsEnabled: {1}, LeaderboardCheck: {2}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, LeaderboardsEnabled
			, SaveData.Instance.Config.LeaderboardCheck));
		#endif
		
		if (!LeaderboardsEnabled)
		{
			SaveData.Instance.Config.LeaderboardCheck = false;
		}
		
		if (!SaveData.Instance.Config.LeaderboardCheck)
		{
			m_scoreData.Clear ();
			m_scoresToSendQueue.Clear ();
			m_leaderBoardsToLoad.Clear ();
			State = RequestState.AuthenticateFail;
			return;
		}
		
		Social.localUser.Authenticate(ProcessAuthentication);
	}
	
	private void RefreshDataBetweenLocalAndGC(string leaderboardID, bool overwrite, ref int localScore)
	{
		int score = 0, best = 0, rank = 0, count = 0;
		bool hasResult = GetScoreData(leaderboardID, ref score, ref best, ref rank, ref count);
		
		if (hasResult && score < localScore && localScore > 0)
		{
			// we need to update the Game Center instead ...
			m_scoresToSendQueue.Add (new KeyValuePair<string, int> 
				(leaderboardID, localScore));
		}
		
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("ScoresManagerScript.{0} leaderboard: {1}, hasResult: {4} local: {2}, gc: {3}, overwrite: {5}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, leaderboardID, localScore, score, hasResult, overwrite));
		#endif
		
		if (overwrite || score > localScore)
		{
			localScore = score;
		}
	}

	private void RefreshAllLeaderboards()
	{
		m_leaderBoardsToLoad.Clear();
		m_scoreData.Clear();
		
		foreach (xsd_ct_WorldInfo wi in SaveData.Instance.Config.WorldInfo)
		{
			xsd_ct_WorldData wd = GameData.Instance.NNUKE_GetWorld(wi.ReferencesTo);
			
			for (int i = 0; i < wi.LevelInfo.Length; i++)
			{
				xsd_ct_LevelInfo li = wi.LevelInfo[i];
				xsd_ct_LevelData ld = GameData.Instance.NNUKE_GetLevelData(wi.ReferencesTo, i);
				if (!string.IsNullOrEmpty(ld.LeaderboardID))
					m_leaderBoardsToLoad.Add(ld.LeaderboardID);
			}
			if (!string.IsNullOrEmpty(wd.LeaderboardID))
				m_leaderBoardsToLoad.Add(wd.LeaderboardID);
		}
		
		State = RequestState.Processing;
		UpdateLeaderboards();
	}
	
	private void UpdateSending()
	{
		if (m_scoresToSendQueue.Count == 0)
		{
			State = RequestState.SubmitScoreOk;
			return;
		}
		
		if (IsInitialized())
		{
			string leaderboard = m_scoresToSendQueue[0].Key;
			int score = m_scoresToSendQueue[0].Value;
			m_scoresToSendQueue.RemoveAt(0);
			
			#if DEBUG_TEXTS
			Debug.Log( string.Format ("ScoresManagerScript.{0} id: {1}, score: {2} (user: {3})"
				, System.Reflection.MethodBase.GetCurrentMethod().ToString()
				, leaderboard, score, Social.localUser.id));
			#endif
			
			Social.ReportScore(score, leaderboard, ProcessReportedScore);
		}
		else
		{
			Initialize();
		}
	}
	
	private void UpdateLeaderboards()
	{
		/*
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("ScoresManagerScript.{0} - m_leaderBoardsToLoad.Count: {1}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, m_leaderBoardsToLoad.Count));
		#endif
		*/
		
		if (m_leaderBoardsToLoad.Count == 0)
		{
			SaveData.Instance.NNUKE_Save();
			State = RequestState.GetScoresOk;
			return;
		}
		
		UpdateLoadScores();
	}
	
	private void UpdateLoadScores()
	{
		if (IsInitialized())
		{
			ILeaderboard leaderboard = Social.CreateLeaderboard();
			leaderboard.id = m_leaderBoardsToLoad[0];
			leaderboard.LoadScores((bool success) =>
				{
					ProcessLoadedScores(leaderboard.id, leaderboard.localUserScore, leaderboard.scores);
					m_leaderBoardsToLoad.RemoveAt(0);
					
					// NOTE! This is ugly recursive call, but does not matter
				
					// continue with the rest, if there's any 
					UpdateLeaderboards();
				}
			);
		}
		else
		{
			Initialize();
		}
	}
	
	// Listeners
	private void ProcessAuthentication(bool success)
	{
		/*
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("ScoresManagerScript.{0} - success: {1}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, success));
		#endif
		*/
		// Game center ID might have changed, so we need to clear the cached data
		m_scoreData.Clear();
		if (!success)
		{
			SaveData.Instance.Config.LeaderboardCheck = false;
			State = RequestState.AuthenticateFail;
			
			return;
		}
		
		ProcessPending();
	}

	private void ProcessPending()
	{
		if (m_showPending)
		{
			ShowUI();
		}
		else if (m_leaderBoardsToLoad.Count > 0)
		{
			UpdateLoadScores ();
		}
		else if (m_scoresToSendQueue.Count > 0)
		{
			UpdateSending();
		}
		else 
		{
			State = RequestState.AuthenticateOk;
		}
	}
	
	private void ProcessLoadedAchievements(IAchievement[] achievements)
	{
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("ScoresManagerScript.{0} - length: {1}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, achievements.Length));
		#endif
	
		foreach (IAchievement achievement in achievements)
		{
			// TODO: currently unimplemented
		}
		
		State = RequestState.AchievementsOk;
	}
	
	private void ProcessLoadedScores(string id, IScore localUserScore, IScore[] scores)
	{
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("ScoresManagerScript.{0} - leaderboard: {1}, localScore != null: {2}, localScore: {3}, scoresLength: {4}, currentId: {5}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, id
			, localUserScore != null
			, (localUserScore != null) ? localUserScore.value.ToString() : "null"
			, scores.Length
			, CurrentUserId
			));
		#endif	
		
		if (localUserScore != null || scores.Length > 0)
		{	
			int currentMyScore = (localUserScore != null) ? (int) localUserScore.value : 0;
			int currentMyRank = (localUserScore != null) ? localUserScore.rank : 0;
			int currentBestScore = (localUserScore != null) ? (int) localUserScore.value : int.MinValue;
			bool foundOwn = false;
			foreach (IScore score in scores)
			{
				currentBestScore = Mathf.Max ((int) score.value, currentBestScore);
				if (score.userID == Social.localUser.id)
					foundOwn = true;
				
				/*
				#if DEBUG_TEXTS
				Debug.Log( string.Format ("ScoresManagerScript.{0} - leaderboard: {1}, rank: {2}, score: {3}, isCurrent: {4}, userID: {5}, myID/Name: {6} / {7} (scoresCount: {8})"
					, System.Reflection.MethodBase.GetCurrentMethod().ToString()
					, score.leaderboardID
					, score.rank
					, score.value
					, score.userID == Social.localUser.id
					, score.userID
					, Social.localUser.id
					, Social.localUser.userName
					, scores.Length
					));
				#endif	
				*/
			}
			
			// store locally
			m_scoreData[id] = new ScoreData();
			ScoreData data = m_scoreData[id];
			
			data.score = currentMyScore;
			data.bestScore = currentBestScore;
			data.scoreCount = scores.Length + ((foundOwn) ? 0 : 1);
			data.rank = currentMyRank;
		}	
	}
	
	private void ProcessReportedScore(bool success)
	{
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("ScoresManagerScript.{0} - success: {1}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, success));
		#endif
		
		if (!success)
		{
			// we cancel all the rest also ...
			m_scoresToSendQueue.Clear();
			State = RequestState.SubmitScoreFail;
		}
		
		UpdateSending();
	}
	
	private void ProcessLoadedFriends(bool success)
	{
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("ScoresManagerScript.{0} - success: {1}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, success));
		#endif
	
		State = (success) ? RequestState.LoadFriendsOk : RequestState.LoadFriendsFail;
		
		foreach (IUserProfile profile in Social.localUser.friends)
		{
			// TODO: currently unimplemented -> not used
			
			/*Debug.Log(string.Format ("ScoresManagerScript.ProcessLoadedFriends - friend id/userName/state: {0}/{1}/{2}"
				, profile.id, profile.userName, profile.state));*/
		}
	}
	#endregion
}
