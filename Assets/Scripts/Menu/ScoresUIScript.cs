//#define DEBUG_TEXTS

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

sealed public class ScoresUIScript : MonoBehaviour 
{
	
	#region Static variables 
	public static bool sm_forceSyncQuestion = false;
	private static bool sm_tryToSync = true;
	#endregion
	
	#region Public variables shown in Unity Editor GUI
	public GameObject m_root;
	public GameObject m_gameCenterButton;
	public UIButton m_worldBackButton;
	public UIButton m_worldNextButton;
	
	public StarRatingScript m_scores;
	
	public UILabel m_title;
	
	public GameObject[] m_hideIfNoLeaderBoards;
	
	public QuestionBoxScript m_questionBox;
	public StatusBoxScript m_statusBox;
	#endregion
	
	#region Private variables
	private ScoresManagerScript.StateChanged m_scoresHandler = null;
	
	private GameObject m_previousSelection;
	
	private LevelButton m_totalScore = new LevelButton();
	
	
	private bool m_syncedLocalToGC = false;
	#endregion
	
	#region Unity MonoBehavior overrides.
	void Awake()
	{
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("ScoresUIScript.{0} - LeaderboardsEnabled: {1}, LeaderboardCheck: {2}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, ScoresManagerScript.LeaderboardsEnabled
			, SaveData.Instance.Config.LeaderboardCheck));
		#endif
		
		m_totalScore.Init ("total score");
	
		if (!ScoresManagerScript.LeaderboardsEnabled)
		{
			Utility.SetState(m_gameCenterButton, false, true);
			foreach (GameObject go in m_hideIfNoLeaderBoards)
			{
				Utility.SetState(go, false, true);
			}
			// also some moving ...
			m_root.transform.localPosition = new Vector3(0f, -40f, 0f);
			sm_tryToSync = false;
	
			return;
		}
		
		if (m_scoresHandler == null)
		{
			m_scoresHandler = new ScoresManagerScript.StateChanged(ScoresChanged);
		}
		ScoresManagerScript.Instance.Changed += m_scoresHandler;
		
		sm_tryToSync = SaveData.Instance.Config.LeaderboardCheck;
		
		m_syncedLocalToGC = false;
	}
	
	void OnDestroy()
	{
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("ScoresUIScript.{0} "
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()));
		#endif
		
		if (m_scoresHandler != null)
			ScoresManagerScript.Instance.Changed -= m_scoresHandler;
	}
	
	void Start()
	{
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("ScoresUIScript.{0} "
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()));
		#endif
		
		if (sm_tryToSync && !ScoresManagerScript.Instance.ScoresLoaded)
		{
			ShowLogInBox();
			ScoresManagerScript.Instance.LoadScores();
		}
		
		UpdateContents();
	}
	#endregion
	
	#region Public methods
	
	#endregion
	
	#region Private methods
	private void CheckIfNeedConfirmation()
	{
		string id = ScoresManagerScript.Instance.CurrentUserId;
		string saveId = SaveData.Instance.Config.PlayerID;
		
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("ScoresUIScript.{0} saveId: {1}, id: {2}, sm_tryToSync: {3}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, saveId, id, sm_tryToSync));
		#endif
		
		if (!sm_tryToSync && !sm_forceSyncQuestion)
			return;
		
		if (!sm_forceSyncQuestion && string.IsNullOrEmpty(saveId))
		{
			// first time to log with GC, so we just
			// use this account for the current scores
			SaveData.Instance.Config.PlayerID = id;
			SaveData.Instance.NNUKE_Save();
			ScoresManagerScript.Instance.SendAllScoresToGameCenter();
		}
		
		if (sm_forceSyncQuestion || id != saveId)
		{
			m_syncedLocalToGC = false;
			
			// current results different than the logged in account
			// -> we need to ask if we perform syncing
			if (OuyaInputWrapper.UseGamepad)
				m_previousSelection = UICamera.selectedObject;
			
			m_questionBox.Show ("menu_scores_question_title", "menu_scores_question_sync");		}
		else
		{
			// first, make sure results are in sync
			// if local score is bigger than gc, this call reschedules the updating of gc score
			if (!m_syncedLocalToGC)
			{
				ScoresManagerScript.Instance.UpdateAllLocalScoresFromGameCenter(false);
				m_syncedLocalToGC = true;
			}
			
			UpdateContents();
			SaveData.Instance.NNUKE_Save();
		}
	}
	
	private void UpdateContents()
	{
		string prevWorld = SaveData.Instance.NNUKE_GetNextWorldName(-1);
		string nextWorld = SaveData.Instance.NNUKE_GetNextWorldName(1);
		
		Utility.SetState(m_worldBackButton.gameObject, !string.IsNullOrEmpty(prevWorld));
		Utility.SetState(m_worldNextButton.gameObject, !string.IsNullOrEmpty(nextWorld));
		
		if (AssignGamepadButtonsScript.Instance != null)
		{
			AssignGamepadButtonsScript.Instance.AssignButtons();
		}
		
		if (UICamera.selectedObject == m_worldBackButton.gameObject
			&& string.IsNullOrEmpty(prevWorld)
			&& OuyaInputWrapper.UseGamepad)
		{
			UICamera.selectedObject = m_worldNextButton.gameObject;
		}
		if (UICamera.selectedObject == m_worldNextButton.gameObject
			&& string.IsNullOrEmpty(nextWorld)
			&& OuyaInputWrapper.UseGamepad)
		{
			UICamera.selectedObject = m_worldBackButton.gameObject;
		}
		
		string worldId = Localization.Localize("menu_title_world");
		string world = SaveData.Instance.Config.SelectedWorld;
		
		string localizedText = worldId + " " + world;
		
		m_title.text = localizedText;
		
		UpdateScores();
	}
	
	private void UpdateScores()
	{
		/*
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("ScoresUIScript.{0} count: {1}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, ScoresManagerScript.Instance.ScoreData.Count
			));
		#endif
		*/
		
		string worldId = SaveData.Instance.Config.SelectedWorld;
		xsd_ct_WorldInfo wi = SaveData.Instance.NNUKE_GetWorld(worldId);
		xsd_ct_WorldData wd = GameData.Instance.NNUKE_GetWorld(wi.ReferencesTo);
		
		int score = SaveData.Instance.TotalScore;
		int best = 0;
		int rank = 0;
		int scoreCount = -1;
		
		if (sm_tryToSync)
		{
			ScoresManagerScript.Instance.GetScoreData(wd.LeaderboardID
				, ref score, ref best, ref rank, ref scoreCount);
			if (SaveData.Instance.TotalScore > score)
			{
				score = SaveData.Instance.TotalScore;
				scoreCount = -1;
			}
		}
		m_totalScore.SetScoreData(score, best, rank, scoreCount);
		
		// NOTE! Here we actually do not mind if the level is open or not 
		// with this save game / Game Center id
		// -> we only mind if there is a score -> if there is, we show the entry
		
		int lastOpen = 0;
		for (int i = 0; i < wi.LevelInfo.Length; i++)
		{
			xsd_ct_LevelData ld = GameData.Instance.NNUKE_GetLevelData(worldId, i);
			xsd_ct_LevelInfo li = wi.LevelInfo[i]; 
			score = li.Highscore;
			best = 0;
			rank = 0;
			scoreCount = -1;
			
			if (sm_tryToSync)
			{
				ScoresManagerScript.Instance.GetScoreData(ld.LeaderboardID
					, ref score, ref best, ref rank, ref scoreCount);
				
				if (li.Highscore > score)
				{
					score = li.Highscore;
					scoreCount = -1;
				}
			}
			bool show = score > 0;
			
			int starCount = GameData.Instance.NNUKE_GetStarCountFor(worldId, i, score);
			m_scores.SetLevelOpen(i, show, starCount, score, false);
			m_scores.SetScoreData(i, show, score, best, rank, scoreCount);
			if (show)
				lastOpen = i;
		}
		
		m_scores.SetStateFrom(lastOpen+1, false);
	}
	
	#endregion
	
	#region Button handlers
	private void OnMessageWorldLeft()
	{
		string prevWorld = SaveData.Instance.NNUKE_GetNextWorldName(-1);
		SaveData.Instance.Config.SelectedWorld = prevWorld;
		
		UpdateContents();
	}
	private void OnMessageWorldRight()
	{
		string prevWorld = SaveData.Instance.NNUKE_GetNextWorldName(1);
		SaveData.Instance.Config.SelectedWorld = prevWorld;

		UpdateContents();
	}
	
	private void ShowLogInBox()
	{
		m_statusBox.Show(
			Localization.Localize("menu_scores_leaderboards_title")
			, Localization.Localize("menu_scores_please_wait_info")
			, 5f);
	}
	
	private void OnMessageGameCenter()
	{
		SaveData.Instance.Config.LeaderboardCheck = true;
		
		sm_tryToSync = true;
		if (ScoresManagerScript.Instance.IsInitialized())
			CheckIfNeedConfirmation();
		else
			ShowLogInBox();
		
		ScoresManagerScript.Instance.ShowUI();
	}
	private void OnMessageGameCenterSynchronize()
	{
		ScoresManagerScript sm = ScoresManagerScript.Instance;
		sm_tryToSync = true;
		// associate current scores with the game center id
		SaveData.Instance.Config.PlayerID = sm.CurrentUserId;
		// update data from Game Center to local
		sm.UpdateAllLocalScoresFromGameCenter(true);
		UpdateScores();
	}

	private void OnMessageGameCenterCancel()
	{
		// we did not want to sync, so we do nothing here
		SaveData.Instance.Config.LeaderboardCheck = false;
		sm_tryToSync = false;
	}
	
	#endregion
	#region Event listener callbacks
	private void ScoresChanged(ScoresManagerScript.RequestState state) 
	{
		#if DEBUG_TEXTS	
		Debug.Log (string.Format ("ScoresUIScript.ScoresChanged - state: {0}, time: {1}", state, Time.time));
		#endif
		
		switch (state)
		{
		case ScoresManagerScript.RequestState.AuthenticateFail:
		{
			m_statusBox.Show(
				Localization.Localize("menu_scores_authenticate_failed_title")
				, Localization.Localize("menu_scores_authenticate_failed_info")
				, 5f);
			
			sm_tryToSync = false;
			UpdateScores();
			break;
		}
		case ScoresManagerScript.RequestState.AuthenticateOk:
		{
			sm_tryToSync = true;
			if (!ScoresManagerScript.Instance.ScoresLoaded)
			{
				ScoresManagerScript.Instance.LoadScores();
			}
			break;
		}
		case ScoresManagerScript.RequestState.ShowOk:
		{
			sm_tryToSync = true;
			if (!ScoresManagerScript.Instance.ScoresLoaded)
			{
				ScoresManagerScript.Instance.LoadScores();
			}
			break;
		}
		case ScoresManagerScript.RequestState.GetScoresOk:
		{
			sm_tryToSync = true;
			CheckIfNeedConfirmation();
			break;
		}
		case ScoresManagerScript.RequestState.SubmitScoreOk:
		{
			sm_tryToSync = true;
			ScoresManagerScript.Instance.LoadScores();
			break;
		}
		default:
			break;
		}
		
	}
	#endregion
	
}
