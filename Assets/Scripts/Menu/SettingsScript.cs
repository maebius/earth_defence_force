//#define DEBUG_TEXTS

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

sealed public class SettingsScript : MonoBehaviour 
{
	#region Public members, visible in Unity GUI
	public GameObject m_sidenessSelection;
	public QuestionBoxScript m_confirmationBox;
	
	public GameObject[] m_liftThese;
	
	public UIPopupList m_resolutionList;
	public GameObject m_resolutionRoot;
	public UIPopupList m_overscanList;
	public GameObject m_overscanRoot;
	#endregion

	#region Private data members
	private UISlider m_musicSlider;
	private UISlider m_soundSlider;
	private UIPopupList m_controlList;
	
	private GameObject m_soundOff;
	private GameObject m_musicOff;
	
	private GameObject m_previousSelection;
	
	#endregion
	public static bool SetResolution(string selection)
	{
		#if DEBUG_TEXTS		
		Debug.Log( string.Format ("SettingsScript.{0} - selection: {1}, maps to: {2}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, selection
			, (SaveData.sm_resolutionMap.ContainsKey(selection)) ? SaveData.sm_resolutionMap[selection] : "null"
			));
		#endif
		
		if (string.IsNullOrEmpty(selection) || !SaveData.sm_resolutionMap.ContainsKey(selection))
		{
			selection = (OuyaInputWrapper.OuyaEnabled) 
				? "menu_settings_resolution_good" 
				: "menu_settings_resolution_best";
		}
		
		string resolution = SaveData.sm_resolutionMap[selection];
		
		string[] parts = resolution.Split(new string[] { " x "}, System.StringSplitOptions.RemoveEmptyEntries);
		int w = 0, h = 0;
		int.TryParse(parts[0], out w);
		int.TryParse(parts[1], out h);
		
		if (SetResolution (w, h))
		{
			SaveData.Instance.Config.Resolution = selection;
			return true;
		}
		return false;
	}
	
	public static bool SetResolution(int w, int h)
	{
		#if DEBUG_TEXTS		
		Debug.Log( string.Format ("SettingsScript.{0} - w: {1}, h: {2}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, w, h));
		#endif
		
		if (w > 0 && h > 0)
		{
			Screen.SetResolution(w, h, true);
			GL.InvalidateState();
			return true;
		}
		return false;
	}

	#region Unity MonoBehaviour base overrides
	void Awake()
	{
		#if DEBUG_TEXTS		
		Debug.Log( string.Format ("SettingsScript.{0}", System.Reflection.MethodBase.GetCurrentMethod().ToString()));
		#endif

		m_musicSlider = (UISlider) Utility.GetObject("music/slider").GetComponent<UISlider>();
		m_soundSlider = (UISlider) Utility.GetObject("sound/slider").GetComponent<UISlider>();
		m_controlList = (UIPopupList) Utility.GetObject("controls/list").GetComponent<UIPopupList>();

		m_soundOff = Utility.GetObject("sound/offIconRoot");
		m_musicOff = Utility.GetObject("music/offIconRoot");
		
		if (OuyaInputWrapper.UseGamepad)
		{
			Utility.SetState(m_sidenessSelection, false, true);
			m_sidenessSelection = null;
		}
		
		if (Application.platform == RuntimePlatform.IPhonePlayer
			|| (Application.platform == RuntimePlatform.Android && !OuyaInputWrapper.OuyaEnabled))
		{
			Utility.SetState (m_overscanRoot.gameObject, false, true);
		}
		
		float highest = float.MinValue;
		float lowest = float.MaxValue;
		List<GameObject> gos = new List<GameObject>();
		foreach (GameObject go in m_liftThese)
		{
			Vector3 oldPos = go.transform.localPosition;
			if (go.active)
			{
				gos.Add (go);
				highest = Mathf.Max (highest, oldPos.y);
				lowest = Mathf.Min (lowest, oldPos.y);
			}
		}
		float newY = highest;
		float delta = (highest - lowest) / (gos.Count - 1);
		foreach(GameObject go in gos)
		{
			Vector3 oldPos = go.transform.localPosition;
			go.transform.localPosition = new Vector3(
				oldPos.x, newY, oldPos.z);
			newY -= delta;
		}
		
		foreach (KeyValuePair<string, string> kv in SaveData.sm_resolutionMap)
		{
			m_resolutionList.items.Add (kv.Key);
		}
		string initial = (OuyaInputWrapper.OuyaEnabled) 
			? "menu_settings_resolution_good" 
			: "menu_settings_resolution_best";
		
		if (!string.IsNullOrEmpty(SaveData.Instance.Config.Resolution))
		{
			initial = SaveData.Instance.Config.Resolution;
		}
		m_resolutionList.selection = initial;
		
		UpdateValues();
	}
	
	#endregion
		
	#region Public object methods
	public void OnMessageBack()
	{
		SaveData.Instance.NNUKE_Save();
		
		Application.LoadLevel("main_menu");
	}
	
	public void OnMusicSliderChange(float fraction)
	{
		MusicManagerScript.Instance.MasterVolume = fraction;
		NGUITools.SetActiveChildren(m_musicOff, MusicManagerScript.Instance.Muted);
	}
	public void OnSoundSliderChange(float fraction)
	{
		SoundManagerScript.Instance.MasterVolume = fraction;
		NGUITools.SetActiveChildren(m_soundOff, SoundManagerScript.Instance.Muted);
	}
	
	public void OnLanguageSelectionChange(string selection)
	{
		#if DEBUG_TEXTS		
		Debug.Log( string.Format ("SettingsScript.{0} - selection: {1}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, selection));
		#endif
		
		SaveData.Instance.Config.Language = selection;
		
		// need to tell this control that the language changed as it can't update itself ...
		if (m_sidenessSelection != null)
		{
			UIPopupList list = m_sidenessSelection.GetComponentInChildren<UIPopupList>();
			list.selection = SaveData.Instance.Config.Controls;
			if (list != null)
				list.SendMessage("OnLocalize", Localization.instance, SendMessageOptions.DontRequireReceiver);
		}
	}
	
	public void OnControlsSelectionChange(string selection)
	{
		#if DEBUG_TEXTS		
		Debug.Log( string.Format ("SettingsScript.{0} - selection: {1}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, selection));
		#endif
		
		SaveData.Instance.Config.Controls = selection;
	}
	
	public void OnMessageOpenResetConfirmation()
	{
		if (OuyaInputWrapper.UseGamepad)
			m_previousSelection = UICamera.selectedObject;
		
		m_confirmationBox.Show ("menu_settings_reset_title", "menu_settings_reset_text");
	}
	
	public void OnMessageResetCancel()
	{
		if (OuyaInputWrapper.UseGamepad)
			UICamera.selectedObject = m_previousSelection;
	}
	
	public void OnMessageResetSave()
	{
		SaveData.Instance.NNUKE_Reset();
		ScoresManagerScript.Instance.Clear();
		
		// NOTE! This makes sm_tryToSync in ScoresUI-scene false,
		// and we need to press the GC button in order to sync
		// -> otherwise we will automatically sync the scores and progression from GC
		// which would be "besides the point" of this whole Reset-functionality ...
		SaveData.Instance.Config.LeaderboardCheck = false;
		ScoresUIScript.sm_forceSyncQuestion = true;
	
		UpdateValues();
		
		if (OuyaInputWrapper.UseGamepad)
			UICamera.selectedObject = m_previousSelection;
	}

	public void OnMessageResolution(string selection)
	{
		SetResolution (selection);
	}
	public void OnMessageOverscan(string selection)
	{
		SaveData.Instance.Config.OverscanCompensation = selection == "menu_settings_overscan_on";
		SafeZoneManagerScript.ApplySafeZone();
	}
	
	#endregion
		
	#region Private object methods
	
	private void UpdateValues()
	{
		m_musicSlider.sliderValue = MusicManagerScript.Instance.MasterVolume;
		m_soundSlider.sliderValue = SoundManagerScript.Instance.MasterVolume;
		if (SaveData.Instance.Config.Controls != null)
			m_controlList.selection = SaveData.Instance.Config.Controls;
		
		SetResolution(SaveData.Instance.Config.Resolution);

		if (m_overscanRoot != null)
		{
			m_overscanList.selection = (SaveData.Instance.Config.OverscanCompensation) 
				? "menu_settings_overscan_on" : "menu_settings_overscan_off";
		}
		
		NGUITools.SetActiveChildren(m_soundOff, SoundManagerScript.Instance.Muted);
		NGUITools.SetActiveChildren(m_musicOff, MusicManagerScript.Instance.Muted);
	}
	
	#endregion
}
