//#define DEBUG_TEXTS

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

sealed public class SlotChangeItem
{
	public GameObject root;
	public UIButtonKeys buttonKeys;
	public UILabel labelNew;
	public UILabel labelName;
	public UILabel labelInfoLabel;
	public UILabel labelInfoValue;

	public UILocalize localizeNew;
	public UILocalize localizeName;
	public UILocalize localizeInfoLabel;
	public UILocalize localizeInfoValue;

	public UISprite icon;
	public UISlicedSprite bgIcon;
	
	public bool canSelect;
	public bool isNew;
	
	public string id;
	public string name;
	public string infoLabel;
	public string infoValue;
	
	public string description;
}

sealed public class SlotSelectorScript : MonoBehaviour 
{
	#region Public members, visible in Unity GUI
	public GameObject m_selectorRoot;
	
	public Color ButtonLabelDisabled;
	public Color ButtonBgSpriteDisabled;
	
	public Color SelectedIconColor;
	public Color NotSelectedIconColor;
	
	public Color SelectedLabelColor;
	public Color NotSelectedLabelColor;
	
	public UIDraggablePanel m_draggablePanel;
	public UIGrid m_grid;
	
	public GameObject m_selectedTarget;
	public string m_selectedTargetFunction;
	
	#endregion

	private GameObject m_selectButton;
	private UILabel m_selectButtonLabel;
	private UISlicedSprite m_selectButtonBgSprite;
	private Color m_originalButtonLabelColor;
	private Color m_originalButtonSpriteColor;
	private UILocalize m_selectButtonLocale;

	private UILabel m_selectionTopic;
	private UILabel m_selectionTopic2nd;
	private UILabel m_selectionDescription;
	
	private int m_currentSlotSelection;
	private GameObject m_previousSelection = null;

	private List<SlotChangeItem> m_slotChangeItems = new List<SlotChangeItem>();
	private List<string> m_selectionEntries = new List<string>();

	private int m_placesActive;
	
	#region Unity MonoBehaviour base overrides
	// Use this for early initialization
	void Awake ()
	{
		// this is out of convenience, if we "forgot" to have this disabled in Editor
		Utility.SetState(m_selectorRoot, true, true);
		
		InitSlotChangeItemReference();
		
		GameObject root = m_selectorRoot.gameObject;
		
		m_selectButton = Utility.GetChildRecursive(root, "buttonSelect", true);
		m_selectButtonLabel = Utility.GetChildRecursive(m_selectButton, "label", true).GetComponent<UILabel>();
		m_selectButtonBgSprite = Utility.GetChildRecursive(m_selectButton, "bgSprite", true).GetComponent<UISlicedSprite>();
		m_selectButtonLocale = m_selectButtonLabel.GetComponent<UILocalize>();
	
		m_originalButtonLabelColor = m_selectButtonLabel.color;
		m_originalButtonSpriteColor = m_selectButtonBgSprite.color;
		
		GameObject obj = Utility.GetChildRecursive(root, "labelSelectionTopic", true);
		m_selectionTopic = obj.GetComponent<UILabel>();
		
		obj = Utility.GetChildRecursive(root, "labelSelectionTopic2nd", true);
		m_selectionTopic2nd = obj.GetComponent<UILabel>();

		obj = Utility.GetChildRecursive(root, "labelSelectionDescription", true);
		m_selectionDescription = obj.GetComponent<UILabel>();
		
		m_selectionEntries.Clear ();
		SetSlotSelectorPlacesActive(0);
	}
	
	#endregion
	
	private void SlotSelectionMade(int index)
	{
#if DEBUG_TEXTS
		Debug.Log(string.Format ("GarageScreenScript.SlotSelectionMade - index: {0}", index));
#endif
		m_currentSlotSelection = index;
		UpdateInfoTexts();
		UpdateSlots();
	}
	
	public void OnMessageSelect()
	{
		if (m_selectedTarget != null) 
		{
			m_selectedTarget.SendMessage(m_selectedTargetFunction
				, m_currentSlotSelection
				, SendMessageOptions.DontRequireReceiver);
		}
	}
	
	public void OnMessageSlotSelectorItem_00()
	{
		SlotSelectionMade(0);
	}
	public void OnMessageSlotSelectorItem_01()
	{
		SlotSelectionMade(1);
	}
	public void OnMessageSlotSelectorItem_02()
	{
		SlotSelectionMade(2);
	}
	public void OnMessageSlotSelectorItem_03()
	{
		SlotSelectionMade(3);
	}
	public void OnMessageSlotSelectorItem_04()
	{
		SlotSelectionMade(4);		
	}
	public void OnMessageSlotSelectorItem_05()
	{
		SlotSelectionMade(5);		
	}
	public void OnMessageSlotSelectorItem_06()
	{
		SlotSelectionMade(6);
	}
	public void OnMessageSlotSelectorItem_07()
	{
		SlotSelectionMade(7);
	}
	public void OnMessageSlotSelectorItem_08()
	{
		SlotSelectionMade(8);
	}
	public void OnMessageSlotSelectorItem_09()
	{
		SlotSelectionMade(9);
	}	
	
	private void SetSlotSelectorPlacesActive(int amount)
	{
		#if DEBUG_TEXTS
		Debug.Log(string.Format ("SlotSelectorScript.SetSlotSelectorPlacesActive - amount: {0} (time: {1})"
			, amount, Time.time));
		#endif	

		m_placesActive = amount;
		
		bool isOn = amount > 0;

		if (isOn && OuyaInputWrapper.UseGamepad)
		{
			m_previousSelection = UICamera.selectedObject;
			UICamera.selectedObject = null;
		}
		
		Utility.SetState(m_selectorRoot, isOn, false);
		
		UIButtonKeys previousButtonKeys = null;
		for (int i = 0; i < m_slotChangeItems.Count; i++)
		{
			SlotChangeItem item  = m_slotChangeItems[i];
			isOn = i < amount;
			
			#if DEBUG_TEXTS
			Debug.Log(string.Format ("SlotSelectorScript.SetSlotSelectorPlacesActive - set {0} to {1}", i , isOn));
			#endif	
			
			if (previousButtonKeys)
			{
				item.buttonKeys.selectOnLeft = (isOn) ? previousButtonKeys : null;
				previousButtonKeys.selectOnRight = (isOn) ? item.buttonKeys : null;
			}
			previousButtonKeys = item.buttonKeys;
			
			Utility.SetState(item.root, isOn, true);
		}
	
		if (m_draggablePanel)
		{
			m_draggablePanel.ResetPosition();
		}
		
		if (!isOn && OuyaInputWrapper.UseGamepad)
		{
			UICamera.selectedObject = m_previousSelection;
		}
		
		if (AddButtonSoundsScript.Instance != null)
		{
			AddButtonSoundsScript.Instance.AddSounds();
		}
		
		UpdateSlots();
	}
	
	private void InitSlotChangeItemReference()
	{
		string baseId = "panelItems/grid";
		
		for (int index = 0; index < 10; index++)
		{
			string b = string.Format ("{0}/Item {1:00}", baseId, index);

			GameObject item = Utility.GetObject (b);
			
			if (item == null)
				return;
			
			GameObject name = Utility.GetObject (string.Format("{0}/labelName", b));
			GameObject nnew = Utility.GetObject (string.Format("{0}/labelNew", b));
			GameObject infoLabel = Utility.GetObject (string.Format("{0}/labelLevel", b));
			GameObject infoValue = Utility.GetObject (string.Format("{0}/labelLevelValue", b));
			GameObject sprite = Utility.GetObject (string.Format("{0}/iconRoot/sprite", b));
			GameObject bgSprite = Utility.GetObject (string.Format("{0}/bgSprite", b));
			
			m_slotChangeItems.Add ( new SlotChangeItem
				{ 
					root = item
					, buttonKeys = item.GetComponent<UIButtonKeys>()
					, labelNew = nnew.GetComponent<UILabel>()
					, labelName = name.GetComponent<UILabel>()
					, labelInfoLabel = infoLabel.GetComponent<UILabel>()
					, labelInfoValue = infoValue.GetComponent<UILabel>()
					, localizeNew = nnew.GetComponent<UILocalize>()
					, localizeName = name.GetComponent<UILocalize>()
					, localizeInfoLabel = infoLabel.GetComponent<UILocalize>()
					, localizeInfoValue = infoValue.GetComponent<UILocalize>()
					, icon = sprite.GetComponent<UISprite>()
					, bgIcon = bgSprite.GetComponent<UISlicedSprite>()
					, isNew = false
					, canSelect = true
				}
			);
		}
		
		#if DEBUG_TEXTS
		Debug.Log(
			string.Format ("SlotSelectorScript.InitSlotChangeItemReference() - slots: {0}"
				, m_slotChangeItems.Count));
		#endif	

	}
	private void ScrollTo(int index, string name) 
	{
		if (m_draggablePanel == null || m_grid == null)
			return;
		
		if (!OuyaInputWrapper.UseGamepad || (!OuyaInputWrapper.OuyaEnabled && UICamera.MouseUsed))
			return;
		
		if (m_draggablePanel.horizontalScrollBar == null || !m_draggablePanel.shouldMoveHorizontally)
			return;
		
		float div = (m_placesActive > 1) ? (m_placesActive-1) : 1f;
		float scrollTo = index / div;
		if (index > 0 && index < m_placesActive -1)
			scrollTo += (0.5f / m_placesActive);
		
		#if DEBUG_TEXTS
		Debug.Log(
			string.Format ("SlotSelectorScript.ScrollTo() name: {0}, index: {1}, scrollTo: {2}, childCount: {3}, placesActive: {4}"
			, name, index, scrollTo, m_grid.transform.GetChildCount(), m_placesActive));
		#endif	
		
		m_draggablePanel.relativePositionOnReset = new Vector2(scrollTo, 0f);
		m_draggablePanel.ResetPosition();
	}
	
	public void UpdateSlots()
	{
		int i = 0;
		foreach (SlotChangeItem item  in m_slotChangeItems)
		{
			// not necessarily yet initialized
			if (string.IsNullOrEmpty(item.id))
				continue;

			item.labelName.text = item.name;
			
			if (string.IsNullOrEmpty(item.infoLabel))
			{
				Utility.SetState(item.labelInfoLabel.gameObject, false, true);
				Utility.SetState(item.labelInfoValue.gameObject, false, true);
			}
			Utility.SetState(item.labelNew.gameObject, item.isNew, true);
			
			bool selected = m_currentSlotSelection == i;
			
			item.icon.color = (selected) ? SelectedIconColor : NotSelectedIconColor;
			item.labelName.color = (selected) ? SelectedLabelColor : NotSelectedLabelColor;
			
			// focus on this, needed for gamepad
			if (selected && OuyaInputWrapper.UseGamepad)
			{
				UICamera.selectedObject = item.root;
				// NOTE! This also changes the color, which in turn annoyingly goes away when we tween
				// away from the object ...
				
				// also scroll to this
				ScrollTo(i, item.root.name);
			}
			i++;
		}
	}
	
	public void UpdateSelectorItem(int index
		, string id
		, string name
		, string selectedTopic
		, string selectedTopic2
		, string description
		, string iconId
		, bool isNew
		, bool canSelect
		)
	{
		m_selectionEntries.Add (id);
			
		SlotChangeItem item  = m_slotChangeItems[index];
		
		// update the data
		item.id = id;
		item.name = name;
		item.infoLabel = selectedTopic;
		item.infoValue = selectedTopic2;

		item.description = description;
		item.isNew = isNew;
		item.canSelect = canSelect;
		item.icon.spriteName = iconId;

		// ... and then the visualization
		item.labelName.text = name;
		item.labelInfoLabel.text = selectedTopic;
		item.labelInfoValue.text = selectedTopic2;
		Utility.SetState(item.labelNew.gameObject, item.isNew, true);
		
		if (string.IsNullOrEmpty(item.icon.spriteName))
		{
			item.icon.enabled = false;
		}
		else
		{
			item.icon.enabled = true;
		}
		
		UpdateInfoTexts();
	}
	
	public void FinalizeSlots()
	{
		#if DEBUG_TEXTS
		Debug.Log(string.Format ("SlotSelectorScript.FinalizeSlots() items: {0}", m_selectionEntries.Count));
		#endif	
		
		SetSlotSelectorPlacesActive(m_selectionEntries.Count);
		m_currentSlotSelection = 0;
		
		UpdateInfoTexts();
	}
	
	private void UpdateInfoTexts()
	{
		SlotChangeItem currentSelection = 
			(m_currentSlotSelection > -1 && m_currentSlotSelection < m_slotChangeItems.Count)
				? m_slotChangeItems[m_currentSlotSelection] : null;
		
		bool isEmpty = currentSelection == null;
		bool canSelect = !isEmpty && currentSelection.canSelect;

		if (isEmpty)
		{
			m_selectButtonLocale.key = "menu_garage_select_empty";
		}
		else
		{
			m_selectButtonLocale.key = "menu_garage_select_buy";
		}
		
		m_selectButton.GetComponent<Collider>().enabled = canSelect;
		m_selectButtonLabel.color = (canSelect) ? m_originalButtonLabelColor : ButtonLabelDisabled;
		m_selectButtonBgSprite.color = (canSelect) ? m_originalButtonSpriteColor : ButtonBgSpriteDisabled;
		
		m_selectionTopic.text = (isEmpty) ? "" : currentSelection.infoLabel;
		m_selectionTopic2nd.text  = (isEmpty) ? "" : currentSelection.infoValue;
		m_selectionDescription.text = (isEmpty) ? "" : currentSelection.description;
		
		m_selectButtonLocale.Localize();
		
		if (AssignGamepadButtonsScript.Instance != null)
		{
			AssignGamepadButtonsScript.Instance.AssignButtons(true);
		}
	}
}
