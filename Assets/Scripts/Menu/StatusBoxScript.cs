using UnityEngine;
using System.Collections;

sealed public class StatusBoxScript : MonoBehaviour 
{
	#region Public members, visible in Unity GUI
	public GameObject m_root;
	public UILabel m_labelTitle;
	public UILabel m_labelText;
	
	public float m_visibleTimeSeconds;
	
	public AudioClip m_soundAppears;
	#endregion

	#region Private members
	private float m_timeLeftSeconds = 0.0f;
	
	#endregion

	#region Public static helpers
	public static void ShowFacebookStatus(StatusBoxScript box
		, SocialLayerManagerScript.RequestState state
		, SocialLayerManagerScript.StateChanged handler
		)
	{
		if (state == SocialLayerManagerScript.RequestState.Fail 
			|| state == SocialLayerManagerScript.RequestState.Ok
			|| state == SocialLayerManagerScript.RequestState.Cancel) 
		{
			if (SocialLayerManagerScript.GetInstance() != null)
				SocialLayerManagerScript.GetInstance().Changed -= handler;
			
			string key = "menu_statusbox_result_cancel";
			if (state == SocialLayerManagerScript.RequestState.Fail)
				key = "menu_statusbox_result_failed";
			else if (state == SocialLayerManagerScript.RequestState.Ok)
				key = "menu_statusbox_result_success";
			
			box.Show(
				Localization.Localize("menu_statusbox_title_fb")
				, Localization.Localize(key));
		}
	}

	public static void ShowTwitterStatus(StatusBoxScript box
		, TwitterManagerScript.RequestState state
		, TwitterManagerScript.StateChanged handler
		)
	{
		if (state == TwitterManagerScript.RequestState.Fail || state == TwitterManagerScript.RequestState.Ok) 
		{
			if (TwitterManagerScript.GetInstance() != null)
				TwitterManagerScript.GetInstance().Changed -= handler;

			string key = "menu_statusbox_result_cancel";
			if (state == TwitterManagerScript.RequestState.Fail)
				key = "menu_statusbox_result_failed";
			else if (state == TwitterManagerScript.RequestState.FailDuplicate)
				key = "menu_statusbox_result_duplicate";
			else
				key = "menu_statusbox_result_success";
			
			box.Show(
				Localization.Localize("menu_statusbox_title_twitter")
				, Localization.Localize(key));
		}
	}
	
	#endregion
	
	#region Unity MonoBehaviour base overrides
	void Start()
	{
		Utility.SetState(m_root, false, true);
	}
	
	void Update() 
	{
		if (m_timeLeftSeconds > 0.0f) 
		{
			m_timeLeftSeconds -= Time.deltaTime;
			if (m_timeLeftSeconds <= 0.0f)
			{
				Utility.ResetTweens(m_root);
				Utility.SetState(m_root, false, true);
			}
		}
	}
	
	public void Show(float time = -1.0f) 
	{
		Utility.SetState(m_root, true, true);
		m_timeLeftSeconds = (time <= 0.0f) ? m_visibleTimeSeconds : time;
		
		// is there a delay?
		float delay = 0f;
		TweenScale ts = m_root.GetComponentInChildren<TweenScale>();
		if (ts != null)
		{
			delay = ts.delay;
		}
		SoundManagerScript.PlayOneTime (gameObject, m_soundAppears, delay);
	}

	public void Show(string title, string text, float time = -1.0f) 
	{
		m_labelTitle.text = title;
		m_labelText.text = text;

		Show(time);
	}
	
	public bool IsShowing()
	{
		return m_visibleTimeSeconds > 0.0f;
	}
	#endregion
}
