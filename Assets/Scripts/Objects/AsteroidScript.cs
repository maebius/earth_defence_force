using UnityEngine;
using System.Collections;

sealed public class AsteroidScript : MonoBehaviour 
{
	#region Unity MonoBehaviour base overrides

	// Update is called once per frame
	void Update ()
	{
		if (!GamePlayCamera.Instance.IsObjectInPlayField(GetComponent<Collider>()) &&
			(PlayerHomePlanetScript.Instance == null
				|| MathHelpers.HeadingAwayFromTarget(transform.position
					, PlayerHomePlanetScript.Instance.transform.position
					, GetComponent<Rigidbody>().velocity)))
		{
			DamageTakerScript dt = GetComponent<DamageTakerScript>();
			dt.MakeDisappear();
		}
	}
	#endregion
		
}
