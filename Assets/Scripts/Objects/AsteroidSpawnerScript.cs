using UnityEngine;
using System.Collections;
using System.Collections.Generic;

sealed public class AsteroidSpawnerScript : MonoBehaviour 
{
	#region Public members, visible in Unity GUI
	public int ConcurrentMaximumCount;
	
	/// <summary>
	/// How much in scale we vary the spawned asteroids. The amount is random binominial times this value.
	/// </summary>
	public float ScaleVariation;
	/// <summary>
	/// How often we check if we spawn more asteroids or not.
	/// </summary>
	public float SpawnDeltaTimeSeconds;
	/// <summary>
	/// How much variation we add to the spawn check time, the amount is random ranging from zero to this value.
	/// </summary>
	public float SpawnVariationTimeSeconds;
	/// <summary>
	/// What is the probability (with 1, we always spawn) that we spawn something when checking.
	/// </summary>
	public float SpawnProbability;
	/// <summary>
	/// The minimum to spawn when check to spawn goes through.
	/// </summary>
	public int SpawnAmountMin;
	/// <summary>
	/// The maximum to spawn, so actual amount is min + (max-min)*random.
	/// </summary>
	public int SpawnAmountMax;
	/// <summary>
	/// The asteroid speed minimum.
	/// </summary>
	public float AsteroidGroupSpeedMin;
	/// <summary>
	/// The asteroid speed max.
	/// </summary>
	public float AsteroidGroupSpeedMax;
	/// <summary>
	/// The possible asteroid speed variation inside group.
	/// </summary>
	public float AsteroidSpeedVariation;
	/// <summary>
	/// What is the distance from player where we initially spawn asteroids.
	/// </summary>
	public float SpawnDistanceRadius;
	/// <summary>
	/// When spawning multiple asteroids, how much to offset each successive asteroid from the first one radially.
	/// This is done so that they do not overlap initially.
	/// </summary>
	public float SpawnOffsetRadius;
	/// <summary>
	/// The asteroid is targeted towards player, but the target is shifted randomly tangentially away from player
	/// center. This is the maximum amount for the shift. If the value is zero, the target is always player.
	/// </summary>
	public float SpawnPlayerBypassRadius;
	/// <summary>
	/// If spawning multiple asteroids, in addition to offsetting target with SpawnOffsetRadius, we also offset
	/// each target of successive asteroids with this value (times random from 0 to 1).
	/// </summary>
	public float SpawnPlayerBypassOffset;
	
	/// <summary>
	/// If this is greater than zero, then every after this seconds we increase asteroid difficulty.
	/// </summary>
	public float HarderDeltaSeconds = -1f;
	
	/// <summary>
	/// If this is greater than zero, then every after MoreAsteroidsDeltaSeconds seconds we increase the amount of asteroids.
	/// </summary>
	public int HarderCountIncrease = 0;

	/// <summary>
	/// If this is greater than zero, then every after MoreAsteroidsDeltaSeconds seconds we increase the amount of asteroids.
	/// </summary>
	public float HarderSpeedMultiplier = 1f;

	/// <summary>
	/// The prefabs used for spawned asteroids. NOTE! We probably should have several, so we would have different
	/// kinds of asteroids.
	/// </summary>
	public AsteroidScript[] AsteroidPrefab;
	#endregion

	#region Public members, not visible to Unity GUI
	#endregion

	#region Private data members
	/// <summary>
	/// Our currently existing asteroids.
	/// </summary>
	private List<DamageTakerScript> m_asteroids = new List<DamageTakerScript>();
	/// <summary>
	/// When to check the next time if we want to spawn asteroids.
	/// </summary>
	private float m_nextSpawnTimeCheck;
	/// <summary>
	/// Helper to calculate speed variation for asteroids, basically max-min.
	/// </summary>
	private float m_asteroidSpeedDelta;
	/// <summary>
	/// Helper to calculate count variation for asteroids we want to spawn, basically max-min.
	/// </summary>
	private int m_spawnCountDelta;
	/// <summary>
	/// Our bonus spawner in level, if available.
	/// </summary>
	private BonusSpawnerScript m_bonusSpawner;
	/// <summary>
	/// When will we increase difficulty.
	/// </summary>
	private float m_harderNextTimeSeconds = -1f;
	#endregion


	#region Unity MonoBehaviour base overrides
		
	// Use this for initialization
	void Start ()
	{
		m_spawnCountDelta = SpawnAmountMax - SpawnAmountMin;
		m_asteroidSpeedDelta = AsteroidGroupSpeedMax - AsteroidGroupSpeedMin;
		
		m_bonusSpawner = (BonusSpawnerScript) FindObjectOfType(typeof(BonusSpawnerScript));
		
		InitNextCheckTime();
		
		m_harderNextTimeSeconds = GameStateScript.Instance.NNUKE_GetWorldTime() + HarderDeltaSeconds;
	}

	// Update is called once per frame
	void Update ()
	{
		if (GameStateScript.Instance.NNUKE_GetWorldTime() > m_nextSpawnTimeCheck)
		{
			if (MathHelpers.Chance(SpawnProbability))
			{
				DoSpawnAsteroids();
			}
			
			InitNextCheckTime();
		}
		
		if (HarderDeltaSeconds > 0f 
			&& GameStateScript.Instance.NNUKE_GetWorldTime() > m_harderNextTimeSeconds)
		{
			ConcurrentMaximumCount += HarderCountIncrease;
			SpawnAmountMax += HarderCountIncrease;
			SpawnAmountMin += HarderCountIncrease;
			AsteroidGroupSpeedMax *= HarderSpeedMultiplier;
			AsteroidGroupSpeedMin *= HarderSpeedMultiplier;
			
			m_spawnCountDelta = SpawnAmountMax - SpawnAmountMin;
			m_asteroidSpeedDelta = AsteroidGroupSpeedMax - AsteroidGroupSpeedMin;
			
			m_harderNextTimeSeconds = GameStateScript.Instance.NNUKE_GetWorldTime() + HarderDeltaSeconds;
		}
	}
	#endregion
		
	#region Public object methods
	public void AsteroidDestroyed(DamageTakerScript sender)
	{
		m_asteroids.Remove(sender);
	}
	#endregion
		
	#region Private object methods
	[Obfuscar.Obfuscate]
	private void InitNextCheckTime()
	{
		m_nextSpawnTimeCheck = GameStateScript.Instance.NNUKE_GetWorldTime()
			+ SpawnDeltaTimeSeconds + MathHelpers.Binominial() * SpawnVariationTimeSeconds;
	}
	
	[Obfuscar.Obfuscate]
	private void DoSpawnAsteroids()
	{
		if (PlayerHomePlanetScript.Instance == null)
			return;
		
		int count = SpawnAmountMin + Random.Range (0, m_spawnCountDelta);
		float angleRad = Mathf.Deg2Rad * Random.Range(0f, 360f);
		
		Vector3 target = PlayerHomePlanetScript.Instance.transform.position;
		Vector3 origin = target + SpawnDistanceRadius * new Vector3(
			Mathf.Cos (angleRad)
			, Mathf.Sin(angleRad)
			, 0f);
		
		Vector3 direction = (target - origin);
		direction.Normalize();
		Vector3 tangent = Vector3.Cross(direction, Vector3.forward);
		tangent.Normalize();
		target = target + SpawnPlayerBypassRadius * MathHelpers.Binominial() * tangent;
		
		float groupSpeed = (AsteroidGroupSpeedMin + Random.Range (0f, 1f) * m_asteroidSpeedDelta);
		for (int i = 0; i < count; i++)
		{
			if (m_asteroids.Count >= ConcurrentMaximumCount)
				return;
			
			Vector3 offset = MathHelpers.RandomVectorOffset(SpawnOffsetRadius, i);
			Vector3 shiftedLocation = origin + offset;
			Vector3 shiftedTarget = target + SpawnPlayerBypassOffset * MathHelpers.Binominial() * tangent;;
			Vector3 dir = shiftedTarget - shiftedLocation;
			dir.Normalize();
			float speed = (groupSpeed + AsteroidSpeedVariation * MathHelpers.Binominial());
			
			Spawn (shiftedLocation, speed, dir);
		}
	}
	
	[Obfuscar.Obfuscate]
	private void Spawn(Vector3 location, float speed, Vector3 dir)
	{
		// randomly select some asteroid prefab ...
		int index = Random.Range(0, AsteroidPrefab.Length);
		
		AsteroidScript clone = (AsteroidScript) Instantiate(AsteroidPrefab[index], location, Quaternion.identity);
		
		float scalingFactor = 1f + ScaleVariation * Random.Range(0f, 1f);
		clone.transform.localScale = scalingFactor * clone.transform.localScale;
		DamageTakerScript damageTaker = clone.GetComponent<DamageTakerScript>();
		if (m_bonusSpawner)
			damageTaker.Died += new DamageTakerScript.DamageTakerDied(m_bonusSpawner.DamageTakerDied);
		
		damageTaker.Energy *= scalingFactor;
		damageTaker.Speed = speed;
		damageTaker.OriginalSpeed = speed;
		
		damageTaker.ScoreToPlayer = (int) (damageTaker.ScoreToPlayer * scalingFactor);

		damageTaker.Died += new DamageTakerScript.DamageTakerDied(AsteroidDestroyed);
		
		clone.gameObject.GetComponent<Rigidbody>().velocity = speed * dir;
		
		m_asteroids.Add(damageTaker);
	}
	#endregion
}
