using UnityEngine;
using System.Collections;

sealed public class AutoDestructScript : MonoBehaviour 
{
	public float DelaySeconds;
	
	void Start () 
	{
		Destroy(gameObject, DelaySeconds);
	}
}
