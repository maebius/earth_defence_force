using UnityEngine;
using System.Collections;

sealed public class RadarDotScript : MonoBehaviour 
{
	public GameObject Follows;
	
	public float FactorFromPlayer
	{
		get { return m_factor; }
	}
	
	private Vector3 m_previousRadarPositionPosition = Vector3.zero;
	private float m_frequency = 0f;
	private float m_factor;
	
	private float m_start;
	private float m_delta;
	
	void FixedUpdate () 
	{
		transform.position = CalculateRadarDotPosition();
	}
	
	void Start()
	{
		if (PlayerHomePlanetScript.IsDead)
		{	
			Destroy(gameObject);
			return;
		}
		
		const float RIM_OFFSET = 10f;
		
		m_start = PlayerHomePlanetScript.Instance.GetCollideRadius() + RIM_OFFSET;
		m_delta = RadarControlScript.Instance.GetRadarRadiusMax() - m_start;
	}
	
	void Update()
	{
		if (PlayerHomePlanetScript.IsDead)
		{	
			Destroy(gameObject);
			return;
		}
		
		GetComponent<Renderer>().material.SetFloat ("_Frequency", m_frequency);
		GetComponent<Renderer>().material.SetFloat ("_GlobalTime", (m_frequency < 0.05f) ? 0f : Time.time);
	}
	
	#region Private object methods
	/// <summary>
	/// Calculates where the radar dot should be that represents this enemy. Does not set the position.
	/// </summary>
	/// <returns>
	/// The radar dot position.
	/// </returns>
	[Obfuscar.Obfuscate]
	private Vector3 CalculateRadarDotPosition()
	{
		if (Follows == null || PlayerHomePlanetScript.Instance == null || PlayerHomePlanetScript.Instance.CheckIfDead())
			return m_previousRadarPositionPosition;
		
		Vector3 direction = Follows.transform.position - PlayerHomePlanetScript.Instance.transform.position;
		float distance = direction.magnitude;

		direction.Normalize();
		// NOTE! shorten the direction a bit, so we get the dot inside the radar sprite (and not right on the rim)
		direction *= 0.95f;
		
		// the bigger this number, the faster the dot approaches player
		const float REAL_DISTANCE_WHEN_DOT_AT_RIM = 350f;
		const float DISTANCE_WHEN_STARTS_FLASHING = 250f;
		
		distance -= m_start;
		distance = Mathf.Max(0f, distance);
		
		m_factor = (distance > REAL_DISTANCE_WHEN_DOT_AT_RIM) 
			? 1f 
			: distance / REAL_DISTANCE_WHEN_DOT_AT_RIM;

		// Faster decay
		m_factor *= m_factor;
		
		m_frequency = (distance < DISTANCE_WHEN_STARTS_FLASHING) ? 
			((DISTANCE_WHEN_STARTS_FLASHING - distance) / DISTANCE_WHEN_STARTS_FLASHING) : 0f;
		
		m_previousRadarPositionPosition = PlayerHomePlanetScript.Instance.transform.position + 
			(m_factor * m_delta + m_start) * direction;
		
		return m_previousRadarPositionPosition;
	}
	#endregion
}
