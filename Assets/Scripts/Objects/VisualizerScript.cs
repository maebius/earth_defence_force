using UnityEngine;
using System.Collections;

sealed public class VisualizerScript : MonoBehaviour 
{
	public float m_offsetDirX = 0f;
	public float m_offsetDirY = 0f;
	public float m_offsetDirZ = 1f;
	
	public GameObject VisualizationPrefab;
	public bool HasParent = true;
	public bool UpdatePosition = true;
	public bool UpdateRotation = true;
	public bool UpdateScale = false;
	public bool InitialModulateParentScale = false;
	public bool UseVelocity = false;
	public float ScaleAdditionFactor;
	public float VelocitySetDelaySeconds;
	public float ParticlesSizeAndSpeed;
	
	public float PositionOffset = 0f;
	public bool ScaleMultiply = false;
	public bool m_faceCamera = false;
	
	
	[HideInInspector]
	public Vector3 Direction;
	[HideInInspector]
	public float Speed;
	
	[HideInInspector]
	public GameObject Visualization
	{
		get { return m_visualization; }
	}

	private GameObject m_visualization;
	private bool m_velocityNotSet;
	private float m_velocitySetTimeSeconds;
	private Vector3 m_scaleAdditionVelocity;

	private ParticleSystem m_particles = null;
	
	private Vector3 m_offsetVector;
	
	void Awake()
	{
		m_offsetVector = new Vector3(m_offsetDirX, m_offsetDirY, m_offsetDirZ);
		m_offsetVector.Normalize();
	}
	
	// Use this for initialization
	void Start () 
	{
		if (VisualizationPrefab)
		{
			m_visualization = (GameObject) Instantiate(
				VisualizationPrefab
				, transform.position
				, transform.rotation);
			
			if (InitialModulateParentScale)
			{
				m_visualization.transform.localScale = new Vector3(
					m_visualization.transform.localScale.x * transform.localScale.x
					, m_visualization.transform.localScale.y * transform.localScale.y
					, m_visualization.transform.localScale.z * transform.localScale.z);
			}
			
			if (HasParent)
				m_visualization.transform.parent = transform;
			
			m_scaleAdditionVelocity = ScaleAdditionFactor * m_visualization.transform.localScale;
		}
		
		m_velocitySetTimeSeconds = GameStateScript.Instance.NNUKE_GetWorldTime() + VelocitySetDelaySeconds;
		m_velocityNotSet = true;
		
		m_particles = (ParticleSystem) GetComponentInChildren<ParticleSystem>();
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		if (m_visualization == null)
			return;
		if (UpdateRotation)
			m_visualization.transform.rotation = transform.rotation;
		if (UpdatePosition)
		{
			m_visualization.transform.position = transform.position + 
				(transform.rotation * (PositionOffset*m_offsetVector));
		}
		if (UpdateScale)
		{
			if (ScaleMultiply)
			{
				m_visualization.transform.localScale = new Vector3(
					m_visualization.transform.localScale.x * transform.localScale.x
					, m_visualization.transform.localScale.y * transform.localScale.y
					, m_visualization.transform.localScale.z * transform.localScale.z);
			}
			else
			{
				m_visualization.transform.localScale = new Vector3(
					transform.localScale.x
					, transform.localScale.y
					, transform.localScale.z);
			}
			
			m_visualization.transform.localScale += 
				GameStateScript.Instance.NNUKE_GetWorldDeltaTime() * m_scaleAdditionVelocity;
			
			if (m_particles != null)
			{
				m_particles.startSize += ParticlesSizeAndSpeed * GameStateScript.Instance.NNUKE_GetWorldDeltaTime();
				//m_particles.startSpeed -= ParticlesSizeAndSpeed * GameStateScript.Instance.NNUKE_GetWorldDeltaTime();
			}
		}
		
		if (UseVelocity && m_velocityNotSet && m_velocitySetTimeSeconds < GameStateScript.Instance.NNUKE_GetWorldTime())
		{
			
			gameObject.GetComponent<Rigidbody>().velocity = Speed * Direction;
			m_velocityNotSet = false;
		}
		if (m_faceCamera)
		{
			// make face camera (as not a 3d object)
			//transform.LookAt(GamePlayCamera.Instance.ActualCamera.transform.position, Vector3.down);
			transform.LookAt(
				GamePlayCamera.Instance.ActualCamera.transform.position
				, -GamePlayCamera.Instance.CameraLeft);
			
		}
	}
	
	void OnDestroy()
	{
		if (m_visualization)
			DestroyObject(m_visualization);
	}
}
