using UnityEngine;
using System.Collections;
using System.Collections.Generic;

sealed public class EnemySpawner : MonoBehaviour 
{
	
	#region Public GUI tweakable members

	// Variables for spawn times
	public bool SpawnEnabled;	
	public bool SpawnTimeAccelerationEnabled;	
	public float SpawnTimeDeltaWhenGoesSmaller;
	public float SpawnTimeDeltaInitial;
	public float SpawnTimeDelta;
	public float SpawnTimeAccelerationFactor;

	public bool EnemySpeedAccelerationEnabled;
	public float EnemySpeedInitial;
	public float EnemySpeedTimeDeltaInitial;
	public float EnemySpeedTimeDelta;
	public float EnemySpeedIncrease;
	
	// The enemy prefab to be used when spawning new enemies
	public EnemyBaseScript EnemyPrefab;

	[HideInInspector]
	public List<DamageTakerScript> Enemies = new List<DamageTakerScript>();
	
	[HideInInspector]
	public static EnemySpawner Instance
	{
		get { return m_instance; }
	}

	#endregion
	
	
	#region Private data members
	private static EnemySpawner m_instance = null;
	
	private float m_nextTimeStampUntilSpawnEnemy;
	private float m_nextTimeStampUntilSpawnTimeReducts;

	private float m_nextTimeStampUntilSpeedIncrease;
	
	private float m_enemySpeed;
	private float m_enemySpeedLimit;
	
	#endregion 
	
	#region Unity MonoBehaviour base overrides

	// Use this for initialization
	void Start () 
	{
		m_instance = this;
		
		m_nextTimeStampUntilSpawnEnemy = GameStateScript.Instance.NNUKE_GetWorldTime() + SpawnTimeDeltaInitial;
		m_nextTimeStampUntilSpawnTimeReducts = GameStateScript.Instance.NNUKE_GetWorldTime() + SpawnTimeDeltaWhenGoesSmaller;
		
		m_enemySpeed = EnemySpeedInitial;
		m_enemySpeedLimit = 10f * EnemySpeedInitial;
		
		m_nextTimeStampUntilSpeedIncrease = GameStateScript.Instance.NNUKE_GetWorldTime() + EnemySpeedTimeDelta;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (PlayerHomePlanetScript.Instance == null || GamePlayCamera.Instance == null)
			return;
		
		if (SpawnEnabled && GameStateScript.Instance.NNUKE_GetWorldTime() > m_nextTimeStampUntilSpawnEnemy)
		{
			DoSpawnEnemy();
			m_nextTimeStampUntilSpawnEnemy = GameStateScript.Instance.NNUKE_GetWorldTime() + SpawnTimeDelta;
		}
		
		if (SpawnTimeAccelerationEnabled && GameStateScript.Instance.NNUKE_GetWorldTime() > m_nextTimeStampUntilSpawnTimeReducts)
		{
			DoReductSpawnTime();
			m_nextTimeStampUntilSpawnTimeReducts = GameStateScript.Instance.NNUKE_GetWorldTime() + SpawnTimeDeltaWhenGoesSmaller;
		}
		
		if (EnemySpeedAccelerationEnabled && GameStateScript.Instance.NNUKE_GetWorldTime() > m_nextTimeStampUntilSpeedIncrease)
		{
			DoIncreaseEnemySpeed();
			m_nextTimeStampUntilSpeedIncrease = GameStateScript.Instance.NNUKE_GetWorldTime() + EnemySpeedTimeDelta;
		}
	}
	
	#endregion
	
	#region Public interface
	
	public void DamageTakerDied(DamageTakerScript enemy)
	{
		Enemies.Remove(enemy);
	}
	
	#endregion
	
	#region Private object methods
	
	/// <summary>
	/// Increases the speed of every newly spawned enemy. Used every now and then to make enemies harder.
	/// </summary>
	private void DoIncreaseEnemySpeed()
	{
		// HARD_CODED if already too fast, do not increase anymore
		if (m_enemySpeed > m_enemySpeedLimit)
		{
			EnemySpeedAccelerationEnabled = false;
			return;
		}		
		m_enemySpeed *= EnemySpeedIncrease;
	}
	
	/// <summary>
	/// Will spawn a new enemy to scene. Does this blindly, just taking a proper distance from player a parameter.
	/// </summary>
	private void DoSpawnEnemy()
	{
		Vector3 targetLocation = Vector3.zero;
		if (PlayerHomePlanetScript.Instance.gameObject != null)
			targetLocation = PlayerHomePlanetScript.Instance.transform.position;
		
		// Spawn location
		Vector3 spawnLocation;
		spawnLocation.z = targetLocation.z;
		float angleRad = Mathf.PI * Random.Range(0.0f, 2.0f);
		float radius = GamePlayCamera.Instance.SafeSpawnDistance;
		spawnLocation.x = radius * Mathf.Cos(angleRad);
		spawnLocation.y = radius * Mathf.Sin(angleRad);
		
		// Velocity for the spawned enemy
		Vector3 velocity = targetLocation - spawnLocation;
		velocity.Normalize();
		
		// Create thea actual enemy
		EnemyBaseScript enemyClone = (EnemyBaseScript) Instantiate(EnemyPrefab, spawnLocation, Quaternion.identity);
		
		DamageTakerScript taker = enemyClone.GetComponent<DamageTakerScript>();
		// we want to know about the enemy
		taker.Died += new DamageTakerScript.DamageTakerDied(DamageTakerDied);
		
		enemyClone.gameObject.GetComponent<Rigidbody>().velocity = 
			new Vector3(m_enemySpeed * velocity.x, m_enemySpeed * velocity.y, 0f);
		
		Enemies.Add(taker);
	}
	
	/// <summary>
	/// Makes the spawning interval shorter, so enemies are spawned more often. Used to make the game harder.
	/// </summary>
	private void DoReductSpawnTime()
	{
		// HARD_CODED If already too small interval, not doing this
		if (SpawnTimeDelta < 1.5f)
		{
			SpawnTimeAccelerationEnabled = false;
			return;
		}
		SpawnTimeDelta *= SpawnTimeAccelerationFactor;
	}

	#endregion
}
