//#define DEBUG_DRAW

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

sealed public class PlayerActionsScript : MonoBehaviour 
{
	
	sealed public class GUIButtonWrapper
	{
		public GameObject root;
		public UISlicedSprite bgSprite;
		public UISprite icon;
		public Collider collider;
		
		public string activeSprite;
		public string inactiveSprite;
		
		public delegate void ButtonActivated();
		public event ButtonActivated Activated;

		public delegate void ButtonReleased();
		public event ButtonReleased Released;
		
		public GUIButtonWrapper(string name, string _ActiveSprite, string _inactiveSprite)
		{
			root = 	Utility.GetObject(name);
			this.activeSprite = _ActiveSprite;
			this.inactiveSprite = _inactiveSprite;
			collider = root.GetComponent<Collider>();
			
			Transform t = root.transform.FindChild("Animation/bgSprite");
			if (t != null)
			{
				bgSprite = t.gameObject.GetComponent<UISlicedSprite>();
			}
			t = root.transform.FindChild("iconRoot/icon");
			if (t != null)
			{			
				icon = t.gameObject.GetComponent<UISprite>();
			}
		}
		
		public void SetActiveState(bool state)
		{
			if (bgSprite != null)
			{
				bgSprite.spriteName = (state) ? activeSprite : inactiveSprite;
				bgSprite.alpha = (state) ? 0.8f : 0.4f;
			}
			
			if (state && Activated != null)
				Activated();
			if (!state && Released != null)
				Released();
		}
	}
	
	#region Public GUI tweakable members
	public GameObject m_root;
	public GameObject m_ouyaRoot;
	
	public UISlider m_playerEnergy;

	public bool m_timerVisible;
	public UILabel m_timeLabel;
	public UILocalize m_phaseLabel;
	public UILabel m_phaseValue;

	public UILabel m_debugFPS;
	
	#endregion

	#region Public members
	public static PlayerActionsScript Instance 
	{
		get { return sm_playerActionScript; }
	}
	#endregion
		
	#region Private data members
	private UISlicedSprite m_itemSprite1;
	private UISlicedSprite m_itemSprite2;
	
	private UISlicedSprite m_weaponSprite1;
	private UISlicedSprite m_weaponSprite2;
	private UISlicedSprite m_weaponSprite3;
	
	private List<GUIButtonWrapper> m_guiElements = new List<GUIButtonWrapper>();
	private Dictionary<Collider, GUIButtonWrapper> m_elementMap = new Dictionary<Collider, GUIButtonWrapper>();
	
	private List<GameObject> m_rightSideControls = new List<GameObject>();
	private List<GameObject> m_leftSideControls = new List<GameObject>();
	
	private List<GameObject> m_uiElements = new List<GameObject>();
	private List<GameObject> m_ouyaInstructionObjects = new List<GameObject>();
	
	private PlayerInputScript m_playerInput;

	private string m_bgSpriteNameW1 = "KehikkoKalvoVasenYla";
	private string m_bgSpriteNameW2 = "KehikkoKalvoKeskiOikea";
	private string m_bgSpriteNameW3 = "KehikkoKalvoOikeeAla";

	private string m_bgSpriteNameW1_Active = "KehikkoKalvoVasenYla";
	private string m_bgSpriteNameW2_Active = "KehikkoKalvoKeskiOikea";
	private string m_bgSpriteNameW3_Active = "KehikkoKalvoOikeeAla";
	
	private string m_bgSpriteNameItem = "NappiKalvo";
	private string m_bgSpriteNameItem_Active = "NappiKalvo";
	
	private UILabel[] m_itemCountLabels;
	
	private bool m_ouyaInstructiosVisible = false;
	
	private bool m_usingDpadControls = false;
	private bool m_turnLeftOn = false;
	private bool m_turnRightOn = false;
	
	private static PlayerActionsScript sm_playerActionScript = null;
	#endregion
	
	#region Unity MonoBehaviour base overrides
	void Awake()
	{
		sm_playerActionScript = this;
		
		// this is out of convenience, if we "forgot" to have this disabled in Editor
		Utility.SetState(m_root, true, true);
		
		GameObject[] objs = GameObject.FindGameObjectsWithTag("ouya_instruction");
		m_ouyaInstructionObjects.AddRange(objs);
		
		// NOTE! This needs to be done right at the start, otherwise other initialization does not work
		m_rightSideControls.Add (Utility.GetObject("Anchor, bottom-right/rootFiring"));
		m_rightSideControls.Add (Utility.GetObject("Anchor, bottom-left/rootMovement"));

		m_leftSideControls.Add (Utility.GetObject("Anchor, bottom-left/rootFiring"));
		m_leftSideControls.Add (Utility.GetObject("Anchor, bottom-right/rootMovement"));
		
		bool rightSided = false;
		bool leftSided = false;
		
		// check if Ouya or not ... different UI in that case -> THIS NEEDS TO BE DONE HERE FIRST!
		
		if (OuyaInputWrapper.UseGamepad)
		{
			m_bgSpriteNameW1_Active = "NappiKalvo";
			m_bgSpriteNameW2_Active = "NappiKalvo";
			m_bgSpriteNameW3_Active = "NappiKalvo";

			m_bgSpriteNameW1 = "";//"NappiKalvo_alpha50";
			m_bgSpriteNameW2 = ""; //"NappiKalvo_alpha50";
			m_bgSpriteNameW3 = "";//"NappiKalvo_alpha50";
			
			DecideControls(false, false, false, false);
		}
		else
		{
			m_ouyaInstructiosVisible = false;
			
			rightSided = (SaveData.Instance.Config.Controls.ToLowerInvariant().Contains("right"));
			leftSided = !rightSided;
			
			m_usingDpadControls = SaveData.Instance.Config.Controls.ToLowerInvariant().Contains("digital");
			
			DecideControls(rightSided, leftSided, !m_usingDpadControls, m_usingDpadControls);
			
			GameObject go = Utility.GetChildRecursive(m_root, "buttonOuyaHide", true);
			if (go != null)
			{
				Utility.SetState(go, false, true);
			}
			
			Utility.SetState(m_ouyaRoot, false, true);
		}
		SetOuyaButtonVisiblity(m_ouyaInstructiosVisible);
		
		string start = "";
		// NOTE! Currently these need to be first in list, as stupidly ExecuteWeaponIconUpdate relies on 
		// These indices ... SHOULD REALLY FIX THIS!!!
		GUIButtonWrapper element = new GUIButtonWrapper(
			string.Format ("{0}buttonWeapon_1", start)
			, m_bgSpriteNameW1_Active
			, m_bgSpriteNameW1);
		m_guiElements.Add (element);
		
		element = new GUIButtonWrapper(
			string.Format ("{0}buttonWeapon_2", start)
			, m_bgSpriteNameW2_Active
			, m_bgSpriteNameW2);
		m_guiElements.Add (element);
		
		element = new GUIButtonWrapper(string.Format ("{0}buttonWeapon_3", start)
			, m_bgSpriteNameW3_Active
			, m_bgSpriteNameW3);
		m_guiElements.Add (element);

		element = new GUIButtonWrapper(
			string.Format ("{0}buttonItem_1", start)
			, m_bgSpriteNameItem_Active
			, m_bgSpriteNameItem);
		m_guiElements.Add (element);

		element = new GUIButtonWrapper(
			string.Format ("{0}buttonItem_2", start)
			, m_bgSpriteNameItem_Active
			, m_bgSpriteNameItem);
		m_guiElements.Add (element);
		
		element = new GUIButtonWrapper(
			string.Format ("{0}buttonFire", start)
			, ""
			, "");
		m_guiElements.Add (element);
		
		// make a map from the element to have fast access when handling input
		foreach (GUIButtonWrapper e in m_guiElements)
		{
			m_elementMap.Add (e.collider, e);
		}
		
		m_itemCountLabels = new UILabel[2];
		m_itemCountLabels[0] = Utility.GetChildRecursive(m_root, "item_1_count", false).GetComponent<UILabel>();
		m_itemCountLabels[1] = Utility.GetChildRecursive(m_root, "item_2_count", false).GetComponent<UILabel>();
		
		Utility.CollectAllVisualGUIElements(m_root.transform, ref m_uiElements);
		
		m_playerInput = GetComponent<PlayerInputScript>();
		
		AssignSprites();
		
		UpdateTimer();
		Utility.SetState(m_timeLabel.gameObject, m_timerVisible, true);
		
		#if !DEBUG_DRAW
		Utility.SetState (m_debugFPS.gameObject, false, true);
		Destroy(m_debugFPS.gameObject);
		m_debugFPS = null;
		#endif
		
	}
	
	public void EnergyChanged(float energyNormalized)
	{
		m_playerEnergy.sliderValue = energyNormalized;
	}
	
	[Obfuscar.Obfuscate]
	private void SetVisibleGUI(bool flag)
	{
		foreach (GameObject o in m_uiElements)
		{
			NGUITools.SetActive(o, flag);
		}
	}
	
	// Use this for initialization
	void Start ()
	{
		ExecuteWeaponIconUpdate();
		ExecuteItemIconUpdate();
		UpdateItemCounts();
	}
	
	void Update()
	{
		#if DEBUG_DRAW
		m_debugFPS.text = string.Format ("{0:0}", FPSCounterScript.Instance.m_fps);
		#endif
		
		if (GameStateScript.Instance.State != GameStateScript.GameState.InGameLive
			|| LevelScript.Instance.Outcome != LevelScript.Result.Ongoing)
			return;

		if (m_usingDpadControls
			&& PlayerHomePlanetScript.Instance != null 
			&& !GameStateScript.Instance.InputDisabled)
		{
			const float delta = 0.25f;
			
			float speed = 0f;
			if (m_turnLeftOn)
				speed += delta;
			if (m_turnRightOn)
				speed -= delta;
			GamePlayCamera.Instance.SetCameraSpeed(speed);
		}
		
		if (m_timerVisible)
		{
			UpdateTimer ();
		}
	}		
		
	#endregion
		
	#region Public object methods
	public void OnButtonHideOuya()
	{
		m_ouyaInstructiosVisible = !m_ouyaInstructiosVisible;
		SetOuyaButtonVisiblity(m_ouyaInstructiosVisible);		
	}	
	
	public void OnMessageFire()
	{
		if (GameStateScript.Instance.DontAcceptInput())
		{
			GameStateScript.Instance.ShootingState = false;
			return;
		}
		
		GameStateScript.Instance.ShootingState = true;
	}
	public void OnMessageFireRelease()
	{
		if (GameStateScript.Instance.DontAcceptInput())
		{
			GameStateScript.Instance.ShootingState = false;
			return;
		}
		
		GameStateScript.Instance.ShootingState = false;
	}
	
	public void OnMessageWeapon1()
	{
		if (GameStateScript.Instance.DontAcceptInput())
			return;
		
		if (WeaponSystemScript.Instance.SetActiveWeaponPrimary(0))
		{
			SaveData.Instance.Config.WeaponPrimarySelected = SaveData.Instance.Config.WeaponPrimarySlot_1;
			ExecuteWeaponIconUpdate();
		}
	}
	public void OnMessageWeapon2()
	{		
		if (GameStateScript.Instance.DontAcceptInput())
			return;
		
		if (WeaponSystemScript.Instance.SetActiveWeaponPrimary(1))
		{
			SaveData.Instance.Config.WeaponPrimarySelected = SaveData.Instance.Config.WeaponPrimarySlot_2;
			ExecuteWeaponIconUpdate();
		}
	}
	public void OnMessageWeapon3()
	{
		if (GameStateScript.Instance.DontAcceptInput())
			return;
		
		if (WeaponSystemScript.Instance.SetActiveWeaponPrimary(2))
		{
			SaveData.Instance.Config.WeaponPrimarySelected = SaveData.Instance.Config.WeaponPrimarySlot_3;
			ExecuteWeaponIconUpdate();
		}
	}	
	public void OnMessageItem1()
	{
		if (GameStateScript.Instance.DontAcceptInput())
			return;
		
		if (ItemSystemScript.Instance.UseItem(0))
			UpdateItemCounts();
	}
	public void OnMessageItem2()
	{
		if (GameStateScript.Instance.DontAcceptInput())
			return;
		
		if (ItemSystemScript.Instance.UseItem(1))
			UpdateItemCounts();
			
		// DEBUG!!!
		//PlayerHomePlanetScript.Instance.DebugReduceEnergy(1.1f);
		/*
		if (GameStateScript.Instance.State != GameStateScript.GameState.InGamePause)
			GameStateScript.Instance.SetToPause();
		else
			GameStateScript.Instance.ReturnToPlay();
		*/
	}
	
	public void AttackWaveLaunched(AttackWaveScript sender)
	{
		UILabel label  = m_phaseLabel.GetComponent<UILabel>();
		label.pivot = UIWidget.Pivot.Right;
		Vector3 pos = label.transform.localPosition;
		label.transform.localPosition = new Vector3(20f, pos.y, pos.z);
		m_phaseLabel.GetComponent<UILabel>().pivot = UIWidget.Pivot.Right;
		m_phaseLabel.key = "menu_ingame_phase";
		m_phaseLabel.Localize();
		m_phaseValue.text = string.Format ("{0}", sender.Order + 1);
	}
	
	public void OnMessageTurnLeft()
	{
		if (GameStateScript.Instance.DontAcceptInput())
			return;
		
		m_turnLeftOn = true;
	}

	public void OnMessageTurnRight()
	{
		if (GameStateScript.Instance.DontAcceptInput())
			return;
		
		m_turnRightOn = true;
	}
	public void OnMessageTurnLeftRelease()
	{
		m_turnLeftOn = false;
	}

	public void OnMessageTurnRightRelease()
	{
		m_turnRightOn = false;
	}
	
	#endregion
		
	#region Private object methods
	[Obfuscar.Obfuscate]
	private void UpdateTimer()
	{
		float elapsedTime = GameStateScript.Instance.NNUKE_GetWorldTime() - LevelScript.Instance.StartTime;
		m_timeLabel.text = string.Format ("{0:0}", elapsedTime);
		
		if (LevelScript.Instance.m_isNeverEnding && elapsedTime > 5f)
		{
			Utility.SetState(m_phaseLabel.gameObject, false, true);
		}
	}
	
	[Obfuscar.Obfuscate]
	private void UpdateItemCounts()
	{
		for (int i = 0; i < ItemSystemScript.Instance.m_configs.Length; i++)
		{
			xsd_ct_Equipment item = ItemSystemScript.Instance.m_configs[i];
			
			bool isOn = item != null;
			Utility.SetState(m_itemCountLabels[i].gameObject, isOn, true);
			if (isOn)
			{
				m_itemCountLabels[i].text = string.Format("{0}", item.Number);
			}
		}
	}
	
	[Obfuscar.Obfuscate]
	private void DecideControls(bool right, bool left, bool analog, bool digital)
	{		
		foreach (GameObject obj in m_rightSideControls)
		{
			NGUITools.SetActiveChildren(obj, right);
		}
		foreach (GameObject obj in m_leftSideControls)
		{
			NGUITools.SetActiveChildren(obj, left);
		}
		
		// in addition, decide if we use the wheel (analog) or the left/right buttons (digital)
		GameObject analogWheel = GameObject.Find ("buttonWheel");
		GameObject analogCollider = GameObject.Find ("rootMovement/collider");
		GameObject digitalRoot = GameObject.Find ("rootDigitalControls");
		
		Utility.SetState(analogWheel, analog, true);
		Utility.SetState(analogCollider, analog, true);
		Utility.SetState(digitalRoot, digital, true);
	}
	
	[Obfuscar.Obfuscate]
	public void ExecuteWeaponIconUpdate()
	{
		for (int i = 0; i < 3; i++)
		{
			m_guiElements[i].SetActiveState(i == WeaponSystemScript.Instance.GetActiveWeaponPrimary());
		}
		UpdateSprite(SaveData.Instance.NNUKE_GetWeaponPrimary(SaveData.Instance.Config.WeaponPrimarySelected), 5, true);
	}
	[Obfuscar.Obfuscate]
	private void ExecuteItemIconUpdate()
	{
		for (int i = 3; i < 5; i++)
		{
			m_guiElements[i].SetActiveState(false);
		}
	}
	
	[Obfuscar.Obfuscate]
	private void AssignSprites()
	{
		int wi = 0;
		if (!string.IsNullOrEmpty(SaveData.Instance.Config.WeaponPrimarySlot_1))
			UpdateSprite(SaveData.Instance.NNUKE_GetWeaponPrimary(SaveData.Instance.Config.WeaponPrimarySlot_1), wi++);
		if (!string.IsNullOrEmpty(SaveData.Instance.Config.WeaponPrimarySlot_2))
			UpdateSprite(SaveData.Instance.NNUKE_GetWeaponPrimary(SaveData.Instance.Config.WeaponPrimarySlot_2), wi++);
		if (!string.IsNullOrEmpty(SaveData.Instance.Config.WeaponPrimarySlot_3))
			UpdateSprite(SaveData.Instance.NNUKE_GetWeaponPrimary(SaveData.Instance.Config.WeaponPrimarySlot_3), wi++);
		while (wi < 3)
		{
			UpdateSprite(null, wi++);
		}

		UpdateSprite(SaveData.Instance.NNUKE_GetItem(SaveData.Instance.Config.ItemSlot_1), 3);
		UpdateSprite(SaveData.Instance.NNUKE_GetItem(SaveData.Instance.Config.ItemSlot_2), 4);

		UpdateSprite(SaveData.Instance.NNUKE_GetWeaponPrimary(SaveData.Instance.Config.WeaponPrimarySelected), 5, true);
	}
	
	[Obfuscar.Obfuscate]
	private void UpdateSprite(xsd_ct_Equipment equipment, int index, bool forceEnabled = false)
	{
		GUIButtonWrapper element = m_guiElements[index];
		string spriteName = "";
		if (equipment != null)
		{
			xsd_ct_GarageSlot slot = GameData.Instance.NNUKE_GetEntry(equipment.Id);
			if (slot != null)
				spriteName = slot.IconSilhouetteId;
		}
		bool available = !string.IsNullOrEmpty(spriteName);
		
		if (element.icon != null) 
		{
			element.icon.enabled = available;
			element.icon.spriteName = spriteName; 
		
			if (available)
			{
				Utility.ScaleSprite(ref element.icon, false);
			}
		}
		
		if (element.bgSprite != null) 
		{
			element.bgSprite.enabled = available || forceEnabled;
		}
		element.collider.enabled = available || forceEnabled;
		
	}
	[Obfuscar.Obfuscate]
	private void SetOuyaButtonVisiblity(bool flag)
	{
		foreach (GameObject o in m_ouyaInstructionObjects)
		{
			Utility.SetState(o, flag, true);
		}
	}

	#endregion

}
