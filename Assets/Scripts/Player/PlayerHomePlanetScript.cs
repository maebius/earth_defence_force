//#define DEBUG_TEXT

using UnityEngine;
using System.Collections;

sealed public class PlayerHomePlanetScript : MonoBehaviour 
{
	#region Public GUI tweakable members
	public GameObject PrefabTextViz;
	public GameObject CloudVisualization;
	public DamageTakerScript DamageTaker;
	
	public Camera WorldCamera;
	public Camera UICamera;
	
	public UILabel m_scoreLabel;
	public UILabel m_creditsLabel;
	
	public AudioClip m_alarmSound;
	public float m_alarmSoundDeltaSeconds;
	
	public Rotate m_rotate;
	public Rotate m_cloudsRotate;
	
	public PlayerActionsScript m_playerActions;
	#endregion

	#region Delegates And Events
	// A delegate type for hooking up change notifications.
	public delegate void PlayerDied(PlayerHomePlanetScript sender);
	// An event that clients can use to be notified whenever enemy dies
	public event PlayerDied Died;
	
	#endregion

	
	#region Public non-GUI members
	[HideInInspector]
	public bool MadeNewHighScore
	{
		get { return m_madeNewHighScore; }
		set { m_madeNewHighScore = value; }
	}	
	[HideInInspector]
	public int StarCountForScore
	{
		get { return m_stars; }
		set { m_stars = value; }
	}
	[HideInInspector]
	public int Score
	{
		get { return m_score; }
		set { m_score = value; }
	}
	[HideInInspector]
	public int BonusHealth
	{
		set { m_bonusHealth = value; }
		get { return m_bonusHealth; }
	}
	[HideInInspector]
	public int BonusEnemies
	{
		set { m_bonusEnemies = value; }
		get { return m_bonusEnemies; }
	}
	[HideInInspector]
	public int BonusBullets
	{
		set { m_bonusBullets = value; }
		get { return m_bonusBullets; }
	}
	[HideInInspector]
	public int EnemiesEncountered
	{
		get { return m_enemiesEncountered; }
	}
	[HideInInspector]
	public int EnemiesKilled
	{
		get { return m_enemiesKilled; }
	}
	[HideInInspector]
	public int BulletsFired
	{
		set { m_bulletsFired = value; }
		get { return m_bulletsFired; }
	}
	[HideInInspector]
	public int BulletsHit
	{
		set 
		{ 
			// don't let this be greater -> it might as we are only counting primary
			// bullets as "fired", but both primary and secondary as "hit"
			// -> not the cleanest solution, but as primary/secondary are fired
			// at the same press it would not be "fair" to count those missed ...
			m_bulletsHit = Mathf.Min (value, BulletsFired);
		}
		get { return m_bulletsHit; }
	}
	[HideInInspector]
	public int Credits
	{
		get { return m_credits; }
	}
	
	[HideInInspector]
	public float PlayTime
	{
		get { return m_playTime; }
		set { m_playTime = value; }
	}
	
	[HideInInspector]
	public static PlayerHomePlanetScript Instance
	{
		get { return sm_instance; }
	}
	public static bool IsDead
	{
		get 
		{ 
			return sm_instance == null
				|| sm_instance.DamageTaker == null
				|| sm_instance.DamageTaker.Energy <= 0f;
		}
	}
	
	#endregion
	
	#region Private data members
	private static PlayerHomePlanetScript sm_instance = null;
	
	private int m_score;
	private int m_bonusHealth;
	private int m_bonusEnemies;
	private int m_bonusBullets;
	private int m_credits;
	private int m_stars;
	
	private int m_enemiesEncountered;
	private int m_enemiesKilled;
	
	private int m_bulletsFired;
	private int m_bulletsHit;
	
	private bool m_madeNewHighScore;
	private float m_playTime;
	
	private bool m_alarmPlaying;
	private float m_alarmSoundInterval;
	private float m_alarmSoundLastAudible;
	private AudioSource m_alarmAudioSource;

	#endregion 
	
	
	#region Unity MonoBehaviour base overrides
	// Use this for early initialization
	void Awake ()
	{
		// this means that we have actually died and ShatterTool just
		// made another instance of us ... don't do anything
		if (sm_instance != null)
		{
			Destroy (this);
			return;
		}
		
		sm_instance = this;
	}
	
	// Use this for initialization
	void Start () 
	{
		#if DEBUG_TEXT
		Debug.Log (string.Format ("PlayerHomePlanetScript.Start - time: {0}", Time.time));
		#endif
		
		m_enemiesEncountered = 0;
		m_enemiesKilled = 0;
		m_bulletsFired = 0;
		m_bulletsHit= 0;
		
		// these are the amounts we get from the level
		// -> not directly from save game as we do not give credit if we die in action
		m_stars = 0;
		m_score = 0;
		m_bonusHealth = 0;
		m_bonusBullets = 0;
		m_bonusEnemies = 0;
		m_credits = 0;
		
		m_alarmAudioSource = SoundManagerScript.Play(gameObject, m_alarmSound, false, false, 1f);
		m_alarmAudioSource.Stop();
		
		UpdateScore();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (GameStateScript.Instance.State != GameStateScript.GameState.InGameLive)
		{
			m_rotate.Active = false;
			m_cloudsRotate.Active = false;
			return;
		}
		m_rotate.Active = true;
		m_cloudsRotate.Active = true;
			
		if (m_alarmAudioSource != null)
		{
			if (m_alarmPlaying)
			{
				float now = GameStateScript.Instance.NNUKE_GetWorldTime();
				
				if (m_alarmAudioSource.isPlaying)
				{
					m_alarmSoundLastAudible = now;
				}
				else if (m_alarmSoundLastAudible + m_alarmSoundInterval < now)
				{
					m_alarmAudioSource.Play();
				}
			}
		}
		
		m_playerActions.EnergyChanged(DamageTaker.EnergyNormalized);
	}
	
	#endregion 
	
	#region Public object methods
	public void SetAlarmFactor(float urgency)
	{
		const float URGENCY_LIMIT_FOR_SOUND = 0.33f;
		
		m_alarmPlaying = (urgency < URGENCY_LIMIT_FOR_SOUND);
		if (m_alarmPlaying)
		{
			m_alarmSoundInterval = (urgency / URGENCY_LIMIT_FOR_SOUND) * m_alarmSoundDeltaSeconds;
		}
	}
	
	public void DamageTakerSpawned(DamageTakerScript source)
	{
		// only if enemy ...
		if (source.transform.parent != null &&  source.transform.parent.gameObject.GetComponent<EnemyBaseScript>() != null)
			m_enemiesEncountered++;
	}
	
	public void DamageTakerDied(DamageTakerScript source)
	{
		if (GameStateScript.Instance.State == GameStateScript.GameState.InGameLive
			&& LevelScript.Instance.Outcome == LevelScript.Result.Ongoing
			&& (source.DiedBecauseOf == DamageTakerScript.DamageType.PlayerBullet
				|| source.DiedBecauseOf == DamageTakerScript.DamageType.PlayerShield))
		{
			#if DEBUG_TEXT
			Debug.Log (string.Format ("PlayerHomePlanetScript.DamageTakerDied - cause: {0}, for player - score {1}, energy {2}, credits {3} (time: {4})"
				, source.DiedBecauseOf, source.ScoreToPlayer, source.EnergyToPlayer, source.CreditsToPlayer, Time.time));
			#endif
			
			// it might be that inside one frame we get multiple calls ...
			if (source.m_playerHandled)
				return;
			
			source.m_playerHandled = true;
			
			string text = "";
			m_score += source.ScoreToPlayer;
			PlayerHomePlanetScript.Instance.GiveEnergy(source.EnergyToPlayer);
			m_credits += source.CreditsToPlayer;
			UpdateScore();
			
			if (source.EnergyToPlayer > 0f)
			{
				text = string.Format ("[9927A2]{0}[-]%", (int) (source.EnergyToPlayer * 100));
			}
			else if (source.CreditsToPlayer > 0)
			{
				text = string.Format ("[80C0D6]{0}[-]", source.CreditsToPlayer);
			} 
			else if (source.ScoreToPlayer > 0)
			{
				text = string.Format ("{0}", source.ScoreToPlayer);
			}
			
			if (PrefabTextViz && !string.IsNullOrEmpty(text))
			{
				GameObject obj = (GameObject) Instantiate(PrefabTextViz
					, Vector3.zero
					, Quaternion.identity);
				UILabel label = (UILabel) obj.GetComponentInChildren<UILabel>();
				label.text = text;

				UpdateGUIToWorldPositionScript ugwps = (UpdateGUIToWorldPositionScript)
					obj.GetComponent<UpdateGUIToWorldPositionScript>();
				if (ugwps)
				{
					ugwps.WorldPosition = source.transform.position;
					ugwps.WorldCamera = WorldCamera;
					ugwps.UICamera = UICamera;
				}
			}
			
			
			// was this enemy?
			if (source.transform.parent && source.transform.parent.GetComponent<EnemyBaseScript>())
				m_enemiesKilled++;
		}
	}
	
	public void LevelEnded(LevelScript level)
	{
		if (level.Outcome == LevelScript.Result.Success && DamageTaker != null)
		{
			
			DamageTaker.EnergyReduces = false;
		}
	}
	
	#endregion
	
	#region Private object methods
	[Obfuscar.Obfuscate]
	private void UpdateScore()
	{
		m_scoreLabel.text = string.Format ("{0}", m_score);
		m_creditsLabel.text = string.Format ("{0}", 
			SaveData.Instance.Config.Credits + m_credits);
	}
	#endregion
	
	void OnCollisionEnter (Collision hit)	
	{
		#if DEBUG_TEXT
		Debug.Log (string.Format ("PlayerHomePlanetScript.OnCollisionEnter - hit by: {0}", hit.gameObject.name));
		#endif
		
		DamageTakerScript taker = hit.gameObject.GetComponent<DamageTakerScript>();
		if (!taker)
			taker = hit.gameObject.GetComponentInChildren<DamageTakerScript>();
		
		if (taker) 
		{
			if (taker.IsPlayer())
				return;
		
			// first give damage to us (need to do this before killing enemy)
			if (DamageTaker != null)
			{
				DamageTaker.DoGiveDamage (
					taker.DamageToPlayerFromCollision
					, DamageTakerScript.DamageType.EnemyCollision
					, hit.transform.position
					);
			}
			// ... this will kill the enemy
			taker.DoGiveDamage(
				taker.Energy + 1f
				, DamageTakerScript.DamageType.PlayerCollision
				, hit.transform.position);
		}
	}
	
	[Obfuscar.Obfuscate]
	public float GetCollideRadius()
	{
		if (GetComponent<Collider>() == null)
			return -1f;
		return GetComponent<Collider>().bounds.extents.x;
	}

	[Obfuscar.Obfuscate]
	public bool CheckIfDead()
	{	
		if (DamageTaker != null)
		{
			return DamageTaker.Energy <= 0f;
		}
		return true;
	}
	
	[Obfuscar.Obfuscate]
	public void DebugReduceEnergy(float pct = 1.1f)
	{
		DamageTaker.DoGiveDamage(pct * DamageTaker.Energy
			, DamageTakerScript.DamageType.EnemyBullet
			, Vector3.zero);
	}
	
	[Obfuscar.Obfuscate]
	public void GiveEnergy(float amount)
	{
		// just to make this more "sane"
		if (amount < 0f) return;
		if (DamageTaker != null)
		{
			DamageTaker.Energy += amount;
			DamageTaker.Energy = Mathf.Min (1f, DamageTaker.Energy);
		}
	}
	
	public void DidDie(DamageTakerScript me)
	{
		// makes sure energy will be zero in visualization
		m_playerActions.EnergyChanged(0f);
		
		if (GetComponent<Rigidbody>())
		{
			GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ;
		}

		if (CloudVisualization)
		{
			Utility.SetState(CloudVisualization.gameObject, false, false);
		}
		if (Died != null)
		{
			Died(this);
		}
	}
	
	
}
