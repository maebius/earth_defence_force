//#define DEBUG_TEXTS

#define USE_OUYA

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

sealed public class PlayerInputScript : MonoBehaviour 
{
	public GameObject[] m_rootsToFindButtons;
	
	public UICamera m_uiCamera;
	
	public KeyCode m_keyBasePlayer;// = KeyCode.JoystickButton0;

	public KeyCode NGUISubmitKey0;// = KeyCode.Enter;
	public KeyCode NGUISubmitKey1;// = KeyCode.Joystick1Button0;
	public KeyCode NGUICancelKey0;// = KeyCode.Escape;
	public KeyCode NGUICancelKey1;// = KeyCode.Joystick1Button1;

	public string NGUIAxisNameVertical;
	public string NGUIAxisNameHorizontal;

	public string AxisChangeWeapon;
	public string AxisTurning;
	public string AxisShieldX;
	public string AxisShieldY;
	
	public PlayerActionsScript m_playerAction;
	
	public AudioClip m_changeWeaponSound;
	
	// xbox:
	// AxisX / Horizontal = Left analog - L/R
	// AxisY / Vertical = Left analog - U/D
	// Axis3 = Trigger
	// Axis4 = Right analog - L/R
	// Axis5 = Right analog - U/D
	// Axis6 = Left digital - L/R
	// Axis7 = Left digital - U/D
	
	public Dictionary<UnityEngine.KeyCode, string> m_gamepadButtonMap 
		= new Dictionary<UnityEngine.KeyCode, string>();
	
	private class AxisValues
	{
		public string name;
		public bool lastNegativeActivation;
		public bool lastPositiveActivation;
		public float negativeActivationLimit;
		public float positiveActivationLimit;
	}
	private Dictionary<string, AxisValues> m_axes = new Dictionary<string, AxisValues>();
	
	private const float TURNING_DEAD_ZONE = 0.25f;
	private const float CHANGE_WEAPONS_DEAD_ZONE = 0.5f;

	private const float SHIELD_DEAD_ZONE = 0.25f;
	private const float SHIELD_COEFFICIENT = 1f / (1f - SHIELD_DEAD_ZONE);

	private const float TURNING_COEFFICIENT = 1f / (1f - TURNING_DEAD_ZONE);
	
	[Obfuscar.Obfuscate]
	public bool GetPositiveActivation(string axis, bool filterNew)
	{
		if (string.IsNullOrEmpty(axis)) return false;
		if (!m_axes.ContainsKey(axis))
		{
			#if DEBUG_TEXTS
			Debug.LogWarning(
				string.Format ("PlayerInputScript.GetNegativeActivation - no axis \"{0}\" for: {1}"
				, axis, gameObject.name));
			#endif
			return false;
		}
		
		AxisValues av = m_axes[axis];
		
		float val = OuyaInputWrapper.GetAxis(axis);
		bool lastActivation = av.lastPositiveActivation;
		bool activation = val > av.positiveActivationLimit;
		av.lastPositiveActivation = activation;
		return activation && (!lastActivation || !filterNew);
	}

	[Obfuscar.Obfuscate]
	public bool GetNegativeActivation(string axis, bool filterNew)
	{
		if (string.IsNullOrEmpty(axis)) return false;
		if (!m_axes.ContainsKey(axis))
		{
			#if DEBUG_TEXTS
			Debug.LogWarning(
				string.Format ("PlayerInputScript.GetNegativeActivation - no axis \"{0}\" for: {1}"
				, axis, gameObject.name));
			#endif
			return false;
		}
		
		AxisValues av = m_axes[axis];
		
		float val = OuyaInputWrapper.GetAxis(axis);
		bool lastActivation = av.lastNegativeActivation;
		bool activation = val < av.negativeActivationLimit;
		av.lastNegativeActivation = activation;
		return activation && (!lastActivation || !filterNew);
	}
	[Obfuscar.Obfuscate]
	public float GetValue(string axis)
	{
		if (string.IsNullOrEmpty(axis)) return 0f;
		if (!m_axes.ContainsKey(axis))
		{
			#if DEBUG_TEXTS
			Debug.LogWarning(
				string.Format ("PlayerInputScript.GetNegativeActivation - no axis \"{0}\" for: {1}"				
				, axis, gameObject.name));
			#endif	
			return 0f;
		}
		return OuyaInputWrapper.GetAxis(axis);
	}
		
	[Obfuscar.Obfuscate]
	private void AddInputAxisMapping(string axis, float negativeLimit, float positiveLimit)
	{
		if (string.IsNullOrEmpty(axis))
		{
			#if DEBUG_TEXTS
			Debug.LogWarning(
				string.Format ("PlayerInputScript.AddInputAxisMapping - some mapping missing for: {0}!"
				, gameObject.name));
			#endif
				return;
		}
		if (m_axes.ContainsKey(axis))
		{
			#if DEBUG_TEXTS
			Debug.LogWarning(
				string.Format ("PlayerInputScript.AddInputAxisMapping - NOTE! Mapping for {0} already defined for: {1}! Skipping ..."
				, axis, gameObject.name));
			#endif
				return;
		}
		
		AxisValues av = new AxisValues() { 
			name = axis
			, lastNegativeActivation = false
			, lastPositiveActivation = false
			, negativeActivationLimit = negativeLimit
			, positiveActivationLimit = positiveLimit
		};
		m_axes.Add (axis, av);
	}
	
	void Awake() 
	{
		// xbox:
		// 0 = A
		// 1 = B
		// 2 = X
		// 3 = Y
		// 4 = LB
		// 5 = RB
		// 6 = select
		// 7 = start
		// 8 = LS
		// 9 = RS
		
		// NOTE! These have correspondence in OuyaInputWrapper.cs - file, in KeyCodes - constructor,
		// where we map the keyCodes to actual Ouya-buttons
		// -> we basically emulate the Xbox - layout here
		
		// gamepad buttons
		
		m_gamepadButtonMap.Add (m_keyBasePlayer + 0, "buttonFire");
		m_gamepadButtonMap.Add (m_keyBasePlayer + 2, "buttonItem_1");
		m_gamepadButtonMap.Add (m_keyBasePlayer + 3, "buttonItem_2");
		m_gamepadButtonMap.Add (m_keyBasePlayer + 7, "buttonPause");
		m_gamepadButtonMap.Add (m_keyBasePlayer + 1, "buttonOuyaHide");

		// gamepad axis binding
		
		AddInputAxisMapping(AxisChangeWeapon, -CHANGE_WEAPONS_DEAD_ZONE, CHANGE_WEAPONS_DEAD_ZONE);
		AddInputAxisMapping(AxisTurning, -TURNING_DEAD_ZONE, TURNING_DEAD_ZONE);
		
		// Ouya gamepad spesific axis mapping
#if USE_OUYA
		OuyaInputWrapper.SetAxisMap(AxisTurning, "LX", OuyaSDK.OuyaPlayer.player1);
		OuyaInputWrapper.SetAxisMap(AxisShieldX, "RX", OuyaSDK.OuyaPlayer.player1);
		OuyaInputWrapper.SetAxisMap(AxisShieldY, "RY", OuyaSDK.OuyaPlayer.player1);
		OuyaInputWrapper.SetAxisMap(AxisChangeWeapon, "DPU", OuyaSDK.OuyaPlayer.player1);
#endif
		// keyboard 
		
		m_gamepadButtonMap.Add (KeyCode.Return, "buttonFire");
		m_gamepadButtonMap.Add (KeyCode.Alpha1, "buttonItem_1");
		m_gamepadButtonMap.Add (KeyCode.Alpha2, "buttonItem_2");
		m_gamepadButtonMap.Add (KeyCode.P, "buttonPause");
		m_gamepadButtonMap.Add (KeyCode.O, "buttonOuyaHide");
		

		// NGUI element navigation
		
		m_uiCamera.submitKey0 = NGUISubmitKey0;
		m_uiCamera.submitKey1 = NGUISubmitKey1;
		m_uiCamera.cancelKey0 = NGUICancelKey0;
		m_uiCamera.cancelKey1 = NGUICancelKey1;
		m_uiCamera.horizontalAxisName = NGUIAxisNameHorizontal;
		m_uiCamera.verticalAxisName = NGUIAxisNameVertical;
	}
	
	void Start()
	{
		AssignGamepadMappingsToButtons();
	}
	
	void Update()
	{
		const float power = 10f;
		
		float speed = 0f;
		int oldCurrent = WeaponSystemScript.Instance.GetActiveWeaponPrimary();
		int current = oldCurrent;
		if (GameStateScript.Instance.InputDisabled)
		{
			GamePlayCamera.Instance.SetCameraSpeed(0f);
		}
		else
		{		
			// only do this if not currently doing touch (= mouse) input
			if (!InputRotateScript.sm_gotSomeRaycasts)
			{	
				if (GetPositiveActivation(AxisTurning, false)
					|| GetNegativeActivation(AxisTurning, false))
				{
					float val = OuyaInputWrapper.GetAxis(AxisTurning);
					val += (val < 0f) ? TURNING_DEAD_ZONE : -TURNING_DEAD_ZONE;
					val *= TURNING_COEFFICIENT;

					float s = Mathf.Sign(val);
					float abs = Mathf.Abs(val);
					
					speed = -s * 0.5f * Mathf.Pow(power, abs) / power;
				}
				
				GamePlayCamera.Instance.SetCameraSpeed(speed);
			}
			
			if (GetPositiveActivation(AxisChangeWeapon, true))
			{
				current++;
			}
			if (GetNegativeActivation(AxisChangeWeapon, true))
			{
				current--;
			}
			if (current != oldCurrent)
			{
				SoundManagerScript.PlayOneTime (gameObject, m_changeWeaponSound);
			}
			
			WeaponSystemScript.Instance.SetActiveWeaponPrimary(current);
			if (WeaponSystemScript.Instance.GetActiveWeaponPrimary() != oldCurrent)
			{
				m_playerAction.ExecuteWeaponIconUpdate();
			}
		}
		
		if (OuyaInputWrapper.UseGamepad)
		{
			float shieldX = OuyaInputWrapper.GetAxis(AxisShieldX);
			float shieldY = OuyaInputWrapper.GetAxis(AxisShieldY);
			if (Mathf.Abs (shieldX) > SHIELD_DEAD_ZONE || Mathf.Abs (shieldY) > SHIELD_DEAD_ZONE)
			{
				// normalize
				shieldX += (shieldX < 0f) ? SHIELD_DEAD_ZONE : -SHIELD_DEAD_ZONE;
				shieldX *= SHIELD_COEFFICIENT;
				shieldY += (shieldY < 0f) ? SHIELD_DEAD_ZONE : -SHIELD_DEAD_ZONE;
				shieldY *= SHIELD_COEFFICIENT;
				
				Vector2 v = new Vector2(shieldX, shieldY);
				v.Normalize();
				
				RadarControlScript.Instance.SetRadarTarget(v.x, v.y);
			}
			else
			{
				RadarControlScript.Instance.ResetRadarTarget();
			}
		}
	}
	
	[Obfuscar.Obfuscate]
	private void AssignGamepadMappingsToButtons()
	{
		foreach (KeyValuePair<UnityEngine.KeyCode, string> keyValue in m_gamepadButtonMap)
		{
			foreach (GameObject root in m_rootsToFindButtons)
			{
				GameObject obj = Utility.GetChildRecursive(root, keyValue.Value, false);
				if (obj != null)
				{
					bool added = false;
					UIButtonKeyBinding[] bindings = obj.GetComponents<UIButtonKeyBinding>();
					foreach (UIButtonKeyBinding binding in bindings)
					{
						if (binding.keyCode == KeyCode.None)
						{
							binding.keyCode = keyValue.Key;
							added = true;
							break;
						}
					}
					if (!added)
					{
						UIButtonKeyBinding newBinding = obj.gameObject.AddComponent<UIButtonKeyBinding>();
						newBinding.keyCode = keyValue.Key;
					}
					
					// we found a match, so break out our foreach
					break;
				}
			}
		}
	}	
}
