using UnityEngine;
using System.Collections;

sealed public class PlayerShieldElementScript : MonoBehaviour 
{
	#region Public data members
	public bool m_blockProjectiles;
	public float m_projectileDamageRatioAfterHit;
	
	public GameObject HitVisualizationPrefab;
	public AudioClip m_audioHit;	
	#endregion
	
	#region Private data members
	private PlayerShieldScript m_parent;
	
	private Vector3 m_originalScale;
	
	#endregion
		
	#region Unity MonoBehaviour base overrides
	// Use this for initialization
	void Start ()
	{
		m_originalScale = transform.localScale;
	}

	// Update is called once per frame
	void Update ()
	{	
		float v = Mathf.Max (0.001f, m_parent.Charge * m_parent.DistanceFactor);
		transform.localScale = v * m_originalScale;
	}
	
	void OnCollisionEnter (Collision hit)	
	{
		if (gameObject == null || m_parent.State != PlayerShieldScript.ShieldState.Live)
			return;
		
		HandleDamageTaker(hit.gameObject.GetComponent<DamageTakerScript>(), hit.transform.position);
	}
	
	void OnTriggerEnter (Collider hit)
	{
		if (gameObject == null || m_parent.State != PlayerShieldScript.ShieldState.Live)
			return;
		
		if (HandleDamageTaker(hit.gameObject.GetComponent<DamageTakerScript>(), hit.transform.position))
			return;
		
		HandleProjectile(hit.gameObject.GetComponent<ProjectileScript>(), hit.transform.position);
	}
	
	#endregion
		
	#region Public object methods
	[Obfuscar.Obfuscate]
	public void SetParent(PlayerShieldScript parent)
	{
		m_parent = parent;
	}
	[Obfuscar.Obfuscate]
	public PlayerShieldScript GetParent()
	{
		return m_parent;
	}
	#endregion
		
	#region Private object methods
	private bool HandleDamageTaker(DamageTakerScript taker, Vector3 pos)
	{
		if (taker == null)
			return false;
		
		if (taker.IsPlayer())
			return false;

		SoundManagerScript.PlayOneTime (gameObject, m_audioHit);
		
		m_parent.DoEnemyHit();
		taker.DoGiveDamage(
			m_parent.Power
			, DamageTakerScript.DamageType.PlayerShield
			, pos);
		
		return true;
	}
	
	private bool HandleProjectile(ProjectileScript projectile, Vector3 pos)
	{
		if (projectile != null)
		{
			SoundManagerScript.PlayOneTime (gameObject, m_audioHit);
			 if (HitVisualizationPrefab)
			{
				GameObject obj = (GameObject) Instantiate(
					HitVisualizationPrefab
					, transform.position
					, transform.rotation);
				obj.active = true;
			}

			// don't react (otherwise than audiovisually above) to own bullets ...
			if (projectile.Owner != ProjectileScript.OwnerType.PlayerPrimary 
				&& projectile.Owner != ProjectileScript.OwnerType.PlayerSecondary)
			{
				if (m_blockProjectiles)
				{
					projectile.DepleteMyself();
				}
				else
				{
					m_parent.DoEnemyHit(true);
					projectile.Power = m_projectileDamageRatioAfterHit * projectile.Power;
				}
			}
			
			return true;
		}
		return false;
	}
	
	#endregion
}
