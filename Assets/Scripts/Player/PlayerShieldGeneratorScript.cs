using UnityEngine;
using System.Collections;
using System.Collections.Generic;

sealed public class PlayerShieldGeneratorScript : MonoBehaviour 
{
	#region Public GUI tweakable members
	public string[] ShieldIds;
	public PlayerShieldScript[] ShieldPrefabs;
	
	public float m_angleBetweenElements = 4.3f;
	public float ChargingSeconds;
	
	private PlayerShieldScript m_shield;
	
	[HideInInspector]
	public static PlayerShieldGeneratorScript Instance
	{
		get { return m_instance; }
	}
	#endregion
		
	#region Private data members
	private static PlayerShieldGeneratorScript m_instance = null;
	
	private Vector3 m_target;
	
	#endregion
		
	#region Unity MonoBehaviour base overrides
	// Use this for early initialization
	void Awake ()
	{
		m_instance = this;
	}
		
	// Use this for initialization
	void Start ()
	{
		PlayerHomePlanetScript.Instance.Died += new PlayerHomePlanetScript.PlayerDied(PlayerDied);
		
		xsd_ct_Equipment config = SaveData.Instance.NNUKE_GetShield(SaveData.Instance.Config.ShieldSlot);

		if (config == null)
			return;
		
		PlayerShieldScript prefab = NNUKE_GetShieldPrefab(config.Id);
		if (prefab == null)
			return;
		Vector3 planetPos = PlayerHomePlanetScript.Instance.transform.position;
		float radius = RadarControlScript.Instance.GetRadarRadiusMax();
			
		DoNewFixedShield(prefab, config, radius, new Vector3(
			planetPos.x - radius
			, planetPos.y
			, planetPos.z));
		
	}
	#endregion
	
	#region Public methods.
	
	public void DoDie()
	{	
		if (m_shield != null)
			Destroy(m_shield.gameObject);
		Destroy (gameObject);
	}
	
	[Obfuscar.Obfuscate]
	public float NNUKE_GetShieldCharge()
	{
		if (m_shield == null)
			return 0f;
		return m_shield.Charge;
	}
	[Obfuscar.Obfuscate]
	public Vector3 GetCurrentPosition()
	{
		if (m_shield == null)
			return Vector3.zero;
		return m_shield.gameObject.transform.position;
	}
	
	[Obfuscar.Obfuscate]
	public void SetShieldEnabled(bool flag)
	{
		if (m_shield != null)
			m_shield.SetEnabled(flag);
	}
	
	public void PlayerDied(PlayerHomePlanetScript player)
	{
		DoDie();
	}

	#endregion
	
	#region Private methods.
	[Obfuscar.Obfuscate]
	private void DoNewFixedShield(PlayerShieldScript prefab
		, xsd_ct_Equipment config
		, float radius, Vector3 position)
	{
		 m_shield = (PlayerShieldScript) Instantiate(
			prefab
			, position
			, Quaternion.identity);
		
		GameData gd = GameData.Instance;
		if (m_shield.PowerLevels.Length > 0) 
		{
			int index = gd.NNUKE_GetSlotPropertyIndex(config, "Power");
			index = Mathf.Min (index, m_shield.PowerLevels.Length - 1);
			m_shield.Power = m_shield.PowerLevels[index];
		}
		if (m_shield.SpeedLevels.Length > 0) 
		{
			int index = gd.NNUKE_GetSlotPropertyIndex(config, "Speed");
			index = Mathf.Min (index, m_shield.SpeedLevels.Length - 1);
			m_shield.Speed = m_shield.SpeedLevels[index];
			m_shield.AngleSpeedDegrees = m_shield.AngleSpeedDegreesLevels[index];
		}
		if (m_shield.ElementCountLevels.Length > 0) 
		{
			int index = gd.NNUKE_GetSlotPropertyIndex(config, "Range");
			index = Mathf.Min (index, m_shield.ElementCountLevels.Length - 1);
			m_shield.ElementCount = m_shield.ElementCountLevels[index];
			
			// NOTE! Basically (as we want the same angle regardless of distance from planet)
			// AngleDegreesAtMaximum == AngleDegreesAtMinimum
			// => only determines how wide apart (in any distance) the elements are from each other
			
			m_shield.AngleDegreesAtMaximum = m_shield.ElementCount * m_angleBetweenElements;
			m_shield.AngleDegreesAtMinimum = m_shield.AngleDegreesAtMaximum;
		}
		
		m_shield.Config = config;
		m_shield.MaximumReach = radius;
	}
	
	[Obfuscar.Obfuscate]
	private PlayerShieldScript NNUKE_GetShieldPrefab(string id)
	{
		if (string.IsNullOrEmpty(id))
			return null;
		int index = 0;
		for ( ; index < ShieldIds.Length; index++)
		{
			if (ShieldIds[index].Equals(id))
				break;
		}
		if (index < ShieldIds.Length && index < ShieldPrefabs.Length)
			return ShieldPrefabs[index];
		
		return null;
	}
	[Obfuscar.Obfuscate]
	public void ResetShieldTarget()
	{
		if (m_shield != null)
			m_shield.SetTargetPosition(m_shield.transform.position);
	}
	
	[Obfuscar.Obfuscate]
	public void SetShieldTarget(Vector3 target)
	{
		if (m_shield != null)
			m_shield.SetTargetPosition(target);
	}
	
	#endregion
		
	#region Private object methods
	#endregion
}
