using UnityEngine;
using System.Collections;
using System.Collections.Generic;

sealed public class PlayerShieldScript : MonoBehaviour 
{
	public enum ShieldState
	{
		Disabled
		, Charging
		, Live
		, Moving
	};
	
	#region Public GUI tweakable members
	// The prefab to be used when creating shield elements
	public PlayerShieldElementScript ElementPrefab;
	
	public float[] PowerLevels;
	public float[] SpeedLevels;
	public float[] AngleSpeedDegreesLevels;
	public int[] ElementCountLevels;
	
	public AudioClip m_sound;
	#endregion

	#region Public members, not showing in Unity GUI.
	[HideInInspector]
	public float Power;
	[HideInInspector]
	public float Speed;
	[HideInInspector]
	public float AngleSpeedDegrees;
	[HideInInspector]
	public int ElementCount;	
	[HideInInspector]
	public float AngleDegreesAtMinimum;
	[HideInInspector]
	public float AngleDegreesAtMaximum;
	[HideInInspector]
	public float MaximumReach;
	[HideInInspector]
	public ShieldState State;
	[HideInInspector]
	public float DistanceFactor;
	[HideInInspector]
	public float Charge;
	[HideInInspector]
	public float AngleOffsetDegrees;
	[HideInInspector]
	public xsd_ct_Equipment Config;
	#endregion
	
	#region Private data members
	private List<PlayerShieldElementScript> m_elements = new List<PlayerShieldElementScript>();
	private Vector3 m_targetPosition;
	
	private float m_untilLiveTimerSeconds;
	
	private Color m_currentColor;
	
	private float m_currentDistanceFromPlanet;
	private float m_targetDistanceFromPlanet;
	
	private float m_currentAngleRad;
	private float m_targetAngleRad;
	
	private bool m_initialize;
	
	private Vector3 m_currentPosition = Vector3.zero;
	
	private bool m_moving = false;
	
	private AudioSource m_audio;
	
	#endregion
		
	#region Unity MonoBehaviour base overrides
		
	// Use this for initialization
	void Start ()
	{
		// instantly live
		m_untilLiveTimerSeconds = -1f;
		State = ShieldState.Live;
		Charge = 1f;
		
		m_currentColor = new Color(0f, 1f, 0f, 1f);
		
		AngleOffsetDegrees = 0f;
		
		m_targetPosition = transform.position;
		Vector3 planetPos = PlayerHomePlanetScript.Instance.transform.position;

		Vector3 direction = m_targetPosition - planetPos;
		m_currentDistanceFromPlanet = direction.magnitude;
		
		m_currentAngleRad = Utility.VectorToAngleRad(direction);
		
		m_initialize = true;
	}
	
	[Obfuscar.Obfuscate]
	private void UpdateCharge()
	{
		switch (State)
		{
		case ShieldState.Charging:
			m_untilLiveTimerSeconds -= GameStateScript.Instance.NNUKE_GetWorldDeltaTime();
			if (m_untilLiveTimerSeconds < 0f)
			{
				State = ShieldState.Live;
				ExecuteSetEnabled(true);
			}
			Charge = (m_untilLiveTimerSeconds > 0f) ?
				(PlayerShieldGeneratorScript.Instance.ChargingSeconds 
					- m_untilLiveTimerSeconds)/PlayerShieldGeneratorScript.Instance.ChargingSeconds :
				1f;
			break;
		case ShieldState.Disabled:
			Charge = 0f;
			break;
		case ShieldState.Live:
		case ShieldState.Moving:
		default:
			break;
		}
	}
	
	[Obfuscar.Obfuscate]
	private void UpdatePosition()
	{
		if (State == ShieldState.Disabled)
			return;
		
		m_currentAngleRad = MathHelpers.ClampRadians(m_currentAngleRad);
		
		float fixedTarget = m_targetAngleRad;
		
		if (Mathf.Abs(m_targetAngleRad - m_currentAngleRad) > Mathf.PI)
		{
			if (m_targetAngleRad > m_currentAngleRad)
				fixedTarget = m_targetAngleRad - 2f*Mathf.PI;
			else
				fixedTarget = m_targetAngleRad + 2f*Mathf.PI;
		}
		
		m_moving = Mathf.Abs (fixedTarget - m_currentAngleRad) > 0.05f 
			|| Mathf.Abs (m_currentDistanceFromPlanet - m_targetDistanceFromPlanet) > 1f;
		
		if (m_moving && !m_audio.isPlaying)
		{
			m_audio.Play();
		}
		else if (!m_moving && m_audio.isPlaying)
		{
			m_audio.Stop ();
		}
		
		m_currentAngleRad = Mathf.MoveTowardsAngle(m_currentAngleRad, fixedTarget, 
			GameStateScript.Instance.NNUKE_GetWorldDeltaTime() * AngleSpeedDegrees * Mathf.Deg2Rad);
				
		m_currentDistanceFromPlanet = Mathf.MoveTowards(
			m_currentDistanceFromPlanet
			, m_targetDistanceFromPlanet
			, GameStateScript.Instance.NNUKE_GetWorldDeltaTime() * Speed);
		
		Vector3 playerPos = PlayerHomePlanetScript.Instance.transform.position;
		
		Vector3 nextPosition = new Vector3(
			playerPos.x + m_currentDistanceFromPlanet * Mathf.Cos (m_currentAngleRad)
			, playerPos.y + m_currentDistanceFromPlanet * Mathf.Sin (m_currentAngleRad)
			, playerPos.z);;
		
		transform.position = nextPosition;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (m_initialize)
		{
			m_initialize = false;
			SetTargetPosition(m_targetPosition);
			m_currentPosition = m_targetPosition;
			ExecuteShieldConstruction();
			
			m_audio = SoundManagerScript.Play (gameObject, m_sound, true, false, 1f, 0f);
			m_audio.Stop();
		}
		
		if (GameStateScript.Instance.State != GameStateScript.GameState.InGameLive)
		{
			if (m_audio.isPlaying)
				m_audio.Stop();
			
			return;
		}
		
		float distance = (transform.position - PlayerHomePlanetScript.Instance.transform.position).magnitude;
		DistanceFactor = distance / MaximumReach;
		
		UpdateCharge();
		if (State != ShieldState.Disabled)
		{
			UpdatePosition ();
			UpdateElementPositions();
		}
	}
	void OnDestroy()
	{
		for (int i = 0; i < m_elements.Count; i++)
		{
			if (m_elements[i] != null && m_elements[i].gameObject != null)
				Destroy(m_elements[i].gameObject);
		}
		m_elements.Clear();
	}
	
	#endregion
		
	#region Public object methods
	
	[Obfuscar.Obfuscate]
	public void DoEnemyHit(bool fast = false)
	{
		State = ShieldState.Charging;
		m_untilLiveTimerSeconds = PlayerShieldGeneratorScript.Instance.ChargingSeconds;
		if (fast) 
			m_untilLiveTimerSeconds *= 0.5f;
		
		ExecuteSetEnabled(false);
	}
	[Obfuscar.Obfuscate]
	public void SetEnabled(bool flag)
	{
		State = (flag) ? ShieldState.Charging : ShieldState.Disabled;
		m_untilLiveTimerSeconds = PlayerShieldGeneratorScript.Instance.ChargingSeconds;
		ExecuteSetEnabled(flag);
	}
	
	[Obfuscar.Obfuscate]
	public void SetTargetPosition(Vector3 target)
	{
		//Debug.Log (string.Format ("PlayerShieldScript.SetTargetPosition : {0}", target));
		
		// don't allow this to go on top of home planet
		Vector3 playerPos = PlayerHomePlanetScript.Instance.transform.position;
		float radius = PlayerHomePlanetScript.Instance.GetCollideRadius();
		Vector3 newTarget = new Vector3(target.x, target.y, playerPos.z);
		
		Vector3 newDirection = newTarget - playerPos;
		float distance = newDirection.magnitude;
		newDirection.Normalize();
		
		if (distance < radius)
		{
			newTarget = radius * newDirection;
			distance = radius;
		}

		m_targetPosition = newTarget;
		
		m_targetAngleRad = Utility.VectorToAngleRad(newDirection);
		m_targetDistanceFromPlanet = distance;
	}

	#endregion
		
	#region Private object methods
	[Obfuscar.Obfuscate]
	private void ExecuteSetEnabled(bool flag)
	{
		for (int i = 0; i < m_elements.Count; i++)
		{
			if (m_elements[i] != null && m_elements[i].GetComponent<Collider>())
				m_elements[i].GetComponent<Collider>().enabled = flag;
		}
	}
	
	[Obfuscar.Obfuscate]
	private void AddElement(Vector3 position)
	{
		PlayerShieldElementScript newElement = (PlayerShieldElementScript) Instantiate(
			ElementPrefab
			, position
			, Quaternion.identity);
		newElement.SetParent(this);

		m_elements.Add(newElement);
	}
	
	[Obfuscar.Obfuscate]
	private void UpdateElementPositions()
	{
		if (State == ShieldState.Disabled)
			return;
		if (PlayerHomePlanetScript.Instance == null)
			return;

		Vector3 playerPos = PlayerHomePlanetScript.Instance.transform.position;
		
		// according to damage factor, we also decide here the sector where the shield lies
		float wholeAngleDegrees = AngleDegreesAtMinimum + 
			DistanceFactor * (AngleDegreesAtMaximum - AngleDegreesAtMinimum);
		float angleDeltaRadians = Mathf.Deg2Rad * wholeAngleDegrees / m_elements.Count;
		
		// element positions
		float startRadians = (m_elements.Count == 1) ? 
			m_currentAngleRad :
			m_currentAngleRad - 0.5f * Mathf.Deg2Rad * wholeAngleDegrees;
				

		for (int i = 0; i < m_elements.Count; i++)
		{
			m_elements[i].transform.position = 
				new Vector3(
					playerPos.x + m_currentDistanceFromPlanet * Mathf.Cos (startRadians)
					, playerPos.y + m_currentDistanceFromPlanet * Mathf.Sin (startRadians)
					, playerPos.z);
			
			startRadians += angleDeltaRadians;
		}
	}
	
	[Obfuscar.Obfuscate]
	private void ExecuteShieldConstruction()
	{
		// no need to here have actual positions, as UpdateElementPositions is called right after construction
		for (int i = 0; i < ElementCount; i++)
		{
			AddElement (transform.position);
		}
	}
	
	#endregion
}
