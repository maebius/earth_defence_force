//#define DEBUG_TEXTS

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

sealed public class ProjectileScript : MonoBehaviour 
{	
	#region Enums
	public enum OwnerType
	{
		None
		, PlayerPrimary
		, PlayerSecondary
		, Enemy
	};
	#endregion
	
	#region Public GUI tweakable members
	
	public bool DestroyWhenHit;
	
	public bool DestroyIfOutOfCamera = false;
	public bool DestroyIfDistance = true;
	
	public float[] PowerLevels;
	public float[] SpeedLevels;
	public float[] RangeLevels;
	
	public AudioClip m_audioHit;
	
	public bool DoDamageOverTime;

	public OwnerType Owner = OwnerType.None;
	#endregion
	
	#region Delegates And Events
	// A delegate type for hooking up change notifications.
	public delegate void ProjectileDepleted(ProjectileScript sender);
	// An event that clients can use to be notified whenever enemy dies
	public event ProjectileDepleted Depleted;

	#endregion

	
	#region Public members, not visible to Unity GUI
	[HideInInspector]
	public WeaponBaseScript Parent;
	
	[HideInInspector]
	public float Power;
	[HideInInspector]
	public float Speed;
	[HideInInspector]
	public float Range;
	
	[HideInInspector]
	public bool IsHitting
	{
		get { return m_isHitting; }
	}
	
	#endregion

	#region Private data members
	private bool m_isHitting = false;
	private Dictionary<DamageTakerScript, float> m_touchingObjects = new Dictionary<DamageTakerScript, float>();
	
	private bool m_notYetCountedAsHit;	
	#endregion
		
	#region Unity MonoBehaviour base overrides
	void Awake()
	{
		if (PowerLevels.Length > 0) Power = PowerLevels[0];
		if (SpeedLevels.Length > 0) Speed = SpeedLevels[0];
		if (RangeLevels.Length > 0) Range = RangeLevels[0];
		
		m_notYetCountedAsHit = true;
	}
	
	// Update is called once per frame
	void Update ()
	{
		
		bool destroyBecauseOfDistance = false;
		if (DestroyIfDistance && PlayerHomePlanetScript.Instance != null)
		{
			bool headingAway = MathHelpers.HeadingAwayFromTarget(transform.position
					, PlayerHomePlanetScript.Instance.transform.position
					, GetComponent<Rigidbody>().velocity);
			const float distanceLimitSquared = 600f*600f;
			float distanceSquared = 
				(transform.position - PlayerHomePlanetScript.Instance.transform.position).sqrMagnitude;
			destroyBecauseOfDistance = distanceSquared > distanceLimitSquared && headingAway;
		}			

		if (DestroyIfOutOfCamera && !GamePlayCamera.Instance.IsObjectInPlayField(GetComponent<Collider>())
			|| DestroyIfDistance && destroyBecauseOfDistance)
		{
			DepleteMyself();
		}
	}
	
	void OnCollisionEnter (Collision hit)	
	{
		//Debug.Log (string.Format ("ProjectileScript.OnCollisionEnter - hit by: {0} (time: {1})", hit.gameObject.name, Time.time));
		
		DamageTakerScript taker = Utility.GetDamageTaker(hit.gameObject);
		if (taker && DoWeProcess(taker.gameObject))
		{
			DoDamageTo(taker, transform.position);
			m_isHitting = true;
		}
	}
	void OnCollisionStay (Collision hit)	
	{
		//Debug.Log (string.Format ("ProjectileScript.OnCollisionStay - hit by: {0} (time: {1})", hit.gameObject.name, Time.time));
		
		DamageTakerScript taker = Utility.GetDamageTaker(hit.gameObject);
		if (taker && DoWeProcess(taker.gameObject))
		{
			m_isHitting = true;
		}
	}	
	
	void OnCollisionExit (Collision hit)	
	{
		//Debug.Log (string.Format ("ProjectileScript.OnCollisionExit - hit by: {0} (time: {1})", hit.gameObject.name, Time.time));
		
		DamageTakerScript taker = Utility.GetDamageTaker(hit.gameObject);
		if (taker && DoWeProcess(taker.gameObject))
		{
			m_isHitting = false;
		}
	}
	
	void OnTriggerEnter (Collider hit)	
	{
		//Debug.Log (string.Format ("ProjectileScript.OnTriggerEnter - hit by: {0} (time: {1})", hit.gameObject.name, Time.time));

		DamageTakerScript taker = Utility.GetDamageTaker(hit.gameObject);
		if (taker && DoWeProcess(taker.gameObject))
		{
			DoDamageTo(taker, transform.position);
			m_isHitting = true;
		}
	}
	
	void OnTriggerStay (Collider hit)	
	{
		//Debug.Log (string.Format ("ProjectileScript.OnTriggerStay - hit by: {0} (time: {1})", hit.gameObject.name, Time.time));
		
		DamageTakerScript taker = Utility.GetDamageTaker(hit.gameObject);
		if (taker && DoWeProcess(taker.gameObject))
		{
			if (DoDamageOverTime)
			{
				float now = GameStateScript.Instance.NNUKE_GetWorldTime();
				if (m_touchingObjects.ContainsKey(taker))
				{
					DamageTakerScript.DamageType type = (Owner == OwnerType.PlayerPrimary || Owner == OwnerType.PlayerSecondary) 
						?  DamageTakerScript.DamageType.PlayerBullet
						: ((Owner == OwnerType.Enemy) 
							? DamageTakerScript.DamageType.EnemyBullet : DamageTakerScript.DamageType.None);

					float deltaTimeSeconds = now - m_touchingObjects[taker];
					taker.DoGiveDamage(Power * deltaTimeSeconds
						, type
						, hit.transform.position);
				}
				m_touchingObjects[taker] = now;
			}
			m_isHitting = true;
		}
	}
	void OnTriggerExit (Collider hit)	
	{
		//Debug.Log (string.Format ("ProjectileScript.OnTriggerExit - hit by: {0} (time: {1})", hit.gameObject.name, Time.time));
		
		DamageTakerScript taker = Utility.GetDamageTaker(hit.gameObject);
		if (taker && DoWeProcess(taker.gameObject))
		{
			if (DoDamageOverTime)
			{
				m_touchingObjects.Remove(taker);
			}
			m_isHitting = false;
		}
	}	
	#endregion
	
		
	#region Public object methods
	[Obfuscar.Obfuscate]
	public DamageTakerScript GetClosestToucherTo(Vector3 pos)
	{
		DamageTakerScript closest = null;
		float closestDistanceSquared = float.MaxValue;
		foreach (KeyValuePair<DamageTakerScript, float> kv in m_touchingObjects)
		{
			if (kv.Key == null)
				continue;
			
			float dstSqr = (pos - kv.Key.transform.position).sqrMagnitude;
			if (dstSqr < closestDistanceSquared)
			{
				closestDistanceSquared = dstSqr;
				closest = kv.Key;
			}
		}
		return closest;
	}
	#endregion
		
	#region Private object methods
	[Obfuscar.Obfuscate]
	private bool DoWeProcess(GameObject obj)
	{
		PlayerHomePlanetScript player = obj.GetComponent<PlayerHomePlanetScript>();
		if (player != null && (Owner == OwnerType.PlayerPrimary || Owner == OwnerType.PlayerSecondary)) 
			return false;
		if (obj.transform.parent != null 
			&& obj.transform.parent.GetComponent<EnemyBaseScript>() != null
			&& Owner == OwnerType.Enemy)
			return false;
		
		return true;
	}
	
	[Obfuscar.Obfuscate]
	private void DoDamageTo(DamageTakerScript taker, Vector3 position)
	{
		if (m_audioHit)
		{
			SoundManagerScript.PlayOneTime (taker.gameObject, m_audioHit);
		}

		if (Owner == OwnerType.PlayerPrimary && m_notYetCountedAsHit)
		{
			m_notYetCountedAsHit = false;
			PlayerHomePlanetScript.Instance.BulletsHit++;
		}
			
		if (DoDamageOverTime)
		{
			m_touchingObjects[taker] = GameStateScript.Instance.NNUKE_GetWorldTime();
		}
		else
		{
			DamageTakerScript.DamageType type = (Owner == OwnerType.PlayerPrimary || Owner == OwnerType.PlayerSecondary) 
				?  DamageTakerScript.DamageType.PlayerBullet
				: ((Owner == OwnerType.Enemy) 
						? DamageTakerScript.DamageType.EnemyBullet : DamageTakerScript.DamageType.None);
			
			taker.DoGiveDamage(Power
				, type
				, position);
			
			if (DestroyWhenHit)
			{
				DepleteMyself();
			}
		}
	}
	
	public void DepleteMyself()
	{
		if (Depleted != null)
			Depleted(this);
		
		Destroy (gameObject);
	}
	
	#endregion
}
