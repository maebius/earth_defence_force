using UnityEngine;
using System.Collections;

sealed public class RotateEarthScript : MonoBehaviour 
{	
	public float RoundsPerSecond;
	public float X;
	public float Y;
	public float Z;
	public bool AroundLocal;
	
	private float m_speed;
	private Vector3 m_axis;
	
	void Start()
	{
		m_speed = 2f*Mathf.PI * RoundsPerSecond;
		m_axis = new Vector3(X, Y, Z);
		m_axis.Normalize();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (AroundLocal)
			transform.RotateAroundLocal(m_axis, m_speed * Time.deltaTime);
		else
			transform.RotateAround(m_axis, m_speed * Time.deltaTime);
	}
}
