//#define DEBUG_TEXTS

#if UNITY_ANDROID 
#define ENABLE_ADS
#endif

using UnityEngine;
using System.Collections;

sealed public class AdSceneManagerScript : MonoBehaviour 
{
	public string AppWhirlSDKKey;
	public string AdmobPublisherID;

	public bool AdsEnabled;
	public bool DisableIfAnythingBought;
	public string[] DisableIfSomeOfTheseBought;
	
	public BannerLocation Location;
	public float DelaySeconds;
	
	// Admob would also support TopLeft/Right, Center, and BottomLeft/Right,
	// but as Adwhirl does not, we just support top/bottom - center here
	public enum BannerLocation
	{
		Top
		, Bottom
	};
	
	private float m_timeToRequestSeconds;
	private bool m_requested;
	
	private bool m_disableBecauseIAP = false;
	
	private void CheckIfDisabled()
	{
		if (DisableIfAnythingBought)
		{
			m_disableBecauseIAP = 
				Application.platform == RuntimePlatform.IPhonePlayer
				|| Application.platform == RuntimePlatform.OSXPlayer
				||	(IAPManagerScript.Instance == null) 
				|| IAPManagerScript.Instance.NNUKE_HasBoughtSomething();
		}		
		
		if (m_disableBecauseIAP)
			return;
		
		foreach (string s in DisableIfSomeOfTheseBought)
		{
			m_disableBecauseIAP = IAPManagerScript.Instance.NNUKE_HasBought(s);
			if (m_disableBecauseIAP)
				break;
		}
	}
	
	void Awake()
	{
		#if DEBUG_TEXTS		
		Debug.Log( string.Format ("AdSceneManagerScript.{0} - OuyaInputWrapper.OuyaEnabled: {1}, Application.isEditor: {2}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, OuyaInputWrapper.OuyaEnabled, Application.isEditor));
		Debug.Log( string.Format ("AdSceneManagerScript.{0} - AdsEnabled: {1}, m_disableBecauseIAP: {2}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, AdsEnabled, m_disableBecauseIAP));
		Debug.Log( string.Format ("AdSceneManagerScript.{0} - AppWhirlSDKKey: {1}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, AppWhirlSDKKey));
		Debug.Log( string.Format ("AdSceneManagerScript.{0} - AdmobPublisherID: {1}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, AdmobPublisherID));
		#endif
		
		#if !ENABLE_ADS
		Destroy(gameObject);
		return;
		#endif
		
		if (OuyaInputWrapper.OuyaEnabled 
			|| Application.isEditor
			|| Application.platform == RuntimePlatform.IPhonePlayer
			|| Application.platform == RuntimePlatform.OSXPlayer
			)
		{
			Destroy(gameObject);
		}
	}

	#if ENABLE_ADS
	
	void Start()
	{
		CheckIfDisabled();
		
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("AdSceneManagerScript.Start() AdsEnabled: {0}, m_disableBecauseIAP: {1}"
			, AdsEnabled, m_disableBecauseIAP));
		#endif
		
		if (m_disableBecauseIAP || !AdsEnabled)
		{
			return;
		}
		
		m_requested = false;
	
		#if UNITY_IPHONE		
		AdWhirlBinding.init( AppWhirlSDKKey );
		#endif
		#if UNITY_ANDROID
		AdMobAndroid.init( AdmobPublisherID );
		//AdMobAndroid.setTestDevices(new string[] {"EBA588A317843147FAD41712AF2118A3"});
		#endif	
		
		m_timeToRequestSeconds = Time.time + DelaySeconds;
	}
	void Update()
	{
		CheckIfDisabled();
		
		if (!m_disableBecauseIAP && AdsEnabled && !m_requested && Time.time > m_timeToRequestSeconds)
		{
			RequestBanner();
			m_requested = true;
		}
		if ((!AdsEnabled || m_disableBecauseIAP) && m_requested)
		{
			DestroyBanner();
			m_requested = false;
		}
	}
	void OnDestroy()
	{
		DestroyBanner();
	}
	
	private void RequestBanner()
	{
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("AdSceneManagerScript.RequestBanner() location: {0}", Location));
		#endif
		
		#if UNITY_IPHONE
		AdWhirlBinding.createBanner( Location == BannerLocation.Top );
		#endif
		#if UNITY_ANDROID
		AdMobAndroid.createBanner( AdMobAndroidAd.smartBanner
			, (Location == BannerLocation.Top)
				? AdMobAdPlacement.TopCenter
				: AdMobAdPlacement.BottomCenter);
		#endif
	}
	
	private void DestroyBanner()
	{
		#if UNITY_IPHONE
		AdWhirlBinding.destroyBanner();
		#endif
		#if UNITY_ANDROID
		AdMobAndroid.destroyBanner();
		#endif
	}
	
	#endif // ENABLE_ADS
}
