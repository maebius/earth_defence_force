//#define DEBUG_TEXTS

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

sealed public class AdderScript : MonoBehaviour 
{
	public string[] m_assigned;
	public string[] m_ids;

	public int[] m_start;
	public int[] m_count;

	public EqType[] m_types;
	
	public enum EqType
	{
		Pri
		, Sec
		, Ite
		, Shi
	}
	
	private List<string> m_assigments = new List<string>();
	
	void Awake()
	{
		m_assigments.AddRange(m_assigned);	
	}
	
	private void DoCheck(string[] vals)
	{
		string key = vals[0];
		bool remove = false;
		bool.TryParse(vals[1], out remove);
		
		#if DEBUG_TEXTS		
		Debug.Log( string.Format ("AdderScript.{0} - key: {1}, m_assigments.Count: {2}, contains: {3}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, key, m_assigments.Count, m_assigments.Contains(key)));
		#endif
		
		if (!m_assigments.Contains(key))
			return;
		
		IAPManagerScript iap = 	IAPManagerScript.Instance;
		SaveData sd = SaveData.Instance;
		bool ok = (iap != null) && iap.NNUKE_HasBought(key);
		
		for (int index = 0; index < m_ids.Length; index++)
		{
			string id = m_ids[index];
			xsd_ct_Equipment eq = null;
			if (m_types[index] == EqType.Pri)
				eq = sd.NNUKE_GetWeaponPrimary(id);
			else if (m_types[index] == EqType.Sec)
				eq = sd.NNUKE_GetWeaponSecondary(id);
			else if (m_types[index] == EqType.Ite)
				eq = sd.NNUKE_GetItem(id);
			else if (m_types[index] == EqType.Shi)
				eq = sd.NNUKE_GetShield(id);
			
			if (ok && eq == null)
			{
				if (m_types[index] == EqType.Pri) 
					sd.NNUKE_AddWeaponPrimary(new xsd_ct_Equipment{ Id = id, Property1 = -1, Property2 = -1, Number = -1  });
				else if (m_types[index] == EqType.Sec) 
					sd.NNUKE_AddWeaponSecondary(new xsd_ct_Equipment{ Id = id, Property1 = -1, Property2 = -1, Number = -1   });
				else if (m_types[index] == EqType.Ite) 
					sd.NNUKE_AddItem(new xsd_ct_Equipment{ Id = id, Property1 = 0, Property2 = 0, Number = 0 });
				else if (m_types[index] == EqType.Shi) 
					sd.NNUKE_AddShield(new xsd_ct_Equipment{ Id = id, Property1 = -1, Property2 = -1, Number = -1   });
			}
			if (!ok && eq != null && remove)
			{
				if (m_types[index] == EqType.Pri) sd.NNUKE_DeleteWeaponPrimary(eq);
				else if (m_types[index] == EqType.Sec) sd.NNUKE_DeleteWeaponSecondary(eq);
				else if (m_types[index] == EqType.Ite) sd.NNUKE_DeleteItem(eq);
				else if (m_types[index] == EqType.Shi) sd.NNUKE_DeleteShield(eq);
			}
		}
		sd.NNUKE_Save();
	}
	
	private void DoOtherCheck(string[] vals)
	{
		string key = vals[0];
		bool remove = false;
		bool.TryParse(vals[1], out remove);

		#if DEBUG_TEXTS		
		Debug.Log( string.Format ("AdderScript.{0} - key: {1}, m_assigments.Count: {2}, contains: {3}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, key, m_assigments.Count, m_assigments.Contains(key)));
		#endif
		
		if (!m_assigments.Contains(key))
			return;
		
		IAPManagerScript iap = 	IAPManagerScript.Instance;
		SaveData sd = SaveData.Instance;
		bool ok = (iap != null) && iap.NNUKE_HasBought(key);
		
		for (int index = 0; index < m_ids.Length; index++)
		{
			string id = m_ids[index];
			xsd_ct_WorldInfo info = sd.NNUKE_GetWorld(id);
			
			if (ok)
			{
				sd.NNUKE_NNUKE_AddWorldData(id, m_start[index], m_count[index]);
			}
			if (!ok && info != null && remove)
			{
				sd.NNUKE_DeleteWorld(info, m_start[index], m_count[index]);
			}
		}
		sd.NNUKE_Save();
	}
	
	private void DoThirdCheck(string[] vals)
	{
		string key = vals[0];
		bool remove = false;
		bool.TryParse(vals[1], out remove);
		
		#if DEBUG_TEXTS		
		Debug.Log( string.Format ("AdderScript.{0} - key: {1}, m_assigments.Count: {2}, contains: {3}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, key, m_assigments.Count, m_assigments.Contains(key)));
		#endif
		
		if (!m_assigments.Contains(key))
			return;
		
		IAPManagerScript iap = 	IAPManagerScript.Instance;
		SaveData sd = SaveData.Instance;
		bool ok = (iap != null) && iap.NNUKE_HasBought(key);
		
		sd.Config.Credits = sd.Config.Credits + 5000;
		sd.NNUKE_Save();
	}
}
