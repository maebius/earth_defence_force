//#define DEBUG_TEXTS

using UnityEngine;
using System.Collections;

sealed public class FPSCounterScript : MonoBehaviour 
{
	// max samples for average fps counting
	private const int MAX_SAMPLES = 32;

	// current sample index
	private int m_tickIndex = 0;
	
	// 20.14 fixed point sum of all samples
	private int m_tickSum = 0;
	
	// all samples, stored
	private int[] m_tickList = new int[MAX_SAMPLES];

	// the current fps
	public float m_fps = 0.0f;
	
	// warning limits (fps)
	public float m_warningLimitFPS1 = 30.0f;
	public float m_warningLimitFPS2 = 20.0f;
	
	// warning limits (time below fps)
	public float m_warningLimitS1 = 3.0f;
	public float m_warningLimitS2 = 5.0f;
	
	// warning limit counters
	private float m_warningLimitCounterS1 = 0.0f;
	private float m_warningLimitCounterS2 = 0.0f;
	
	// have we shown the warning?
	public bool m_warningGiven1 = false;
	public bool m_warningGiven2 = false;

	// warning infobox prefab
	public Transform m_warningInfoBoxPrefab = null;
	
	// the warning infobox
	private Transform m_warningInfoBox = null;
	
	// warning infobox timer
	public float m_warningInfoBoxTimerS = 10.0f;
	
	// instance of this
	private static FPSCounterScript sm_instance = null;
	
	public static FPSCounterScript Instance
	{
		get { return sm_instance; }
	}
	
	
	void Awake() 
	{

		if (sm_instance != null)
			DestroyImmediate(gameObject);
		else 
		{
			DontDestroyOnLoad(gameObject);
			sm_instance = this;
		}
	}
	
	void Start() 
	{
		for (int i = 0; i < MAX_SAMPLES; i++)
			m_tickList[i] = 0;
	}
	
	void Update() 
	{
		float deltaTime = Time.deltaTime;
		
		m_fps = AddSample(deltaTime);
	
		// run the warning infobox
		if (m_warningInfoBox != null) 
		{
			m_warningInfoBoxTimerS -= deltaTime;
			if (m_warningInfoBoxTimerS <= 0.0f) 
			{
				Destroy(m_warningInfoBox.gameObject);
				m_warningInfoBox = null;
			}
		}
		
		// the primary warning counter
		if (m_fps < m_warningLimitFPS1) 
		{
			m_warningLimitCounterS1 += deltaTime;
			if (m_warningLimitCounterS1 > m_warningLimitS1) 
			{
				if (!m_warningGiven1) {
					// we've stayed under the warning limit fps for too long -> issue warning!
					
					#if DEBUG_TEXTS
					Debug.Log( string.Format ("FPSCounterScript.{0} (1) LOW FPS WARNING GIVEN! FPS BELOW {1}"
						, System.Reflection.MethodBase.GetCurrentMethod().ToString()
						, ((int)m_warningLimitFPS1)));
					#endif
						
					// show the infobox
					if (m_warningInfoBoxPrefab != null)
						m_warningInfoBox = (Transform)Instantiate(m_warningInfoBoxPrefab, new Vector3(0.0f, 0.0f, 0.0f), Quaternion.identity);
					
					m_warningGiven1 = true;
				}
			}
		}
		else
			m_warningLimitCounterS1 = 0.0f;
		
		// the secondary warning counter
		if (m_fps < m_warningLimitFPS2) 
		{
			m_warningLimitCounterS2 += deltaTime;
			if (m_warningLimitCounterS2 > m_warningLimitS2) 
			{
				if (!m_warningGiven2) {
					
					#if DEBUG_TEXTS
					Debug.Log( string.Format ("FPSCounterScript.{0} (2) LOW FPS WARNING GIVEN! FPS BELOW {1}"
						, System.Reflection.MethodBase.GetCurrentMethod().ToString()
						, ((int)m_warningLimitFPS2)));
					#endif

					// we've stayed under the warning limit fps for too long -> issue warning!
					m_warningGiven2 = true;
				}
			}
		}
		else
			m_warningLimitCounterS2 = 0.0f;
	}
	
	[Obfuscar.Obfuscate]
	public static bool HasWarningBeenGiven(int warningID) 
	{
		if (warningID == 0)
			return sm_instance.m_warningGiven1;
		else
			return sm_instance.m_warningGiven2;
	}
	
	[Obfuscar.Obfuscate]
	float AddSample(float deltaTime) 
	{
		int newTick = (int)(deltaTime * 8192.0f);
		
		m_tickSum -= m_tickList[m_tickIndex];
		m_tickSum += newTick;
		m_tickList[m_tickIndex++] = newTick;
		if (m_tickIndex >= MAX_SAMPLES)
			m_tickIndex = 0;
		
		float average = (m_tickSum / 8192.0f) / MAX_SAMPLES;
		if (average < 0.0001f)
			average = 0.0001f;
		
		return 1.0f / average;
	}
}
