//#define DEBUG_TEXTS

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

	
sealed public class IAPHandler
{
	public string id;
	public GameObject handler;
	public string handlerFunction;
	public bool isConsumable;
	
	public int activationCount;
}

sealed public class IAPBindingsScript : MonoBehaviour 
{
	public string[] m_commonlyNamedProductIds;
	public string[] m_iosProductIds;
	public string[] m_ouyaProductIds;
	public string[] m_ouyaProductDescriptions;

	public string[] m_spriteNames;

	public GameObject[] m_productHandlers;
	public string[] m_productHandlerFunctions;
	public bool[] m_productIsConsumable;

	private Dictionary<string, IAPHandler> m_iapHandlers = new Dictionary<string, IAPHandler>();
	
	public string[] ProductIds
	{
		get 
		{
			if (OuyaInputWrapper.OuyaEnabled)
			{
				return m_ouyaProductIds;
			}
			else if (Application.platform == RuntimePlatform.IPhonePlayer
				|| Application.platform == RuntimePlatform.OSXPlayer)
			{
				return m_iosProductIds;
			}
			
			return m_commonlyNamedProductIds;
		}
	}

#region Unity MonoBehaviour base overrides
	
	// NOTE! This class needs to be instantiated before other IAP classes, as they rely on
	// getting the product ids as early as possible.
	
	void Awake()
	{
		#if DEBUG_TEXTS
		Debug.Log( "IAPBindingsScript.Awake" );
		#endif
		
		int i = 0;
		
		foreach (string id in ProductIds)
		{
			IAPHandler handler = new IAPHandler();
			handler.id = id;
			handler.handler = (i < m_productHandlers.Length) ? m_productHandlers[i] : null;
			handler.handlerFunction = (i < m_productHandlerFunctions.Length) ? m_productHandlerFunctions[i] : null;
			handler.isConsumable = (i < m_productIsConsumable.Length) ? m_productIsConsumable[i] : false;
			handler.activationCount = 0;
			
			m_iapHandlers.Add (id, handler);
			
			i++;
		}
		
	}
	
#endregion
	
#region Public object methods
	
	[Obfuscar.Obfuscate]
	public IAPHandler NNUKE_GetIAPHandler(string id)
	{
		if (m_iapHandlers.ContainsKey(id))
		{
			return m_iapHandlers[id];
		}
		return null;
	}
	
	[Obfuscar.Obfuscate]
	public bool NNUKE_IsConsumable(string id)
	{
		if (m_iapHandlers.ContainsKey(id))
			return m_iapHandlers[id].isConsumable;
		return false;
	}
	
	[Obfuscar.Obfuscate]
	public void NNUKE_DoActivate(string id, bool skipConsumables)
	{
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("IAPBindingsScript.NNUKE_DoActivate - id: {0}, has handler: {1}, skipConsumables: {2}"
			, id, m_iapHandlers.ContainsKey(id), skipConsumables ));
		#endif
		
		if (m_iapHandlers.ContainsKey(id))
		{
			IAPHandler handler = m_iapHandlers[id];
			
			if (handler.isConsumable && skipConsumables)
			{
				#if DEBUG_TEXTS
				Debug.Log( string.Format (
					"IAPBindingsScript.NNUKE_DoActivate - id: {0}, was consumable, and set as skip those, exiting ..."
					, id));
				#endif
				
				return;
			}
			
			handler.activationCount++;
			
			if (handler.handler == null || string.IsNullOrEmpty(handler.handlerFunction))
			{
				#if DEBUG_TEXTS
				Debug.LogWarning( string.Format ("IAPBindingsScript.NNUKE_DoActivate - no handler for id: {0}!", id ));
				#endif
				return;
			}
			
			handler.handler.SendMessage(handler.handlerFunction
				, new string[] { id, "False" }
				, SendMessageOptions.DontRequireReceiver);
		}
	}
	[Obfuscar.Obfuscate]
	public void NNUKE_DoValidateAll(bool removeFlag)
	{
		foreach (IAPHandler h in m_iapHandlers.Values)
		{
			if (h.handler == null || string.IsNullOrEmpty(h.handlerFunction))
			{
				#if DEBUG_TEXTS
				Debug.LogWarning( string.Format ("IAPBindingsScript.NNUKE_DoValidateAll - no handler for id: {0}!", h.id ));
				#endif
				continue;
			}
			if (h.isConsumable)
			{
				#if DEBUG_TEXTS
				Debug.Log( string.Format (
					"IAPBindingsScript.NNUKE_DoValidateAll - id: {0}, was consumable, skipping ..."
					, h.id));
				#endif
				
				continue;
			}
			
			h.handler.SendMessage(h.handlerFunction
				, new string[] { h.id, removeFlag.ToString() }
				, SendMessageOptions.DontRequireReceiver);
		}
	}
#endregion
	
}
