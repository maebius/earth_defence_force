//#define DEBUG_TEXTS

//#define USE_OUYA

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

sealed public class IAPEventListenerScript : MonoBehaviour 
#if USE_OUYA
	, OuyaSDK.IGetProductsListener
	, OuyaSDK.IPurchaseListener
	, OuyaSDK.IGetReceiptsListener 
#endif
{
	#region Public variables showing in Unity Editor.
	public IAPManagerScript m_iapManager;
	public IAPBindingsScript m_iapBindings;
	#endregion
	
	#region Unity Mono behavior overrides.
	void Awake()
	{
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("IAPEventListenerScript.{0} (time: {1})"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, Time.time));
		#endif
	
		if (OuyaInputWrapper.OuyaEnabled)
		{
#if USE_OUYA
			OuyaSDK.registerGetProductsListener(this);
			OuyaSDK.registerPurchaseListener(this);
			OuyaSDK.registerGetReceiptsListener(this);
#endif
		}
	}

	void OnDestroy()
	{
		if (OuyaInputWrapper.OuyaEnabled)
		{
#if USE_OUYA
			OuyaSDK.unregisterGetProductsListener(this);
			OuyaSDK.unregisterPurchaseListener(this);
			OuyaSDK.unregisterGetReceiptsListener(this);
#endif
		}
	}
	
	void OnEnable()
	{
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("IAPEventListenerScript.{0} (time: {1})"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, Time.time));
		#endif
		
		#if UNITY_ANDROID || UNITY_IPHONE
		BindListeners();
		#else
		Debug.Log( string.Format ("IAPEventListenerScript.OnEnable - {0} does not support IAP!"
			, Application.platform.ToString()));
		#endif
	}
	
	void OnDisable()
	{
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("IAPEventListenerScript.{0} (time: {1})"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, Time.time));
		#endif
		
		#if UNITY_ANDROID || UNITY_IPHONE
		UnBindListeners();
		#else
		Debug.Log( string.Format ("IAPEventListenerScript.OnDisable - {0} does not support IAP!"
			, Application.platform.ToString()));
		#endif
	}	
	
	#endregion
	
	#region Ouya spesific event listeners
	
#if USE_OUYA
	public void OuyaGetProductsOnSuccess(List<OuyaSDK.Product> products)
	{
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("IAPEventListenerScript.{0} (time: {1}) - products.Count: {2}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, Time.time
			, products.Count
			));
		#endif
		
		m_iapManager.CanMakePayments = true;
		
		foreach (OuyaSDK.Product product in products)
		{
			IAPData item = new IAPData();
		
			item.description = "";
			item.title = product.getName();
			item.productId = product.getIdentifier();
			item.price = string.Format ("{0:0.00}", product.getPriceInCents() * 0.01f);
			
			// TODO: NOTE! These are hardcoded as Ouya does not provide as anything ...
			item.formattedPrice = string.Format ("{0} USD", item.price);
			item.currencyCode = "USD";
			item.currencySymbol = "$";
			
			int index = 0;
			for ( ; index < m_iapBindings.ProductIds.Length; index++)
			{
				if (m_iapBindings.ProductIds[index] == item.productId)
				{
					break;
				}
			}
			if (index < m_iapBindings.ProductIds.Length
				&& index < m_iapBindings.m_ouyaProductDescriptions.Length)
			{
				item.descriptionLocalizationId = m_iapBindings.m_ouyaProductDescriptions[index];
			}

			m_iapManager.NNUKE_AddProduct (item);
		}
		// fire away a request for all purchased items right away ... this is what we are after anyway
		OuyaSDK.requestReceiptList();
		
		// NOTE! We do not set the m_iapManager.State here to OK_GetItems yet, only after the receipts have been
		// (or not) got
	}

	public void OuyaGetProductsOnFailure(int errorCode, string errorMessage)
	{
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("IAPEventListenerScript.{0} (time: {1}) - error={2} errorMessage={3}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, Time.time
			, errorCode, errorMessage));
		#endif
		
		#if ENABLE_LUMOS
		Lumos.Event("IAP Listener - Get Products FAILED - Ouya");
		#endif
		
		m_iapManager.State = IAPManagerScript.RequestState.FAIL_GetItems;
	}
	
	public void OuyaGetProductsOnCancel()
	{
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("IAPEventListenerScript.{0} (time: {1})"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, Time.time);
		#endif
			
		m_iapManager.State = IAPManagerScript.RequestState.FAIL_GetItems;
	}
	
	public void OuyaPurchaseOnSuccess(OuyaSDK.Product product)
	{
		#if DEBUG_TEXTS
		Debug.Log(string.Format("IAPEventListenerScript.OuyaPurchaseOnSuccess: id={0}, name={1} price={2}"
			, product.getIdentifier()
			, product.getName()
			, product.getPriceInCents()));
		#endif
		
		#if ENABLE_LUMOS
		Lumos.Event("IAP Listener - Purchase succesfull - Ouya");
		#endif
		
		m_iapManager.NNUKE_PurchaseSuccesfull(
				product.getIdentifier()
				, 1
				, string.Empty);
		
	}

	public void OuyaPurchaseOnFailure(int errorCode, string errorMessage)
	{
		#if DEBUG_TEXTS
		Debug.Log(string.Format("IAPEventListenerScript.OuyaPurchaseOnFailure: error={0} errorMessage={1}", errorCode, errorMessage));
		#endif
		
		#if ENABLE_LUMOS
		Lumos.Event("IAP Listener - Purchase Failed - Ouya");
		#endif
		
		m_iapManager.State = IAPManagerScript.RequestState.FAIL_Purchase;
	}
	public void OuyaPurchaseOnCancel()
	{
		#if DEBUG_TEXTS
		Debug.Log(string.Format("IAPEventListenerScript.OuyaPurchaseOnCancel"));
		#endif
		m_iapManager.State = IAPManagerScript.RequestState.CANCEL_Purchase;
	}
		
		
	public void OuyaGetReceiptsOnSuccess(List<OuyaSDK.Receipt> receipts)
	{
		#if DEBUG_TEXTS
		Debug.Log(string.Format("IAPEventListenerScript.OuyaGetReceiptsOnSuccess: receipts.Count={0}", receipts.Count));
		#endif
		
		foreach (OuyaSDK.Receipt receipt in receipts)
		{
			m_iapManager.NNUKE_PurchaseSuccesfull(
				receipt.getIdentifier()
				, 1
				, string.Empty
				, true
				);
		}
		m_iapManager.m_bindings.NNUKE_DoValidateAll(true);
		m_iapManager.State = IAPManagerScript.RequestState.OK_GetItems;
	}

	public void OuyaGetReceiptsOnFailure(int errorCode, string errorMessage)
	{
		#if DEBUG_TEXTS
		Debug.Log(string.Format("IAPEventListenerScript.OuyaGetReceiptsOnFailure: error={0} errorMessage={1}", errorCode, errorMessage));
		#endif
		
		m_iapManager.State = IAPManagerScript.RequestState.FAIL_GetItems;
	}
	
	public void OuyaGetReceiptsOnCancel()
	{
		#if DEBUG_TEXTS
		Debug.Log(string.Format("IAPEventListenerScript.OuyaGetReceiptsOnCancel"));
		#endif
		
		m_iapManager.State = IAPManagerScript.RequestState.FAIL_GetItems;
	}
#endif
		
	#endregion
	
#region Private object methods
	
	#if UNITY_ANDROID
	private void BindListeners()
	{
		if (OuyaInputWrapper.OuyaEnabled || Application.isEditor)
			return;
		
		GoogleIABManager.queryInventoryFailedEvent += productListRequestFailed;
		GoogleIABManager.purchaseFailedEvent += purchaseFailed;

		GoogleIABManager.billingSupportedEvent += billingSupportedEvent;
		GoogleIABManager.billingNotSupportedEvent += billingNotSupportedEvent;
		GoogleIABManager.queryInventorySucceededEvent += queryInventorySucceededEvent;
		GoogleIABManager.purchaseCompleteAwaitingVerificationEvent += purchaseCompleteAwaitingVerificationEvent;
		GoogleIABManager.purchaseSucceededEvent += purchaseSucceededEvent;
		GoogleIABManager.consumePurchaseSucceededEvent += consumePurchaseSucceededEvent;
		GoogleIABManager.consumePurchaseFailedEvent += consumePurchaseFailedEvent;
	}
	private void UnBindListeners()
	{
		if (OuyaInputWrapper.OuyaEnabled || Application.isEditor)
			return;
		
		// Remove all event handlers
		GoogleIABManager.queryInventoryFailedEvent -= productListRequestFailed;
		GoogleIABManager.purchaseFailedEvent -= purchaseFailed;

		GoogleIABManager.billingSupportedEvent -= billingSupportedEvent;
		GoogleIABManager.billingNotSupportedEvent -= billingNotSupportedEvent;
		GoogleIABManager.queryInventorySucceededEvent -= queryInventorySucceededEvent;
		GoogleIABManager.purchaseCompleteAwaitingVerificationEvent += purchaseCompleteAwaitingVerificationEvent;
		GoogleIABManager.purchaseSucceededEvent -= purchaseSucceededEvent;
		GoogleIABManager.consumePurchaseSucceededEvent -= consumePurchaseSucceededEvent;
		GoogleIABManager.consumePurchaseFailedEvent -= consumePurchaseFailedEvent;
	}
	#elif UNITY_IPHONE
	private void BindListeners()
	{
		// Listens to all the StoreKit events.  All event listeners MUST be removed before this object is disposed!
		StoreKitManager.productListRequestFailedEvent += productListRequestFailed;
		StoreKitManager.purchaseFailedEvent += purchaseFailed;

		StoreKitManager.productPurchaseAwaitingConfirmationEvent += productPurchaseAwaitingConfirmationEvent;
		StoreKitManager.purchaseSuccessfulEvent += purchaseSuccessful;
		StoreKitManager.purchaseCancelledEvent += purchaseCancelled;
		StoreKitManager.receiptValidationFailedEvent += receiptValidationFailed;
		StoreKitManager.receiptValidationRawResponseReceivedEvent += receiptValidationRawResponseReceived;
		StoreKitManager.receiptValidationSuccessfulEvent += receiptValidationSuccessful;
		StoreKitManager.productListReceivedEvent += productListReceived;
		StoreKitManager.restoreTransactionsFailedEvent += restoreTransactionsFailed;
		StoreKitManager.restoreTransactionsFinishedEvent += restoreTransactionsFinished;
		StoreKitManager.paymentQueueUpdatedDownloadsEvent += paymentQueueUpdatedDownloadsEvent;
	}
	
	private void UnBindListeners()
	{
		// Remove all the event handlers
		StoreKitManager.productListRequestFailedEvent -= productListRequestFailed;
		StoreKitManager.purchaseFailedEvent -= purchaseFailed;

		StoreKitManager.productPurchaseAwaitingConfirmationEvent -= productPurchaseAwaitingConfirmationEvent;
		StoreKitManager.purchaseSuccessfulEvent -= purchaseSuccessful;
		StoreKitManager.purchaseCancelledEvent -= purchaseCancelled;
		StoreKitManager.receiptValidationFailedEvent -= receiptValidationFailed;
		StoreKitManager.receiptValidationRawResponseReceivedEvent -= receiptValidationRawResponseReceived;
		StoreKitManager.receiptValidationSuccessfulEvent -= receiptValidationSuccessful;
		StoreKitManager.productListReceivedEvent -= productListReceived;
		StoreKitManager.restoreTransactionsFailedEvent -= restoreTransactionsFailed;
		StoreKitManager.restoreTransactionsFinishedEvent -= restoreTransactionsFinished;
		StoreKitManager.paymentQueueUpdatedDownloadsEvent -= paymentQueueUpdatedDownloadsEvent;
	}
	#endif // UNITY_IPHONE
	
#endregion
	
#region Event listeners
	
	[Obfuscar.Obfuscate]
	private void productListRequestFailed( string error )
	{
		#if DEBUG_TEXTS
		Debug.Log( "productListRequestFailed: " + error );
		#endif
		
		#if ENABLE_LUMOS
		Lumos.Event("IAP Listener - Get Products FAILED - iOS");
		#endif
		
		m_iapManager.State = IAPManagerScript.RequestState.FAIL_GetItems;
	}
	
	[Obfuscar.Obfuscate]
	private void purchaseFailed( string error )
	{
		#if DEBUG_TEXTS
		Debug.Log( "purchaseFailed: " + error );
		#endif
		
		#if ENABLE_LUMOS
		Lumos.Event("IAP Listener - Purchase failed");
		#endif
		
		m_iapManager.State = IAPManagerScript.RequestState.FAIL_Purchase;
	}
	
	#if UNITY_ANDROID
	[Obfuscar.Obfuscate]
	private void billingSupportedEvent()
	{
		#if DEBUG_TEXTS
		Debug.Log( "billingSupportedEven" );
		#endif
		
		m_iapManager.CanMakePayments = true;
		m_iapManager.NNUKE_GetProductData();
	}

	[Obfuscar.Obfuscate]
	private void billingNotSupportedEvent( string error )
	{
		#if DEBUG_TEXTS
		Debug.Log( "billingNotSupportedEvent error: " + error );
		#endif
		m_iapManager.CanMakePayments = false;
	}

	[Obfuscar.Obfuscate]
	private void queryInventorySucceededEvent( List<GooglePurchase> purchases, List<GoogleSkuInfo> skus )
	{
		#if DEBUG_TEXTS
		Debug.Log( "queryInventorySucceededEvent - skuCount: " + skus.Count );
		#endif
		
		#if ENABLE_LUMOS
		Lumos.Event("IAP Listener - Get Products FAILED - Google");
		#endif
		
		foreach (GoogleSkuInfo sku in skus)
		{
			#if DEBUG_TEXTS
			Debug.Log( string.Format ("sku : {0}", sku));
			#endif			
			IAPData item = new IAPData();
		
			item.description = sku.description;
			item.title = sku.title;
			item.productId = sku.productId;
			item.price = sku.price;
			item.formattedPrice = sku.price;
			
			item.type = sku.type;
		
			m_iapManager.NNUKE_AddProduct (item);
		}
		
		m_iapManager.NNUKE_UpdateTransactions (purchases);
		
		m_iapManager.State = IAPManagerScript.RequestState.OK_GetItems;
	}

	[Obfuscar.Obfuscate]
	private void purchaseCompleteAwaitingVerificationEvent( string purchaseData, string signature )
	{
		#if DEBUG_TEXTS
		Debug.Log( "purchaseCompleteAwaitingVerificationEvent - purchaseData: " + purchaseData );
		#endif
	}

	[Obfuscar.Obfuscate]
	private void purchaseSucceededEvent( GooglePurchase purchase )
	{
		#if DEBUG_TEXTS
		Debug.Log( "purchaseSucceededEvent - purchase: " + purchase );
		#endif
		
		#if ENABLE_LUMOS
		Lumos.Event("IAP Listener - Purchase succesfull - Google");
		#endif
		
		m_iapManager.NNUKE_PurchaseSuccesfull(
				purchase.productId
				, 1
				, purchase.orderId);
		
		//packageName
		//orderId
		//productId
		//developerPayload
		//purchaseTime
		//purchaseState
		//purchaseToken
		
		// with IAB, we need to handle the "consumable" nature of the purchase ourselves
		if (m_iapBindings.NNUKE_IsConsumable(purchase.productId))
		{
			GoogleIAB.consumeProduct(purchase.productId);
		}
	}

	[Obfuscar.Obfuscate]
	private void consumePurchaseSucceededEvent( GooglePurchase purchase )
	{
		#if DEBUG_TEXTS
		Debug.Log( "consumePurchaseSucceededEvent - purchase: " + purchase );
		#endif
	}

	[Obfuscar.Obfuscate]
	private void consumePurchaseFailedEvent( string error )
	{
		#if DEBUG_TEXTS
		Debug.Log( "consumePurchaseFailedEvent - error: " + error );
		#endif
	}
	#endif // UNITY_ANDROID

		
	#if UNITY_IPHONE
	[Obfuscar.Obfuscate]
	private void productListReceived( List<StoreKitProduct> productList )
	{
		#if DEBUG_TEXTS
		Debug.Log( "productListReceived - count: " + productList.Count );
		#endif
		
		foreach (StoreKitProduct product in productList)
		{
			IAPData item = new IAPData();
			
			item.description = product.description;
			item.currencySymbol = product.currencySymbol;
			item.currencyCode = product.currencyCode;
			item.formattedPrice = product.formattedPrice;
			item.price = product.price;
			item.productId = product.productIdentifier;
			item.title = product.title;
				
			m_iapManager.NNUKE_AddProduct(item);
		}

		m_iapManager.State = IAPManagerScript.RequestState.OK_GetItems;
	}
	
	[Obfuscar.Obfuscate]
	private void productPurchaseAwaitingConfirmationEvent( StoreKitTransaction transaction )
	{
		#if DEBUG_TEXTS
		Debug.Log( "productPurchaseAwaitingConfirmationEvent - transaction: " + transaction );
		#endif
	}
	
	[Obfuscar.Obfuscate]
	private void purchaseSuccessful( StoreKitTransaction transaction )
	{
		#if DEBUG_TEXTS
		Debug.Log( "purchaseSuccessful - transaction: " + transaction );
		#endif
		
		#if ENABLE_LUMOS
		Lumos.Event("IAP Listener - Purchase succesfull - iOS");
		#endif
		
		m_iapManager.NNUKE_PurchaseSuccesfull(transaction.productIdentifier
			, transaction.quantity
			, transaction.base64EncodedTransactionReceipt);
	}
	[Obfuscar.Obfuscate]
	private void paymentQueueUpdatedDownloadsEvent( List<StoreKitDownload> downloads )
	{
		#if DEBUG_TEXTS
		Debug.Log( "paymentQueueUpdatedDownloadsEvent");
		foreach( var dl in downloads )
			Debug.Log( dl );
		#endif	
	}	
	
	[Obfuscar.Obfuscate]
	private void receiptValidationSuccessful()
	{
		#if DEBUG_TEXTS
		Debug.Log( "receiptValidationSuccessful");
		#endif	
	}
	
	[Obfuscar.Obfuscate]
	private void receiptValidationFailed( string error )
	{
		#if DEBUG_TEXTS
		Debug.Log( "receiptValidationFailed - error: " + error );
		#endif
	}
	
	[Obfuscar.Obfuscate]
	private void receiptValidationRawResponseReceived( string response )
	{
		#if DEBUG_TEXTS
		Debug.Log( "receiptValidationRawResponseReceived - response: " + response );
		#endif
	}

	[Obfuscar.Obfuscate]
	private void purchaseCancelled( string error )
	{
		#if DEBUG_TEXTS
		Debug.Log( "purchaseCancelled - error: " + error );
		#endif
		
		#if ENABLE_LUMOS
		Lumos.Event("IAP Listener - Purchase cancelled");
		#endif
		
		m_iapManager.State = IAPManagerScript.RequestState.CANCEL_Purchase;
	}
	
	[Obfuscar.Obfuscate]
	private void restoreTransactionsFailed( string error )
	{
		#if DEBUG_TEXTS
		Debug.Log( "restoreTransactionsFailed - error: " + error );
		#endif
		m_iapManager.State = IAPManagerScript.RequestState.FAIL_Restore;
	}
	
	[Obfuscar.Obfuscate]
	private void restoreTransactionsFinished()
	{
		#if DEBUG_TEXTS
		Debug.Log( "restoreTransactionsFinished" );
		#endif		
		m_iapManager.NNUKE_UpdateSavedTransactions();
		
		m_iapManager.State = IAPManagerScript.RequestState.OK_Restore;
	}
	#endif // UNITY_IPHONE
	
#endregion
	
}