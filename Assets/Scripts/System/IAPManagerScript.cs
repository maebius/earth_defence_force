//#define DEBUG_TEXTS

//#define DEBUG_TEST_IAP

#define USE_OUYA

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Our own wrapper for the IAP data, so that we can have this crossplatform.
/// </summary>
sealed public class IAPData
{
	// both in apple and google iap
	public string description;
	public string descriptionLocalizationId;
	public string price;
	public string productId;
	public string title;

	// only in apple iap
	public string currencySymbol;
	public string currencyCode;
	public string formattedPrice;
	
	// only in google iap
	public string type;
	
	// custom, need to be provided by application
	public bool consumable;
	public string spriteName;
	
	public override string ToString()
	{
		return string.Format(
			"<IAPData>\nID: {0}\nTitle: {1}\nDescription: {2}\nPrice: {3}\nCurrency Symbol: {4}\nFormatted Price: {5}\nCurrency Code: {6}\nConsumable: {7}"
			, productId
			, title
			, description
			, price
			, currencySymbol
			, formattedPrice
			, currencyCode
			, consumable
			);
	}
}

sealed public class IAPManagerScript : MonoBehaviour 
{
	#region Enums, structs, and inner classes
	public enum RequestState
	{
		None
		, Processing
		, OK_Purchase
		, OK_Restore
		, OK_GetItems
		, FAIL_Purchase
		, FAIL_Restore
		, FAIL_GetItems
		, CANCEL_Purchase
	};
	#endregion
	
	#region Delegates And Events
	public delegate void StateChanged(RequestState state);
	public event StateChanged Changed;
#endregion
	
	
	#region Public members, not visible to Unity GUI
	public IAPBindingsScript m_bindings;
	public string m_googleLicenseKey;
#endregion
	
#region Public members, not visible to Unity GUI
	[HideInInspector]
	public static bool IsCreated
	{
		get
		{
			return sm_instance != null;
		}
	}
	[HideInInspector]
	public static IAPManagerScript Instance 
	{
		get 
		{
			if (sm_instance == null)
			{
				// as we cannot instantiate MonoBehaviors directy with "new", we need to create
				// gameobject and add a component to that ...
				sm_instance = Object.FindObjectOfType(typeof(IAPManagerScript)) as IAPManagerScript;
				
				if (sm_instance == null)
				{
					#if DEBUG_TEXTS
					Debug.LogError( 
						string.Format (
						"IAPManagerScript.Instance.get - m_instance is null! We need to have this somewhere"));
					#endif
				}
			}
			return sm_instance;
		}
	}
	[HideInInspector]
	public bool CanMakePayments
	{
		set { m_canMakePayments = value; }
		get 
		{ 
			#if UNITY_IPHONE
			return StoreKitBinding.canMakePayments();
			#endif
			
			return m_canMakePayments; 
		}
	}

	[HideInInspector]
	public RequestState State
	{
		set 
		{
			if (value == RequestState.OK_GetItems)
				m_connectionOk = true;
			else if (value == RequestState.FAIL_GetItems)
				m_connectionOk = false;
			
			float now = Time.time;
			if (m_lastStateChangeTime + 1f > now &&
				(m_state == RequestState.OK_Restore || m_state == RequestState.FAIL_Restore))
				return;
			
			m_lastStateChangeTime = now;
			
			m_state = value;
			if (Changed != null)
			{
				Changed(m_state);
			}
		}
	}
	public bool ConnectionOk
	{
		get 
		{
			return m_connectionOk;
		}
	}
#endregion
	
#region Private data members
	private static IAPManagerScript sm_instance;
	
	private List<IAPData> m_iapItems = new List<IAPData>();
	private List<string> m_completedTransactionIds = new List<string>();
	
	private bool m_canMakePayments;
	private bool m_connectionOk = false;
	#if UNITY_ANDROID
	private List<GooglePurchase> m_googlePurchases;
	#endif
	
	private RequestState m_state = RequestState.None;
	private float m_lastStateChangeTime = -1f;
#endregion
	
#region Unity MonoBehaviour base overrides

	void Awake () 
	{ 
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("IAPManagerScript.Awake - m_instance is null: {0}", sm_instance == null));
		#endif
		
		if (sm_instance == null) 
		{ 
			sm_instance = this;
			DontDestroyOnLoad(gameObject);
		} 
		else 
			Destroy(gameObject); 
	}
	void Start()
	{
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("IAPManagerScript.Start - m_instance is null: {0}", sm_instance == null));
		#endif
		
		sm_instance.NNUKE_Initialize();
	}
	
	void OnEnable () 
	{ 
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("IAPManagerScript.OnEnable - m_instance is null: {0}", sm_instance == null));
		#endif	
		
		if (sm_instance == null) 
		{
			sm_instance = this; 
		}
	}
	
	void OnDestroy () 
	{ 
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("IAPManagerScript.OnDestroy - m_instance is null: {0}", sm_instance == null));
		#endif
		
		if (sm_instance == this)
		{
			NNUKE_UnInitialize();
			sm_instance = null;
		}
	}
	
	
#endregion
	
#region Public object methods	
	[Obfuscar.Obfuscate]
	public IAPData NNUKE_GetIAPItem(int index)
	{
		if (index < 0 || index >= m_iapItems.Count)
			return null;
		
		return m_iapItems[index];
	}
	[Obfuscar.Obfuscate]
	public IAPData NNUKE_GetIAPItem(string id)
	{
		for (int i = 0; i < m_iapItems.Count; i++)
		{
			if (m_iapItems[i].productId == id)
				return m_iapItems[i];
		}
		return null;
	}
	
	[Obfuscar.Obfuscate]
	public bool NNUKE_HasBought(string id)
	{
		IAPHandler handler = m_bindings.NNUKE_GetIAPHandler(id);
		
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("IAPManagerScript.NNUKE_HasBought - id: {0}, handler != null: {1}, activationCount: {2}"
			, id, handler != null, (handler != null) ? handler.activationCount : -1));
		#endif

		if (handler != null && handler.activationCount > 0)
		{
			return true;
		}
		return false;	
	}
	
	[Obfuscar.Obfuscate]
	public bool NNUKE_HasBoughtSomething()
	{
		foreach (string id in m_bindings.ProductIds)
		{
			IAPHandler handler = m_bindings.NNUKE_GetIAPHandler(id);
			if (handler != null && handler.activationCount > 0)
			{
				return true;
			}
		}
		return false;	
	}
	
	[Obfuscar.Obfuscate]
	public bool NNUKE_DoTransaction(string id, int quantity)
	{
		#if DEBUG_TEXTS
		Debug.Log( "IAPManagerScript.NNUKE_DoTransaction: " + id + ", quantity: " + quantity);
		#endif
		if (quantity < 1)
			return false;
		
		State = RequestState.Processing;
	
 		#if DEBUG_TEST_IAP
		NNUKE_PurchaseSuccesfull(id, quantity, string.Empty);
		return true;
		#endif
		
		if (CanMakePayments)
		{
			#if UNITY_IPHONE
			StoreKitBinding.purchaseProduct(id, quantity);
			#endif
			
			#if UNITY_ANDROID
			if (OuyaInputWrapper.OuyaEnabled)
			{
				OuyaSDK.requestPurchase(id);
			}
			else
			{
				for (int i = 0; i < quantity; i++) 
					GoogleIAB.purchaseProduct(id);
			}
			#endif
			
			return true;
		}
		return false;
	}
	
	#if UNITY_IPHONE
	/// <summary>
	/// Restores the completed transactions. 
	/// 
	/// NOTE! This should not be called every time we start up!
	/// -> only when player has a new device, e.g., we could do this 
	/// when we have no save game, and also have this as a selection in options-menu
	/// 
	/// NOTE! We do not have similar functionality with Google / Android. In there, we just check if something
	/// has been bought.
	/// </summary>
	[Obfuscar.Obfuscate]
	public void NNUKE_RestoreCompletedTransactions()
	{
		#if DEBUG_TEXTS
		Debug.Log( "IAPManagerScript.RestoreCompletedTransactions");
		#endif
		
 		#if DEBUG_TEST_IAP
		return;
		#endif
		
		State = RequestState.Processing;

		// NOTE! This only restores non-consumables (which makes sense)
		StoreKitBinding.restoreCompletedTransactions();
	}
	#endif
	
	[Obfuscar.Obfuscate]
	public void NNUKE_MarkTransactionAsUsed(string transactionId)
	{
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("IAPManagerScript.NNUKE_MarkTransactionAsUsed id: {0}", transactionId));
		#endif
		
		m_completedTransactionIds.Add (transactionId);
	}
	

	[Obfuscar.Obfuscate]
	public void NNUKE_AddProduct(IAPData item)
	{
#if DEBUG_TEXTS
		Debug.Log( string.Format ("IAPManagerScript.NNUKE_AddProduct() to index: {0},  item: {1}"
			, m_iapItems.Count, item.ToString()));
#endif
		
		// google adds some stupid extra stuff in title ...
		item.title = item.title.Replace("(Space Buggers)", string.Empty);
		item.title = item.title.Trim();
		
		// sprite for this? 
		int index = -1;
		for (index = 0 ; index < m_bindings.ProductIds.Length; index++)
		{
			if (m_bindings.ProductIds[index] == item.productId)
				break;
		}
		
		item.spriteName = (index < m_bindings.m_spriteNames.Length) ? m_bindings.m_spriteNames[index] : "";
		item.consumable = m_bindings.NNUKE_IsConsumable(item.productId);
		
		m_iapItems.Add (item);
	}
	
	/// <summary>
	/// Purchase transaction finished succesfully, so we call whatever delegates we have bind to the action here.
	/// These should give the player the stuff that we want to.
	/// </summary>
	/// <param name='id'>
	/// Identifier.
	/// </param>
	/// <param name='quantity'>
	/// Quantity.
	/// </param>
	[Obfuscar.Obfuscate]
	public void NNUKE_PurchaseSuccesfull(string id, int quantity, string transactionIdentifier, bool skipConsumables = false)
	{
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("IAPManagerScript.NNUKE_PurchaseSuccesfull() id={0}, quantity={1}, transactionId={2}, skipConsumables={3}"
			, id, quantity, transactionIdentifier, skipConsumables ));
		#endif
		
		// only do this if we haven't yet in this session for the transaction
		if (!string.IsNullOrEmpty(transactionIdentifier)
			&& m_completedTransactionIds.Contains(transactionIdentifier))
		{
			#if DEBUG_TEXTS
			Debug.Log( string.Format ("IAPManagerScript.NNUKE_PurchaseSuccesfull: already handled purchase for {0} (id: {1})!"
				, id, transactionIdentifier));
			#endif
			return;
		}
		
		for (int i = 0; i < quantity; i++)
		{
			m_bindings.NNUKE_DoActivate(id, skipConsumables);
		}
		
		IAPData itemData = NNUKE_GetIAPItem(id);
		if (itemData == null)
		{
			// NOTE! This happens when we validate the already bought items at the start up ...
			
			#if DEBUG_TEXTS
			Debug.LogWarning( string.Format (
				"IAPManagerScript.NNUKE_PurchaseSuccesfull: DID NOT FOUND MAPPED ITEM FOR: {0}!"
				, id));
			#endif
		}
		
		m_completedTransactionIds.Add(transactionIdentifier);
		
		State = IAPManagerScript.RequestState.OK_Purchase;
	}
	
	[Obfuscar.Obfuscate]
	public void NNUKE_GetProductData()
	{
		#if DEBUG_TEXTS
		Debug.Log( "IAPManagerScript.NNUKE_GetProductData" );
		#endif
		m_iapItems.Clear ();
		
		State = RequestState.Processing;

		#if UNITY_IPHONE
		StoreKitBinding.requestProductData(m_bindings.ProductIds);
		#endif
	
		#if UNITY_ANDROID
		
		if (OuyaInputWrapper.OuyaEnabled)
		{
			GetOuyaProductData();
		}
		else
		{
			GoogleIAB.queryInventory(m_bindings.ProductIds);
		}
		#endif
	}
		
	private void GetOuyaProductData()
	{
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("IAPManagerScript.{0} isIAPInitComplete: {1} (time: {2})"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, OuyaSDK.isIAPInitComplete()
			, Time.time));
		#endif
		
#if USE_OUYA
		// don't do this if not initialized yet ...
		if (!OuyaSDK.isIAPInitComplete())
		{
			State = RequestState.None;
			return;
		}
		
		List<OuyaSDK.Purchasable> productIdentifierList = new List<OuyaSDK.Purchasable>();

		foreach (string productId in m_bindings.ProductIds)
		{
			productIdentifierList.Add(new OuyaSDK.Purchasable(productId));
		}
		OuyaSDK.requestProductList(productIdentifierList);
#endif
	}
	
	/// <summary>
	/// Updates the saved transactions. 
	/// </summary>
	[Obfuscar.Obfuscate]
	public void NNUKE_UpdateSavedTransactions()
	{
		#if UNITY_IPHONE
		List<StoreKitTransaction> transactionList = StoreKitBinding.getAllSavedTransactions();
		
		#if DEBUG_TEXTS
		Debug.Log(string.Format ("IAPManagerScript.NNUKE_UpdateSavedTransactions - count: {0}", transactionList.Count));
		#endif
		
		foreach (StoreKitTransaction transaction in transactionList)
		{
			#if DEBUG_TEXTS
			Debug.Log(string.Format ("IAPManagerScript.NNUKE_UpdateSavedTransactions - \"{0}\"", transaction.ToString()));
			#endif
			
			NNUKE_PurchaseSuccesfull(
				transaction.productIdentifier
				, transaction.quantity
				, transaction.base64EncodedTransactionReceipt
				, true);
		}
		m_bindings.NNUKE_DoValidateAll(true);
		#endif
	}
	
	#if UNITY_ANDROID
	/// <summary>
	/// Updates the saved transactions. 
	/// </summary>
	[Obfuscar.Obfuscate]
	public void NNUKE_UpdateTransactions(List<GooglePurchase> purchases)
	{
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("IAPManagerScript.NNUKE_UpdateTransactions - purchases.Count={0}, m_iapItems.Count={1}"
			, purchases.Count, m_iapItems.Count ));
		#endif
		
		m_googlePurchases = purchases;
		foreach (GooglePurchase purchase in m_googlePurchases)
		{
			NNUKE_PurchaseSuccesfull(
				purchase.productId
				, 1
				, purchase.orderId
				, true);
			//packageName
			//orderId
			//productId
			//developerPayload
			//purchaseTime
			//purchaseState
			//purchaseToken
			
			if (m_bindings.NNUKE_IsConsumable(purchase.productId))
			{
				GoogleIAB.consumeProduct(purchase.productId);
			}
		}
		m_bindings.NNUKE_DoValidateAll(true);
	}
	#endif

#endregion

#region Private methods
	
	[Obfuscar.Obfuscate]
	private void NNUKE_TestMethodMakeFakeItem()
	{
		NNUKE_AddProduct(new IAPData
				{ 
					description = "This is a test item description for weapon pack"
					, currencySymbol = "€"
					, currencyCode = "eur"
					, formattedPrice = "0.89 eur"
					, price = "0.89"
					, productId = (OuyaInputWrapper.OuyaEnabled) 
						? "com_kankibros_space_buggers_content_pack_1" 
						: "com.kankibros.spacebuggers.iap_content_pack_1"
					, title = "Weapon pack 1"
					, consumable = false
				}
		);
		NNUKE_AddProduct(new IAPData
				{ 
					description = "This is a test item description for level pack"
					, currencySymbol = "€"
					, currencyCode = "eur"
					, formattedPrice = "1.29 eur"
					, price = "1.29"
					, productId = (OuyaInputWrapper.OuyaEnabled) 
						? "com_kankibros_spacebuggers_iap_level_pack_01" 
						: "com.kankibros.spacebuggers.iap_level_pack_01"
					, title = "Level pack 1"
					, consumable = false
				}
		);
		NNUKE_AddProduct(new IAPData
				{ 
					description = "This is a test item description for coin pack"
					, currencySymbol = "€"
					, currencyCode = "eur"
					, formattedPrice = "0.89 eur"
					, price = "0.89"
					, productId = (OuyaInputWrapper.OuyaEnabled) 
						? "com_kankibros_spacebuggers_iap_credit_pack" 
						: "com.kankibros.spacebuggers.iap_credit_pack"
					, title = "Coin pack"
					, consumable = true
			}
		);
	}
	
	[Obfuscar.Obfuscate]
	private void NNUKE_Initialize()
	{
		#if DEBUG_TEXTS
		Debug.Log( "IAPManagerScript.Initialize" );
		#endif
				
		m_canMakePayments = false;
		
		State = RequestState.Processing;
		
		#if DEBUG_TEST_IAP
		NNUKE_TestMethodMakeFakeItem();
		return;
		#endif
		
		#if UNITY_IPHONE
		StoreKitBinding.finishPendingTransactions();
		NNUKE_GetProductData();
		
		// we want to enable iap-save-stuff here, not save that in NNUKE_SaveGame, so that's why this call is here
		NNUKE_UpdateSavedTransactions();
		#elif UNITY_ANDROID
		
		if (OuyaInputWrapper.OuyaEnabled)
		{
			NNUKE_GetProductData();
		}
		else
		{
			// with google, we have to wait for async init before initiating NNUKE_GetProductData
			GoogleIAB.init(m_googleLicenseKey);
		}
		#endif
	}
	
	[Obfuscar.Obfuscate]
	private void NNUKE_UnInitialize()
	{
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("IAPManagerScript.UnInitialize"));
		#endif
		
		#if UNITY_ANDROID
		if (!OuyaInputWrapper.OuyaEnabled)
		{
			GoogleIAB.unbindService();
		}
		#endif
	}	
	
	#if UNITY_IPHONE
	[Obfuscar.Obfuscate]
	private void ValidateReceipts()
	{
		// this is DEPRECATED
		// -> Prime31 docs say that:
		// "If you want to validate a receipt it should be done securely on an external server."
		
		List<StoreKitTransaction> transactionList = StoreKitBinding.getAllSavedTransactions();
		foreach (StoreKitTransaction ta in transactionList)
			// Validates the given receipt for consumable and non-consumable products.  
			// If you are using the sandbox server (not a live sale) set isTest to true.
			StoreKitBinding.validateReceipt( ta.base64EncodedTransactionReceipt, true );
	}
	#endif	
	
#endregion
}
