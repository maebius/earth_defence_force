using UnityEngine;
using System.Collections;

sealed public class MusicScript : MonoBehaviour 
{
	public string m_name;
	public MusicManagerScript.SwitchMode m_mode;
	public bool m_loop;
	
	// Use this for initialization
	void Start () 
	{
		MusicManagerScript.Instance.PlayClip(m_name, m_mode, m_loop);
	}
}
