
using UnityEngine;
using System.Collections;

sealed public class ResolutionChangerScript : MonoBehaviour 
{
	public bool HardSetResolution = false;
	public bool Fullscreen = false;
	public int Width = 1280;
	public int Height = 720;
	
	void Awake() 
	{
		if (HardSetResolution)
		{
			Screen.SetResolution(Width, Height, Fullscreen);
		}
	}
}
