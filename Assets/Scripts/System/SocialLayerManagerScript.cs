//#define DEBUG_TEXTS

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Prime31;

// trick when using both the iOS and Android version of the plugin in the same project. Add this block to the
// top of the file you are calling the Facebook methods from so they can share code. Note that it will only work
// when calling methods that are common to both platforms!

#if UNITY_ANDROID
using FacebookAccess = FacebookAndroid;
#elif UNITY_IPHONE
using FacebookAccess = FacebookBinding;
#endif

sealed public class SocialLayerManagerScript : MonoBehaviour
{
	#region Enums, structs, and inner classes
	public enum RequestState
	{
		None
		, Processing
		, Cancel
		, Fail
		, Ok
	};
	#endregion
	

	#region Delegates And Events
	public delegate void StateChanged(RequestState state);
	public event StateChanged Changed;
	#endregion

	#region Public members, visible in Unity GUI
	public bool FacebookEnabled
	{
		get { return m_facebookEnabled; }
		set
		{
			Debug.Log (string.Format ("SocialLayerManagerScript.FacebookEnabled._set({0})", value));
		
			m_facebookEnabled = value;
		}
	}
	#endregion
	
	#region Public interface
	public static SocialLayerManagerScript GetInstance()
	{
#if UNITY_ANDROID || UNITY_IPHONE
		if (sm_instance == null)
		{
			sm_instance =
				(SocialLayerManagerScript) GameObject.FindObjectOfType(typeof(SocialLayerManagerScript));
		}
		if (sm_instance == null)
		{
			Debug.LogWarning (string.Format ("SocialLayerManagerScript.OpenDialog - NO object instance in scene! We need to have one!"));
		}
#endif
		return sm_instance;
	}
	
	public static void PostToWall(string msg, string url, string urlCaption, string imageUrl, string imageCaption)
	{
#if UNITY_ANDROID || UNITY_IPHONE
		SocialLayerManagerScript social = GetInstance();
		if (social != null)
		{
			social.DoPublishToWall(msg, url, urlCaption, imageUrl, imageCaption);
		}
#endif
	}
	
	public static void OpenDialog(string url, string urlCaption, string imageUrl, string imageCaption, string message = null)
	{
#if UNITY_ANDROID || UNITY_IPHONE		
		SocialLayerManagerScript social = GetInstance();
		if (social != null)
		{
			social.DoOpenDialog(url, urlCaption, imageUrl, imageCaption, message);
		}		
#endif
	}

	public static void ReportScore(int score)
	{
#if UNITY_ANDROID || UNITY_IPHONE		
		SocialLayerManagerScript social = GetInstance();
		if (social != null)
		{
			social.PostScore(score);
		}
#endif
	}
	#endregion
	
	#region Public data members
	public bool m_useLogin = false;
	#endregion
	
	#region Private data members
	private RequestState State
	{
		set 
		{
			m_state = value;
			if (Changed != null)
			{
				Changed(m_state);
			}
		}
	}
	
	private RequestState m_state = RequestState.None;

	private bool m_facebookEnabled = true;
	private bool m_cachedPostAvailable;
	
	private bool m_withDialog;
	private string m_cachedMsg;
	private string m_cachedUrl;
	private string m_cachedUrlCaption;
	private string m_cachedImageUrl;
	private string m_cachedImageCaption;
	
	private string m_userId;
	
	private static bool sm_initialized = false;
	
	private static SocialLayerManagerScript sm_instance = null;
	
	#endregion
	
	#region Unity MonoBehaviour base overrides

#if UNITY_ANDROID || UNITY_IPHONE
	// Use this for initialization
	void Awake ()		
	{
#if DEBUG_TEXTS
		Debug.Log (string.Format ("SocialLayerManagerScript.Awake"));
#endif
		
		m_cachedPostAvailable = false;

		// if enabled, we login here as it's better to do it in application start than
		// in the middle of the game
		if (FacebookEnabled)
			Initialize();
	}
		
	void OnDestroy()
	{
#if DEBUG_TEXTS
		Debug.Log (string.Format ("SocialLayerManagerScript.OnDestroy"));
#endif
		sm_instance = null;
	}
	
	// Listens to all the events.  All event listeners MUST be removed before this object is disposed!
	void OnEnable()
	{
#if DEBUG_TEXTS
		Debug.Log( "SocialLayerManagerScript.OnEnable" );
#endif
		
		FacebookManager.sessionOpenedEvent += sessionOpenedEvent;
		FacebookManager.loginFailedEvent += loginFailedEvent;
		
		FacebookManager.dialogCompletedWithUrlEvent += dialogCompletedWithUrlEvent;
		FacebookManager.dialogFailedEvent += dialogFailedEvent;
		
		FacebookManager.graphRequestCompletedEvent += graphRequestCompletedEvent;
		FacebookManager.graphRequestFailedEvent += facebookCustomRequestFailed;
		FacebookManager.facebookComposerCompletedEvent += facebookComposerCompletedEvent;
		
		FacebookManager.reauthorizationFailedEvent += reauthorizationFailedEvent;
		FacebookManager.reauthorizationSucceededEvent += reauthorizationSucceededEvent;
	}
	
	void OnDisable()
	{
#if DEBUG_TEXTS
		Debug.Log( "SocialLayerManagerScript.OnDisable" );
#endif
		
		// Remove all the event handlers when disabled
		FacebookManager.sessionOpenedEvent -= sessionOpenedEvent;
		FacebookManager.loginFailedEvent -= loginFailedEvent;
		
		FacebookManager.dialogCompletedWithUrlEvent -= dialogCompletedWithUrlEvent;
		FacebookManager.dialogFailedEvent -= dialogFailedEvent;
		
		FacebookManager.graphRequestCompletedEvent -= graphRequestCompletedEvent;
		FacebookManager.graphRequestFailedEvent -= facebookCustomRequestFailed;
		FacebookManager.facebookComposerCompletedEvent -= facebookComposerCompletedEvent;
		
		FacebookManager.reauthorizationFailedEvent -= reauthorizationFailedEvent;
		FacebookManager.reauthorizationSucceededEvent -= reauthorizationSucceededEvent;
	}
#endif
	
	#endregion
		
	#region Private object methods

#if UNITY_ANDROID || UNITY_IPHONE
	
	private void TestUserMethod()
	{
#if DEBUG_TEXTS
		Debug.Log (string.Format ("SocialLayerManagerScript.TestUserMethod"));
#endif
	
		string graphPath = "https://graph.facebook.com/469494586445574/accounts/test-users";
		string httpMethod = "POST";

		Dictionary<string, object> parameters = new Dictionary<string, object>();

		parameters.Add ("installed", "true");
		parameters.Add ("name", "Tepa Testailija");
		parameters.Add ("locale", "en_US");
		parameters.Add ("permissions", "read_stream");
		parameters.Add ("method", "POST");
		parameters.Add ("access_token", Facebook.instance.accessToken);
		
		FacebookAccess.graphRequest(graphPath, httpMethod, parameters);
	}
	
	private void TestMethod()
	{
#if DEBUG_TEXTS
		Debug.Log (string.Format ("SocialLayerManagerScript.TestMethod"));
#endif
		
		string msg = "Rustaile kankee!";
		string url = "http://www.kankibros.com";
		string urlCaption = "Kanki Bros";
		string imageUrl = "http://www.kankibros.com/img/bean.png";
		string imageCaption = "Logo";
		
		DoPublishToWall(msg, url, urlCaption, imageUrl, imageCaption);
	}
	
	private void DoPublishToWall(string msg, string url, string urlCaption, string imageUrl, string imageCaption)
	{
#if DEBUG_TEXTS
		Debug.Log (string.Format ("SocialLayerManagerScript.DoPublishToWall - FacebookEnabled: {0}", FacebookEnabled));
#endif
		
		// don't do it if turned off!
		if (!FacebookEnabled)
			return;
		
		State = RequestState.Processing;
		
		m_withDialog = false;
		m_cachedPostAvailable = true;
		m_cachedMsg = msg;
		m_cachedUrl = url;
		m_cachedUrlCaption = urlCaption;
		m_cachedImageUrl = imageUrl;
		m_cachedImageCaption = imageCaption;
		
		// this will eventually do the sending, first just checks that we're logged in with good access token
		Login();
	}
	
	private void DoOpenDialog(string url, string urlCaption, string imageUrl, string imageCaption, string message)
	{
#if DEBUG_TEXTS
		Debug.Log (string.Format ("SocialLayerManagerScript.DoOpenDialog - FacebookEnabled: {0}", FacebookEnabled));
#endif
		
		// don't do it if turned off!
		if (!FacebookEnabled)
			return;
		
		State = RequestState.Processing;
		
		m_withDialog = true;
		m_cachedPostAvailable = true;
		m_cachedUrl = url;
		m_cachedUrlCaption = urlCaption;
		m_cachedImageUrl = imageUrl;
		m_cachedImageCaption = imageCaption;
		m_cachedMsg = message;
		
		Login ();
	}

	private void Initialize()
	{
#if DEBUG_TEXTS
		Debug.Log (string.Format ("SocialLayerManagerScript.Initialize - FacebookEnabled: {0}, sm_initialized: {1}"
			, FacebookEnabled, sm_initialized));
#endif
		
		if (sm_initialized)
			return;
		
		FacebookAccess.init();

		/*
		// NOTE! It is not recommended to have the app secret here in code,
		// it should be on our "own secure server" ... for testing purposes it is now here
		Facebook.instance.getAppAccessToken( "469494586445574", "1223029b4786abc5dce7de86dfb56e8f", token =>
		{
			Debug.Log( "app access token retrieved: " + token );
		});*/
		
		sm_initialized = true;
	}
	
	private void Login()
	{
#if DEBUG_TEXTS
		Debug.Log (string.Format ("SocialLayerManagerScript.Login - FacebookAccess.isSessionValid() = {0}, m_useLogin = {1}"
			, FacebookAccess.isSessionValid(), m_useLogin));
#endif
		
		if (!m_useLogin)
		{
			DoCheckCachedPosting();
			return;
		}

		// NOTE! Facebook guidelines require that we first login with read-only permissions,
		// that's why we call just Login here -> the successful listener for this launches publish-permission login
		// -> if session is already valid, then we can skip this
		if (FacebookAccess.isSessionValid())
		{
			ReLoginWithPublish();
		}
		else 
		{
			//FacebookAccess.loginWithReadPermissions( new string[] { "email", "user_birthday" } );
			FacebookAccess.login();
		}
	}
	
	private void GetMyInfo()
	{
#if DEBUG_TEXTS
		Debug.Log( string.Format ("SocialLayerManagerScript.ReLoginWithPublish") );
#endif
		
		Facebook.instance.graphRequest( "me", HTTPVerb.GET, ( error, obj ) =>
		{
			// if we have an error we dont proceed any further
			if( error != null  || obj == null)
			{
#if DEBUG_TEXTS
		Debug.Log( string.Format ("SocialLayerManagerScript.GetMyInfo() ERROR: error != null => {0}, obj == null => {1}"
					, error != null, obj == null));
#endif
				return;
			}
				
			// grab the userId and persist it for later use
			var ht = obj as Hashtable;
			m_userId = ht["id"].ToString();
				
#if DEBUG_TEXTS
			Debug.Log( "me Graph Request finished: " );
			Prime31.Utils.logObject( ht );
#endif
			// also, at this moment we want to check if we need to post something ...
			DoCheckCachedPosting();
		});
	}
	
	private void PostScore(int score)
	{
		if (string.IsNullOrEmpty(m_userId))
		{
			Debug.LogError( string.Format ("SocialLayerManagerScript.PostScore() - no valid m_userId!"));
			return;
		}

		State = RequestState.Processing;

		Facebook.instance.postScore(score, didSucceed =>
		{
#if DEBUG_TEXTS
			Debug.Log( "score post suceeded? " + didSucceed );
#endif
			
			State = (didSucceed) ? RequestState.Ok : RequestState.Fail;

		});
	}
	
	private void ReLoginWithPublish()
	{
#if DEBUG_TEXTS
		Debug.Log( "SocialLayerManagerScript.ReLoginWithPublish" );
#endif
		if (DoWeHavePublishPermissions())
		{
			DoCheckCachedPosting();
			return;
		}
		
#if UNITY_ANDROID
		FacebookAccess.loginWithPublishPermissions( new string[] { "publish_actions" } );
		// with android, 2nd parameter for "reauthorize"-func is "FacebookSessionDefaultAudience.EVERYONE"
#elif UNITY_IPHONE
		
		// NOTE! We don't have the loginWithPublishPermissions with iOS, so using reauthorizeWithPublishPermissions instead
		FacebookAccess.reauthorizeWithPublishPermissions( 
			new string[] { "publish_actions" }
			, FacebookSessionDefaultAudience.Everyone
		);
#endif
	}
	
	private void Logout()
	{
#if DEBUG_TEXTS
		Debug.Log( "SocialLayerManagerScript.Logout" );
#endif
		FacebookAccess.logout();
	}
	
	private bool DoWeHavePublishPermissions()
	{
		List<object> permissions = FacebookAccess.getSessionPermissions();
		bool val = false;
		foreach (object obj in permissions)
		{
			if (obj.ToString() == "publish_actions")
			{
				val = true;
				break;
			}
		}
#if DEBUG_TEXTS
		Debug.Log( string.Format ("SocialLayerManagerScript.DoWeHavePublishPermissions - {0}", val) );
#endif
		return val;
	}
	
	private void DoCheckCachedPosting()
	{
#if DEBUG_TEXTS
		Debug.Log( string.Format (
			"SocialLayerManagerScript.DoCheckCachedPosting - m_cachedPostAvailable: {0}, m_withDialog: {1}"
			, m_cachedPostAvailable, m_withDialog ));
#endif
		
		if (m_cachedPostAvailable)
		{
			m_cachedPostAvailable = false;
			
			if (m_withDialog) 
			{
				ShowDialog (m_cachedMsg
					, m_cachedUrl
					, m_cachedUrlCaption
					, m_cachedImageUrl
					, m_cachedImageCaption);
			}
			else
			{
				PostMessage (m_cachedMsg
					, m_cachedUrl
					, m_cachedUrlCaption
					, m_cachedImageUrl
					, m_cachedImageCaption);
			}
		}
	}
	
	private void PostImage(byte[] bytes, string caption)
	{
		Facebook.instance.postImage( bytes, caption, completionHandler );
	}
	
	private void PostMessage(string message)
	{
		Facebook.instance.postMessage( message, completionHandler );
	}	
	
	private void ShowDialog(string msg, string linkUrl, string linkCaption, string imageUrl, string imageCaption)
	{
#if DEBUG_TEXTS
		Debug.Log( string.Format ("SocialLayerManagerScript.ShowDialog - msg: {0}, lnk: {1}, cap: {2}, img: {3}, imgCap: {4}"
			, (msg != null) ? msg : "null"
			, (linkUrl != null) ? linkUrl : "null"
			, (linkCaption != null) ? linkCaption : "null"
			, (imageUrl != null) ? imageUrl : "null"
			, (imageCaption != null) ? imageCaption : "null"));
#endif
		
		var parameters = new Dictionary<string,string>
		{
			{ "link", linkUrl },
			{ "name", linkCaption },
			{ "picture", imageUrl },
			{ "caption", imageCaption }
		};
		if (!string.IsNullOrEmpty(msg))
		{
			parameters.Add ("message", msg);
		}
		
		FacebookAccess.showDialog( "stream.publish", parameters );
	}
	
	private void PostMessage(string msg, string linkUrl, string linkCaption, string imageUrl, string imageCaption)
	{
#if DEBUG_TEXTS
		Debug.Log( string.Format ("SocialLayerManagerScript.PostMessage - msg: {0}, lnk: {1}, cap: {2}, img: {3}, imgCap: {4}"
			, (msg != null) ? msg : "null"
			, (linkUrl != null) ? linkUrl : "null"
			, (linkCaption != null) ? linkCaption : "null"
			, (imageUrl != null) ? imageUrl : "null"
			, (imageCaption != null) ? imageCaption : "null"));
#endif
		
		Facebook.instance.postMessageWithLinkAndLinkToImage( 
			msg
			, linkUrl
			, linkCaption
			, imageUrl
			, imageCaption
			, completionHandler );
	}
	
	// common event handler used for all Facebook graph requests that logs the data to the console
	void completionHandler( string error, object result )
	{
		if( error != null )
		{
			State = RequestState.Fail;
#if DEBUG_TEXTS
			Debug.LogError( error );
#endif
		}
		else
		{
			State = RequestState.Ok;
#if DEBUG_TEXTS
			Prime31.Utils.logObject( result );
#endif
		}
	}
#endif
	
	#endregion
		
	#region Event listeners

#if UNITY_ANDROID || UNITY_IPHONE

	void sessionOpenedEvent()
	{
#if DEBUG_TEXTS
		Debug.Log( "SocialLayerManagerScript.sessionOpenedEvent" );
#endif
		
		ReLoginWithPublish();
	}
	
	void loginFailedEvent( P31Error error )
	{
#if DEBUG_TEXTS
		Debug.Log( "SocialLayerManagerScript.loginFailedEvent: " + error );
#endif

		State = RequestState.Fail;
	}

	void dialogCompletedWithUrlEvent( string url )
	{
#if DEBUG_TEXTS
		Debug.Log( "SocialLayerManagerScript.dialogCompletedWithUrlEvent: " + url );
#endif
		// in the url we should have the posted message, if it is empty, then we
		// conclude that the posting failed (even if the user just pressed "cancel")
		State = (!string.IsNullOrEmpty(url) && url.ToLower().Contains("post_id"))
			? RequestState.Ok : RequestState.Cancel;
	}
	
	void dialogFailedEvent( P31Error error )
	{
#if DEBUG_TEXTS
		Debug.Log( "SocialLayerManagerScript.dialogFailedEvent: " + error );
#endif
		State = RequestState.Fail;
	}
	
	void graphRequestCompletedEvent( object obj )
	{
#if DEBUG_TEXTS
		Debug.Log( "SocialLayerManagerScript.graphRequestCompletedEvent" );
		Prime31.Utils.logObject( obj );
#endif
		State = RequestState.Ok;
	}
	
	
	void facebookCustomRequestFailed( P31Error error )
	{
#if DEBUG_TEXTS
		Debug.Log( "SocialLayerManagerScript.facebookCustomRequestFailed failed: " + error );
#endif
		State = RequestState.Fail;
	}
	
	void facebookComposerCompletedEvent( bool didSucceed )
	{
#if DEBUG_TEXTS
		Debug.Log( "SocialLayerManagerScript.facebookComposerCompletedEvent - did succeed: " + didSucceed );
#endif
		State = (didSucceed) ? RequestState.Ok : RequestState.Fail;
	}
	
	
	void reauthorizationSucceededEvent()
	{
#if DEBUG_TEXTS
		Debug.Log( "SocialLayerManagerScript.reauthorizationSucceededEvent" );
#endif
		
		DoCheckCachedPosting();
	}
	
	void reauthorizationFailedEvent( P31Error error )
	{
#if DEBUG_TEXTS
		Debug.Log( "SocialLayerManagerScript.reauthorizationFailedEvent: " + error );
#endif
		State = RequestState.Fail;
	}

#endif // UNITY_ANDROID || UNITY_IPHONE

	#endregion

}
