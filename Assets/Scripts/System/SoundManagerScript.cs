//#define DEBUG_TEXTS

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

sealed public class SoundManagerScript : MonoBehaviour 
{
	#region Enums, structs, and inner classes
	public enum SoundType
	{
		None
		, ExplosionSmall
		, ExplosionMedium
		, ExplosionHuge
		, ExplosionEarth
		, WeaponCannon
		, WeaponLaser
		, WeaponBeam
		, WeaponMissile
		, WeaponTurret
		, WeaponElectrifier
		
		, ItemShockwave
		, ItemTimeFreeze
		, ItemInvulnearability
		, ItemMine
		, CollisionSmall
		, CollisionBig
		, ShieldNormal
		, ShieldFast
		, ShieldPowerful
		, ShieldHit

		, EnemyShieldHit
		, EnemyShootPayback
		, EnemyShootScorpio
		, EnemyShootHomerMissile
		, EnemyShootMultiplierSpawn
		, EnemyShootBoss

		, ChangeWeapon
		
		, ItemSpawn
		, ItemCollect
		
		, MenuSwoosh
		, MenuAppears
		, WindowAppears
	}
	
	#endregion

	#region Delegates And Events
	#endregion

	#region Public members, visible in Unity GUI
	public AudioClip[] Clips;
	
	public SoundScript SoundPrefab;
	
	#endregion
	
	#region Public members, not visible to Unity GUI
	[HideInInspector]
	public bool Muted
	{
		set 
		{
			MasterVolume = (m_muted) ? m_previousMasterVolume : 0f;
		}
		get { return m_muted; }
	}
	
	[HideInInspector]
	public float MasterVolume
	{
		set 
		{
			m_previousMasterVolume = m_masterVolume;
			
			m_muted = value <= MUTE_LEVEL_LIMIT;
			m_masterVolume = (m_muted) ? 0f : value;
			m_masterVolume = Mathf.Min (m_masterVolume, 1f);
			
			NGUITools.soundVolume = m_masterVolume;
			
			// we want to update this to NNUKE_SaveGame config
			SaveData.Instance.Config.SoundVolume = m_masterVolume;
		}
		get
		{
			return m_masterVolume;
		}
		
	}
	[HideInInspector]
	public static SoundManagerScript Instance 
	{
		get 
		{
			if (sm_instance == null)
			{
				// as we cannot instantiate MonoBehaviors directy with "new", we need to create
				// gameobject and add a component to that ...
				sm_instance = Object.FindObjectOfType(typeof(SoundManagerScript)) as SoundManagerScript;

				if (sm_instance == null)
				{
					// the name for the object is "arbitrary", could be anything ...
					GameObject go = new GameObject("_SoundManagerScript");
					DontDestroyOnLoad(go);
					sm_instance = go.AddComponent<SoundManagerScript>();
				}
			}
			return sm_instance;
		}
	}
	#endregion

	#region Private data members
	private static SoundManagerScript sm_instance = null;
	
	private Dictionary<SoundType, AudioClip[] > m_typeToClip = new Dictionary<SoundType, AudioClip[] >();
	private const float MUTE_LEVEL_LIMIT = 0.01f;
	private bool m_muted = false;
	private float m_masterVolume = 1f;
	private float m_previousMasterVolume = 1f;

	private static Dictionary<SoundType, string[] > sm_typeToName = 
		new Dictionary<SoundType, string[] >()
	{
		{ SoundType.CollisionSmall, new string[] {"collision_small", "collision_small_2", "collision_small_3" } }
		, { SoundType.CollisionBig, new string[] {"collision_big", "collision_big_2" } }
		
		, { SoundType.ExplosionSmall, new string[] {"explosion_small", "explosion_small_2"} }
		, { SoundType.ExplosionMedium, new string[] {"explosion_medium", "explosion_medium_2", "explosion_medium_3" } }
		, { SoundType.ExplosionHuge, new string[] {"explosion_big", "explosion_big_02" } }
	};
	private static Dictionary<SoundType, int > sm_nextToPlayIndex = new Dictionary<SoundType, int >();
	#endregion
	
	#region Unity MonoBehaviour base overrides
	void Awake () 
	{ 
		//Debug.Log(string.Format ("SoundManagerScript.Awake()"));
		
		if (sm_instance == null) 
		{ 
			sm_instance = this;
			DontDestroyOnLoad(gameObject); 
			sm_instance.Initialize();
		} 
		else 
			Destroy(gameObject); 	
	}
	#endregion
		
	#region Public object methods
	
	[Obfuscar.Obfuscate]
	public static AudioSource Play(GameObject go, SoundType type, bool loop, bool volumeFiltering, float frequency
		, float delay = 0f)
	{
		return Instance.AddAudioSource(go, type, true, volumeFiltering, loop, 1f, frequency, delay);
	}
	[Obfuscar.Obfuscate]
	public static AudioSource Play(GameObject go, AudioClip clip, bool loop, bool volumeFiltering, float frequency
		, float delay = 0f)
	{
		return Instance.AddAudioSource(go, clip, true, volumeFiltering, loop, 1f, frequency, delay);
	}
	[Obfuscar.Obfuscate]
	public static void PlayOneTime(Vector3 pos, AudioClip clip, float delay = 0f)
	{
		SoundScript so = (SoundScript) Instantiate(Instance.SoundPrefab
					, pos
					, Quaternion.identity);
		so.m_audio = clip;
		so.m_delay = delay;
	}
	
	[Obfuscar.Obfuscate]
	public static void PlayOneTime(GameObject go, SoundType type, float delay = 0f)
	{
		AudioSource source = Instance.AddAudioSource(go, type, true, true, false, 1f, 1f, delay);
		if (source)
			Destroy (source, source.clip.length + delay);
	}
	[Obfuscar.Obfuscate]
	public static void PlayOneTime(GameObject go, AudioClip clip, float delay = 0f)
	{
		AudioSource source = Instance.AddAudioSource(go, clip, true, true, false, 1f, 1f, delay);
		if (source)
			Destroy (source, source.clip.length + delay);
	}
	
	[Obfuscar.Obfuscate]
	private AudioSource AddAudioSource(
		GameObject go, SoundType type, bool playOnStart, bool volumeFiltering
		, bool loop, float volume, float pitch, float delaySeconds = 0f) 
	{
		AudioClip clip = m_typeToClip[type][sm_nextToPlayIndex[type]];
		if (clip == null)
		{
			Debug.LogError(string.Format (
					"SoundManagerScript.AddAudioSource() no audio for:  {0}, requested by: {1}, index: {2} (max: {3})"
						, type.ToString(), go.name
						, sm_nextToPlayIndex[type], m_typeToClip[type].Length
				));
			
			return null;
		}
		sm_nextToPlayIndex[type] = sm_nextToPlayIndex[type] + 1;
		if (sm_nextToPlayIndex[type] >= m_typeToClip[type].Length)
			sm_nextToPlayIndex[type] = 0;
		
		return AddAudioSource(go, clip, playOnStart, volumeFiltering, loop, volume, pitch, delaySeconds);
	}
	
	[Obfuscar.Obfuscate]
	private AudioSource AddAudioSource(
		GameObject go, AudioClip clip, bool playOnStart, bool volumeFiltering
		, bool loop, float volume, float pitch, float delaySeconds = 0f) 
	{
		#if DEBUG_TEXTS
		Debug.Log(string.Format (
					"SoundManagerScript.AddAudioSource() clip: {0}, playOnStart: {1}, loop: {2}, MasterVolume: {3}, volume: {4}, volumeFiltering: {5}, AudioListener pause/vol: {6}/{7}"
						, clip.name, playOnStart, loop, MasterVolume, volume, volumeFiltering
						, AudioListener.pause, AudioListener.volume));
		#endif
		
		volume *= MasterVolume;
		if (volumeFiltering && volume <= 0.001f)
			return null;
	
		AudioSource source = (AudioSource) go.AddComponent<AudioSource>();
		source.rolloffMode = AudioRolloffMode.Linear;
		source.minDistance = 1f;
		source.maxDistance = 1000f;
		source.clip = clip;
		source.volume = volume;
		source.pitch = pitch;
		source.loop = loop;
		
		if (playOnStart)
		{
			source.Play ( (ulong) (delaySeconds * clip.frequency));
		}
		
		return source;
	}

	
	#endregion
	
	#region Private object methods
	[Obfuscar.Obfuscate]
	private void Initialize()
	{
		//Debug.Log(string.Format ("SoundManagerScript.Initialize() audio != null => {0}", audio != null));
		
		foreach (KeyValuePair<SoundType, string[]> typeNamePair in sm_typeToName)
		{
			int index = 0;
			m_typeToClip[typeNamePair.Key] = new AudioClip[typeNamePair.Value.Length];
			
			foreach (string n in typeNamePair.Value)
			{
				AudioClip clip = null;
				foreach (AudioClip ac in Clips)
				{
					if (ac != null && ac.name == n)
					{
						clip = ac;
						break;
					}
				}
				if (clip == null)
				{
					Debug.LogWarning(string.Format (
						"SoundManagerScript.Initialize() no audio for:  {0} (clip name marked as: \"{1}\""
							, typeNamePair.Key.ToString(), n));
					
					continue;
				}
				
				#if DEBUG_TEXTS
				Debug.Log(string.Format (
						"SoundManagerScript.Initialize() add audio for:  {0}, index {1} : {2}"
							, typeNamePair.Key.ToString(), index, clip.name));
				#endif
				
				m_typeToClip[typeNamePair.Key][index] = clip;
				index++;
			}
		}
		
		foreach (SoundType t in sm_typeToName.Keys)
		{
			sm_nextToPlayIndex[t] = 0;
		}
		
		MasterVolume = SaveData.Instance.Config.SoundVolume;
	}
	
	
	#endregion
}
