using UnityEngine;
using System.Collections;

sealed public class SoundScript : MonoBehaviour 
{
	public AudioClip m_audio;
	
	public SoundManagerScript.SoundType m_pool = SoundManagerScript.SoundType.None;
	
	public bool m_loop;
	public float m_delay;
	public bool m_deleteAfterPlayed;
	
	private AudioSource m_source;
	
	// Use this for initialization
	void Start () 
	{
		if (m_audio)		
			m_source = SoundManagerScript.Play (gameObject, m_audio, m_loop, true, 1f, m_delay);
		else if (m_pool != SoundManagerScript.SoundType.None)
			m_source = SoundManagerScript.Play (gameObject, m_pool, m_loop, true, 1f, m_delay);
		else
			return;
			
		const float deleteOffset = 0.1f;
		
		// NOTE! If sounds are off, we still do not want to delete this right away,
		// as many visualizations depend on this deletion, that's why we allow 2 secs dying time ...
		
		if (m_deleteAfterPlayed)
		{
			float delay = (m_source) 
				? m_source.clip.length + m_delay + deleteOffset
				: 2f +  + m_delay + deleteOffset;
			
			Destroy (gameObject, delay);
		}
	}
	
	public void SetVolume(float volume)
	{
		m_source.volume = volume;
	}
}
