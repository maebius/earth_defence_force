#define DEBUG_TEXTS

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#if UNITY_ANDROID
using TwitterAccess = TwitterAndroid;
#elif UNITY_IPHONE
using TwitterAccess = TwitterBinding;
#endif

sealed public class TwitterManagerScript : MonoBehaviour 
{
	#region Public members, visible in Unity GUI
	public string m_consumerKey;
	public string m_consumerSecret;
	public Texture2D m_defaultImage = null;
	#endregion
	
	#region Enums, structs, and inner classes
	public enum RequestState
	{
		None
		, Processing
		, FailDuplicate
		, Fail
		, Ok
	};
	#endregion
	

	#region Delegates And Events
	public delegate void StateChanged(RequestState state);
	public event StateChanged Changed;
	#endregion

	#region Public interface	
	public static TwitterManagerScript GetInstance()
	{
#if UNITY_ANDROID || UNITY_IPHONE
		if (sm_instance == null)
		{
			sm_instance =
				(TwitterManagerScript) GameObject.FindObjectOfType(typeof(TwitterManagerScript));
		}
		if (sm_instance == null)
		{
#if DEBUG_TEXTS
			Debug.LogWarning (string.Format ("TwitterManagerScript.GetInstance - NO object instance in scene! We need to have one!"));
#endif
		}
#endif
		return sm_instance;
	}
	
	public static void Post(string msg, Texture2D image = null)
	{
#if UNITY_ANDROID || UNITY_IPHONE
		TwitterManagerScript twitter = GetInstance();
		if (twitter != null)
		{
			twitter.DoPost(msg, image);
		}
#endif
	}
	#endregion
	
	#region Public variables
	public bool m_useLogin = false;
	public bool m_useComposer = true;
	#endregion
	
	#region Private variables
	private RequestState State
	{
		set 
		{
			m_state = value;
			if (Changed != null)
			{
				Changed(m_state);
			}
		}
	}
	
	private RequestState m_state = RequestState.None;
	
	private bool m_cachedPostAvailable;
	private string m_cachedMsg;
	private Texture2D m_cachedImage;
	
	private static bool sm_initialized = false;
	
	private static TwitterManagerScript sm_instance = null;
	#endregion
	
	#region Public Unity interface
	
#if UNITY_ANDROID || UNITY_IPHONE

	// Use this for initialization
	void Awake ()		
	{
		m_cachedPostAvailable = false;

		Initialize();
	}
	
	void OnDestroy()
	{
		sm_instance = null;
	}
	
	void OnEnable()
	{
#if DEBUG_TEXTS
		Debug.Log( "TwitterManagerScript.OnEnable" );
#endif
		// Listens to all the events.  All event listeners MUST be removed before this object is disposed!

		// android
#if UNITY_ANDROID
		TwitterAndroidManager.loginDidSucceedEvent += loginDidSucceedEvent;
		TwitterAndroidManager.loginDidFailEvent += loginDidFailEvent;
		TwitterAndroidManager.requestSucceededEvent += requestSucceededEvent;
		TwitterAndroidManager.requestFailedEvent += requestFailedEvent;
		TwitterAndroidManager.twitterInitializedEvent += twitterInitializedEvent;
#elif UNITY_IPHONE
		// ios
		TwitterManager.loginSucceededEvent += loginDidSucceedEvent;
		TwitterManager.loginFailedEvent += loginDidFailEvent;
		TwitterManager.requestDidFinishEvent += requestSucceededEvent;
		TwitterManager.requestDidFailEvent += requestFailedEvent;
		TwitterManager.tweetSheetCompletedEvent += tweetSheetCompletedEvent;
#endif
	}
	
	void OnDisable()
	{
#if DEBUG_TEXTS
		Debug.Log( "TwitterManagerScript.OnDisable" );
#endif
		
		// Remove all the event handlers when disabled
#if UNITY_ANDROID
		TwitterAndroidManager.loginDidSucceedEvent -= loginDidSucceedEvent;
		TwitterAndroidManager.loginDidFailEvent -= loginDidFailEvent;
		TwitterAndroidManager.requestSucceededEvent -= requestSucceededEvent;
		TwitterAndroidManager.requestFailedEvent -= requestFailedEvent;
		TwitterAndroidManager.twitterInitializedEvent -= twitterInitializedEvent;
#elif UNITY_IPHONE
		TwitterManager.loginSucceededEvent -= loginDidSucceedEvent;
		TwitterManager.loginFailedEvent -= loginDidFailEvent;
		TwitterManager.requestDidFinishEvent -= requestSucceededEvent;
		TwitterManager.requestDidFailEvent -= requestFailedEvent;
		TwitterManager.tweetSheetCompletedEvent -= tweetSheetCompletedEvent;
#endif
	}
#endif // UNITY_ANDROID || UNITY_IPHONE
	#endregion

	#region Private methods

#if UNITY_ANDROID || UNITY_IPHONE
	private void Initialize()
	{
#if DEBUG_TEXTS
		Debug.Log (string.Format ("TwitterManagerScript.Initialize"));
#endif
		
		if (sm_initialized)
			return;

		TwitterAccess.init( m_consumerKey, m_consumerSecret);

		sm_initialized = true;
		
		if (Application.platform == RuntimePlatform.Android)
		{
			m_useLogin = true;
			m_useComposer = false;
		}
		
#if DEBUG_TEXTS
		Debug.Log (string.Format ("TwitterManagerScript.Initialize - platform = {0}, m_useLogin = {1}, m_useComposer={2}"
			, Application.platform, m_useLogin, m_useComposer));
#endif
		
	}
	
	private void DoPost(string msg, Texture2D img)
	{
#if DEBUG_TEXTS
		Debug.Log (string.Format ("TwitterManagerScript.DoPost - image? {0}, msg: {1}", (img != null), msg));
#endif

		State = RequestState.Processing;

		m_cachedPostAvailable = true;
		m_cachedMsg = msg;
		m_cachedImage = (img != null) ? img : m_defaultImage;
		
		// this will eventually do the sending, first just checks that we're logged in with good access token
		Login();
	}
	
	private void DoCheckCachedPosting()
	{
#if DEBUG_TEXTS
		Debug.Log (string.Format ("TwitterManagerScript.DoCheckCachedPosting() - m_cachedPostAvailable: {0}"
			, m_cachedPostAvailable));
#endif

		if (m_cachedPostAvailable)
		{
			m_cachedPostAvailable = false;
			
			if (m_cachedImage != null) 
			{
				PostMessageWithImage(m_cachedMsg, m_cachedImage);
			}
			else
			{
				PostMessage(m_cachedMsg);
			}
		}
	}
	
	private void Login()
	{
#if DEBUG_TEXTS
		Debug.Log (string.Format ("TwitterManagerScript.Login() - IsLoggedIn: {0}, m_useLogin: {1}"
			, IsLoggedIn(), m_useLogin ));
#endif
		
		if (IsLoggedIn() || !m_useLogin)
		{
			DoCheckCachedPosting();
		}
		else
		{
			TwitterAccess.showLoginDialog();
		}
	}
	
	private void Logout()
	{
		TwitterAccess.logout();
	}
	
	private bool IsLoggedIn()
	{
		bool loggedIn = TwitterAccess.isLoggedIn();

#if DEBUG_TEXTS
		Debug.Log (string.Format ("TwitterManagerScript.IsLoggedIn() - {0}", loggedIn));
#endif
		
		return loggedIn;
	}
	
	private void PostMessage(string msg)
	{
#if DEBUG_TEXTS
		Debug.Log (string.Format ("TwitterManagerScript.PostMessage() [length: {0}] msg: {1}"
			, msg.Length, msg));
#endif
		
#if UNITY_ANDROID
		TwitterAccess.postUpdate( msg );
#elif UNITY_IPHONE
		if (m_useComposer)
			TwitterBinding.showTweetComposer( msg );
		else
			TwitterBinding.postStatusUpdate( msg );
#endif
	}
	
	private void PostMessageWithImage(string msg, Texture2D image)
	{
		byte[] bytes = image.EncodeToPNG();

#if DEBUG_TEXTS
		Debug.Log (string.Format ("TwitterManagerScript.PostMessageWithImage() [length: {0}, bytes: {2}] msg: {1}"
			, msg.Length, msg, bytes.Length));
#endif
		
#if UNITY_ANDROID
		TwitterAndroid.postUpdateWithImage( msg, bytes );
#elif UNITY_IPHONE
		string pathToImage =  Application.persistentDataPath + "/imageToSendToTwitter.png";
		System.IO.File.WriteAllBytes(pathToImage, bytes);
		
		if (m_useComposer)
			TwitterBinding.showTweetComposer( msg, pathToImage);
		else
			TwitterBinding.postStatusUpdate( msg, pathToImage );
#endif
	}
#endif // if UNITY_ANDROID || UNITY_IPHONE
	
	#endregion
	
	#region Event listeners

#if UNITY_ANDROID || UNITY_IPHONE

#if UNITY_ANDROID
	void loginDidSucceedEvent( string username)
#elif UNITY_IPHONE
	void loginDidSucceedEvent(string result)
#endif
	{
#if DEBUG_TEXTS
		Debug.Log( "TwitterManagerScript.loginDidSucceedEvent");
#endif		
		DoCheckCachedPosting();
	}

	void loginDidFailEvent( string error )
	{
#if DEBUG_TEXTS
		Debug.Log( "TwitterManagerScript.loginDidFailEvent. error: " + error );
#endif
		State = RequestState.Fail;
	}

	void requestSucceededEvent( object response )
	{
#if DEBUG_TEXTS
		Debug.Log( "TwitterManagerScript.requestSucceededEvent" );
		Prime31.Utils.logObject( response );
#endif
		State = RequestState.Ok;

	}

	void requestFailedEvent( string error )
	{
#if DEBUG_TEXTS
		Debug.Log( "TwitterManagerScript.requestFailedEvent. error: " + error );
#endif
		State = RequestState.Fail;
	}
	
#if UNITY_ANDROID
	
	void twitterInitializedEvent()
	{
#if DEBUG_TEXTS
		Debug.Log( "TwitterManagerScript.twitterInitializedEvent" );
#endif
	}

#elif UNITY_IPHONE
	void postSucceeded()
	{
#if DEBUG_TEXTS
		Debug.Log( "TwitterManagerScript.postSucceeded() - Successfully posted to Twitter" );
#endif
		State = RequestState.Ok;
	}

	void postFailed( string error )
	{
#if DEBUG_TEXTS
		Debug.Log( "TwitterManagerScript.postFailed() - Twitter post failed: " + error );
#endif
		State = RequestState.Fail;
	}

	void homeTimelineFailed( string error )
	{
#if DEBUG_TEXTS
		Debug.Log( "TwitterManagerScript.homeTimelineFailed() - Twitter HomeTimeline failed: " + error );
#endif
		State = (error.ToLower().Contains("duplicate")) ? RequestState.FailDuplicate : RequestState.Fail;
	}
	
	void homeTimelineReceived( List<object> result )
	{
#if DEBUG_TEXTS
		Debug.Log( "TwitterManagerScript.homeTimelineReceived() - received home timeline with tweet count: " + result.Count );
#endif
		State = RequestState.Ok;
	}
	
	void tweetSheetCompletedEvent( bool didSucceed )
	{
#if DEBUG_TEXTS
		Debug.Log( "TwitterManagerScript.tweetSheetCompletedEvent() - tweetSheetCompletedEvent didSucceed: " + didSucceed );
#endif
		State = RequestState.Ok;
	}
#endif

#endif // UNITY_ANDROID || UNITY_IPHONE
	
	#endregion
}
