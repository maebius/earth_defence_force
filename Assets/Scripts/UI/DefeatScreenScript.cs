using UnityEngine;
using System.Collections;

sealed public class DefeatScreenScript : MonoBehaviour 
{

	#region Enums, structs, and inner classes
	#endregion

	#region Delegates And Events
	#endregion

	#region Public members, visible in Unity GUI
	public GameObject m_root;
	public UILabel m_timeLabel;
	public UILabel m_phaseValue;
	
	public AudioClip m_appearsSound;
	
	#endregion

	#region Public members, not visible to Unity GUI
	public float StartTimeDelaySeconds;
	#endregion

	#region Private data members
	
	private UILabel m_enemiesKilledLabel;
	private UILabel m_enemiesSeenLabel;
	
	private UISprite m_completionForeground;

	private LevelScript m_level;
	private float m_timeToStartScreen;
	#endregion


	#region Unity MonoBehaviour base overrides
	void Awake ()
	{
		const string rootName = "UI Defeat Screen/Camera/Anchor, center/panel";
		
		m_enemiesKilledLabel = 
			Utility.GetObject (string.Format ("{0}/enemiesRoot/killedValue", rootName)).GetComponent<UILabel>();
		m_enemiesSeenLabel =
			Utility.GetObject (string.Format ("{0}/enemiesRoot/seenValue", rootName)).GetComponent<UILabel>();
		
		m_completionForeground =
			GameObject.Find (string.Format ("{0}/completionRoot/valueSprite", rootName)).GetComponent<UISprite>();

		m_level = (LevelScript) GameObject.FindObjectOfType(typeof(LevelScript));
		
		if (m_level != null)
		{
			m_level.Ended += new LevelScript.LevelEnded(LevelEnded);
		}

		Utility.SetState(m_root, false, false);
		m_timeToStartScreen = -1f;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (m_timeToStartScreen > 0 && GameStateScript.Instance.NNUKE_GetWorldTime() > m_timeToStartScreen)
		{
			m_timeToStartScreen = -1f;
			Utility.SetState(m_root, true, true);

			SoundManagerScript.PlayOneTime (gameObject, m_appearsSound);

			if (AssignGamepadButtonsScript.Instance != null)
			{
				AssignGamepadButtonsScript.Instance.AssignButtons(true);
			}
		}
	}
	#endregion
		
	#region Public object methods
	public void LevelEnded(LevelScript sender)
	{
		// how did it go? if well, we enable ourselves
		if (sender.Outcome == LevelScript.Result.Failure && !sender.m_isNeverEnding)
		{
			m_enemiesKilledLabel.text = string.Format ("{0}", 
				PlayerHomePlanetScript.Instance.EnemiesKilled);
			m_enemiesSeenLabel.text = string.Format ("{0}", 
				PlayerHomePlanetScript.Instance.EnemiesEncountered);

			Vector3 originalScale = m_completionForeground.transform.localScale;
			m_completionForeground.transform.localScale = new Vector3(
				0.001f + sender.GetCompletionPercentage() * originalScale.x
				, originalScale.y, originalScale.z);

			m_timeToStartScreen = GameStateScript.Instance.NNUKE_GetWorldTime() + StartTimeDelaySeconds;
			
			int seconds = (int) PlayerHomePlanetScript.Instance.PlayTime;
			m_timeLabel.text = string.Format ("{0}:{1:00}", seconds / 60, seconds % 60);
			m_phaseValue.text = string.Format ("{0}", sender.CurrentWaveOrder + 1);
			
			GameStateScript.Instance.InputDisabled = true;

			MusicManagerScript.Instance.PlayClip("defeat_screen", MusicManagerScript.SwitchMode.CrossFade, false);
		}
	}
	
	public void OnMessageRetry()
	{
		Application.LoadLevel(SaveData.Instance.NNUKE_GetCurrentSceneName());
	}
	
	public void OnMessageBack()
	{
		Application.LoadLevel("garage");
	}
	
	#endregion
		
	#region Private object methods
	#endregion
	
}
