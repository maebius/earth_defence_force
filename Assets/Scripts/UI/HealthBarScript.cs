using UnityEngine;
using System.Collections;

sealed public class HealthBarScript : MonoBehaviour 
{
	public GameObject PrefabHealthBar = null;
	
	public Color FullHealthColor;
	public Color EmptyHealthColor;
	
	private GameObject m_healthBar = null;
	private UISlider m_healthBarSlider = null;
	private UIFollowTarget m_follower = null;
	private UISlicedSprite m_sprite = null;
	
	void Awake()
	{
		if (PrefabHealthBar != null)
		{
			m_healthBar = (GameObject) Instantiate(PrefabHealthBar);
			
			m_follower = (UIFollowTarget) m_healthBar.GetComponent<UIFollowTarget>();
			if (m_follower != null)
			{
				//m_follower.target = gameObject.transform.parent.transform;
				m_follower.target = gameObject.transform;
			}
			m_healthBarSlider = (UISlider) m_healthBar.GetComponent<UISlider>();
			m_sprite = (UISlicedSprite) m_healthBar.GetComponentInChildren<UISlicedSprite>();
		}
		else
		{
			m_healthBarSlider = GetComponent<UISlider>();
			m_sprite = GetComponentInChildren<UISlicedSprite>();
		}
	}
	
	void OnDestroy()
	{
		if (m_healthBar != null)
		{
			Destroy (m_healthBar);
		}
	}

	[Obfuscar.Obfuscate]
	public void SetVisibility(bool flag)
	{
		Utility.SetState(m_healthBarSlider.gameObject, flag, true);
		Utility.SetState(m_sprite.gameObject, flag, true);
	}
	
	[Obfuscar.Obfuscate]
	public void EnergyChanged(float normalizedEnergy)
	{
		if (m_healthBarSlider != null)
		{
			m_healthBarSlider.sliderValue = normalizedEnergy;
			if (m_sprite != null)
			{
				m_sprite.color = MathHelpers.InterpolateColor(
					EmptyHealthColor, FullHealthColor, normalizedEnergy);
			}
		}
	}
}
