//#define DEBUG_TEXTS

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

sealed public class LevelButton
{
	#region Public members
	public GameObject root;
	
	public UILabel levelNumberLabel;
	public UILabel scoreLabel;
	public UISprite bgContentsSprite;
	public UISlicedSprite bgBorderSprite;
	
	public UILabel amongBestLabel;
	public UILabel rankLabel;
	public UILabel rankAllLabel;
	public UILabel rankDivider;
	
	public string originalSprite;
	
	public Collider collider;

	public GameObject stars;

	public UISlicedSprite star_1_outline;
	public UISlicedSprite star_1_value;
	public UISlicedSprite star_2_outline;
	public UISlicedSprite star_2_value;
	public UISlicedSprite star_3_outline;
	public UISlicedSprite star_3_value;
	
	
	#endregion
	
	#region Public interface
	public void Init(string baseName)
	{
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("StarRatingScript.{0} basename: {1}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, baseName));
		#endif
		
		this.root = GameObject.Find (string.Format ("{0}", baseName));
		
		this.collider = this.root.GetComponent<Collider>();
		
		GameObject obj = GameObject.Find (string.Format ("{0}/Animation/labelLevelNumber", baseName));
		if (obj != null)
			this.levelNumberLabel = obj.GetComponent<UILabel>();
		
		obj = GameObject.Find (string.Format ("{0}/labelScore", baseName));
		if (obj != null)
			this.scoreLabel = obj.GetComponent<UILabel>();

		obj = GameObject.Find (string.Format ("{0}/valuePercentage", baseName));
		if (obj != null)
			this.amongBestLabel = obj.GetComponent<UILabel>();
		obj = GameObject.Find (string.Format ("{0}/valueRank", baseName));
		if (obj != null)
			this.rankLabel = obj.GetComponent<UILabel>();
		obj = GameObject.Find (string.Format ("{0}/valueRankAll", baseName));
		if (obj != null)
			this.rankAllLabel = obj.GetComponent<UILabel>();
		obj = GameObject.Find (string.Format ("{0}/valueRankDivider", baseName));
		if (obj != null)
			this.rankDivider = obj.GetComponent<UILabel>();
	
		obj = GameObject.Find (string.Format ("{0}/Animation/iconRoot/icon", baseName));
		if (obj != null)
		{
			this.bgContentsSprite = obj.GetComponent<UISprite>();
			this.originalSprite = this.bgContentsSprite.spriteName;
		}
		obj = GameObject.Find (string.Format ("{0}/Animation/bgRoot/UISlicedSprite", baseName));
		if (obj != null)
			this.bgBorderSprite = obj.GetComponent<UISlicedSprite>();
		
		this.stars =
			GameObject.Find (string.Format ("{0}/stars", baseName));
	
		if (this.stars != null)
		{
			this.star_1_outline = 
				GameObject.Find (string.Format ("{0}/stars/1/outlineRoot/icon", baseName)).GetComponent<UISlicedSprite>();
			this.star_1_value = 
				GameObject.Find (string.Format ("{0}/stars/1/valueRoot/icon", baseName)).GetComponent<UISlicedSprite>();
			this.star_2_outline = 
				GameObject.Find (string.Format ("{0}/stars/2/outlineRoot/icon", baseName)).GetComponent<UISlicedSprite>();
			this.star_2_value = 
				GameObject.Find (string.Format ("{0}/stars/2/valueRoot/icon", baseName)).GetComponent<UISlicedSprite>();
			this.star_3_outline = 
				GameObject.Find (string.Format ("{0}/stars/3/outlineRoot/icon", baseName)).GetComponent<UISlicedSprite>();
			this.star_3_value = 
				GameObject.Find (string.Format ("{0}/stars/3/valueRoot/icon", baseName)).GetComponent<UISlicedSprite>();
		}
	}
	
	[Obfuscar.Obfuscate]
	public void SetScoreData(int myScore, int bestScore, int rank, int totalCount)
	{
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("StarRatingScript.{0} score: {1}, best: {2}, rank: {3}, count: {4} (name: {5}, parentName: {6})"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, myScore, bestScore, rank, totalCount
			, root.gameObject.name
			, (root.transform.parent != null) ? root.transform.parent.gameObject.name : "null"
			));
		#endif
		
		this.scoreLabel.text = string.Format ("{0}", myScore);
		
		bool rankEnabled = totalCount > 0 && rank > 0;
		
		Utility.SetState(this.amongBestLabel.gameObject, rankEnabled, true);
		Utility.SetState(this.rankLabel.gameObject, rankEnabled, true);
		Utility.SetState(this.rankAllLabel.gameObject, rankEnabled, true);
		Utility.SetState(this.rankDivider.gameObject, rankEnabled, true);
		if (rankEnabled)
		{
			float amongBest;
			if (rank == 0) 
				amongBest = 100f;
			else if (rank == 1)
				amongBest = 1f;
			else
				amongBest = 100f * rank / totalCount;
			
			this.rankLabel.text = string.Format ("{0}", (rank == 0) ? "-" : rank.ToString());
			this.rankAllLabel.text = string.Format ("{0}", totalCount);
			this.amongBestLabel.text = string.Format ("{0:0} %", amongBest);
		}
	}
	[Obfuscar.Obfuscate]
	public void SetScoreState(bool state)
	{
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("StarRatingScript.{0} state: {1} (name: {2}, parentName: {3})"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, state
			, root.gameObject.name
			, (root.transform.parent != null) ? root.transform.parent.gameObject.name : "null"
			));
		#endif
		
		if (levelNumberLabel != null)
			Utility.SetState(this.levelNumberLabel.gameObject, state, true);
		
		Utility.SetState(this.amongBestLabel.gameObject, state, true);
		Utility.SetState(this.rankLabel.gameObject, state, true);
		Utility.SetState(this.rankAllLabel.gameObject, state, true);
		Utility.SetState(this.rankDivider.gameObject, state, true);
		Utility.SetState(this.scoreLabel.gameObject, state, true);
		
		// NOTE! Only disabling, as we can't enable if star-count condition does
		// not fullfill -> some other needs to set these
		if (!state)
		{
			Utility.SetState(this.star_1_outline.gameObject, state, true);
			Utility.SetState(this.star_1_value.gameObject, state, true);
			Utility.SetState(this.star_2_outline.gameObject, state, true);
			Utility.SetState(this.star_2_value.gameObject, state, true);
			Utility.SetState(this.star_3_outline.gameObject, state, true);
			Utility.SetState(this.star_3_value.gameObject, state, true);
		}
	}
	#endregion
};

sealed public class StarRatingScript : MonoBehaviour 
{
	#region Public members shown in Unity Editor.
	public string BaseName;
	public int LevelCount;
	public string LevelOpenSprite;
	public string LevelClosedSprite;
	public bool SetLabel;
	public string LabelPostFix;
	
	public bool m_dynamicScaleOfLabel = true;
	#endregion
	
	#region Private members
	private List<LevelButton> m_levelButtons = new List<LevelButton>();
	private UnityEngine.Color m_originalBgIconColor;
	#endregion
	
	#region Unity Monobehavior overrides.
	// Use this for early initialization
	void Awake ()
	{
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("StarRatingScript.{0} LevelCount: {1}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, LevelCount));
		#endif
		
		for (int i = 1; i <= LevelCount; i++)
		{
			string baseName = string.Format ("{0}{1}", BaseName, i);
			
			GameObject root = GameObject.Find (string.Format ("{0}", baseName));
			
			if (root == null)
				continue;
			
			LevelButton button = new LevelButton();
			button.Init (baseName);
			m_levelButtons.Add (button);
		}
		if (m_levelButtons.Count > 0 && m_levelButtons[0].bgBorderSprite != null)
			m_originalBgIconColor = m_levelButtons[0].bgBorderSprite.color;
	}
	#endregion
	
	#region Public object methods
	[Obfuscar.Obfuscate]
	public void SetScoreData(int index, bool isOpen, int myScore, int bestScore, int rank, int totalCount)
	{
		if (index < 0 || index >= m_levelButtons.Count)
			return;
		
		LevelButton entry = m_levelButtons[index];
		entry.SetScoreState(isOpen);
		if (isOpen)
			entry.SetScoreData(myScore, bestScore, rank, totalCount);
	}
	[Obfuscar.Obfuscate]
	public void SetStateFrom(int index, bool state)
	{
		if (index < 0 || index >= m_levelButtons.Count)
			return;
	
		for (int i = index; i < m_levelButtons.Count; i++)
		{
			m_levelButtons[i].SetScoreState(state);
		}
	}
	
	[Obfuscar.Obfuscate]
	public void InitItems()
	{	
		SaveData data = SaveData.Instance;
		string worldName = data.Config.SelectedWorld;
		xsd_ct_WorldInfo selectedWorld = data.NNUKE_GetWorld(worldName);
		
		if (selectedWorld == null)
		{
			#if DEBUG_TEXTS
			Debug.LogWarning( string.Format ("StarRatingScript.{0} NO SELECTED WORLD PRESENT: {1}"
				, System.Reflection.MethodBase.GetCurrentMethod().ToString()
				, worldName));
			#endif
			
			return;
		}
		
		#if DEBUG_TEXTS
			Debug.Log( string.Format ("StarRatingScript.{0} - levelCount: {1}, buttonCount: {2}"
				, System.Reflection.MethodBase.GetCurrentMethod().ToString()
				, selectedWorld.LevelInfo.Length, m_levelButtons.Count));
		#endif
		
		int levelIndex = 0;
		for (levelIndex = 0 ; levelIndex < selectedWorld.LevelInfo.Length; levelIndex++)
		{
			bool levelOpen = selectedWorld.LevelInfo[levelIndex].Open;
			int playerScore = selectedWorld.LevelInfo[levelIndex].Highscore;
			int starCount = GameData.Instance.NNUKE_GetStarCountFor(selectedWorld.ReferencesTo
				, levelIndex, playerScore);

			xsd_ct_LevelData level = GameData.Instance.NNUKE_GetLevelData(selectedWorld.ReferencesTo, levelIndex);
			SetLevelOpen(levelIndex, levelOpen, starCount, playerScore, false, level.LocalizationKey);
		}
		// ... and the rest rest do not exist
		for (; levelIndex < m_levelButtons.Count; levelIndex++)
		{
			SetLevelOpen(levelIndex, false, 0, 0, true);
		}
		
		#if DEBUG_TEXTS
			Debug.Log( string.Format ("StarRatingScript.{0} exiting"
				, System.Reflection.MethodBase.GetCurrentMethod().ToString()));
		#endif
	}

	[Obfuscar.Obfuscate]
	public void SetLevelOpen(int index, bool open, int stars, int score
		, bool hideBackground, string textLocalizationKey = null)
	{
		#if DEBUG_TEXTS
		Debug.Log( string.Format ("StarRatingScript.{0} index: {1}, open: {2}, buttonCount: {3}"
			, System.Reflection.MethodBase.GetCurrentMethod().ToString()
			, index, open, m_levelButtons.Count));
		#endif
		
		if (index >= m_levelButtons.Count)
		{
			#if DEBUG_TEXTS	
			Debug.Log (string.Format ("StarRatingScript.SetLevelOpen - no button for: {0} (max: {1})"
				, index, m_levelButtons.Count));
			#endif

			return;
		}
		
		LevelButton button = m_levelButtons[index];
		
		// these will be set visible by SetScoreData if at all
		if (button.amongBestLabel != null)
			Utility.SetState(button.amongBestLabel.gameObject, false, true);
		if (button.rankLabel != null)
			Utility.SetState(button.rankLabel.gameObject, false, true);
		if (button.rankDivider != null)
			Utility.SetState(button.rankDivider.gameObject, false, true);
		if (button.rankAllLabel != null)
			Utility.SetState(button.rankAllLabel.gameObject, false, true);
		
		if (button.collider != null)
		{
			button.collider.enabled = open;
		}
		if (button.levelNumberLabel != null)
		{
			if (SetLabel || !string.IsNullOrEmpty(textLocalizationKey))
			{
				button.levelNumberLabel.text = 
					(string.IsNullOrEmpty(textLocalizationKey)) 
						? string.Format ("{0}{1}", index+1, LabelPostFix)
						: Localization.Localize(textLocalizationKey);
				
				if (m_dynamicScaleOfLabel)
				{
					float scale = (button.levelNumberLabel.text.Length > 5) ? 24f : 64f;
					button.levelNumberLabel.transform.localScale = new Vector3(scale, scale, 1f);
				}
			}
			
			NGUITools.SetActiveSelf(button.levelNumberLabel.gameObject, open);
		}
		if (button.bgContentsSprite != null)
		{
			// NOTE! This is needed, even if we hide the button, otherwise null-pointer might occur
			button.bgContentsSprite.spriteName = LevelClosedSprite;
			
			if (!string.IsNullOrEmpty(LevelOpenSprite) && open)
			{
				button.bgContentsSprite.spriteName =  LevelOpenSprite;
				Utility.SetState(button.bgContentsSprite.gameObject, true, false);
			}
			else if (!string.IsNullOrEmpty(LevelClosedSprite) && !open)
			{
				button.bgContentsSprite.spriteName = LevelClosedSprite;
				Utility.SetState(button.bgContentsSprite.gameObject, true, false);
			}
			else if (!string.IsNullOrEmpty(button.originalSprite))
			{
				button.bgContentsSprite.spriteName = button.originalSprite;
				Utility.SetState(button.bgContentsSprite.gameObject, true, false);
			}
			else
			{
				Utility.SetState(button.bgContentsSprite.gameObject, false, false);
			}
		}
		
		if (button.scoreLabel != null)
		{
			button.scoreLabel.text = string.Format ("{0}", score);
			NGUITools.SetActiveSelf(button.scoreLabel.gameObject, open);
		}
		if (button.stars != null)
			NGUITools.SetActiveSelf(button.stars, open);
		
		NGUITools.SetActiveSelf(button.star_1_outline.gameObject, open);
		NGUITools.SetActiveSelf(button.star_2_outline.gameObject, open);
		NGUITools.SetActiveSelf(button.star_3_outline.gameObject, open);
		
		NGUITools.SetActiveSelf(button.star_1_value.gameObject, open && stars > 0);
		NGUITools.SetActiveSelf(button.star_2_value.gameObject, open && stars > 1);
		NGUITools.SetActiveSelf(button.star_3_value.gameObject, open && stars > 2);
		
		if (button.bgBorderSprite != null)
		{
			Utility.SetState(button.bgBorderSprite.gameObject, open || !hideBackground, true);
			button.bgBorderSprite.color = (open) ? m_originalBgIconColor :
						0.5f*m_originalBgIconColor;
		}
		
		if (button.bgContentsSprite != null)
			Utility.SetState(button.bgContentsSprite.gameObject, open || !hideBackground, true);
	}
	#endregion
	
}
