using UnityEngine;
using System.Collections;
using System.Collections.Generic;

sealed public class TextFaderScript : MonoBehaviour 
{
	#region Enums, structs, and inner classes
	public enum State
	{
		Invalid
		, LeadIn
		, FadeIn
		, Stay
		, FadeOut
		, Pause
		, LeadOut
	}
	#endregion

	#region Delegates And Events
	#endregion

	#region Public members, visible in Unity GUI
	public GameObject[] FadingElements;
	public float LeadInTimeSeconds;
	public float LeadOutTimeSeconds;
	public float FadeInTimeSeconds;
	public float StayVisibleSeconds;
	public float FadeOutTimeSeconds;

	public float PauseTimeSeconds;

	public AudioClip m_sound;
	public bool[] SoundOnLabel;
	public float Frequency;
	
	public bool DontUseFadeOutInLast;
	
	public bool Loop;
	
	public bool[] RotateOnLabel;
	
	public GameObject RotateObject;
	public float RotateEulerX;
	public float RotateEulerY;
	public float RotateEulerZ;
	public bool ResetAfterFadeOut;
	public float RotateSecondsAfterFadeOut;
	public bool RotateInFadeOut;
	public bool ChangeDirectionAfterReset;
	
	public GameObject HandlerAfterLeadOut;
	public string FunctionAfterLeadout;
	#endregion

	#region Public members, not visible to Unity GUI
	#endregion

	#region Private data members
	private int m_currentIndex;
	private List<Color> m_originalColors = new List<Color>();
	
	private float m_lastChangeTime;
	private State m_state;
	
	private Vector3 m_rotateSpeed;
	private Quaternion m_originalOrientation;
	
	private UILabel m_currentLabel;
	private UISprite m_currentSprite;
	#endregion


	#region Unity MonoBehaviour base overrides
	
	// Use this for initialization
	void Start ()
	{
		foreach (GameObject obj in FadingElements)
		{
			Color original = Color.white;
			UILabel label = (UILabel) obj.GetComponent<UILabel>();
			if (label != null)
			{
				original = label.color;
				label.color = new Color(original.r, original.g, original.b, 0f);
			}
			else
			{
				UISprite sprite = (UISprite) obj.GetComponent<UISprite>();
				original = sprite.color;
				sprite.color = new Color(original.r, original.g, original.b, 0f);
			}
			
			m_originalColors.Add (original);
		}
		m_state = State.Invalid;
		
		if (RotateObject)
		{
			m_originalOrientation = RotateObject.transform.rotation;
			m_rotateSpeed = new Vector3(RotateEulerX, RotateEulerY, RotateEulerZ);
			NGUITools.SetActive(RotateObject, false);
		}
	}
	
	[Obfuscar.Obfuscate]
	private bool SetCurrentElement(int index)
	{
		m_currentIndex = index;
		bool ok = m_currentIndex >= 0 && m_currentIndex < FadingElements.Length;
		if (ok)
		{
			GameObject obj = FadingElements[m_currentIndex];
			m_currentLabel = (UILabel) obj.GetComponent<UILabel>();
			m_currentSprite = (UISprite) obj.GetComponent<UISprite>();
		}
		else
		{
			m_currentLabel = null;
			m_currentSprite = null;
		}
		return ok;
	}
	
	// Update is called once per frame
	void Update ()
	{
		switch (m_state)
		{
			case State.Invalid:
			{
				m_lastChangeTime = Time.time;
				m_state = State.LeadIn;
				break;
			}
			case State.LeadIn:
			{
				if (Time.time > m_lastChangeTime + LeadInTimeSeconds)
				{
					SetCurrentElement(0);
					m_state = State.FadeIn;
					m_lastChangeTime = Time.time;
					if (RotateObject && RotateOnLabel[0]) NGUITools.SetActive(RotateObject, true);
					if (SoundOnLabel.Length > 0 && SoundOnLabel[0]) 
					{
						SoundManagerScript.Play (gameObject, m_sound, false, true, Frequency);
					}
				}
				break;
			}
			case State.FadeIn:
			{
				if (UpdateAlpha(FadeInTimeSeconds, false))
				{
					m_state = State.Stay;
					m_lastChangeTime = Time.time;
				}
				if (RotateObject) RotateObject.transform.Rotate(Time.deltaTime * m_rotateSpeed);
				break;
			}
			case State.Stay:
			{
				if (Time.time > m_lastChangeTime + StayVisibleSeconds)
				{
					if (DontUseFadeOutInLast && m_currentIndex == FadingElements.Length - 1)
					{
						m_state = State.LeadOut;
					}	
					else
						m_state = State.FadeOut;
				

					m_lastChangeTime = Time.time;
				}
				if (RotateObject) RotateObject.transform.Rotate(Time.deltaTime * m_rotateSpeed);
				return;
				break;
			}
			case State.FadeOut:
			{
				if (UpdateAlpha(FadeOutTimeSeconds, true))
				{
					m_lastChangeTime = Time.time;
				
					bool ongoing = SetCurrentElement(m_currentIndex + 1);
					if (ongoing)
					{
						if (RotateObject) NGUITools.SetActive(RotateObject, RotateOnLabel[m_currentIndex]);
						if (SoundOnLabel.Length > m_currentIndex && SoundOnLabel[m_currentIndex]) 
							SoundManagerScript.Play (gameObject, m_sound, false, true, Frequency);
					
						m_state = State.Pause;
					}
					else
					{
						m_state = State.LeadOut;
					}
				
					if (RotateObject)
					{
						if (ResetAfterFadeOut)
						{
							RotateObject.transform.rotation = m_originalOrientation;
							if (ChangeDirectionAfterReset)
							{
								m_rotateSpeed = -1f * m_rotateSpeed;
							}
						}
					
						RotateObject.transform.Rotate(RotateSecondsAfterFadeOut * m_rotateSpeed);
					}
				}
				if (RotateObject && RotateInFadeOut) RotateObject.transform.Rotate(Time.deltaTime * m_rotateSpeed);
				break;
			}
			case State.Pause:
			{
				if (Time.time > m_lastChangeTime + PauseTimeSeconds)
				{
					m_lastChangeTime = Time.time;
					m_state = State.FadeIn;
				}
				break;
			}
			case State.LeadOut:
			{
				if (Time.time > m_lastChangeTime + LeadOutTimeSeconds)
				{
					m_lastChangeTime = Time.time;

					if (HandlerAfterLeadOut)
					{
						HandlerAfterLeadOut.SendMessage(FunctionAfterLeadout);
					}
					if (Loop)
					{
						m_state = State.LeadIn;
					}
				}
				break;
			}
		}
	}
	
	[Obfuscar.Obfuscate]
	private bool UpdateAlpha(float fadeTime, bool decreasing)
	{
		float ce = (decreasing) ? 0f : 1f;
		bool maxedOut = (fadeTime < 0.01f);
		if (!maxedOut)
		{
			ce = (Time.time - m_lastChangeTime) / fadeTime;
			maxedOut = ce > 1f;
			ce = Mathf.Min (1f, ce);
			if (decreasing)
			{
				ce = 1f - ce;
			}
		}
		Color original = m_originalColors[m_currentIndex];
		if (m_currentLabel != null)
		{
			m_currentLabel.color = new Color(original.r, original.g, original.b, ce);
		}
		if (m_currentSprite != null)
		{
			m_currentSprite.color = new Color(original.r, original.g, original.b, ce);
		}
		
		return maxedOut;
	}
	
	
	#endregion
		
	
}
