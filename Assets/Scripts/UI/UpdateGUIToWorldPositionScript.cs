using UnityEngine;
using System.Collections;

sealed public class UpdateGUIToWorldPositionScript : MonoBehaviour 
{
	public Camera WorldCamera;
	public Camera UICamera;
	public Vector3 WorldPosition;
	
	private TweenPosition m_positionTweener = null;
	
	void Start()
	{
		m_positionTweener = (TweenPosition) gameObject.GetComponentInChildren<TweenPosition>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		Vector3 pos = WorldCamera.WorldToViewportPoint(WorldPosition);
		pos = UICamera.ViewportToWorldPoint(pos);
		pos.z = 0f;
		gameObject.transform.position = pos;
		
		if (m_positionTweener != null)
		{
			m_positionTweener.from = pos;
			m_positionTweener.to = new Vector3(pos.x, pos.y + 0.25f, pos.z);
		}
	}
}
