using UnityEngine;
using System.Collections;

sealed public class VictoryScreenScript : MonoBehaviour 
{
	public GameObject m_root;
	
	#region Enums, structs, and inner classes
	sealed public class LevelStars
	{
		public GameObject root;

		public UISlicedSprite star_1_outline;
		public UISlicedSprite star_1_value;
		public UISlicedSprite star_2_outline;
		public UISlicedSprite star_2_value;
		public UISlicedSprite star_3_outline;
		public UISlicedSprite star_3_value;
	};
	#endregion

	#region Delegates And Events
	#endregion

	#region Public members, visible in Unity GUI
	public StatusBoxScript m_statusBox;
	public float StartTimeDelaySeconds;
	
	public GameObject m_nextButton;
	public GameObject m_menuButton;

	public GameObject m_bonusRootBullets;
	public GameObject m_bonusRootEnemies;
	public GameObject m_bonusRootEnergy;
	public GameObject m_titleLabel;

	public UILabel m_enemiesKilledLabel;
	public UILabel m_enemiesTotalLabel;
	public UILabel m_enemiesBonusLabel;
	
	public UILabel m_bulletsHitLabel;
	public UILabel m_bulletsTotalLabel;
	public UILabel m_bulletsBonusLabel;
	
	public UILabel m_healthLabel;
	public UILabel m_healthBonusLabel;
	
	public UILabel m_scoreLabel;
	public UILabel m_bestScoreLabel;

	public UILabel m_newBestInfoLabel;
	public UILabel m_oldBestInfoLabel;

	public UILabel m_timeLabel;
	
	public AudioClip m_appearsSound;

	#endregion

	#region Public members, not visible to Unity GUI
	#endregion

	#region Private data members
	private SocialLayerManagerScript.StateChanged m_fbHandler = null;
	private TwitterManagerScript.StateChanged m_twitterHandler = null;
	
	private LevelScript m_level;
	private LevelStars m_stars;
	
	private float m_timeToStartScreen;
	
	#endregion

	#region Unity MonoBehaviour base overrides	
	void Awake ()
	{
		const string rootName = "UI Victory Screen/Camera/Anchor, center/panel";
		
		m_stars = new LevelStars();
		m_stars.root = Utility.GetObject(string.Format ("levelStars", rootName));
		
		m_stars.star_1_outline = 
			Utility.GetObject (string.Format ("{0}/levelStars/1/outlineRoot/icon", rootName)).GetComponent<UISlicedSprite>();
		m_stars.star_1_value = 
			Utility.GetObject (string.Format ("{0}/levelStars/1/valueRoot/icon", rootName)).GetComponent<UISlicedSprite>();
		m_stars.star_2_outline = 
			Utility.GetObject (string.Format ("{0}/levelStars/2/outlineRoot/icon", rootName)).GetComponent<UISlicedSprite>();
		m_stars.star_2_value = 
			Utility.GetObject (string.Format ("{0}/levelStars/2/valueRoot/icon", rootName)).GetComponent<UISlicedSprite>();
		m_stars.star_3_outline = 
			Utility.GetObject (string.Format ("{0}/levelStars/3/outlineRoot/icon", rootName)).GetComponent<UISlicedSprite>();
		m_stars.star_3_value = 
			Utility.GetObject (string.Format ("{0}/levelStars/3/valueRoot/icon", rootName)).GetComponent<UISlicedSprite>();

		m_level = (LevelScript) GameObject.FindObjectOfType(typeof(LevelScript));
		
		if (m_level != null)
		{
			m_level.Ended += new LevelScript.LevelEnded(LevelEnded);
		}

		Utility.SetState(m_root, false, false);
		m_timeToStartScreen = -1f;
	}

	// Update is called once per frame
	void Update ()
	{
		if (m_timeToStartScreen > 0 && GameStateScript.Instance.NNUKE_GetWorldTime() > m_timeToStartScreen)
		{
			MusicManagerScript.Instance.PlayClip("victory_screen", MusicManagerScript.SwitchMode.CrossFade, false);
			
			m_timeToStartScreen = -1f;
			Utility.SetState(m_root, true, false);

			Utility.SetState(m_newBestInfoLabel.gameObject, PlayerHomePlanetScript.Instance.MadeNewHighScore, false);
			Utility.SetState(m_oldBestInfoLabel.gameObject, !PlayerHomePlanetScript.Instance.MadeNewHighScore, false);
			Utility.SetState(m_bestScoreLabel.gameObject, !PlayerHomePlanetScript.Instance.MadeNewHighScore, false);
			
			SetStarCount (true);
			SoundManagerScript.PlayOneTime (gameObject, m_appearsSound);
			
			if (AssignGamepadButtonsScript.Instance != null)
			{
				AssignGamepadButtonsScript.Instance.AssignButtons(true);
			}

			if (OuyaInputWrapper.UseGamepad)
			{
				UICamera.selectedObject = m_nextButton;
			}
		}
	}
	#endregion
		
	#region Public object methods
	public void LevelEnded(LevelScript sender)
	{
		// how did it go? if well, we enable ourselves
		if (sender.Outcome == LevelScript.Result.Success || sender.m_isNeverEnding)
		{
			m_scoreLabel.text = string.Format ("{0}", PlayerHomePlanetScript.Instance.Score);
			
			m_enemiesKilledLabel.text = string.Format ("{0}", PlayerHomePlanetScript.Instance.EnemiesKilled);
			m_enemiesTotalLabel.text = string.Format ("{0}", PlayerHomePlanetScript.Instance.EnemiesEncountered);
			m_enemiesBonusLabel.text = string.Format ("{0}", PlayerHomePlanetScript.Instance.BonusEnemies);

			m_bulletsHitLabel.text = string.Format ("{0}", PlayerHomePlanetScript.Instance.BulletsHit);
			m_bulletsTotalLabel.text = string.Format ("{0}", PlayerHomePlanetScript.Instance.BulletsFired);
			m_bulletsBonusLabel.text = string.Format ("{0}", PlayerHomePlanetScript.Instance.BonusBullets);

			m_healthLabel.text = string.Format ("{0}", (int) (100f*PlayerHomePlanetScript.Instance.DamageTaker.EnergyNormalized));
			m_healthBonusLabel.text = string.Format ("{0}", PlayerHomePlanetScript.Instance.BonusHealth);

			if (!PlayerHomePlanetScript.Instance.MadeNewHighScore)
			{
				xsd_ct_LevelInfo level = SaveData.Instance.NNUKE_GetCurrentLevel();
				if (level != null)
					m_bestScoreLabel.text = string.Format ("{0}", level.Highscore);
			}
			
			if (sender.m_isNeverEnding)
			{
				Utility.SetState(m_bonusRootBullets, false, true);
				Utility.SetState(m_bonusRootEnemies, false, true);
				Utility.SetState(m_bonusRootEnergy, false, true);
				Utility.SetState(m_titleLabel, false, true);
			}
			
			int seconds = (int) PlayerHomePlanetScript.Instance.PlayTime;
			m_timeLabel.text = string.Format ("{0}:{1:00}", seconds / 60, seconds % 60);

			m_timeToStartScreen = GameStateScript.Instance.NNUKE_GetWorldTime() + StartTimeDelaySeconds;
			
			GameStateScript.Instance.InputDisabled = true;
		}
	}
	
	public void OnMessageNext()
	{
		bool isLast = SaveData.Instance.NNUKE_IsCurrentLastOpenLevel();

		if (!isLast && !LevelScript.Instance.m_isNeverEnding)
		{
			SaveData.Instance.NNUKE_ChooseNextLevel();
		}
		
		Application.LoadLevel("garage");
	}
	
	public void OnMessageBack()
	{
		// NOTE! This has been redesigned to Retry same level even if got through
		string sceneName = SaveData.Instance.NNUKE_GetCurrentSceneName();
		if (!string.IsNullOrEmpty(sceneName))
		{			
			Application.LoadLevel(sceneName);
		}
		else
		{
			Application.LoadLevel("garage");
		}
	}
	
	public void OnMessageFacebook()
	{
		#if ENABLE_LUMOS
		Lumos.Event("VictoryScreen - Facebook");
		#endif
		
		if (SocialLayerManagerScript.GetInstance() != null)
		{
			m_statusBox.Show(
				Localization.Localize("menu_statusbox_title_fb")
				, Localization.Localize("menu_statusbox_processing")
				, 20.0f);
			
			if (m_fbHandler == null) 
			{
				m_fbHandler = new SocialLayerManagerScript.StateChanged(FacebookChanged);
			}
			SocialLayerManagerScript.GetInstance().Changed += m_fbHandler;
			
			string worldName = SaveData.Instance.Config.SelectedWorld;
			xsd_ct_WorldInfo world = SaveData.Instance.NNUKE_GetWorld(worldName);

			string captionKey = PlayerHomePlanetScript.Instance.MadeNewHighScore ?
				"menu_fb_scores_caption_best" : "menu_fb_scores_caption";
			
			string message = Localization.Localize(captionKey);
			message += string.Format (" {0} {1} - {2} {3} : {4}"
				, Localization.Localize("menu_title_world"), worldName
				, Localization.Localize("menu_title_level"), world.SelectedLevel + 1
				, PlayerHomePlanetScript.Instance.Score);

			SocialLayerManagerScript.OpenDialog(
				Localization.Localize("menu_fb_url")
				, message
				, Localization.Localize("menu_fb_image_url")
				, Localization.Localize("menu_fb_image_caption")
				, message
				);
		}
		else 
		{
			FacebookChanged(SocialLayerManagerScript.RequestState.Fail);
		}
	}

	public void OnMessageTwitter()
	{
		#if ENABLE_LUMOS
		Lumos.Event("VictoryScreen - Twitter");
		#endif
		
		if (TwitterManagerScript.GetInstance() != null) 
		{
			m_statusBox.Show(
				Localization.Localize("menu_statusbox_title_twitter")
				, Localization.Localize("menu_statusbox_processing")
				, 20.0f);
			
			if (m_twitterHandler == null) 
			{
				m_twitterHandler = new TwitterManagerScript.StateChanged(TwitterChanged);
			}
			TwitterManagerScript.GetInstance().Changed += m_twitterHandler;
			
			string worldName = SaveData.Instance.Config.SelectedWorld;
			xsd_ct_WorldInfo world = SaveData.Instance.NNUKE_GetWorld(worldName);
			
			string message = string.Format ("{0} {1} - {2} {3} : {4}"
				, Localization.Localize("menu_title_world"), worldName
				, Localization.Localize("menu_title_level"), world.SelectedLevel + 1
				, PlayerHomePlanetScript.Instance.Score);

			string captionKey = PlayerHomePlanetScript.Instance.MadeNewHighScore ?
				"menu_twitter_scores_caption_best" : "menu_twitter_scores_caption";

			string msg = Localization.Localize(captionKey) 
				+ " " 
				+ message 
				+ " "
				+ Localization.Localize("menu_twitter_scores_postfix");
			
			TwitterManagerScript.Post(msg);
		}
		else 
		{
			TwitterChanged(TwitterManagerScript.RequestState.Fail);
		}
	}
	
	
	private void FacebookChanged(SocialLayerManagerScript.RequestState state) 
	{
#if DEBUG_TEXTS	
		Debug.Log (string.Format ("VictoryScreenScript.FacebookChanged - state: {0}, time: {1}", state, Time.time));
#endif
		
		StatusBoxScript.ShowFacebookStatus(m_statusBox, state, m_fbHandler);
	}
	
	private void TwitterChanged(TwitterManagerScript.RequestState state) 
	{
		
#if DEBUG_TEXTS	
		Debug.Log (string.Format ("VictoryScreenScript.TwitterChanged - state: {0}, time: {1}", state, Time.time));
#endif
		StatusBoxScript.ShowTwitterStatus(m_statusBox, state, m_twitterHandler);
	}	
	
	
	#endregion
		
	#region Private object methods
	[Obfuscar.Obfuscate]
	private void SetStarCount(bool flag)
	{
		int starCount = PlayerHomePlanetScript.Instance.StarCountForScore;
		
		NGUITools.SetActive(m_stars.root, flag);
		
		NGUITools.SetActive(m_stars.star_1_outline.gameObject, flag);
		NGUITools.SetActive(m_stars.star_2_outline.gameObject, flag);
		NGUITools.SetActive(m_stars.star_3_outline.gameObject, flag);
		
		NGUITools.SetActive(m_stars.star_1_value.gameObject, flag && starCount > 0);
		NGUITools.SetActive(m_stars.star_2_value.gameObject, flag && starCount > 1);
		NGUITools.SetActive(m_stars.star_3_value.gameObject, flag && starCount > 2);
		
	}
	
	#endregion
	
}
