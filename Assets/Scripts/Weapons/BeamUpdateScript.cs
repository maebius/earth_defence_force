using UnityEngine;
using System.Collections;

sealed public class BeamUpdateScript : MonoBehaviour 
{
	#region Public members, visible in Unity GUI
	public float ScaleChangePerSecond;
	#endregion

	#region Public members, not visible to Unity GUI
	#endregion

	#region Private data members
	private VisualizerScript m_visualizerScript;
	
	private ProjectileScript m_projectile;
	#endregion


	#region Unity MonoBehaviour base overrides
		
	// Use this for initialization
	void Start ()
	{
		m_visualizerScript = GetComponent<VisualizerScript>();
		m_projectile = GetComponent<ProjectileScript>();
		
		float ce = m_projectile.Speed / m_projectile.SpeedLevels[0];
		
		float m_originalScaleChange = ScaleChangePerSecond;
		ScaleChangePerSecond = m_originalScaleChange * ce;
		
		float visScaleOriginal = m_visualizerScript.ScaleAdditionFactor;
		m_visualizerScript.ScaleAdditionFactor = visScaleOriginal * ce;
	}
	
	public void FixedUpdate()
	{
		float addition =  ScaleChangePerSecond * GameStateScript.Instance.NNUKE_GetWorldDeltaTime();
		
		transform.localScale = new Vector3(
			transform.localScale.x + addition
			, transform.localScale.y + addition
			, transform.localScale.z);
	}
	
	#endregion

}
