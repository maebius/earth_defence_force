using UnityEngine;
using System.Collections;

sealed public class ElectrifierUpdateScript : MonoBehaviour {

	#region Public members, visible in Unity GUI
	public float RangeLaunchFactor;
	public float DecayTimeSeconds;
	
	public GameObject PathVisualizationPrefab;
	public GameObject FireVisualizationPrefab;
	public GameObject NoTargetVisualizationPrefab;
	#endregion

	#region Private data members
	private ProjectileScript m_projectileScript;
	
	private GameObject m_targetObject;
	private Vector3 m_targetPoint;
	private Vector3 m_targetScale;
	
	private float m_startTimeSeconds;
	private float m_depleteTimeSeconds;
	private bool m_decaying;
	private int m_visualizationsFired;
	
	private float m_rangeReach;
	private float m_rangeLaunch;
	
	private bool m_gotInitialTarget;
	#endregion


	#region Unity MonoBehaviour base overrides
		
	// Use this for initialization
	void Start ()
	{
		m_projectileScript = GetComponent<ProjectileScript>();
		m_startTimeSeconds = GameStateScript.Instance.NNUKE_GetWorldTime();
		m_decaying = false;
		
		m_rangeReach = m_projectileScript.Range;
		m_rangeLaunch = RangeLaunchFactor * m_rangeReach;
		
		m_targetScale = transform.localScale;
		// start as zero
		transform.localScale = new Vector3(m_targetScale.x, m_targetScale.y, 0.001f);
		
		m_visualizationsFired = 0;
		m_startTimeSeconds = GameStateScript.Instance.NNUKE_GetWorldTime();
		
		m_gotInitialTarget = DecideTarget();
		
		GameObject startViz = (m_targetObject != null) ?
			FireVisualizationPrefab : NoTargetVisualizationPrefab;
		
		LaunchVisualization(startViz, transform.position);
	}
	
	[Obfuscar.Obfuscate]
	private void LaunchVisualization(GameObject vizPrefab, Vector3 pos)
	{
		if (vizPrefab == null)
			return;
		
		GameObject vizClone = (GameObject) Instantiate(
			PathVisualizationPrefab
			, pos
			, transform.rotation * PathVisualizationPrefab.transform.localRotation);
		
		vizClone.active = true;
	}
	
	[Obfuscar.Obfuscate]
	private bool DecideTarget()
	{
		DamageTakerScript enemy = LevelScript.Instance.GetClosestEnemyTo(transform.position, m_rangeLaunch);
		m_targetObject = (enemy != null) ? enemy.gameObject : null;
		return m_targetObject != null;
	}
	
	[Obfuscar.Obfuscate]
	private Vector3 GetTargetPoint(out Vector3 look, out float distance, Vector3 targetPosition)
	{
		Transform reference = m_projectileScript.Parent.transform;
		look = targetPosition - reference.position;
		distance = Mathf.Min (m_rangeReach, look.magnitude);
		look.Normalize();
		
		return reference.position + distance * look;
	}
	
	// Update is called once per frame
	void Update ()
	{
		float now = GameStateScript.Instance.NNUKE_GetWorldTime();
		float deltaSeconds = now - m_startTimeSeconds;

		if (!m_gotInitialTarget || (m_decaying && m_depleteTimeSeconds < now))
		{
			m_projectileScript.DepleteMyself();
			return;
		}
		
		Vector3 lookDirection;
		float distance;
		
		m_targetPoint = GetTargetPoint(out lookDirection, out distance
				, (m_targetObject != null) 
					? m_targetObject.transform.position
					: m_targetPoint
			);
		
		Vector3 startPos = m_projectileScript.Parent.transform.position;
		float zScale = m_projectileScript.Speed * deltaSeconds;
		
		float xyScale = 1f;
		// need a bit scaling down to squeeze the chance of numerical errors away
		if (zScale >= m_rangeReach*0.99f || zScale >= distance)
		{
			zScale = Mathf.Min (distance, zScale);
			zScale = Mathf.Min (m_rangeReach, zScale);
			
			if (!m_decaying)
			{
				m_decaying = true;
				m_depleteTimeSeconds = now + DecayTimeSeconds;
				
				if (PathVisualizationPrefab && m_visualizationsFired < 1)
				{
					LaunchVisualization(PathVisualizationPrefab, startPos + zScale*lookDirection);
					m_visualizationsFired++;
				}
			}
			xyScale = (m_depleteTimeSeconds - now)/DecayTimeSeconds;
		}
		
		transform.position = startPos + 0.5f*zScale*lookDirection;
		transform.localScale = new Vector3(xyScale * m_targetScale.x, xyScale * m_targetScale.y, zScale);
		transform.LookAt(m_targetPoint, Vector3.left);
	}
	#endregion
}
