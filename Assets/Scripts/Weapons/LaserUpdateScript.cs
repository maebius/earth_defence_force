using UnityEngine;
using System.Collections;

sealed public class LaserUpdateScript : MonoBehaviour 
{
	#region Public data members
	public VisualizerScript m_baseVisualization = null;
	public VisualizerScript m_tipVisualization = null;
	#endregion
	
	
	#region Private data members
	private ProjectileScript m_projectileScript;
	
	private float m_lastHitTime = -1f;
	private float m_lastHitRange = -1f;
	#endregion

	#region Unity MonoBehaviour base overrides
		
	// Use this for initialization
	void Start ()
	{
		m_projectileScript = GetComponent<ProjectileScript>();
		
		transform.localScale = new Vector3(
			transform.localScale.x
			, transform.localScale.y
			, m_projectileScript.Range);
		
		// this is needed in order not to have one-couple-frame errors ...
		LateUpdate ();
	}

	void LateUpdate ()
	{
		if (!m_projectileScript.Parent.Active)
		{
			m_projectileScript.DepleteMyself();
			return;
		}
		// we move when our parent moves
		Transform reference = m_projectileScript.Parent.transform;
		Vector3 look = reference.rotation * Vector3.forward;
		
		Vector3 targeting = Quaternion.AngleAxis(
			m_projectileScript.Parent.OffsetTargetingAngle
			, Vector3.forward) * look;
		targeting.Normalize();
		
		float range = m_projectileScript.Range;
		
		if (m_projectileScript.IsHitting)
		{
			DamageTakerScript closest = m_projectileScript.GetClosestToucherTo(reference.position);
			if (closest != null)
			{
				Vector3 collisionCenter = closest.GetComponent<Collider>().bounds.center;
				range = (collisionCenter - reference.position).magnitude;
				//Utility.DrawCircleXY(collisionCenter, 12f, 10, Color.yellow);
			}
		}
		
		float now = GameStateScript.Instance.NNUKE_GetWorldTime();
		if (range > m_lastHitRange && now < m_lastHitTime + 0.15f)
		{
			range = m_lastHitRange;
		}
		if (m_projectileScript.IsHitting)
		{
			m_lastHitTime = now;
			m_lastHitRange = range;
		}
		
		float halfRange = 0.5f * range;
		
		transform.position = reference.position + halfRange * targeting;
		transform.rotation = reference.rotation;

		if (m_tipVisualization != null)
		{
			m_tipVisualization.PositionOffset = halfRange;
		}
		if (m_baseVisualization != null)
		{
			m_baseVisualization.PositionOffset = -halfRange + 20f;
		}

		transform.localScale = new Vector3(
			transform.localScale.x
			, transform.localScale.y
			, 2f*halfRange);
	}
	#endregion
}
