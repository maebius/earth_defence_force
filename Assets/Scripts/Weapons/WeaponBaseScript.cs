using UnityEngine;
using System.Collections;
using System.Collections.Generic;

sealed public class WeaponBaseScript : MonoBehaviour 
{
	#region Enums, structs, and inner classes
	#endregion

	#region Delegates And Events
	#endregion

	#region Public members, visible in Unity GUI
	public float[] FireRateLevels;
	
	public bool IsSecondary;
	
	public float OffsetRadius;
	public float OffsetPositionAngle;
	public float OffsetTargetingAngle;
	
	public bool AllowConcurrentProjectiles;
	
	public ProjectileScript ProjectilePrefab;
	
	public GameObject FiringVisualizationPrefab;
	public float FiringVizOffset;
	
	public float m_continousDepletionRate = 0f;
	public float m_constantDepletion = 0f;
	public float m_restoreRate = 0f;
	public float m_usabilityLimit = 0f;
	#endregion

	#region Public members, not visible to Unity GUI
	[HideInInspector]
	public bool Active
	{
		get { return m_active; }
	}
	[HideInInspector]
	public xsd_ct_Equipment m_config = null;
	
	[HideInInspector]
	public float Energy
	{
		get { return m_energyLevel; }
	}
	
	#endregion

	#region Private data members
	private bool m_active;
	private float m_lastShootTime;
	
	private Vector3 m_bulletStartPoint;
	private Vector3 m_bulletStartDirection;
	
	private int m_speedLevel = 0;
	private int m_powerLevel = 0;
	private int m_rangeLevel = 0;
	
	private float m_fireRate = 0f;
	
	private float m_energyLevel = 1f;
	private float m_allowActiveOnLevel = 0f;
	
	private List<ProjectileScript> m_aliveProjectiles = new List<ProjectileScript>();
	#endregion


	#region Unity MonoBehaviour base overrides
		
	// Use this for initialization
	void Start ()
	{
		m_active = false;
		m_lastShootTime = -100f;
		
		GameData gd = GameData.Instance;
		
		if (FireRateLevels.Length > 0)
		{
			int index = gd.NNUKE_GetSlotPropertyIndex(m_config, "Rate");
			m_fireRate = FireRateLevels[index];
			
			// NOTE! This is a fixed behavior, we want
			// to reduce depletion rate if rate increases so that not all are shot in one burst ...
			m_constantDepletion -= 0.5f * m_constantDepletion * index / FireRateLevels.Length;
		}

		m_speedLevel = gd.NNUKE_GetSlotPropertyIndex(m_config, "Speed");
		m_powerLevel = gd.NNUKE_GetSlotPropertyIndex(m_config, "Power");
		m_rangeLevel = gd.NNUKE_GetSlotPropertyIndex(m_config, "Range");
	}
	
	public void UpdateEnergy()
	{
		m_energyLevel += m_restoreRate * GameStateScript.Instance.NNUKE_GetWorldDeltaTime();
		m_energyLevel = Mathf.Min (1f, m_energyLevel);
	}
	
	// Update is called once per frame
	void Update ()
	{
		float lastFireDelta = GameStateScript.Instance.NNUKE_GetWorldTime() - m_lastShootTime;
		float dt = GameStateScript.Instance.NNUKE_GetWorldDeltaTime();
		float toShoot =  m_constantDepletion + dt * m_continousDepletionRate;
			
		if (m_active 
			&& lastFireDelta >= m_fireRate 
			&& m_energyLevel >=  m_allowActiveOnLevel
			&& m_energyLevel >= toShoot)
		{
			ExecuteShoot();
			
			m_allowActiveOnLevel = 0f;
			m_energyLevel -= toShoot;
			m_energyLevel = Mathf.Max (0f, m_energyLevel);
			// mark this for next round, act as a a kind of cooling down
			// time for continous weapons (laser, so that it does not flicker)
			if (m_energyLevel < toShoot)
			{
				m_active = false;
				m_allowActiveOnLevel = m_usabilityLimit;
			}
		}
	}
	#endregion
		
	#region Public object methods
	[Obfuscar.Obfuscate]
	public void SetActive(bool flag)
	{
		m_active = flag;
	}
	
	public void ProjectileDepleted(ProjectileScript sender)
	{
		m_aliveProjectiles.Remove(sender);
	}
	
	#endregion
		
	#region Private object methods
	[Obfuscar.Obfuscate]
	private void ExecuteShoot()
	{
		if (ProjectilePrefab == null)
			return;
		
		if (!AllowConcurrentProjectiles && m_aliveProjectiles.Count > 0)
			return;
		
		// where do we shoot? parent (the slot) actually decides, you should rotate the parent of this object
		// to determine this
		Quaternion rotation = transform.rotation;
		Vector3 look = rotation * Vector3.forward;
		
		look.Normalize();
		
		ProjectileScript bulletClone = (ProjectileScript) Instantiate(
			ProjectilePrefab
			, transform.position + OffsetRadius * look
			, transform.rotation * ProjectilePrefab.transform.localRotation);
		
		bulletClone.Owner = (IsSecondary)
			? ProjectileScript.OwnerType.PlayerSecondary 
			: ProjectileScript.OwnerType.PlayerPrimary;

		if (bulletClone.SpeedLevels.Length > 0)
			bulletClone.Speed = bulletClone.SpeedLevels[m_speedLevel];
		if (bulletClone.PowerLevels.Length > 0)
			bulletClone.Power = bulletClone.PowerLevels[m_powerLevel];
		if (bulletClone.RangeLevels.Length > 0)
			bulletClone.Range = bulletClone.RangeLevels[m_rangeLevel];

		bulletClone.gameObject.GetComponent<Rigidbody>().velocity = look * bulletClone.Speed;
		bulletClone.Parent = this;
		bulletClone.Depleted += new ProjectileScript.ProjectileDepleted(ProjectileDepleted);
		
		m_aliveProjectiles.Add (bulletClone);

		if (FiringVisualizationPrefab)
		{
			GameObject vizClone = (GameObject) Instantiate(
				FiringVisualizationPrefab
				, transform.position + FiringVizOffset * look
				, transform.rotation * FiringVisualizationPrefab.transform.localRotation);
			vizClone.active = true;
		}
		
		m_lastShootTime = GameStateScript.Instance.NNUKE_GetWorldTime();
		
		if (!IsSecondary)
		{
			PlayerHomePlanetScript.Instance.BulletsFired++;
		}
	}

	#endregion
}
