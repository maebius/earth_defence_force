using UnityEngine;
using System.Collections;
using System.Collections.Generic;

sealed public class WeaponSystemScript : MonoBehaviour 
{
	#region Public GUI tweakable members
	public GameObject Root;
	
	public string[] PrimaryWeaponIds;
	public WeaponBaseScript[] PrimaryWeapons;
	public string[] SecondaryWeaponIds;
	public WeaponBaseScript[] SecondaryWeapons;
	
	public GameObject m_changeVizualisation;
	#endregion
	
	#region Public members, not visible in GUI
	[HideInInspector]
	public static WeaponSystemScript Instance
	{
		get { return sm_instance; }
	}
	#endregion
	
	#region Private data members
	private static WeaponSystemScript sm_instance = null;
	
	private bool m_doShoot = false;

	private WeaponBaseScript m_currentPrimaryWeapon;
	private Transform m_primarySlot;
	private List<WeaponBaseScript> m_weaponsPrimary = new List<WeaponBaseScript>();
	private List<WeaponBaseScript> m_weaponsSecondary = new List<WeaponBaseScript>();

	private Transform m_secondaryLeftFirstSlot;
	private Transform m_secondaryLeftSecondSlot;
	private Transform m_secondaryRightFirstSlot;
	private Transform m_secondaryRightSecondSlot;
	
	private int m_weaponIndex;
	
	private Dictionary<WeaponBaseScript, HealthBarScript> m_healthBars = 
		new Dictionary<WeaponBaseScript, HealthBarScript>();
	
	#endregion
	
	#region Unity MonoBehaviour base overrides
	// Use this for early initialization
	void Awake ()
	{
		sm_instance = this;
		m_primarySlot = Utility.GetChildRecursive(Root, "PrimarySlot", true).transform;
		m_secondaryLeftFirstSlot = Utility.GetChildRecursive(Root, "SecondaryLeftFirstSlot", true).transform;
		m_secondaryLeftSecondSlot = Utility.GetChildRecursive(Root, "SecondaryLeftSecondSlot", true).transform;
		m_secondaryRightFirstSlot = Utility.GetChildRecursive(Root, "SecondaryRightFirstSlot", true).transform;
		m_secondaryRightSecondSlot = Utility.GetChildRecursive(Root, "SecondaryRightSecondSlot", true).transform;
	
	}
	
	
	// Use this for initialization
	void Start ()
	{
		transform.position = PlayerHomePlanetScript.Instance.transform.position;

		PlayerHomePlanetScript.Instance.Died += new PlayerHomePlanetScript.PlayerDied(PlayerDied);
		
		xsd_ct_Equipment wp_1 = SaveData.Instance.NNUKE_GetWeaponPrimary(SaveData.Instance.Config.WeaponPrimarySlot_1);
		xsd_ct_Equipment wp_2 = SaveData.Instance.NNUKE_GetWeaponPrimary(SaveData.Instance.Config.WeaponPrimarySlot_2);
		xsd_ct_Equipment wp_3 = SaveData.Instance.NNUKE_GetWeaponPrimary(SaveData.Instance.Config.WeaponPrimarySlot_3);

		xsd_ct_Equipment ws_l_1 = 
			SaveData.Instance.NNUKE_GetWeaponSecondary(SaveData.Instance.Config.WeaponSecondarySlotLeft_1);
		xsd_ct_Equipment ws_l_2 = 
			SaveData.Instance.NNUKE_GetWeaponSecondary(SaveData.Instance.Config.WeaponSecondarySlotLeft_2);
		xsd_ct_Equipment ws_r_1 = 
			SaveData.Instance.NNUKE_GetWeaponSecondary(SaveData.Instance.Config.WeaponSecondarySlotRight_1);
		xsd_ct_Equipment ws_r_2 = 
			SaveData.Instance.NNUKE_GetWeaponSecondary(SaveData.Instance.Config.WeaponSecondarySlotRight_2);
		
		AddWeapon(wp_1, ref PrimaryWeaponIds, ref PrimaryWeapons
			, m_primarySlot, ref m_weaponsPrimary
			, SaveData.Instance.Config.WeaponPrimarySlot_1 == SaveData.Instance.Config.WeaponPrimarySelected);
		AddWeapon(wp_2, ref PrimaryWeaponIds, ref PrimaryWeapons
			, m_primarySlot, ref m_weaponsPrimary
			, SaveData.Instance.Config.WeaponPrimarySlot_2 == SaveData.Instance.Config.WeaponPrimarySelected);
		AddWeapon(wp_3, ref PrimaryWeaponIds, ref PrimaryWeapons
			, m_primarySlot, ref m_weaponsPrimary
			, SaveData.Instance.Config.WeaponPrimarySlot_3 == SaveData.Instance.Config.WeaponPrimarySelected);
		
		AddWeapon(ws_l_1, ref SecondaryWeaponIds, ref SecondaryWeapons
			, m_secondaryLeftFirstSlot, ref m_weaponsSecondary, true);
		AddWeapon(ws_l_2, ref SecondaryWeaponIds, ref SecondaryWeapons
			, m_secondaryLeftSecondSlot, ref m_weaponsSecondary, true);
		AddWeapon(ws_r_1, ref SecondaryWeaponIds, ref SecondaryWeapons
			, m_secondaryRightFirstSlot, ref m_weaponsSecondary, true);
		AddWeapon(ws_r_2, ref SecondaryWeaponIds, ref SecondaryWeapons
			, m_secondaryRightSecondSlot, ref m_weaponsSecondary, true);
		
		// this is needed for initially showing the health bar
		SetWeaponState(m_currentPrimaryWeapon, true, m_primarySlot);
	}
	
	[Obfuscar.Obfuscate]
	public void ExecuteMovement()
	{
		// NOTE! This is called from GamePlayCamera
		// -> as weapons are bound to camera rotation, this needs to be executed immediately after
		// camera rotation update
		
		if (PlayerHomePlanetScript.Instance == null)
			return;
		
		// force set the location, for weapons system, it is the same as player center
		transform.position = PlayerHomePlanetScript.Instance.transform.position;

		// just "look at the same direction as camera" -> orients the weapon system straight ahead
		transform.LookAt(
			transform.position + GamePlayCamera.Instance.LookNonTilted
			, GamePlayCamera.Instance.CameraLeft);
	}
	
	void Update ()
	{
		if (PlayerHomePlanetScript.IsDead)
		{
			return;
		}		

		if (m_currentPrimaryWeapon)
		{
			m_currentPrimaryWeapon.SetActive(m_doShoot);
			
			if (m_healthBars.ContainsKey(m_currentPrimaryWeapon))
			{
				m_healthBars[m_currentPrimaryWeapon].EnergyChanged(m_currentPrimaryWeapon.Energy);
			}
		}
		foreach (WeaponBaseScript p in m_weaponsPrimary)
		{
			if (p != null)
			{
				p.UpdateEnergy();
			}
		}
		
		foreach (WeaponBaseScript secondary in m_weaponsSecondary)
		{
			secondary.UpdateEnergy();
			secondary.SetActive(m_doShoot);
			if (m_healthBars.ContainsKey(secondary))
			{
				m_healthBars[secondary].EnergyChanged(secondary.Energy);
			}
		}
	}
	#endregion
		
	#region Public object methods
	[Obfuscar.Obfuscate]
	public bool SetActiveWeaponPrimary(int weaponIndex)
	{
		if (weaponIndex < 0 
			|| weaponIndex >= m_weaponsPrimary.Count
			|| m_weaponsPrimary[weaponIndex] == m_currentPrimaryWeapon 
			|| m_weaponsPrimary[weaponIndex] == null)
			return false;
		
		if (m_changeVizualisation)
		{
			GameObject obj = (GameObject) Instantiate(
				m_changeVizualisation
				, m_primarySlot.position
				, m_primarySlot.rotation);
			obj.active = true;
		}
		
		m_weaponIndex = weaponIndex;
		// make sure the current one will cease to function
		SetWeaponState(m_currentPrimaryWeapon, false, m_primarySlot);
		m_currentPrimaryWeapon = m_weaponsPrimary[weaponIndex];
		SetWeaponState(m_currentPrimaryWeapon, true, m_primarySlot);
		
		return true;
	}
	[Obfuscar.Obfuscate]
	public int GetActiveWeaponPrimary()
	{
		return m_weaponIndex;
	}
	[Obfuscar.Obfuscate]
	public void Shoot(bool flag)
	{
		m_doShoot = flag;
	}
	
	public void PlayerDied(PlayerHomePlanetScript player)
	{
		m_doShoot = false;
		foreach (WeaponBaseScript weapon in m_weaponsSecondary)
		{
			StartSelfDestruct(weapon);
		}
		StartSelfDestruct(m_currentPrimaryWeapon);
	}
	
	[Obfuscar.Obfuscate]
	public Vector3 GetPrimarySlotPosition()
	{
		return 	m_primarySlot.transform.position;
	}
	
	#endregion
		
	#region Private object methods
	[Obfuscar.Obfuscate]
	private void StartSelfDestruct(WeaponBaseScript weapon)
	{
		if (weapon != null && weapon.GetComponent<Rigidbody>())
		{
			float angle = Random.Range(0f, 2f*Mathf.PI);
			float angle2 = Random.Range(0f, 2f*Mathf.PI);
			weapon.GetComponent<Rigidbody>().AddRelativeForce(Random.Range(1000f, 2500f) * 
				new Vector3(Mathf.Cos (angle), Mathf.Sin (angle), 0f));
			weapon.GetComponent<Rigidbody>().AddRelativeTorque(Random.Range(50f, 400f) * 
				new Vector3(Mathf.Cos (angle2), Mathf.Sin (angle2), Mathf.Sin (angle2) * Mathf.Cos (angle2)));
		}
		if (m_healthBars.ContainsKey(weapon))
		{
			GameObject go = m_healthBars[weapon].gameObject;
			m_healthBars.Remove(weapon);
			Destroy(go);
		}
	}
	
	[Obfuscar.Obfuscate]
	private WeaponBaseScript GetWeaponPrefab(string id, ref string[] ids, ref WeaponBaseScript[] weapons)
	{
		if (string.IsNullOrEmpty(id))
			return null;
		int index = 0;
		for ( ; index < ids.Length; index++)
		{
			if (ids[index].Equals(id))
				break;
		}
		if (index < weapons.Length)
			return weapons[index];
		
		return null;
	}
	
	[Obfuscar.Obfuscate]
	private void AddWeapon(xsd_ct_Equipment equipment
		, ref string[] equipmentIds
		, ref WeaponBaseScript[] equipments
		, Transform parentTransform
		, ref List<WeaponBaseScript> list
		, bool initialState)
	{	
		// get healt bar
		HealthBarScript healthBar = parentTransform.GetComponent<HealthBarScript>();
		if (equipment == null)
		{
			healthBar.SetVisibility(false);
			return;
		}
		
		WeaponBaseScript prefab = GetWeaponPrefab(equipment.Id, ref equipmentIds, ref equipments);
		
		// NOTE! We add the weapon in our struct anyway, even if null, so that the indices
		// match with PlayerInputScript and SaveData config ...
		
		if (prefab != null)
		{
			WeaponBaseScript weaponClone =
				(WeaponBaseScript) Instantiate(
					prefab
					, Vector3.zero
					, Quaternion.identity);
			
			if (healthBar != null)
			{
				m_healthBars[weaponClone] = healthBar;
			}
			
			weaponClone.transform.parent = parentTransform;
			// assign also the equipment config, this is used to determine some properties
			weaponClone.m_config = equipment;
			
			SetWeaponState(weaponClone, initialState, parentTransform);
			
			if (initialState && list == m_weaponsPrimary)
			{
				m_currentPrimaryWeapon = weaponClone;
				m_weaponIndex = list.Count;
			}
			// HACK! If we have some offset angle (for having turrets to rotate to side, for example)
			// then we need to differentiate between left and right the angle diff ... doing it here as
			// don't want to think of a finer solution
			if (list == m_weaponsSecondary)
			{
				float angle = weaponClone.OffsetTargetingAngle;
				if (parentTransform.localPosition.y > 0)
				{
					angle *= -1f;
				}
				parentTransform.Rotate(new Vector3(angle, 0f, 0f));
			}
			
			list.Add (weaponClone);
		}
	}
	
	[Obfuscar.Obfuscate]
	private void SetWeaponState(WeaponBaseScript weapon, bool state, Transform parentTransform)
	{
		if (!weapon)
			return;
		
		weapon.SetActive(state);
		weapon.transform.parent = (state) ? parentTransform : null;
		weapon.transform.localPosition = Vector3.zero;
		weapon.transform.localRotation = Quaternion.identity;

		weapon.gameObject.SetActiveRecursively(state);
		
		if (m_healthBars.ContainsKey(weapon))
		{
			m_healthBars[weapon].SetVisibility(state);
		}
	}
	
	
	
	#endregion
}
