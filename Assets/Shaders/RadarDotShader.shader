Shader "Custom/RadarDotShader" 
{
	Properties
	{
		_NeutralColor ("Neutral Color", Color) = (0,0,1,1)
		_CriticalColor ("Critical Color", Color) = (1,0,0,1)
		_MainTex ("Texture", 2D) = "white" { }
	}
	SubShader 
	{
		Lighting Off 
		Fog { Mode Off }
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha
		
		LOD 100
		
		Pass 
		{
			Name "BASE"
CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "RadarDotShader.cginc"
ENDCG
		}
	}
	FallBack "Diffuse"
}
