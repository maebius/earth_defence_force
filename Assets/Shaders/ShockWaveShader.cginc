#include "UnityCG.cginc"

float4 _Color;
sampler2D _MainTex;
float _GlobalTime;

struct v2f 
{
	float4 pos : SV_POSITION;
	float2 uv : TEXCOORD0;
};

float4 _MainTex_ST;

struct i2v 
{
	float4 vertex : POSITION;
	float3 normal : NORMAL;
	float4 texcoord : TEXCOORD0;
};

v2f vert (i2v v)
{
	v2f o;
	o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
	o.uv = TRANSFORM_TEX (v.texcoord, _MainTex);
	return o;
}

half4 frag (v2f i) : COLOR
{
	half4 texcol = tex2D (_MainTex, i.uv);
	texcol = texcol * _Color;
	float x = i.uv.x - 0.5f;
	float y = i.uv.y - 0.5f;
	float r = x*x + y*y;
	
	texcol.a = texcol.a * (0.5f - r) * 1.5f;
	
	if (r < 0.2f)
	{
		texcol.a = texcol.a * (r - 0.1f)*20f;
	}
	else if (r < 0.1f)
	{
		texcol.a = 0.0f;
	}
	return texcol;
}
