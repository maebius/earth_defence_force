#include "UnityCG.cginc"

float4 _Color;
sampler2D _MainTex;
float _GlobalTime;

struct v2f 
{
	float4 pos : SV_POSITION;
	float2 uv : TEXCOORD0;
};

float4 _MainTex_ST;

struct i2v 
{
	float4 vertex : POSITION;
	float3 normal : NORMAL;
	float4 texcoord : TEXCOORD0;
};

v2f vert (i2v v)
{
	v2f o;
	o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
	o.uv = TRANSFORM_TEX (v.texcoord, _MainTex);
	return o;
}

half4 frag (v2f i) : COLOR
{
	half4 texcol = tex2D (_MainTex, i.uv);
	texcol = texcol * _Color;
	return texcol;
}
