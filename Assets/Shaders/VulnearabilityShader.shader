Shader "Custom/VulnearabilityShader" 
{
	Properties
	{
		_Color ("Main Color", Color) = (1,1,1,1)
		_MainTex ("Texture", 2D) = "white" { }
	}
	SubShader 
	{
		Lighting Off 
		Fog { Mode Off }
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha
		
		LOD 100
		
		Pass 
		{
			Name "BASE"
CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "VulnearabilityShader.cginc"
ENDCG
		}
	}
	FallBack "Diffuse"
}
