﻿# import the needed modules
import zipfile
import xml.parsers.expat
import sys

print "Our encoding is set to: " + str(sys.getdefaultencoding())

# get content xml data from OpenDocument file
ziparchive = zipfile.ZipFile("localization.ods", "r")
xmldata = ziparchive.read("content.xml")
ziparchive.close()

class Element(list):
	def __init__(self, name, attrs):
		self.name = name
		self.attrs = attrs

class TreeBuilder:
	def __init__(self):
		self.root = Element("root", None)
		self.path = [self.root]
	def start_element(self, name, attrs):
		element = Element(name, attrs)
		self.path[-1].append(element)
		self.path.append(element)
	def end_element(self, name):
		assert name == self.path[-1].name
		self.path.pop()
	def char_data(self, data):
		self.path[-1].append(data)

rowIndex = -1
cellValue = ""
columns = []
rows = []
repeatKey = "table:number-columns-repeated"
appendLimit = 100
appendCount = 0

def checkValue(node):
	global rowIndex
	global columns
	global rows
	global cellValue
	global appendCount
	
	for i in range(appendCount):
		#if appendCount > 1: print str(rows[len(rows)-1][0]) + " - appending '" + str(cellValue) + "' " + str(i) + " / " + str(appendCount)  + ", whole size: " + str(len(rows[len(rows)-1]))
		
		rows[len(rows)-1].append(cellValue)
		if rowIndex == 0 and len(cellValue) > 0:
			#print "appendCount: " + str(appendCount) + ", i: " + str(i) + ", column '" + str(cellValue) + "' (length: " + str(len(cellValue)) + ")"
			columns.append(cellValue)
	cellValue = ""
	
	if node != None:
		checkRepeat(node)
	
def newRow():
	global rows
	global rowIndex
	global appendCount
	
	appendCount = 0
	rows.append([])
	rowIndex += 1
	
def checkRepeat(node):
	global appendCount
	global appendLimit
	global repeatKey
	
	appendCount = 1
	if repeatKey in node.attrs:
		appendCount = int(node.attrs[repeatKey])
		#print "set " + node.name + " to repeat: " + appendCount
	if appendCount > appendLimit: appendCount = 1

def showtree(node, prefix=""):
	global cellValue

	if node.name == "table:table-row":
		checkValue(node)
		newRow()
		
	if node.name == "table:table-cell":
		# this handles the LAST cell value
		checkValue(node)
	
	for e in node:
		if isinstance(e, Element):
			showtree(e, prefix + "  ")
		else:
			cellValue += e

# create parser and parsehandler
parser = xml.parsers.expat.ParserCreate()

treebuilder = TreeBuilder()
# assign the handler functions
parser.StartElementHandler  = treebuilder.start_element
parser.EndElementHandler    = treebuilder.end_element
parser.CharacterDataHandler = treebuilder.char_data

# parse the data
parser.Parse(xmldata, True)

# build lists based on the data, so that each row is a list and put those lists in a list, so we have a list of rows, and rows themselves are list of values (columns)
showtree(treebuilder.root)
# this is needed as it might be that "showtree " does not handle the last element
checkValue(None)

# make a dictionary out of all the constructed rows so that key is the first rows column-values,
# and a value is the first column value plus " = " plus the current column value
languages = dict()
columnCount = len(columns)

print "columnCount: " + str(columnCount)

for row in rows:
	if len(row) > 0 and len(row[0]) > 0:
		# we want to pad rows with empty strings so that we write also the last empty values ...
		for padding in range(columnCount - len(row)):
			row.append("")
		
		#print str(row[0]) + " - length: " + str(len(row))
		for index, val in enumerate(row):
			if index > 0 and index < columnCount:
				value = str(row[0]) + " = " + str(row[index]) + "\n"
				if columns[index] in languages:
					value = str(languages[columns[index]]) + str(value)
				languages[columns[index]] = str(value)

columnsToWrite = [ 'English', 'Deutsch', 'Française', 'Suomi', 'Nederlands']
#columnsToWrite = [ 'English', 'Suomi', 'Nederlands', 'Deutsch', 'Español', 'Française', 'Italia']

# output to files
for language, v in languages.items():
	if language in columnsToWrite:
		fileName = language + ".txt"
		print "Outputting language '" + language + "' to file: " + fileName + "..."
		output = open(fileName, 'w')
		output.write(v)
		output.close()

