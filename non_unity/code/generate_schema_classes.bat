@echo off

cls

set SCHEMA_DIR="EDF Data Formats"
set XSD_PATH="C:\Program Files (x86)\Microsoft SDKs\Windows\v7.0A\Bin\xsd.exe"
rem set OUTPUT_PATH="..\Assets\Scripts"
set OUTPUT_CS_PATH="..\..\Assets\Scripts\Data"
set OUTPUT_XML_PATH="..\..\Assets\Xml

%XSD_PATH% /c /o:%OUTPUT_CS_PATH% %SCHEMA_DIR%\savegame_schema.xsd
%XSD_PATH% /c /o:%OUTPUT_CS_PATH% %SCHEMA_DIR%\gamedata_schema.xsd

copy %SCHEMA_DIR%\gamedata_schema.xsd %OUTPUT_XML_PATH%\gamedata_schema.xml
copy %SCHEMA_DIR%\savegame_schema.xsd %OUTPUT_XML_PATH%\savegame_schema.xml
