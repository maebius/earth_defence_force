#!/bin/bash
MONO_PREFIX=/Library/Frameworks/Mono.framework/Versions/2.10.11
MONO=$MONO_PREFIX/bin/mono
XSD=$MONO_PREFIX/lib/mono/2.0/xsd.exe

export MONO_PATH=$MONO_PREFIX/lib/mono/2.0
export MONO_CFG_DIR=$MONO_PREFIX/etc
export LD_LIBRARY_PATH=$MONO_PREFIX/lib

INPUT_DIR="EDF\ Data\ Formats"
OUTPUT_PATH=../Assets/Scripts
OUTPUT_CS_PATH=../../Assets/Scripts/Data
OUTPUT_XML_PATH=../../Assets/Xml

echo "Input dir: " $INPUT_DIR
echo "Output dir: " $OUTPUT_CS_PATH

eval $MONO $XSD $INPUT_DIR/savegame_schema.xsd /c /o:$OUTPUT_CS_PATH /n
eval $MONO $XSD $INPUT_DIR/gamedata_schema.xsd /c /o:$OUTPUT_CS_PATH /n

eval cp $INPUT_DIR/gamedata_schema.xsd $OUTPUT_XML_PATH
eval cp $INPUT_DIR/savegame_schema.xsd $OUTPUT_XML_PATH
mv $OUTPUT_XML_PATH/gamedata_schema.xsd $OUTPUT_XML_PATH/gamedata_schema.xml
mv $OUTPUT_XML_PATH/savegame_schema.xsd $OUTPUT_XML_PATH/savegame_schema.xml