@echo off
set SOURCE_DIR=..\..\build
set SOURCE_FILE=spacebuggers.apk
set TARGET_DIR=pkg

mkdir %TARGET_DIR%
move %SOURCE_DIR%\%SOURCE_FILE% %TARGET_DIR%
7z.exe x  %TARGET_DIR%\%SOURCE_FILE% -o%TARGET_DIR%
rmdir /S /Q %TARGET_DIR%\META-INF
del %TARGET_DIR%\%SOURCE_FILE%