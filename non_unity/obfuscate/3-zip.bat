@echo off
set SOURCE_DIR=pkg
set TARGET_FILE=spacebuggers.apk

cd %SOURCE_DIR%
7z.exe a -mx5 -tzip  %TARGET_FILE%
move %TARGET_FILE% ..\
cd ..
rmdir /S /Q %SOURCE_DIR%
