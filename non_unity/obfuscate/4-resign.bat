@echo off
set SOURCE_FILE=spacebuggers.apk
set KEY_STORE=..\android\user.keystore
set KEY=spacebuggers

jarsigner -verify %SOURCE_FILE%
jarsigner -verbose -sigalg MD5withRSA -digestalg SHA1 -keystore %KEY_STORE% %SOURCE_FILE% %KEY%