@echo off
set SOURCE_FILE=spacebuggers.apk
set TARGET_FILE=spacebuggers-ouya-1.0.6_20130620.apk
set ADB_EXE=C:\libs\android-sdk\platform-tools\adb

zipalign -f -v 4  %SOURCE_FILE% %TARGET_FILE%
rm %SOURCE_FILE%
jarsigner -verify %TARGET_FILE%
%ADB_EXE% install -r %TARGET_FILE%